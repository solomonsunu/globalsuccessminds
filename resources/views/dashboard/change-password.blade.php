@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="passwordCtrl">
<div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active"><a href="#pin" data-toggle="tab">Change Password </a>
              </li>
          </ul> 
        </div>
    </div>
          <div class="col-lg-12">
              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <div ng-bind-html="messages.success"></div>
              </div>
              <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                  <ul ng-repeat="x in messages.error">
                      <li>@{{x}}</li>
                  </ul>
                  
                  
              </div>
              <div class="panel panel-flat">
                  <div class="panel-body">
                  	 <div class="tab-content">
                        <div class="tab-pane fade in active animated zoomIn" id="pin">
                          <div class="col-lg-1"></div>
                                  <div class="col-lg-8">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="changePassword();" >
                                         
                                          <div class="form-group">
                                                <label  class="col-sm-4 control-label">Enter Old Password:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="password" ng-model="postData.old_password">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group" >
                                                <label  class="col-sm-4 control-label">Enter New Password:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="password" ng-model="postData.new_password">
                                                </div>
                                                
                                            </div>
                                            
                                            
                                            <div class="form-group"  >
                                                <label  class="col-sm-4 control-label">Repeat New Password:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="password" ng-model="postData.new_password_repeat">
                                                </div>
                                                
                                            </div>
                                            
                                          <div class="text-right">
                                                    <button class="btn btn-success" ng-if="!loading.password ">change password</button>
                                                    <span class="btn btn-success" ng-if="loading.password"><i class="fa fa-spinner fa-spin"></i></span>
                                          </div>
      
                                          
                                      </form>
                                      
                                  </div>
                                  <div class="col-lg-1"></div>
			                  </div>
                       
                      </div>
                      
                  </div>
              </div>
          </div>
</div>
     
@endSection               
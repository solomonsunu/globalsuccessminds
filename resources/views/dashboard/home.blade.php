@extends('layouts.dash')
@section('content')
       <!--  <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header ">Dashboard</h3>
                </div>
        </div> -->
		<div class="row" >
                <!-- Welcome -->
            <div class="col-md-12">  
                    <div class="alert " >
                        <i class="fa fa-folder-open"></i><b>&nbsp;Hello ! </b>Welcome Back <b>{{Auth::user()->firstname}} </b>


                        
                        
                    </div>
            </div>
             @if(Auth::user()->agent == 0)
            <div class="col-md-12"><center><b>Invite New Users with your referral link: <span style="color: red">{{Auth::user()->link}}</span> </b></center></div>
            <div class="col-md-12" style="background-color: #EFF4F3">
                <div class="row" style="padding-top: 10px;">
                    <a class="col-md-4 col-xs-4 btn btn-medium center-content color-black color-black radius3 bg-blue" href="#" socialshare socialshare-provider="facebook" socialshare-text="{{Auth::user()->link}}"  socialshare-url="{{Auth::user()->link}}"> <i class="fa fa-facebook"></i> Facebook</a>

                     <a class="col-md-4 col-xs-4 btn btn-medium center-content color-black color-black radius3 bg-danger-400" href="#" socialshare socialshare-provider="twitter" socialshare-text="{{Auth::user()->link}}" socialshare-hashtags="GSM" socialshare-url="{{Auth::user()->link}}"><i class="fa fa-twitter"></i> Twitter</a>

                   


                     <a class="col-md-4 col-xs-4 btn btn-medium center-content color-black color-black radius3 bg-green" socialshare="" socialshare-provider="whatsapp" socialshare-text="{{Auth::user()->link}}" socialshare-url="{{Auth::user()->link}}" href="whatsapp://send?text={{Auth::user()->link}}" target="_top">
                        <i class="fa fa-whatsapp"></i>
                         Whatsapp
                      </a>
                    </div>  
               


                   <!--  <div class="row" style="padding-top: 10px;" >
                        Copy and Share your referral Link:<input  class="form-control " type="text" value="{{Auth::user()->link}}" readonly>
                         
                    </div> -->
            </div>
            @endif
          
            <!--end  Welcome -->
        </div>

       @if(Auth::user()->admin != 0 )
        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
            <div class="col-md-6">
                <p class="btn btn-primary col-md-12"><b>Current Total Active Users : </b><b>{{Auth::user()->active}}</b> </p>
            </div>
            <div class="col-md-6">
                <p class="btn btn-danger col-md-12"><b>Current Total Inactive Users : </b>{{Auth::user()->inactive}} <b></p>
            </div>
            <br>
        </div>
        @endif
	 <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <center><h3>GSM Profile</h3></center>
                           
                            <div class="table-responsive col-md-8">
                                <table class="table table-bordered table-striped">
                                    
                                    <tbody>
                                         <tr>
                                            
                                            <th>Account Status:</th>
                                            <td >
                                             @if(Auth::user()->premium_referrer && Auth::user()->admin == 0 && Auth::user()->agent == 0 && Auth::user()->bagam == 0)
                                                <b class="btn btn-primary" ng-controller="adminRankCtrl">
                                                   @{{currentData.ranks.rank_name}}
                                                </b>
                                              <!--  <a class="btn btn-danger" href="{{url('bagam-user/upgrade')}}">Advance to Bagam</a> -->
                                            @elseif(Auth::user()->premium_referrer && Auth::user()->admin == 0 && Auth::user()->agent == 0 && Auth::user()->bagam == 1)
                                                <b class="btn btn-primary" ng-controller="adminRankCtrl">
                                                   @{{currentData.ranks.rank_name}} (Bagam)
                                                </b>
                                                
                                             @elseif(!Auth::user()->premium_referrer && Auth::user()->admin == 0 && Auth::user()->agent == 0 )
                                                <b>Reqular User</b>
                                                <a class="btn btn-danger" href="{{url('premium-user/upgrade')}}">Upgrade to Premium User</a>
                                           
                                            @elseif(!Auth::user()->premium_referrer && Auth::user()->admin == 0 && Auth::user()->agent == 1 )
                                                <b>Agent Account</b>
                                                
                                             @else
                                                <b class="btn btn-primary">
                                                    Admin Account
                                                </b>
                                             @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                            <th>Name:</th>
                                            <td >{{Auth::user()->firstname}}</td>
                                        </tr>
                                        <tr>
                                           
                                            <th>Email Address:</th>
                                            <td>{{Auth::user()->email}}</td>
                                        </tr>
                                        <tr>
                                            
                                            <th>
                                                Phone Number:
                                            </th>
                                            <td>
                                                {{Auth::user()->phone}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Username:</th>
                                            <td >{{Auth::user()->username}}</td>
                                        </tr>
                                        <tr>
                                            <th>Date of Birth:</th>
                                            <td >{{Auth::user()->dob}}</td>
                                           
                                        </tr>
                                        <tr>
                                            <th>Country:</th>
                                            <td >{{Auth::user()->country}}</td>
                                           
                                        </tr>
                                        <tr>
                                            <th>Joined Since:</th>
                                            <td >{{Auth::user()->created_at}}</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                            	@if(Auth::user()->profile_image == '')
                            		@if(Auth::user()->gender == 'female')
                            			<img src="{{asset('uploads/girl.png')}}" class="img img-responsive img-thumbnail">
                            		@else
                            			<img src="{{asset('uploads/man.png')}}" class="img img-responsive img-thumbnail">
                            		@endif
                            	@else
                            		<img src="{{asset(Auth::user()->profile_image)}}" class="img img-responsive img-thumbnail">
                            		
                            	@endif
                            </div>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <center><h3>Notifications</h3>
                            </center>
                            <div class=" col-md-12">
                                <div class="tab-pane fade in active animated bounceIn" id="home-pills" >
                                    <ul class="nav nav-pills">
                                        <li class="active"><a href="#unread" data-toggle="tab">Unread Notifications</a>
                                        </li>
                                        <li><a href="#read" data-toggle="tab">Read Notifications</a>
                                        </li>
                                    </ul> 
                                    <div class="tab-content">
                                        <div class="tab-pane fade  in active panel-body" id="unread">
                                            <div class="list-group" ng-if="unRead.length > 0">
                                                <div class="list-group-item" ng-repeat="x in unRead">
                                                    <i class="fa fa-comment fa-fw"></i>
                                                    <b style="margin-right: 10px;">@{{x.title  }}</b>
                                                    <span>@{{x.detail}}</span>
                                                    <b style="margin-right: 10px;">@{{x.created_at|date:"fullDate" }}</b>
                                                    <button title="Mark as Read" type="button" class="pull-right btn btn-primary btn-circle" ng-click="markRead(x.notification_id)"><i class="fa fa-check"></i></button>
                                                </div>
                                            </div>
                                            <h4 ng-if="unRead.length == 0">Unread notification list empty</h4>   
                                            
                                        </div>
                                        <div class="tab-pane fade panel-body" id="read">
                                            
                                            <div class="list-group" ng-if="read.length > 0">
                                                <div class="list-group-item" ng-repeat="x in read">
                                                    <i class="fa fa-comment fa-fw"></i>
                                                    <b style="margin-right: 10px;">@{{x.title  }}</b>
                                                    <span>@{{x.detail}}</span>
                                                     <b style="margin-right: 10px;">@{{x.created_at|date:"fullDate" }}</b>
                                                </div>
                                            </div>
                                            <h4 ng-if="read.length == 0">Read notification list empty</h4>   
                                        </div>
                                    </div>
                                            </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
    </div>

@endSection

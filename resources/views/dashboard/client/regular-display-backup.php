@extends('layouts.dash')
@section('content')

	 <div class="row" ng-controller="regularUserCtrl">
     <div class="col-lg-12">
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                         
                            <li class="active"><a href="#refferals" data-toggle="tab" >Referral Tree</a>
                            </li>
                            @if(Auth::user()->premium_referrer)
                            <li><a href="#premium_refferals" data-toggle="tab" ng-click="getPremiumReferral();">Premium Referral Tree</a>
                            </li>
                            @endif
                           
                          
                      </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        @{{messages.success}}
                    </div>
                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <ul ng-repeat="x in messages.error">
                            <li>@{{x}}</li>
                        </ul>
                        
                        
                    </div>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                        	 <div class="tab-content">
                                
                               
                                <div class="tab-pane fade in active animated bounceIn" id="refferals">
                                  <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showLevel1" ng-click="getRefL1();">View Level1</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel1">
                                       <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                       <span ng-if="level1.length > 0">
                                       <span  ng-repeat="x in level1">
                                            <a href="#"  title="click to view user info"  ng-click="open(x)">
                                                
                                                <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                

                                            </a>
                                            
                                       </span>
                                        <span class="pull-right">Total: GH @{{t1}} </span>
                                       </span>

                                   </div>
                                   <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showLevel2 && showLevel1" ng-click="getRefL2(level_2);">Show Next Level</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel2">
                                       <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                        <span ng-if="level2.length > 0">
                                          <span ng-repeat="x in level2">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                    
                                        <span class="pull-right">Total: GH @{{t2}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel3 && showLevel2" ng-click="getRefL3(level_3);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel3">
                                       <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="level3.length > 0">
                                       
                                           <span ng-repeat="x in level3">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                       
                                        <span class="pull-right">Total: GH @{{t3}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel4 && showLevel3" ng-click="getRefL4(level_4);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel4">
                                       <span>Level 4 </span><img src="{{asset('images/gold.png')}}" width="40" height="40">
                                         <span ng-if="level4.length > 0">
                               
                                           <span ng-repeat="x in level4">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                  
                                        <span class="pull-right">Total: GH @{{t4}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel5 && showLevel4" ng-click="getRefL5(level_5);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel5">
                                       <span>Level 5 </span><img src="{{asset('images/diamond.jpg')}}" width="40" height="40">
                                         <span ng-if="level5.length > 0">
                                      
                                        <span ng-repeat="x in level5">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>    
                                      
                                        <span class="pull-right">Total: GH @{{t5}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel6 && showLevel5" ng-click="getRefL6(level_6);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel6">
                                       <span>Level 6 </span><img src="{{asset('images/platinum.jpg')}}" width="40" height="40">
                                         <span ng-if="level6.length > 0">
                                       
                                        <span ng-repeat="x in level6">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>    
                                        
                                   
                                        <span class="pull-right">Total: GH @{{t6}} </span>
                                       </span>
                                   </div>
                                   
                                </div>

                                <div class="tab-pane fade animated bounceIn" id="premium_refferals">
                                  <div class="list-group-item level"> <b class="btn btn-primary">Rank : @{{currentData.ranks.rank_name}} </b><b class="pull-right btn btn-success">Sponsor Bonus Total: @{{currentData.total_bonus}} Cedis</b><br></div>
                                        <div class="list-group-item level">
                                            <!-- <span>Level 1 </span> -->
                                             
                                            <span  class="row">
                                                <!-- LEVEL 1 -->
                                                <span  ng-repeat="(k, x) in plevel1" class="col-sm-2 text-center">                                               
                                                    <a href="#"  title="click to view user info" class="">
                                                        <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40"  ng-click="open(x)" >
                                                        <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40"  ng-click="open(x)" >
                                                     
                                                        <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                                        <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>

                                                        <!-- LEVEL 2 -->
                                                        <div class="mt-10" ng-show="x.nextLevel" class="text-center">
                                                            <span  ng-repeat="(a,y) in x.nextLevel" class="col-sm-2 ">  
                                                                                                           
                                                                <div href="#"  title="click to view user info" " class="text-center">
                                                                  
                                                                    <img ng-if="y.profile_image != null " ng-src="@{{y.profile_image}}" width="35" height="35" ng-click="open(y)">
                                                                    <img ng-if="y.profile_image == null " src="{{asset('img/avatar.png')}}" width="35" height="35" ng-click="open(y)">
                                                                 
                                                                    <span ng-if="y.premium_status == 0" class="status-mark border-danger position-top "></span>
                                                                    <span ng-if="y.premium_status == 1" class="status-mark border-success position-top "></span>
                                                                </div>
                                                            
                                                                <!-- LEVEL 3 -->
                                                                <span class="mt-10" ng-show="y.nextLevel" class="row text-center">
                                                                    <span  ng-repeat="z in y.nextLevel" class="">  
                                                                                                                   
                                                                        <span href="#"  title="click to view user info" " class="text-center">
                                                                          
                                                                            <img ng-if="z.profile_image != null " ng-src="@{{z.profile_image}}" width="30" height="30" ng-click="open(z)">
                                                                            <img ng-if="z.profile_image == null " src="{{asset('img/avatar.png')}}" width="30" height="30" ng-click="open(z)">
                                                                         
                                                                            <span ng-if="z.premium_status == 0" class="status-mark border-danger position-top "></span>
                                                                            <span ng-if="z.premium_status == 1" class="status-mark border-success position-top "></span>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                
                                                </span>
                                            <span class="col-sm-2">Total: GH @{{pt1 * 3.0}} </span>

                                             </span>
                                        </div>
                                     <div class="list-group-item level" ng-if="!showPLevel2">
                                        <center><span class="btn btn-primary"  ng-click="getRefPremiumL2(plevel1);">Show Next Level</span></center>
                                     </div>
                                     <div class="list-group-item level" ng-if="!showPLevel3 && showPLevel2">
                                        <center><span class="btn btn-primary"  ng-click="getRefPremiumL3();">Show Next Level</span></center>
                                     </div>
                                     
                                     
                                  </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
     
@endSection              
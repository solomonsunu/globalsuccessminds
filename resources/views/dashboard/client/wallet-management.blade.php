@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="WalletCtrl">
    <div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active"><a href="#pin" data-toggle="tab">Manage Pin </a>
              </li>
              <li><a href="#transaction" data-toggle="tab" ng-click="getWalletHistory(0);">Transaction History</a>
              </li>
              
               <li><a href="#balance" data-toggle="tab" ng-click="getWalletBalance();">Balance Info</a>
              </li>
                <li><a href="#transfer" data-toggle="tab">Tranfer Funds</a>
              </li>
              
              
          </ul> 
        </div>
    </div>
          <div class="col-lg-12">
              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <div ng-bind-html="messages.success"></div>
              </div>
              <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                  <ul ng-repeat="x in messages.error">
                      <li>@{{x}}</li>
                  </ul>
                  
                  
              </div>
              <div class="panel">
                  <div class="panel-body">
                  	 <div class="tab-content">
                        <div class="tab-pane fade in active animated bounceIn" id="pin">
                        <div class="col-lg-12">
      
                             <ul class="nav nav-pills">
                                <li class="active"><a href="#viewpin" data-toggle="tab">View Pin</a>
                                </li>
                                <li><a href="#changepin" data-toggle="tab">Change Pin</a>
                                </li>
                               
                            </ul> 
                         
                        </div>
                        <div class="tab-content">
                           <div class="tab-pane fade in active" id="viewpin">
                              <div class="col-lg-3"></div>
                              <div class="col-lg-6">
                                 <div class="panel panel-primary" ng-if="userPin">
                                    <div class="panel-heading">
                                        <center>Wallet Pin</center>
                                    </div>
                                    <div class="panel-body">
                                        <div class="" >
                                      
                                              <center><h2 ng-if="userPin">@{{userPin}}</h2> </center>
                                                
                                              
                                        </div>
                                    </div>
                                 </div>
                                 <h4 ng-if="!userPin" class="text-center"><button class="btn btn-primary" ng-if="!loading.pin" ng-click="getPin();">Generate Pin</button><span class="btn btn-success" ng-if="loading.pin"><i class="fa fa-spinner fa-spin"></i></span></h4>   
                                
                              </div> 
                              <div class="col-lg-3"></div> 
                           </div>
                           <div class="tab-pane fade in " id="changepin">
                              <div class="col-lg-3"></div>
                              <div class="col-lg-6">
                               <form role="form" class="form-horizontal addstudent"  ng-submit="changePin();" >    
                                    <div class="form-group">
                                        <label for="scl" class="col-sm-6 control-label">Enter Old Pin</label>
                                        <div class="col-sm-6">
                                             <input type="text" ng-model="postData.old_pin" id="scl" class="form-control" maxlength="4">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="scl" class="col-sm-6 control-label">Enter New Pin</label>
                                        <div class="col-sm-6">
                                             <input type="text" ng-model="postData.new_pin" id="scl" class="form-control" maxlength="4">
                                        </div>

                                    </div>
                                    <div class="form-group" ng-if="postData.new_pin.length == 4">
                                        <label for="scl" class="col-sm-6 control-label">Enter Password </label>
                                        <div class="col-sm-6">
                                             <input type="password" ng-model="postData.password" id="scl" class="form-control">
                                        </div>

                                    </div>

                                    <div class="text-right">
                                        <button class="btn btn-success" ng-if="!loading.change && postData.new_pin.length > 0">submit</button>
                                        <span class="btn btn-success" ng-if="loading.change"><i class="fa fa-spinner fa-spin"></i></span>
                                    </div>
                                </form>
                              </div> 
                              <div class="col-lg-3"></div> 
                           </div>
                        </div>
                                
			                  </div>
                        <div class="tab-pane fade animated bounceIn" id="transaction">
                          
                                  <div class="col-lg-12">
                                    <span>Wallet History</span> <span class="pull-right"><button class="btn btn-danger" ng-click="getWalletHistory(1);">Refresh Data From Server</button></span>
                                    <div class="table-responsive col-lg-12" ng-if="walletHistory.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('type')">Transaction Type
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('transfer')">Transfer
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                

                                                <th ng-click="sort('fullname')">Transaction By
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('fullname')">Recipient
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('amount')">Amount
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('balance')">Current Balance
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='balance'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in walletHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{roll.type}} </td>
                                                <td><span ng-if="roll.transfer == 1">Yes</span><span ng-if="roll.transfer == 0">No</span> <span ng-if="roll.transfer == 2">Withdrawal</span></td>
                                                <td>@{{roll.users.fullname}} </td>
                                                <td>@{{roll.recipient}} </td>
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.balance}} </td>
                                                <td>@{{roll.created_at}}</td>
                                                
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <h4 ng-if="walletHistory.length == 0" class="text-center">No Transaction History Found</h4>   
                                 </div>
                                 
                        </div>
                          
                        <div class="tab-pane fade animated bounceIn" id="balance">
                               <div class="col-lg-2"></div>
                                  <div class="col-lg-8">
                                     <div class="panel panel-primary" >
                                      <div class="panel-heading text-center">
                                          Wallet Balance
                                      </div>
                                      <div class="panel-body">
                                          <center><h2 ng-if="walletBalance">GH&#8373; @{{walletBalance}}</h2></center>
                                                
                                      </div>
                                   </div>
                                     
                                  </div>
                                  <div class="col-lg-2"></div>
                        </div>
                        <div class="tab-pane fade animated bounceIn" id="transfer">
                               <div class="col-lg-1"></div>
                                  <div class="col-lg-8">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="transferBalance();" >
                                          
                                          <div class="form-group">
                                              <label for="scl" class="col-sm-6 control-label">Enter Recipient Phone Number </label>
                                              <div class="col-sm-6">
                                                   <input type="text" ng-model="postData.phone" id="scl" class="form-control">
                                              </div>
                                              

                                          </div>
                                          <div class="form-group">
                                                <label  class="col-sm-6 control-label">Enter Amount:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.amount">
                                                </div>
                                                
                                            </div>
                                          
      
                                          <div class="text-right">
                                              <button class="btn btn-success" ng-if="!loading.transfer ">submit</button>
                                              <span class="btn btn-success" ng-if="loading.transfer"><i class="fa fa-spinner fa-spin"></i></span>
                                          </div>
                                      </form>
                                      
                                  </div>
                                  <div class="col-lg-1"></div>
                        </div>
                          
                      </div>
                      
                  </div>
              </div>
          </div>
</div>
     
@endSection               
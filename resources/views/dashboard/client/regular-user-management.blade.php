@extends('layouts.dash')
@section('content')

	 <div class="row" ng-controller="regularUserCtrl">
     <div class="col-lg-12">
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                         
                            <li class="active"><a href="#refferals" data-toggle="tab" >Referral Tree</a>
                            </li>
                            @if(Auth::user()->premium_referrer)
                            <li><a href="#premium_refferals" data-toggle="tab" >Premium Referral Tree</a>
                            </li>
                           <!--  <li><a href="#premium_refferal_count" data-toggle="tab" ng-click="getPremiumReferralFigure();">Premium Referral Actuals</a>
                            </li> -->
                            @endif
                             @if(Auth::user()->agent_ref_status == 1)
                            <li><a href="#agent_refferals" data-toggle="tab" ng-click="getAgentReferral();">Agent Referral Info</a>
                            </li>
                           <!--  <li><a href="#premium_refferal_count" data-toggle="tab" ng-click="getPremiumReferralFigure();">Premium Referral Actuals</a>
                            </li> -->
                            @endif

                            @if(Auth::user()->bagam)
                            <li><a href="#bagam_refferals" data-toggle="tab" >Bagam Referral Tree <button class="btn btn-danger">Total Direct: {{Auth::user()->bagam_direct_count}}</button></a>
                            </li>
                           <!--  <li><a href="#premium_refferal_count" data-toggle="tab" ng-click="getPremiumReferralFigure();">Premium Referral Actuals</a>
                            </li> -->
                            @endif
                           
                          
                      </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                   
                    <div class="panel panel-flat">
                        <div class="panel-body">
                        	 <div class="tab-content">
                                
                               
                                <div class="tab-pane fade in active animated bounceIn" id="refferals">
                                  <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showLevel1" ng-click="getRefL1();">View Level1</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel1">
                                       <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                       <span ng-if="level1.length > 0">
                                       <span  ng-repeat="x in level1">
                                            <a href="#"  title="click to view user info"  ng-click="open(x)">
                                                
                                                <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                

                                            </a>
                                            
                                       </span>
                                        <span class="pull-right">Total: GH @{{t1}} </span>
                                       </span>

                                   </div>
                                   <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showLevel2 && showLevel1" ng-click="getRefL2(level1);">Show Next Level</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel2">
                                       <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                        <span ng-if="level2.length > 0">
                                          <span ng-repeat="x in level2">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                    
                                        <span class="pull-right">Total: GH @{{t2}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel3 && showLevel2" ng-click="getRefL3(level2);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel3">
                                       <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="level3.length > 0">
                                       
                                           <span ng-repeat="x in level3">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                       
                                        <span class="pull-right">Total: GH @{{t3}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel4 && showLevel3" ng-click="getRefL4(level3);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel4">
                                       <span>Level 4 </span><img src="{{asset('images/gold.png')}}" width="40" height="40">
                                         <span ng-if="level4.length > 0">
                               
                                           <span ng-repeat="x in level4">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                  
                                        <span class="pull-right">Total: GH @{{t4}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel5 && showLevel4" ng-click="getRefL5(level4);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel5">
                                       <span>Level 5 </span><img src="{{asset('images/diamond.jpg')}}" width="40" height="40">
                                         <span ng-if="level5.length > 0">
                                      
                                        <span ng-repeat="x in level5">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>    
                                      
                                        <span class="pull-right">Total: GH @{{t5}} </span>
                                       </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showLevel6 && showLevel5" ng-click="getRefL6(level5);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showLevel6">
                                       <span>Level 6 </span><img src="{{asset('images/platinum.jpg')}}" width="40" height="40">
                                         <span ng-if="level6.length > 0">
                                       
                                        <span ng-repeat="x in level6">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>    
                                        
                                   
                                        <span class="pull-right">Total: GH @{{t6}} </span>
                                       </span>
                                   </div>
                                   
                                </div>
                                 <div class="tab-pane fade animated bounceIn" id="bagam_refferals">
                                  <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showBLevel1" ng-click="getRefBL1();">View Level1</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showBLevel1">
                                       <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                       <span ng-if="Blevel1.length > 0">
                                       <span  ng-repeat="x in Blevel1">
                                            <a href="#"  title="click to view user info"  ng-click="open(x)">
                                                
                                                <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                

                                            </a>
                                            
                                       </span>
                                       <!--  <span class="pull-right">Total: GH @{{tB1}} </span>
                                       </span> -->

                                   </div>
                                   <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showBLevel2 && showBLevel1" ng-click="getRefBL2(Blevel1);">Show Next Level</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showBLevel2">
                                       <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                        <span ng-if="Blevel2.length > 0">
                                          <span ng-repeat="x in Blevel2">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                    
                                        <!-- <span class="pull-right">Total: GH @{{tB2}} </span>
                                       </span> -->
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showBLevel3 && showBLevel2" ng-click="getRefBL3(Blevel2);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showBLevel3">
                                       <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="Blevel3.length > 0">
                                       
                                           <span ng-repeat="x in Blevel3">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                       
                                       <!--  <span class="pull-right">Total: GH @{{tB3}} </span>
                                       </span> -->
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showBLevel4 && showBLevel3" ng-click="getRefBL4(Blevel3);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showBLevel4">
                                       <span>Level 4 </span><img src="{{asset('images/gold.png')}}" width="40" height="40">
                                         <span ng-if="levelB4.length > 0">
                               
                                           <span ng-repeat="x in Blevel4">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                
                                            </a>
                                            
                                          </span>
                                  
                                       <!--  <span class="pull-right">Total: GH @{{tB4}} </span>
                                       </span> -->
                                   </div>
                                  
                                   
                                </div>

                                 <div class="tab-pane fade animated bounceIn" id="premium_refferals">
                                <div class="list-group-item level"> <b class="btn btn-primary">Rank : @{{currentData.ranks.rank_name}} </b><b class="pull-right btn btn-success">Sponsor Bonus Total: @{{currentData.total_bonus}} Cedis</b><br></div>
                                   <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showPremiumLevel1" ng-click="getPremiumRefL1();">View Level1</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showPremiumLevel1">
                                       <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                      <span ng-if="plevel1.length > 0">
                                        <span  ng-repeat="x in plevel1">
                                        <a href="#"  title="click to view user info"  ng-click="open(x)">

                                        <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                        <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                        <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                            <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                            <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                            <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                            <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                            <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                            <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                            <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                            <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                            <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                            <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                            </a>
                                            </span>
                                            <span class="pull-right">Total: GH @{{pt1 * 3.0}} </span>
                                      </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><a class="btn btn-primary center" ng-if="!showPremiumLevel2 && showPremiumLevel1" ng-click="getPremiumRefL2(plevel1);">Show Next Level</a></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showPremiumLevel2">
                                       <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                        <span ng-if="plevel2.length > 0">


                                          <span ng-repeat="x in plevel2">
                                          <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                          <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                          <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                          <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                              <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                              <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                              <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                              <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                              <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                              <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                              <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                              <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                              <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                              <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                              </a>
                                              </span>
                                              <span class="pull-right">Total: GH @{{pt2 * 3.0}} </span>
                                        </span>
                                   </div>

                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showPremiumLevel3 && showPremiumLevel2" ng-click="getPremiumRefL3(plevel2);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showPremiumLevel3">
                                       <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="plevel3.length > 0">

                                        <span ng-repeat="x in plevel3">
                                        <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                        <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                        <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                        <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                            <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                            <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                            <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                            <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                            <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                            <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                            <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                            <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                            <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                            <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                            </a>
                                            </span>
                                            <span class="pull-right">Total: GH @{{pt3 * 3.0}} </span>
                                            </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showPremiumLevel4 && showPremiumLevel3" ng-click="getPremiumRefL4(plevel3);">Show Next Level</span></center>
                                   </div>

                                     <div class="list-group-item level" ng-if="showPremiumLevel4">
                                       <span>Level 4 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="plevel4.length > 0">

                                        <span ng-repeat="x in plevel4">
                                        <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                        <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                        <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                        <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                            <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                            <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                            <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                            <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                            <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                            <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                            <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                            <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                            <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                            <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                            </a>
                                            </span>
                                            <span class="pull-right">Total: GH @{{pt4 * 3.0}} </span>
                                            </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showPremiumLevel5 && showPremiumLevel4" ng-click="getPremiumRefL5(plevel4);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showPremiumLevel5">
                                       <span>Level 5 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="plevel5.length > 0">

                                          <span ng-repeat="x in plevel5">
                                          <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                          <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                          <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                          <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                              <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                              <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                              <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                              <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                              <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                              <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                              <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                              <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                              <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                              <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                              </a>
                                            </span>
                                            <span class="pull-right">Total: GH @{{pt5 * 3.0}} </span>
                                            </span>
                                   </div>
                                   <div class="list-group-item level">
                                      <center><span class="btn btn-primary" ng-if="!showPremiumLevel6 && showPremiumLevel5" ng-click="getPremiumRefL6(plevel5);">Show Next Level</span></center>
                                   </div>
                                   <div class="list-group-item level" ng-if="showPremiumLevel6">
                                       <span>Level 6 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                         <span ng-if="plevel6.length > 0">

                                        <span ng-repeat="x in plevel6">
                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                            <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                            <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">

                                            <span ng-if="x.premium_status == 0" class="status-mark border-danger position-top "></span>
                                            <span ng-if="x.premium_status == 1" class="status-mark border-success position-top "></span>
                                            <span ng-if="x.ranks.id == 2" class="badge badge-success position-top ">B</span>
                                            <span ng-if="x.ranks.id == 3" class="badge badge-success position-top ">S</span>
                                            <span ng-if="x.ranks.id == 4" class="badge badge-success position-top ">P</span>
                                            <span ng-if="x.ranks.id == 5" class="badge badge-success position-top ">G</span>
                                            <span ng-if="x.ranks.id == 6" class="badge badge-success position-top ">SP</span>
                                            <span ng-if="x.ranks.id == 7" class="badge badge-success position-top ">D</span>
                                            <span ng-if="x.ranks.id == 8" class="badge badge-success position-top ">PL</span>
                                            <span ng-if="x.ranks.id == 9" class="badge badge-success position-top ">PS</span>
                                            <span ng-if="x.ranks.id == 10" class="badge badge-success position-top ">Pz</span>
                                            </a>
                                          </span>
                                            <span class="pull-right">Total: GH @{{pt6 * 3.0}} </span>
                                          </span>
                                   </div>
                                   
                                </div>
                                <div class="tab-pane fade animated bounceIn" id="agent_refferals">
                                  <div class="panel panel-default" ng-if="agentRef">
                                      <div class="panel-heading">
                                          Agent Referral Details (Commision Earned On Agent) <b class="pull-right">Amount Earned: GHC @{{totalRef}}</b>
                                      </div>
                                      <div class="panel-body">
                                          <div class="table-responsive table-bordered">
                                              <table class="table">
                                                <tr>
                                                  <th>#</th>
                                                  <th>Agent Name</th>
                                                  <th>Total Sales (Current Month)</th>
                                                  <th>Commision (0.5%)</th>
                                                  
                                                </tr>
                                                <tr ng-repeat="row in agentRef">
                                                  <td>@{{$index +1 }}</td>
                                                  <td>@{{row.fullname}}</td>
                                                  <td>GHC @{{row.amt}}</td>
                                                  <td>GHC @{{row.totalCom}}</td>
                                                </tr>
                                                
                                              </table>
                                          </div>
                                      </div>
                                   </div>
                                </div>
                                 
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
     
@endSection               
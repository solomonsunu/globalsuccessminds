@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="MobileMoneyCtrl">
        <div class="col-lg-12">    
            <div class="tabbable">
               <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                    
                    
                    
                    

                    <!-- <li class="active"><a href="#app-gt" data-toggle="tab">Free Self Load Service (Direct Push)</a>
                    </li>
                    <li ><a href="#app-pills" data-toggle="tab">Load Wallet VISA/MASTERCARD</a>
                    </li> -->
                    <li class="active" ><a href="#home-pills" data-toggle="tab">Self Load Wallet Service  <b style="color:red">(Available for MTN registered Users Only!!)</b></a>
                    </li>

                   
                    
                </ul> 
            </div>
        </div>
                <div class="col-lg-12">
                
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active   animated bounceIn" id="home-pills">
                                   <div class="col-lg-6">
                                    <div class="container-fluid">
                                        <div class="jumbotron">
                                           <h3>Instructions for Self Loading</h3>
                                           
                                           <ol>
                                               <li>Enter Amount you want to credit your wallet with</li>

                                                <li>Ensure this account is registered with a mobile money enabled <b>MTN</b> number </li>

                                                 <li>Click load wallet and authorize payment prompt sent to your mobile phone</li>
                                                 <li>Transaction Charge for amounts between GHC 1.00  to GHC 49.99 is GHC 1.00 (Wallet will be loaded less GHC 1.00)</li>
                                                 <li>Transaction Charge for amounts between GHC 50.00  to GHC 99.99 is GHC 0.5 (Wallet will be loaded less GHC 0.5)</li>
                                                 <li>No Transaction Charge for amounts ranging from GHC 100.00  and  above. (Wallet will be loaded with exact amount debited on Mobile Money)</li>
                                           </ol>
                                           
                                       </div>
                                    </div>
                                       
                                   </div>
                                    <div class="col-lg-6">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="requestWalletLoad();" >
                                            <h2>
                                                Perform Wallet Load
                                            </h2>
                                             
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Amount </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.amount" id="scl" class="form-control" placeholder="eg. 2.00 (Amount must be in this format)" required>
                                                </div>


                                            </div>
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">MTN Mobile Money Number </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.mtn_number" id="scl" class="form-control" placeholder="eg. 024XXXXXXX" required>
                                                </div>
                                                

                                            </div>
                                             <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">GSM Recipient Account Phone # </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.gsm_phone" id="scl" class="form-control" required>
                                                </div>
                                                

                                            </div>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    
                                 </div>

                                 <div class="tab-pane fade  animated bounceIn" id="app-gt">
                                   <div class="col-lg-6">
                                    <div class="container-fluid">
                                        <div class="jumbotron">
                                           <h3>Instructions for Self Loading </h3>
                                           <ol>
                                               <li>Enter Amount you want to credit your wallet with</li>
                                                 <!-- <li>Transaction Charge is GHC 1.00 (Wallet will be loaded less GHC 1.00)</li> -->
                                           </ol>
                                           
                                       </div>
                                    </div>
                                       
                                   </div>
                                    <div class="col-lg-6">

                                       
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="requestDirectWalletLoad();" >
                                            <h2>
                                                Perform Wallet Load
                                            </h2>
                                             
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Amount </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.amount" id="scl" class="form-control" placeholder="eg. 2.00 (Amount must be in this format)" required>
                                                </div>


                                            </div>
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">MTN MOMO Number For Wallet Loading</label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.mtn_number" id="scl" class="form-control" placeholder="eg. 024XXXXXXX" required readonly>
                                                </div>
                                                

                                            </div>
                                             <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">GSM Recipient Account Phone # </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.gsm_phone" id="scl" class="form-control" required>
                                                </div>
                                                

                                            </div>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>

                                         
                                    </div>
                                    
                                 </div>
                               
                                <div class="tab-pane fade  animated bounceIn" id="app-pills">
                                    @if(Session::has('error'))
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                                @foreach($errors->all() as $error)
                                            <div class="alert alert-danger alert-dismissable">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                {{ $error }}
                                            </div>
                                @endforeach
                                   <div class="col-lg-6">
                                    <div class="container-fluid">
                                        <div class="jumbotron">
                                           <h3>Instructions for Self Loading </h3>
                                           <ol>
                                               <li>Enter Amount you want to credit your wallet with</li>
                                                 <li>Provide the GSM number to load </li>
                                                 <li> Click on submit to proceed to next level</li>
                                           </ol>
                                           
                                       </div>
                                    </div>
                                       
                                   </div>
                                    <div class="col-lg-6">

                                        <?php 
                                             $clientid = "f6de59af-33d2-4545-ae7f-5324d9ddc486";
                                             $clientsecret = "bffa580e-d9e6-4de1-8f2c-68b3dbe9fbf3";
                                             $itemname = "Sample item name here";
                                             $clientref = "123456789";
                                             $amount = "10.00";
                                             $returnurl = "https://globalsuccessminds.com/api/v1/gt_callback";
                                             $baseurl = "https://myghpay.com/myghpayclient/";
                                         ?>
                                        <form role="form" class="form-horizontal addstudent"  action="/save-gtpayment" method="post" >
                                            <h2>
                                                Perform Wallet Load
                                            </h2>
                                              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                    @{{messages.success}}
                                                </div>
                                                <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                    <ul ng-repeat="x in messages.error">
                                                        <li>@{{x}}</li>
                                                    </ul>
                                                    
                                                    
                                                </div>
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Amount </label>
                                                <div class="col-sm-7">
                                                     <input type="text" name="amount" id="scl" class="form-control" placeholder="eg. 2" required>
                                                </div>


                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">GSM Recipient Account Phone # </label>
                                                <div class="col-sm-7">
                                                     <input type="text" name="gsm_phone" id="scl" class="form-control" required>
                                                </div>
                                                

                                            </div>
                                            <!--  <input type="hidden" class="form-control" name="clientid" id="inputAmount" name="description" value="f6de59af-33d2-4545-ae7f-5324d9ddc486">
                                            <input type="hidden" class="form-control" id="inputMerchantId" name="clientsecret" value="bffa580e-d9e6-4de1-8f2c-68b3dbe9fbf3">
                                            <input type="hidden" class="form-control" id="inputItemCode" name="itemname" value="GSM Wallet Load">
                                            <input type="hidden" class="form-control" id="inputItemCode" name="clientref" value="171752535353">
                                            
                                            <input type="hidden" name="returnurl" value="https://globalsuccessminds.com/api/v1/gt_callback" /> -->
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" type="submit">Save & Continue</button>
                                                <!-- <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span> -->
                                            </div>
                                        </form>

                                         
                                    </div>
                                    
                                 </div>
                               
                               
                            </div>
                                
                                
                                
                        </div>
                            
                    </div>
                </div>
        </div>
    
@endSection               

@extends('layouts.dash')
@section('content')

<div class="row" ng-controller="WalletCtrl">
<div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active"><a href="#pin" data-toggle="tab">Request Withdrawal </a>
              </li>
              <li><a href="#transaction" data-toggle="tab" ng-click="getWithdrawal();">Withdrawal History</a>
              </li>
            
              
          </ul> 
        </div>
    </div>
          <div class="col-lg-12">
              
              <div class="panel panel-flat">
                  <div class="panel-body">
                  	 <div class="tab-content">
                        <div class="tab-pane fade in active animated bounceIn" id="pin">
                          <div class="col-lg-1"></div>
                                  <div class="col-lg-8">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="withdrawalRequest();" >
                                         
                                          <div class="form-group">
                                                <label  class="col-sm-4 control-label">Enter Amount:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.amount">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group" ng-if="!postData.mobile">
                                                <label  class="col-sm-4 control-label">Enter Mobile Money #:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.phone">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                <i class="col-sm-6 ">Default Payment Method: <b>Mobile Money</b></i>
                                                   <div class="col-sm-6">
                                                   
                                                      <label class="col-sm-8">
                                                        <input type="checkbox" ng-model="postData.mobile" ng-click="check();">
                                                        Bank Payment
                                                      </label>
                                                      <!-- <div class="col-sm-8" ng-if="postData.mobile">
                                                        <input class="form-control" type="text" ng-model="postData.phone" placeholder="Enter Mobile Money Number">
                                                      </div>
                                                     -->
                                                 
                                                  </div>
                                                
                                            </div>
                                            <div class="form-group"  ng-if="postData.mobile">
                                                <label  class="col-sm-4 control-label">Enter Account Name:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.account_name">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group"  ng-if="postData.mobile">
                                                <label  class="col-sm-4 control-label">Enter Account Number:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.account_number">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group"  ng-if="postData.mobile">
                                                <label  class="col-sm-4 control-label">Enter Bank Name:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.bank_name">
                                                </div>
                                                
                                            </div>
                                            <div class="form-group"  ng-if="postData.mobile">
                                                <label  class="col-sm-4 control-label">Enter Branch Name:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.branch_name">
                                                </div>
                                                
                                            </div>
                                            
                                          <div class="text-right">
                                                    <button class="btn btn-success" ng-if="!loading.withdrawal ">submit request</button>
                                                    <span class="btn btn-success" ng-if="loading.withdrawal"><i class="fa fa-spinner fa-spin"></i></span>
                                          </div>
      
                                          
                                      </form>
                                      
                                  </div>
                                  <div class="col-lg-1"></div>
			                  </div>
                        <div class="tab-pane fade animated bounceIn" id="transaction">
                          <div class="col-lg-1"></div>
                                  <div class="col-lg-10">
                                    <h4>Wallet Withdrawal History</h4>
                                    <div class="table-responsive col-lg-12" ng-if="withdrawal.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                
                                                <th ng-click="sort('amount')">Amount
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('type')">Payment Method
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('type')">Payment Status
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in withdrawal|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.created_at}} </td>
                                                <td><span ng-if='roll.cash_pickup == "1"'> @{{roll.phone_number}}</span>

                                                <span ng-if='roll.cash_pickup == "0"'>Account Name: @{{roll.account_name}} <br>
                                                  Account Number: @{{roll.account_number}} <br>
                                                  Bank Name: @{{roll.bank_name}} <br>
                                                  Branch: @{{roll.branch_name}} 
                                                </span></td>
                                                <td><span ng-if="roll.paid == '1'"> Paid </span><span ng-if="roll.paid == '0'"> Unpaid</span></td>
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <h4 ng-if="withdrawal.length == 0" class="text-center">No Transaction History Found</h4>   
                                 </div>
                                 <div class="col-lg-1"></div>
                        </div>
                          
                        <div class="tab-pane fade animated flipInX" id="balance">
                               <div class="col-lg-2"></div>
                                  <div class="col-lg-8">
                                     <div class="panel panel-primary" >
                                      <div class="panel-heading text-center">
                                          Wallet Balance
                                      </div>
                                      <div class="panel-body">
                                          <center><h2 ng-if="walletBalance">GH&#8373; @{{walletBalance}}</h2> <button class="btn btn-primary" ng-click="getWalletBalance()" ng-if="!walletBalance"> Show Balance</button><button class="btn btn-danger" ng-click="resetBalance()" ng-if="walletBalance"> Hide Balance</button></center>
                                                
                                      </div>
                                   </div>
                                     
                                  </div>
                                  <div class="col-lg-2"></div>
                        </div>
                        <div class="tab-pane fade animated zoomIn" id="transfer">
                               <div class="col-lg-1"></div>
                                  <div class="col-lg-8">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="transferBalance();" >
                                          
                                          <div class="form-group">
                                              <label for="scl" class="col-sm-6 control-label">Enter Recipient Phone Number </label>
                                              <div class="col-sm-6">
                                                   <input type="text" ng-model="postData.phone" id="scl" class="form-control">
                                              </div>
                                              

                                          </div>
                                          <div class="form-group">
                                                <label  class="col-sm-6 control-label">Enter Amount:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.amount">
                                                </div>
                                                
                                            </div>
                                          
      
                                          <div class="text-right">
                                              <button class="btn btn-success" ng-if="!loading.transfer ">submit</button>
                                              <span class="btn btn-success" ng-if="loading.transfer"><i class="fa fa-spinner fa-spin"></i></span>
                                          </div>
                                      </form>
                                      
                                  </div>
                                  <div class="col-lg-1"></div>
                        </div>
                          
                      </div>
                      
                  </div>
              </div>
          </div>
</div>
     
@endSection               
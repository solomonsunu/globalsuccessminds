@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="StatementCtrl">
    <div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active"><a href="#pin" data-toggle="tab">Daily Transaction Statement </a>
              </li>
              <li><a href="#transactionMonthly" data-toggle="tab" >Monthly Account Statement</a>
              </li>
              
              <!--  <li><a href="#balance" data-toggle="tab">Balance Info</a>
              </li>
                <li><a href="#transfer" data-toggle="tab">Tranfer Funds</a>
              </li> -->
              
              
          </ul> 
        </div>
    </div>
          <div class="col-lg-12">
              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <div ng-bind-html="messages.success"></div>
              </div>
              <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                  <ul ng-repeat="x in messages.error">
                      <li>@{{x}}</li>
                  </ul>
                  
                  
              </div>
              <div class="panel">
                  <div class="panel-body">
                  	 <div class="tab-content">
                        <div class="tab-pane fade in active animated bounceIn" id="pin">
                          <div class="col-lg-12">
        
                              <div class="col-lg-6">
                                      <div class="container-fluid">
                                          <div class="jumbotron">
                                             <h3>Instructions for Request</h3>
                                             <ol>
                                                <li>This will provide details of daily transaction history</li>
                                                 <li>Provide the duration (Start date and End date)</li>

                                                
                                             </ol>
                                             <h3>Charge List</h3>
                                             <table class="table table-responsive">
                                               <tr>
                                                 <td>Duration</td>
                                                 <td>Charge (Cedis)</td>
                                               </tr>
                                               <tr>
                                                 <td>1 month – 3 months</td>
                                                 <td>0.5</td>
                                               </tr>
                                               <tr>
                                                 <td>3 months – 12 months</td>
                                                 <td>1</td>
                                               </tr>
                                               <tr>
                                                 <td>12 months and above</td>
                                                 <td>2</td>
                                               </tr>
                                             </table>
                                             
                                         </div>
                                      </div>
                                         
                                     </div>
                                      <div class="col-lg-6">
                                          <form role="form" class="form-horizontal addstudent"  ng-submit="generateTransactionStatement();" >
                                              <h2>
                                                  Perform Request Here
                                              </h2>
                                                <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                  </div>
                                                  <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                  </div>
                                              <div class="form-group">
                                                  <label for="scl" class="col-sm-5 control-label">Start Date </label>
                                                  <div class="col-sm-7">
                                                       <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                                                  </div>


                                              </div>
                                               <div class="form-group">
                                                  <label for="scl" class="col-sm-5 control-label">End Date</label>
                                                  <div class="col-sm-7">
                                                       <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                                  </div>
                                                  

                                              </div>
                                              <div class="p-md">
                                                  <button class="btn btn-success pull-right" ng-if="!loading.statement">submit</button>
                                                  <span class="btn btn-success pull-right" ng-if="loading.statement"><i class="fa fa-spinner fa-spin"></i></span>
                                              </div>
                                          </form>
                                          
                                      </div>
                                      
                           
                          </div>
                        
                                
			                  </div>
                        <div class="tab-pane fade animated bounceIn" id="transactionMonthly">
                            <div class="col-lg-12">
        
                              <div class="col-lg-6">
                                      <div class="container-fluid">
                                          <div class="jumbotron">
                                             <h3>Instructions for Request</h3>
                                             <ol>
                                                <li>This will provide details of monthly aggregate of transaction history (Grouped as Credit and Debit with sub totals)</li>
                                                 <li>Provide the duration (Start date and End date)</li>

                                                
                                             </ol>
                                             <h3>Charge List</h3>
                                             <table class="table table-responsive">
                                               <tr>
                                                 <td>Duration</td>
                                                 <td>Charge (Cedis)</td>
                                               </tr>
                                               <tr>
                                                 <td>1 month – 3 months</td>
                                                 <td>0.5</td>
                                               </tr>
                                               <tr>
                                                 <td>3 months – 12 months</td>
                                                 <td>1.5</td>
                                               </tr>
                                               <tr>
                                                 <td>12 months and above</td>
                                                 <td>3</td>
                                               </tr>
                                             </table>
                                             
                                         </div>
                                      </div>
                                         
                                     </div>
                                      <div class="col-lg-6">
                                          <form role="form" class="form-horizontal addstudent"  ng-submit="generateAccountStatement();" >
                                              <h2>
                                                  Perform Request Here
                                              </h2>
                                                <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                  </div>
                                                  <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                  </div>
                                               <div class="form-group">
                                                  <label for="scl" class="col-sm-5 control-label">Start Date </label>
                                                  <div class="col-sm-7">
                                                       <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                                                  </div>


                                              </div>
                                               <div class="form-group">
                                                  <label for="scl" class="col-sm-5 control-label">End Date</label>
                                                  <div class="col-sm-7">
                                                       <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                                  </div>
                                                  

                                              </div>
                                              <div class="p-md">
                                                  <button class="btn btn-success pull-right" ng-if="!loading.statement">submit</button>
                                                  <span class="btn btn-success pull-right" ng-if="loading.statement"><i class="fa fa-spinner fa-spin"></i></span>
                                              </div>
                                          </form>
                                          
                                      </div>
                                      
                           
                          </div>
                                 
                        </div>
                          
                        <div class="tab-pane fade animated bounceIn" id="balance">
                               <div class="col-lg-2"></div>
                                  <div class="col-lg-8">
                                     <div class="panel panel-primary" >
                                      <div class="panel-heading text-center">
                                          Wallet Balance
                                      </div>
                                      <div class="panel-body">
                                          <center><h2 ng-if="walletBalance">GH&#8373; @{{walletBalance}}</h2></center>
                                                
                                      </div>
                                   </div>
                                     
                                  </div>
                                  <div class="col-lg-2"></div>
                        </div>
                        <div class="tab-pane fade animated bounceIn" id="transfer">
                               <div class="col-lg-1"></div>
                                  <div class="col-lg-8">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="transferBalance();" >
                                          
                                          <div class="form-group">
                                              <label for="scl" class="col-sm-6 control-label">Enter Recipient Phone Number </label>
                                              <div class="col-sm-6">
                                                   <input type="text" ng-model="postData.phone" id="scl" class="form-control">
                                              </div>
                                              

                                          </div>
                                          <div class="form-group">
                                                <label  class="col-sm-6 control-label">Enter Amount:</label>
                                                <div class="col-sm-6">
                                                  <input class="form-control" type="text" ng-model="postData.amount">
                                                </div>
                                                
                                            </div>
                                          
      
                                          <div class="text-right">
                                              <button class="btn btn-success" ng-if="!loading.transfer ">submit</button>
                                              <span class="btn btn-success" ng-if="loading.transfer"><i class="fa fa-spinner fa-spin"></i></span>
                                          </div>
                                      </form>
                                      
                                  </div>
                                  <div class="col-lg-1"></div>
                        </div>
                          
                      </div>
                      
                  </div>
              </div>
          </div>
</div>
     
@endSection               
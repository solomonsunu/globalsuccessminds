
<html>
 
  <table >
                                    
      <tbody>
      <tr>
      <td >Transaction Type</td>
      <td >Transfer</td>
      <td >Transaction By</td>
      
      <td >Recipient</td>
      
      <td >Amount </td>
      <td >Remaining Balance</td>
      <td>Date</td>
       
        
      </tr>
       
       
        @foreach($dataCredit as $v)
          
            <tr>
            <td >{{$v["type"]}}</td>
              @if($v["transfer"] == 1)
              <td >Yes</td>
              @elseif($v["transfer"] == 2)
              <td >Withdrawal</td>
              @else
              <td >NO</td>
              @endif
              <td >{{$v->users["fullname"]}}</td>
              
              <td >{{$v["recipient"]}}</td>
             
            
              <td >{{$v["amount"]}}</td>
                <td >{{$v["balance"]}}</td>
              <td> 
                  {{$v["created_at"]}}
              </td>
              

          </tr>
            
           
          
        @endforeach

        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><b>Credit Total</b></td>
          <td><b>{{$creditTotal}}</b></td>
        </tr>
        
      </tbody>
                                        
  
                                    
  </table>

   
  <table >
                                    
      <tbody>
      <tr>
      <td >Transaction Type</td>
      <td >Transfer</td>
      <td >Transaction By</td>
      
      <td >Recipient</td>
      
      <td >Amount </td>
      <td >Remaining Balance</td>
      <td>Date</td>
       
        
      </tr>
       
       
        @foreach($dataDebit as $v)
          
            <tr>
            <td >{{$v["type"]}}</td>
              @if($v["transfer"] == 1)
              <td >Yes</td>
              @elseif($v["transfer"] == 2)
              <td >Withdrawal</td>
              @else
              <td >NO</td>
              @endif
              <td >{{$v->users["fullname"]}}</td>
              
              <td >{{$v["recipient"]}}</td>
             
            
              <td >{{$v["amount"]}}</td>
                <td >{{$v["balance"]}}</td>
              <td> 
                  {{$v["created_at"]}}
              </td>
              

          </tr>
            
           
          
        @endforeach
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><b>Debit Total</b></td>
          <td><b>{{$debitTotal}}</b></td>
        </tr>
        
      </tbody>
                                        
  
                                    
  </table>
 

  

  <table>
    <tbody>
      <tr>
        <td><b style="color:red;">Date Created</b></td>
        <td><b style="color:red;">Source</b></td>
      </tr>
      <tr>
        <td><b>{{Carbon\Carbon::today()->format('Y-m-d')}}</b></td>
        <td><b>Global Success Minds</b></td>
      </tr>
    </tbody>
  </table>

           

</html>
                              
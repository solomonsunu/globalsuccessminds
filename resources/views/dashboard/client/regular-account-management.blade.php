@extends('layouts.dash')
@section('content')

	 <div class="row" ng-controller="regularUserCtrl">
   <div class="col-lg-12">
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                          <li class="active"><a href="#home-pills" data-toggle="tab">Bank Accounts Information</a>
                          </li>
                          <li><a href="#profile-pills" data-toggle="tab">Payment History</a>
                          </li>
                            
                             <li><a href="#accounts" data-toggle="tab">Add Bank Details</a>
                            </li>
                          
                      </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        @{{messages.success}}
                    </div>
                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <ul ng-repeat="x in messages.error">
                            <li>@{{x}}</li>
                        </ul>
                        
                        
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	 <div class="tab-content">
                              <div class="tab-pane fade in active" id="home-pills">
                                   <div class="panel panel-default" ng-if="currentData.account">
                                      <div class="panel-heading">
                                          Bank Details
                                      </div>
                                      <div class="panel-body">
                                          <div class="table-responsive table-bordered">
                                              <table class="table">
                                                <tr>
                                                  <th>Account Name</th>
                                                  <td>@{{currentData.account.accountname}}</td>
                                                </tr>
                                                <tr>
                                                  <th>Account Number</th>
                                                  <td>@{{currentData.account.accountnumber}}</td>
                                                </tr>
                                                <tr>
                                                  <th>Bank Name</th>
                                                  <td>@{{currentData.account.bankname}}</td>
                                                </tr>
                                                <tr>
                                                  <th>Branch Name</th>
                                                  <td>@{{currentData.account.branch}}</td>
                                                </tr>
                                              </table>
                                          </div>
                                      </div>
                                   </div>
                                   <h4 ng-if="!currentData.account">Bank Details not found!!</h4>   
                                            
						                  </div>
                              <div class="tab-pane fade" id="profile-pills">
                                    <h4>Payment History</h4>
                                    <div class="table-responsive col-lg-12" ng-if="payHistory.accountdetail.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('type')">Payment Type
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('amount')">Amount
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in payHistory.accountdetail|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{roll.type}} </td>
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.created_at}}</td>
                                                
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                          </div>
                              </div>
                                
                                <div class="tab-pane fade" id="accounts">
                                     <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <form role="form" class="form-horizontal addstudent" ng-if="!currentData.account" ng-submit="saveAccount();" >
                                                
                                                <div class="form-group">
                                                    <label for="scl" class="col-sm-3 control-label">Bank Name </label>
                                                    <div class="col-sm-9">
                                                         <input type="text" ng-model="postData.bankname" id="scl" class="form-control">
                                                    </div>

                                                </div>
                                            
                                                <div class="form-group">
                                                    <label for="mn" class="col-sm-3 control-label">Account Name </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="postData.accountname" id="md" class="form-control">
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label for="mn" class="col-sm-3 control-label">Account Number </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="postData.accountnumber" id="md" class="form-control">
                                                    </div>
                                                </div>
                    
                                                <div class="form-group">
                                                    <label for="email" class="col-sm-3 control-label">Bank Branch</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="postData.branch" id="email" class="form-control">
                                                    </div>
                                                </div>
                                               
                                                <hr>
                                                
                                                <hr>
                                                <div class="p-md">
                                                    <button class="btn btn-success" ng-if="!loading.user">submit</button>
                                                    <span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                </div>
                                            </form>
                                            <form role="form" class="form-horizontal addstudent" ng-if="currentData.account" ng-submit="updateAccount();" >
                                                
                                                <div class="form-group">
                                                    <label for="scl" class="col-sm-3 control-label">Bank Name </label>
                                                    <div class="col-sm-9">
                                                         <input type="text" ng-model="currentData.account.bankname" id="scl" class="form-control">
                                                    </div>

                                                </div>
                                            
                                                <div class="form-group">
                                                    <label for="mn" class="col-sm-3 control-label">Account Name </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="currentData.account.accountname" id="md" class="form-control">
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label for="mn" class="col-sm-3 control-label">Account Number </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="currentData.account.accountnumber" id="md" class="form-control">
                                                    </div>
                                                </div>
                    
                                                <div class="form-group">
                                                    <label for="email" class="col-sm-3 control-label">Bank Branch</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="currentData.account.branch" id="email" class="form-control">
                                                    </div>
                                                </div>
                                               
                                                <hr>
                                                
                                                <hr>
                                                <div class="p-md">
                                                    <button class="btn btn-success" ng-if="!loading.user">Update Account</button>
                                                    <span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
     
@endSection               
@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="MobileMoneyCtrl" >
      <div class="col-lg-12">
                  
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                           
                            <li class="active"><a href="#app-pills" data-toggle="tab">Buy Airtime  </a>
                            </li>
                           
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="tab-content">
                                <div class="tab-pane fade in active animated bounceIn" id="app-pills">
                                   
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="topup();" >
                                            <h2>
                                                Perform Airtime Topup
                                            </h2>
                                              
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Amount </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.amount" id="scl" class="form-control" placeholder="eg. 2.00 (Amount must be in this format)" required>
                                                </div>


                                            </div>
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Mobile Number </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.number" id="scl" class="form-control" placeholder="eg. 024XXXXXXX" required>
                                                </div>
                                                

                                            </div>
                                            <div class="form-group ">
                                                <label for="scl" class="col-sm-5 control-label">Network Type </label>
                                                <div class="col-sm-7">
                                                  <select ng-model="postData.network" class=" form-control " required>
                                                    <option value="">Network Type</option>
                                                    <option value="1">AIRTEL</option>
                                                    <option value="3">Glo</option>
                                                    <option value="4">MTN</option>
                                                    <option value="5">TIGO</option>
                                                    <option value="6">VODAFONE</option>
                                                      
                                                    
                                                     
                                                  </select>
                                                </div>
                                                
                                            </div>
                                             <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Pin </label>
                                                <div class="col-sm-7">
                                                     <input type="password" ng-model="postData.pin" id="scl" class="form-control" required>
                                                </div>
                                                

                                            </div>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    
                                 </div>
                               
						    </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
@endSection               
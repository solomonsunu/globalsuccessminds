@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="UserMgtCtrl">
  <div class="col-lg-12">
    <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              	<!-- <li class="active"><a href="#notification" data-toggle="tab">Notifications</a>
	            </li> -->
	       		<li class="active" ><a href="#home-pills" data-toggle="tab" >Active Users</a>
	            </li>
	            
	            <li><a href="#settings-pills" data-toggle="tab">Update Account</a>
	            </li>
	            <li><a href="#superuser" data-toggle="tab">Create Super User</a>
	            </li>
             
                
              
          </ul> 
      </div>
  </div>
  <div class="col-lg-12">

      <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        @{{messages.success}}
      </div>
      <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        <ul ng-repeat="x in messages.error">
          <li>@{{x}}</li>
        </ul>  
      </div>
      <div class="panel ">
        <div class="panel-body">
           <div class="tab-content">
           				
                <div class="tab-pane fade active  animated bounceIn" id="home-pills">
                	<div class="col-md-12">
                		<h3 class="pull-left">Search Of active User Information </h3>
                	</div>
                    
                    
		            <!-- <center ng-if="loadActive"><button class="btn btn-primary" ng-click="getNew_User();">View Active List</button></center> -->
		            <div class="col-md-4">
		            	<h3 class=" btn btn-lg btn-danger"><b ng-if="!totalBalance" ng-click="getNewUser()">Get Total User Balance</b><b ng-if="totalBalance">Total User Balance</b> <b class=""> @{{totalBalance | currency : "GH " : 3}}</b></h3>
		            </div>
		            <div class="col-md-8">
		            	<form role="form" class="form-horizontal" >                
                            <div class="form-group">
                              <label  class="col-sm-3 control-label">** Search User:</label>
                              <div class="col-sm-6">
                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminAllUser(postData.username);" placeholder="Enter phone number / username / firstname / lastname">
                              </div>
                              
                            </div>
                            
                            
                        </form>
		            </div>
	            	<div class="table-responsive col-lg-12" ng-if="allUser.length > 0 && postData.username.length >0">
	            		
	        
	                    <table class="table table-striped table-bordered responsive">
	                     
	                      <tr>
	                       <th ng-click="sort('fullname')">Name
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='fullname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            
                            <th ng-click="sort('username')">Username
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                            @if(Auth::user()->admin == 1)
                            <th ng-click="sort('pin')">Pin
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='pin'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                            @endif
                             <th ng-click="sort('current_balance')">Current Balance
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='current_balance'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                             <th ng-click="sort('email')">Email
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='email'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                            <th ng-click="sort('phone')">Carrier
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='carrier'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                             <th ng-click="sort('phone')">Phone
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
                             <!-- <th ng-click="sort('country')">Country
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='country'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th> -->
                            <th ng-click="sort('created_at')">Registration Date
                                 <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                           
                            </th>
	                    
	                       
	                      </tr>
	                      
	                      
	                      <tr  ng-repeat="roll in allUser">
	                        <td>@{{roll.fullname}} </td>
	                        
	                        <td><a  ng-href="@{{'/users/'+roll.user_id}}">@{{roll.username}}</a>  </td>
	                         @if(Auth::user()->admin == 1)
	                        <td>@{{roll.pin}}</td>
	                        @endif
	                        <td>@{{roll.account.current_balance}}  </td>
	                        <td>@{{roll.email}}</td>
	                        <td>@{{roll.carrier}}</td>
	                        <td>@{{roll.phone}}</td>
	                        <!-- <td>@{{roll.country}}</td> -->
	                        <td>@{{roll.created_at}}</td>
	                      
	                      </tr>
	                     
	                    </table>
	                    
                  	</div>
                  	<div ng-if="allUser.length == 0" class="col-lg-12">
			            <div class="alert alert-danger alert-styled-right alert-arrow-right alert-bordered ">
			                <center> No Data Found </center>
			            </div>
			        </div>
		            
		        </div>
                
                <div class="tab-pane fade animated bounceIn" id="settings-pills">
                    <div class="col-lg-1"></div>
			    		<div class="col-lg-8">
				    		<form role="form" class="form-horizontal addstudent" ng-submit="updateUser();" >
				    			
				    			<div class="form-group">
				    				<label for="scl" class="col-sm-3 control-label">First Name </label>
				    				<div class="col-sm-9">
				    					<input type="text" ng-model="currentData.user_id" id="scl" class="form-control" style="display: none">
				    					<input type="text" ng-model="currentData.firstname" id="scl" class="form-control">
				    				</div>

				    			</div>
				    		
				    			<div class="form-group">
				    				<label for="mn" class="col-sm-3 control-label">Last Name </label>
				    				<div class="col-sm-9">
				    					<input type="text" ng-model="currentData.lastname" id="md" class="form-control">
				    				</div>
				    			</div>
				    			
	
				    			<div class="form-group">
					    			<label for="email" class="col-sm-3 control-label">Email</label>
									<div class="col-sm-9">
					    				<input type="email" ng-model="currentData.email" id="email" class="form-control">
					    			</div>
				    			</div>
								<div class="form-group">
					    			<label for="carrier" class="col-sm-3 control-label">Network Carrier</label>
					    			<div class="col-sm-9">
                                        <select name="carrier" class=" form-control">
                                            <option value="mtn">MTN</option>
                                            <option value="vodaphone">VODAPHONE</option>
                                            <option value="airtel">Airtel</option>
                                            <option value="glo">Glo</option>
                                             <option value="tigo">Tigo</option>
                                              <option value="expresso">Expresso</option>
                                        </select>
                                    </div>
                        		</div>
           						<div class="form-group">
					    			<label for="phone" class="col-sm-3 control-label">Want to Change Phone Number</label>
					    			<div class="col-sm-9">
					    				<label class="radio-inline"><input type="radio" ng-model="currentData.choicePhone" value="0">No</label>
					    				<label class="radio-inline"><input type="radio" ng-model="currentData.choicePhone" value="1">Yes</label>
					    			</div>
				    			</div>
				    			<div class="form-group"  ng-if="currentData.choicePhone==1">
					    			<label for="phone" class="col-sm-3 control-label">Phone Number</label>
					    			<div class="col-sm-9">
					    				<input type="text" ng-model="currentData.phone" id="phone" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group">
					    			<label for="phone" class="col-sm-3 control-label">Want to Change Password</label>
					    			<div class="col-sm-9">
					    				<label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="0">No</label>
					    				<label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="1">Yes</label>
					    			</div>
				    			</div>
				    			<div class="form-group" ng-if="currentData.choice==1">
					    			<label for="pass" class="col-sm-3 control-label">Change Password</label>
					    			<div class="col-sm-9">
					    				<input type="password" ng-model="currentData.password" id="pass" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group" ng-if="currentData.choice==1">
					    			<label for="pass1" class="col-sm-3 control-label">Repeat New Password</label>
					    			<div class="col-sm-9">
					    				<input type="password" ng-model="currentData.password_confirmation" id="pass1" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group">
					    			<label for="gender" class="col-sm-3 control-label">Gender</label>
					    			<div class="col-sm-9">
					    				<select name="gender" id="gender" ng-model='currentData.gender' class='form-control'>
					    					<option></option>
					    					<option value="male">Male</option>
					    					<option value="female">Female</option>
					    				</select>
					    			</div>
					    		</div>
								<div  class="form-group">	
									<label for="" class="col-sm-3 control-label">upload Image</label>
									<div class="col-md-9">
										<div class="img-container">
											<img src="{{ asset('img/profile.png') }}" class="img-responsive profilePic">
										</div>
										<div class="img-control">
											<!--<label for="fInput" class="btn btn-default btn-outline text-center">Select Image</label>-->
											<div id="upld"><input type="file" file-model="currentData.file" accept="image/*" id="fInput"></div>
										</div>
									</div>
								</div>
								<hr>
								
					    		<hr>
				    			<div class="p-md">
				    				<button class="btn btn-success" ng-if="!loading.user">submit changes</button>
				    				<span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
				    			</div>
				    		</form>
				    	</div>
			    		<div class="col-lg-1"></div>
                </div>
                
		        <div class="tab-pane fade animated bounceIn" id="superuser">
                    <div class="col-lg-1"></div>
			    		<div class="col-lg-8">
				    		<form role="form" class="form-horizontal addstudent" ng-submit="createUser();" >
				    			
				    			<div class="form-group">
				    				<label for="scl" class="col-sm-3 control-label">First Name </label>
				    				<div class="col-sm-9">
				    					<input type="text" ng-model="postData.user_id" id="scl" class="form-control" style="display: none">
				    					<input type="text" ng-model="postData.firstname" id="scl" class="form-control">
				    				</div>

				    			</div>
				    		
				    			<div class="form-group">
				    				<label for="mn" class="col-sm-3 control-label">Last Name </label>
				    				<div class="col-sm-9">
				    					<input type="text" ng-model="postData.lastname" id="md" class="form-control">
				    				</div>
				    			</div>

				    			<div class="form-group">
				    				<label for="mn" class="col-sm-3 control-label">User Name </label>
				    				<div class="col-sm-9">
				    					<input type="text" ng-model="postData.lastname" id="md" class="form-control">
				    				</div>
				    			</div>
				    			
	
				    			<div class="form-group">
					    			<label for="email" class="col-sm-3 control-label">Email</label>
									<div class="col-sm-9">
					    				<input type="email" ng-model="postData.email" id="email" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group">
				    				<label for="country" class="col-sm-3 control-label">Country</label>
					    			<div class="col-md-8 pull-right bfh-selectbox bfh-countries" data-name="country" data-country="GH" data-available="GH,US,NG" data-flags="true">
		                                  <input type="hidden" value="" ng-model="country">
		                                  <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
		                                    <span class="bfh-selectbox-option" data-option=""></span>
		                                   
		                                  </a>
		                                  <div class="bfh-selectbox-options">
		                                    <input type="text" ng-model="country" class="form-control bfh-selectbox-filter">
		                                    <div role="listbox">
		                                        <ul role="option">
		                                        </ul>
		                                    </div>
		                                  </div>
		                            </div>
	                            </div>
								<div class="form-group">
					    			<label for="carrier" class="col-sm-3 control-label">Network Carrier</label>
					    			<div class="col-sm-9">
                                        <select ng-model="carrier" class=" form-control">
                                            <option value="mtn">MTN</option>
                                            <option value="vodaphone">VODAPHONE</option>
                                            <option value="airtel">Airtel</option>
                                            <option value="glo">Glo</option>
                                        	<option value="tigo">Tigo</option>
                                            <option value="expresso">Expresso</option>
                                        </select>
                                    </div>
                        		</div>
           
				    			<div class="form-group">
					    			<label for="phone" class="col-sm-3 control-label">Phone Number</label>
					    			<div class="col-sm-9">
					    				<input type="text" ng-model="PostData.phone" id="phone" class="form-control">
					    			</div>
				    			</div>
				    			
				    			<div class="form-group" >
					    			<label for="pass" class="col-sm-3 control-label">Change Password</label>
					    			<div class="col-sm-9">
					    				<input type="password" ng-model="postData.password" id="pass" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group" >
					    			<label for="pass1" class="col-sm-3 control-label">Repeat New Password</label>
					    			<div class="col-sm-9">
					    				<input type="password" ng-model="postData.password_confirmation" id="pass1" class="form-control">
					    			</div>
				    			</div>
				    			<div class="form-group">
					    			<label for="gender" class="col-sm-3 control-label">Gender</label>
					    			<div class="col-sm-9">
					    				<select  id="gender" ng-model='postData.gender' class='form-control'>
					    					<option></option>
					    					<option value="male">Male</option>
					    					<option value="female">Female</option>
					    				</select>
					    			</div>
					    		</div>
					    		<div class="form-group">
					    			<label for="gender" class="col-sm-3 control-label">Assign Role</label>
					    			<div class="col-sm-9">
					    				<select  id="gender" ng-model='postData.gender' class='form-control'>
	
					    					<option value="1">GSMadmin</option>
					    					<option value="2">GSM 1</option>
					    					<option value="3">GSM 2</option>
					    					<option value="4">GSM 3</option>
					    					<option value="5">GSM 4</option>
					    					<option value="6">GSM 5</option>
					    					<option value="7">GSM 6</option>
					    				</select>
					    			</div>
					    		</div>
								<div  class="form-group">	
									<label for="" class="col-sm-3 control-label">upload Image</label>
									<div class="col-md-9">
										<div class="img-container">
											<img src="{{ asset('img/profile.png') }}" class="img-responsive profilePic">
										</div>
										<div class="img-control">
											<!--<label for="fInput" class="btn btn-default btn-outline text-center">Select Image</label>-->
											<div id="upld"><input type="file" file-model="currentData.file" accept="image/*" id="fInput"></div>
										</div>
									</div>
								</div>
								<hr>
								
					    		<hr>
				    			<div class="p-md">
				    				<button class="btn btn-success" ng-if="!loading.user">Save</button>
				    				<span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
				    			</div>
				    		</form>
				    	</div>
			    	<div class="col-lg-1"></div>
                </div>
                          
            </div>
            
        </div>
    </div>
       
  </div>
</div>
@endsection






@extends('layouts.dash')
@section('content')
    <div ng-controller="UserCtrl" class="view-stud">
		<div class="row ">
	        <div class="col-lg-12">
	            <h2 class="page-header pull-left">Admin Activity Monitor </h2>
                <div class="form-inline page-header pull-right">
                    <input class="form-control" type="date" ng-min="@{{minDate|date:'2015-12-01'}}" ng-model="postData.setDate">
                    <button class=" form-control btn btn-success" ng-click="defActivity(postData.setDate)">check</button>
                </div>

	        </div>
	        
	    </div>
        <div class="row">
    		<div class="col-lg-12">
    			<div class="ibox float-e-margins">
    		            <div class="ibox-content">
                             
                                    <div class="tab-body profile printme">

                                        <div  class="row">
                                            <h3 ng-if="message.length > 0" class="alert alert-info">@{{message}}</h3>
                                            <div class="col-md-12 col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-list"></i> Administrative Charges History
                                                    </div>
                                                    <div class="panel-body">
                                                       
                                                        <div class="table-responsive col-lg-12" ng-if="total_profit.length > 0">
                                                            <form class="form-inline pull-right">
                                                                <div class="form-group">
                                                                    <label >Search</label>
                                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                                </div>
                                                            </form>
                                                             <dir-pagination-controls
                                                                max-size="5"
                                                                direction-links="true"
                                                                boundary-links="true" >
                                                            </dir-pagination-controls>
                                                            <table class="table table-striped table-bordered responsive">
                                                             
                                                              <tr>
                                                               <th ng-click="sort('month')">Month
                                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='month'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                                </th>
                                                                <th ng-click="sort('amount')">Amount
                                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                               
                                                                </th>
                                                                <th ng-click="sort('created_at')">Date
                                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                                </th>

                                                            
                                                               
                                                              </tr>
                                                              
                                                              
                                                              <tr  dir-paginate="roll in total_profit|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                                <td>@{{roll.month}} </td>
                                                                <td>@{{roll.amount}}  </td>
                                                                <td>@{{roll.created_at}} </td>
                                                                
                                                              </tr>
                                                             
                                                            </table>
                                                             <dir-pagination-controls
                                                                max-size="5"
                                                                direction-links="true"
                                                                boundary-links="true" class="pull-right">
                                                            </dir-pagination-controls>
                                                          </div>
                                                         <div ng-if="total_profit.length == 0">
                                                             <center>
                                                                 <h3>
                                                                     No Results found
                                                                 </h3>
                                                             </center>
                                                         </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="row">
                                            <h3 ng-if="message.length > 0" class="alert alert-info">@{{message}}</h3>
                                            <div class="col-md-12 col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-list"></i> TopUp Commision History
                                                    </div>
                                                    <div class="panel-body">
                                                       
                                                        <div class="table-responsive col-lg-12" ng-if="topup_profit.length > 0">
                                                            <form class="form-inline pull-right">
                                                                <div class="form-group">
                                                                    <label >Search</label>
                                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                                </div>
                                                            </form>
                                                             <dir-pagination-controls
                                                                max-size="5"
                                                                direction-links="true"
                                                                boundary-links="true" >
                                                            </dir-pagination-controls>
                                                            <table class="table table-striped table-bordered responsive">
                                                             
                                                              <tr>
                                                               <th ng-click="sort('month')">Network
                                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='month'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                                </th>
                                                                <th ng-click="sort('amount')">Amount
                                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                               
                                                                </th>
                                                                <th ng-click="sort('created_at')">Date
                                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                                </th>

                                                            
                                                               
                                                              </tr>
                                                              
                                                              
                                                              <tr  dir-paginate="roll in topup_profit|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                                <td>@{{roll.network}} </td>
                                                                <td>@{{roll.commision}}  </td>
                                                                <td>@{{roll.created_at}} </td>
                                                                
                                                              </tr>
                                                             
                                                            </table>
                                                             <dir-pagination-controls
                                                                max-size="5"
                                                                direction-links="true"
                                                                boundary-links="true" class="pull-right">
                                                            </dir-pagination-controls>
                                                          </div>
                                                         <div ng-if="topup_profit.length == 0">
                                                             <center>
                                                                 <h3>
                                                                     No Results found
                                                                 </h3>
                                                             </center>
                                                         </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-list"></i> User Wallet Topup Summary
                                                    </div>
                                                    <div class="panel-body">
                                                         <table class="table">
                                                            <tr>
                                                                <th>Admin Type</th>
                                                                <th>Total Account Top Up</th>
                                                                <th>Network</th>
                                                                
                                                            </tr>
                                                          
                                                            <tr>
                                                                <td>Admin 1</td>
                                                                <td>@{{ admin1 }}</td>
                                                                <td>Airtel</td>
                                                               
                                                            </tr>
                                                             <tr>
                                                                <td>Admin 2</td>
                                                                <td>@{{ admin2 }}</td>
                                                                <td>Mtn</td>
                                                            </tr>
                                                             <tr>
                                                                <td>Admin 3</td>
                                                                <td>@{{ admin3 }}</td>
                                                                <td>Tigo</td>
                                                            </tr>
                                                             <tr>
                                                                <td>Admin 4</td>
                                                                <td>@{{ admin4 }}</td>
                                                                <td>Vodafone</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Admin 5</td>
                                                                <td>@{{ admin5 }}</td>
                                                                <td>Glo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Admin 6</td>
                                                                <td>@{{ admin6 }}</td>
                                                                <td>Expresso</td>
                                                            </tr>
                                                        </table>
                                                          
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                               
    		            </div>
    		    </div>
    		</div>
	    </div>
	   
    </div>
   
@endSection  
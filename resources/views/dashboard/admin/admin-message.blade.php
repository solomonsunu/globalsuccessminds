@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="MessageCtrl">
      <div class="col-lg-12">
                  
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                            <li class="active"><a href="#home-pills" data-toggle="tab">Blast Dashboard Message </a>
                            </li>
                             <li ><a href="#email-pills" data-toggle="tab">Blast Email Message</a>
                            </li>
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                  <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        @{{messages.success}}
                    </div>
                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <ul ng-repeat="x in messages.error">
                            <li>@{{x}}</li>
                        </ul>
                        
                        
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="tab-content">
                                <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                   <div class="col-lg-1"></div>
                                    <div class="col-lg-8">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="sendMessage();" >
                                            
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-3 control-label">Message Title </label>
                                                <div class="col-sm-9">
                                                     <input type="text" ng-model="postData.title" id="scl" class="form-control">
                                                </div>

                                            </div>
                                        
                                            <div class="form-group">
                                                <label for="mn" class="col-sm-3 control-label">Message Content </label>
                                                <div class="col-sm-9">
                                                    <textarea id="mn" class="form-control" ng-model="postData.message" cols="40" rows="10"></textarea>
                                                    
                                                </div>
                                            </div>
                                           
                                            <hr>
                                            
                                            <hr>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    <div class="col-lg-1"></div>
                                 </div>
                                <div class="tab-pane fade animated bounceIn" id="email-pills">
                                     <div class="col-lg-1"></div>
                                    <div class="col-lg-8">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="sendMail();" >
                                            
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-3 control-label">Email Title </label>
                                                <div class="col-sm-9">
                                                     <input type="text" ng-model="postData.title" id="scl" class="form-control">
                                                </div>

                                            </div>
                                        
                                            <div class="form-group">
                                                <label for="mn" class="col-sm-3 control-label">Email Content </label>
                                                <div class="col-sm-9">
                                                    <textarea id="mn" class="form-control" ng-model="postData.message" cols="40" rows="10"></textarea>
                                                    
                                                </div>
                                            </div>
                                           
                                            <hr>
                                            
                                            <hr>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
						    </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
@endSection               
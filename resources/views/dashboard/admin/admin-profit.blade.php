@extends('layouts.dash')
@section('content')
 <div class="row" ng-controller="InsuranceCtrl">
 <div class="col-lg-12">
      
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                <li class="active"><a href="#home-pills" data-toggle="tab">GSM Team Sales Commision Profits </a>
                </li>
                
                
            </ul> 
        </div>
    </div>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    @{{messages.success}}
                </div>
                <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <ul ng-repeat="x in messages.error">
                        <li>@{{x}}</li>
                    </ul> 
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                    	<div class="tab-content">
                            <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                <div class="col-lg-12" ng-if="sales.length > 0">
                                    <form class="form-inline pull-right">
                                        <div class="form-group">
                                            <label >Search</label>
                                            <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" >
                                    </dir-pagination-controls>
                                    <table class="table table-striped table-bordered responsive">
                                      <tr>
                                       
                                       
                                        <th ng-click="sort('month')">Date
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        
                                        <th ng-click="sort('amount')">Profit
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                      </tr>
                                      
                                      
                                      <tr  dir-paginate="roll in sales|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                        
                                        <td>@{{roll.month}}</td>
                                        <td>@{{roll.amount}}</td>
                                        
                                      </tr>
                                     
                                    </table>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" class="pull-right">
                                    </dir-pagination-controls>
                                     
                                </div>
                                <div class="col-lg-12" ng-if="sales.length == 0">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          Company Earning Information Currently Unavailable
                                      </button>
                                    </center>
                                </div>
                                
                             </div>
                           
					    </div>
                            
                            
                            
                        </div>
                        
                    </div>
                </div>
             
            </div>

 </div>
@endSection               
@extends('layouts.dash')
@section('content')

<div class="row" ng-controller="adminWalletCtrl">
        <div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active"><a href="#pin" data-toggle="tab">Pending Withdrawal Request  </a>
              </li>
              <li><a href="#transaction" data-toggle="tab">User Withdrawal History</a>
              </li>
              
               <!-- <li><a href="#balance" data-toggle="tab">Balance Info</a>
              </li>
                <li><a href="#transfer" data-toggle="tab">Tranfer Funds</a>
              </li> -->
              
          </ul> 
        </div>
    </div>
          <div class="col-lg-12">
              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <div ng-bind-html="messages.success"></div>
              </div>
              <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                  <ul ng-repeat="x in messages.error">
                      <li>@{{x}}</li>
                  </ul>
                  
                  
              </div>
              <div class="panel">
                  <div class="panel-body">
                  	 <div class="tab-content">
                        <div class="tab-pane fade in active animated bounceIn" id="pin">
                          
                                  <div class="col-lg-12">
                                       <h4>Pending Wallet Withdrawal Request</h4>
                                       <center ng-if="!withdrawal"><button class="btn btn-primary" ng-click="getWithdrawal()">View List</button></center>
                                    <div class="table-responsive col-lg-12" ng-if="withdrawal.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('amount')">Name
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('amount')">Phone Number
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('amount')">Amount
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('type')">Payment Method
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('type')">Payment Status
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in withdrawal|orderBy:sortKey:reverse|filter:search|filter:filter2|itemsPerPage:10">
                                                <td>@{{roll.user.fullname}} </td>
                                                <td>@{{roll.user.phone}} </td>
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.created_at}} </td>

                                                <td><span ng-if='roll.cash_pickup == "1"'> @{{roll.phone_number}}</span>
                                                <span ng-if='roll.cash_pickup == "0"'>Account Name: @{{roll.account_name}} <br>
                                                  Account Number: @{{roll.account_number}} <br>
                                                  Bank Name: @{{roll.bank_name}} <br>
                                                  Branch: @{{roll.branch_name}} 
                                                </span>
                                                </td>
                                                <td><button ng-if="!loading.check" ng-click="checkPaid(roll.user_withdrawal_id)">Check paid</button><span class="btn btn-success pull-right" ng-if="loading.check"><i class="fa fa-spinner fa-spin"></i></span></td>
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <h4 ng-if="withdrawal.length == 0" class="text-center">No Transaction History Found</h4> 
                                      
                                  </div>
                                  
			                  </div>
                        <div class="tab-pane fade animated bounceIn" id="transaction">
                        
                                    <h4>Wallet Withdrawal History</h4>
                                       <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                         
                    
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="getWithdrawalHistory();" >
                                            
                                            <div class="form-group">
                                              <label  class="col-sm-3 control-label">** Search User:</label>
                                              <div class="col-sm-3">
                                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminManageUser(postData.username);">
                                              </div>
                                              <div class="col-sm-3">
                                                <input class="form-control" type="text" ng-model="senderData.username" readonly>
                                              </div>
                                               <div class="text-right">
                                                <button class="btn btn-success" ng-if="!loading.history">submit</button>
                                                <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label  class="col-sm-4 control-label"></label>
                                              <div class="col-sm-4">
                                                <div>
                                                  <a href="#" class="list-group-item" ng-repeat="x in managedUser" ng-click="selectUser(x);">@{{x.username}}</a>
                                                </div>
                                              </div>
                                            
                                            </div>
                                            
        
                                            
                                        </form>
                                        
                                    </div>
                                    <div class="col-lg-1"></div>
                                  <div class="col-lg-1"></div>
                                  <div class="col-lg-10">
                                    <div class="table-responsive col-lg-12" ng-if="withdrawalHistory.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                
                                                <th ng-click="sort('amount')">Amount
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('type')">Payment Method
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('type')">Payment Status
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th>
                                                  Payment Date
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in withdrawalHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.created_at}} </td>
                                                <td><span ng-if='roll.cash_pickup == "1"'> @{{roll.phone_number}}</span>
                                                <span ng-if='roll.cash_pickup == "0"'>Account Name: @{{roll.account_name}} <br>
                                                  Account Number: @{{roll.account_number}} <br>
                                                  Bank Name: @{{roll.bank_name}} <br>
                                                  Branch: @{{roll.branch_name}} 
                                                </span></td>
                                                <td><span ng-if="roll.paid == '1'"> Paid </span><span ng-if="roll.paid == '0'"> Unpaid</span></td>
                                                <td>@{{roll.paid_date}}</td>
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <h4 ng-if="withdrawalHistory.length == 0" class="text-center">No Transaction History Found</h4>   
                                 </div>
                          <div class="col-lg-1"></div>
                        </div>
                          
                        
                          
                      </div>
                      
                  </div>
              </div>
          </div>
</div>
     
@endSection               
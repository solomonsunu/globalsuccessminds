@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="adminWalletCtrl">
  <div class="col-lg-12">
    <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active">
                <a href="#credit" data-toggle="tab">Credit Account</a>
              </li>
              <li>
                <a href="#transaction" data-toggle="tab">Transaction History</a>
              </li>
              
              <li>
                <a href="#balance" data-toggle="tab">Balance Info</a>
              </li>

              <li>
                <a href="#funds" data-toggle="tab">Tranfer Funds</a>
              </li>
              
              <li>
                <a href="#adminTransaction" data-toggle="tab" >Admin Transaction History</a>
              </li>

              <li ng-show="currentData.admin == 1" >
                <a href="#adminHistory" data-toggle="tab">All Admins  History</a>
              </li>

              
                
              
          </ul> 
      </div>
  </div>
  <div class="col-lg-12">

      <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        @{{messages.success}}
      </div>
      <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        <ul ng-repeat="x in messages.error">
          <li>@{{x}}</li>
        </ul>  
      </div>
      <div class="panel">
        <div class="panel-body">
           <div class="tab-content">
              <div class="tab-pane fade in active animated bounceIn" id="credit">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8" ng-show="currentData.admin != 1">
                      <h4 class="text-center">Credit User Account</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  ng-submit="creditAccount();" >
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">** Search User:</label>
                                <div class="col-sm-4">
                                <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminManageUser(postData.username);">
                                </div>
                                <div class="col-sm-4">
                                  <input class="form-control" type="text" ng-model="senderData.username" readonly>
                                </div>
                                
                              </div>
                              <div class="form-group">
                                <label  class="col-sm-4 control-label"></label>
                                <div class="col-sm-4">
                                  <div>
                                    <a href="#" class="list-group-item" ng-repeat="x in managedUser" ng-click="selectUser(x);">@{{x.username}}</a>
                                  </div>
                                </div>
                              
                              </div>
                             
                              <div class="form-group">
                                  <label  class="col-sm-4 control-label">** Enter Amount:</label>
                                  <div class="col-sm-4">
                                    <input class="form-control" type="text" ng-model="postData.amount" >
                                  </div>
                                  <div class="pull-right">
                                      <button class="btn btn-success" ng-if="!loading.credit">credit account</button>
                                      <span class="btn btn-success" ng-if="loading.credit"><i class="fa fa-spinner fa-spin"></i></span>
                                  </div>
                              </div>
                                  
                              

                              
                          </form>
                          
                      </div>
                    </div>
                    <div class="col-lg-8" ng-show="currentData.admin == 1">
                      <h4 class="text-center">Credit User Account</h4>
                      <div class="col-lg-12">
                           <form role="form" class="form-horizontal addstudent"  ng-submit="creditAccount();" >
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">** Search User:</label>
                                <div class="col-sm-4">
                                <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminAllUser(postData.username);">
                                </div>
                                <div class="col-sm-4">
                                  <input class="form-control" type="text" ng-model="senderData.username" readonly>
                                </div>
                                
                              </div>
                              <div class="form-group">
                                <label  class="col-sm-4 control-label"></label>
                                <div class="col-sm-4">
                                  <div>
                                    <a href="#" class="list-group-item" ng-repeat="x in alluser" ng-click="selectUser(x);">@{{x.username}}</a>
                                  </div>
                                </div>
                              
                              </div>
                             
                              <div class="form-group">
                                  <label  class="col-sm-4 control-label">** Enter Amount:</label>
                                  <div class="col-sm-4">
                                    <input class="form-control" type="text" ng-model="postData.amount" >
                                  </div>
                                  <div class="text-right">
                                      <button class="btn btn-success" ng-if="!loading.credit">credit account</button>
                                      <span class="btn btn-success" ng-if="loading.credit"><i class="fa fa-spinner fa-spin"></i></span>
                                  </div>
                              </div>
                                  
                                  

                              
                          </form>
                          
                      </div>
                    </div>
                  <div class="col-lg-1"></div>
                                  
              </div>
              <div class="tab-pane fade animated bounceIn " id="transaction">
                
                <div class="col-lg-12" ng-show="currentData.admin != 1">
                    <h4 class="text-center">Wallet History</h4>
                  
                    <div class="col-lg-12">
                        <form role="form" class="form-horizontal addstudent"  ng-submit="getWalletHistoryAdminSearch();" >
                            <div class="form-group">
                              <label for="scl" class="col-sm-3 control-label">Start Date </label>
                              <div class="col-sm-7">
                                   <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                              </div>
                             
                            </div>
                            <div class="form-group">
                                <label for="scl" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-7">
                                     <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                </div>
                                

                            </div>
                            <div class="form-group">
                              <label  class="col-sm-3 control-label">** Search User:</label>
                              <div class="col-sm-3">
                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminManageUser(postData.username);">
                              </div>
                              <div class="col-sm-3">
                                <input class="form-control" type="text" ng-model="senderData.username" readonly>
                              </div>
                               <div class="text-right">
                                <button class="btn btn-success" ng-if="!loading.history">submit</button>
                                <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label  class="col-sm-4 control-label"></label>
                              <div class="col-sm-4">
                                <div>
                                  <a href="#" class="list-group-item" ng-repeat="x in managedUser" ng-click="selectUser(x);">@{{x.username}}</a>
                                </div>
                              </div>
                            
                            </div>


                        </form>
                        
                    </div>
                    
             
                    <div class="table-responsive col-lg-12" ng-if="walletHistory.length > 0">
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label >Search</label>
                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" >
                            </dir-pagination-controls>
                            <table class="table table-striped table-bordered responsive">
                              <tr>
                                <th ng-click="sort('type')">Transaction Type
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')">Transfer
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('fullname')">Recipient
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('fullname')">Transaction By
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('amount')">Amount
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                               
                                </th>
                                <th ng-click="sort('balance')">Current Balance
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='balance'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                               
                                </th>
                                <th ng-click="sort('created_at')">Date
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                               
                                </th>
                                
                              </tr>
                              
                              
                              <tr  dir-paginate="roll in walletHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                <td>@{{roll.type}} </td>
                                <td><span ng-if="roll.transfer == 1">Yes</span><span ng-if="roll.transfer == 0">No</span> <span ng-if="roll.transfer == 2">Withdrawal</span></td>
                                <td>@{{roll.recipient}} </td>
                                <td>@{{roll.users.fullname}} </td>
                                <td>@{{roll.amount}} </td>
                                <td>@{{roll.balance}} </td>
                                <td>@{{roll.created_at}}</td>
                                
                              </tr>
                             
                            </table>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" class="pull-right">
                            </dir-pagination-controls>
                    </div>
                </div>
                <div class="col-lg-12" ng-show="currentData.admin == 1">
                  <h4 class="text-center">Wallet History</h4>
                  
                  <div class="col-lg-12">
                      <form role="form" class="form-horizontal addstudent"  ng-submit="getWalletHistoryAdminSearch();" >
                           <div class="form-group">
                              <label for="scl" class="col-sm-3 control-label">Start Date </label>
                              <div class="col-sm-7">
                                   <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                              </div>
                             
                            </div>
                            <div class="form-group">
                                <label for="scl" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-7">
                                     <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                </div>
                                

                            </div>
                          <div class="form-group">
                              <label  class="col-sm-3 control-label">** Search User:</label>
                              <div class="col-sm-3">
                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminAllUser(postData.username);">
                              </div>
                              <div class="col-sm-3">
                                <input class="form-control" type="text" ng-model="senderData.username" readonly>
                              </div>
                              <div class="text-right">
                              <button class="btn btn-success" ng-if="!loading.history">submit</button>
                              <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                            </div>
                              
                            </div>
                            <div class="form-group">
                              <label  class="col-sm-4 control-label"></label>
                              <div class="col-sm-4">
                                <div>
                                  <a href="#" class="list-group-item" ng-repeat="x in alluser" ng-click="selectUser(x);">@{{x.username}}</a>
                                </div>
                              </div>
                            
                            </div>
                          
                          

                          
                      </form>
                      
                  </div>
                 
                  <div class="table-responsive col-lg-12" ng-if="walletHistory.length > 0">
                          <form class="form-inline pull-right">
                              <div class="form-group">
                                  <label >Search</label>
                                  <input type="text" ng-model="search" class="form-control" placeholder="Search">
                              </div>
                          </form>
                          <dir-pagination-controls
                              max-size="5"
                              direction-links="true"
                              boundary-links="true" >
                          </dir-pagination-controls>
                          <table class="table table-striped table-bordered responsive">
                            <tr>
                              <th ng-click="sort('type')">Transaction Type
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('transfer')">Transfer
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('fullname')">Recipient
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('fullname')">Transaction By
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('amount')">Amount
                                   <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                             
                              </th>
                              <th ng-click="sort('balance')">Current Balance
                                   <span class="glyphicon sort-icon" ng-show="sortKey=='balance'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                             
                              </th>
                              <th ng-click="sort('created_at')">Date
                                   <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                             
                              </th>
                              
                            </tr>
                            
                            
                            <tr  dir-paginate="roll in walletHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                              <td>@{{roll.type}} </td>
                              <td><span ng-if="roll.transfer == 1">Yes</span><span ng-if="roll.transfer == 0">No</span> <span ng-if="roll.transfer == 2">Withdrawal</span> </td>
                              <td>@{{roll.recipient}} </td>
                              <td>@{{roll.users.fullname}} </td>
                              <td>@{{roll.amount}} </td>
                              <td>@{{roll.balance}} </td>
                              <td>@{{roll.created_at}}</td>
                              
                            </tr>
                           
                          </table>
                          <dir-pagination-controls
                              max-size="5"
                              direction-links="true"
                              boundary-links="true" class="pull-right">
                          </dir-pagination-controls>
                  </div>
                </div>
                      
              </div>
                          
              <div class="tab-pane fade animated bounceIn" id="balance">
                     
                        <div class="col-lg-12" ng-show="currentData.admin != 1">
                           <div class="panel panel-primary" >
                              <div class="panel-heading text-center">
                                  Wallet Balance
                              </div>
                              <div class="panel-body">
                               
                                  <div class="col-lg-12">
                                      <form role="form" class="form-horizontal addstudent"  ng-submit="getWalletBalance();" >
                                            <div class="form-group">
                                              <label  class="col-sm-3 control-label">** Search User:</label>
                                              <div class="col-sm-3">
                                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminManageUser(postData.username);">
                                              </div>
                                              <div class="col-sm-3">
                                                <input class="form-control" type="text" ng-model="senderData.username" readonly>
                                              </div>
                                               <div class="text-right">
                                                  <button class="btn btn-success" ng-if="!loading.history ">check</button>
                                                  <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                                              </div>
                                              
                                            </div>
                                            <div class="form-group">
                                              <label  class="col-sm-4 control-label"></label>
                                              <div class="col-sm-4">
                                                <div>
                                                  <a href="#" class="list-group-item" ng-repeat="x in managedUser" ng-click="selectUser(x);">@{{x.username}}</a>
                                                </div>
                                              </div>
                                            
                                            </div>
                                        
                                          
                                      </form>
                                      
                                  </div>
                                
                                
                              </div>
                            <div class="text-center" ng-if="walletBalance.length > 0">
                              <h2>@{{walletBalance}}</h2>
                            </div>
                          </div>
                         
                        </div>
                        <div class="col-lg-12" ng-show="currentData.admin == 1">
                          <div class="panel panel-primary" >
                            <div class="panel-heading text-center">
                                Wallet Balance
                            </div>
                            <div class="panel-body">
                              <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <form role="form" class="form-horizontal addstudent"  ng-submit="getWalletBalance();" >
                                        <div class="form-group">
                                              <label  class="col-sm-4 control-label">** Search User:</label>
                                              <div class="col-sm-4">
                                              <input class="form-control" type="search" ng-model="postData.username"  ng-change="adminAllUser(postData.username);">
                                              </div>
                                              <div class="col-sm-4">
                                                <input class="form-control" type="text" ng-model="senderData.username" readonly>
                                              </div>
                                              
                                            </div>
                                            <div class="form-group">
                                              <label  class="col-sm-4 control-label"></label>
                                              <div class="col-sm-4">
                                                <div>
                                                  <a href="#" class="list-group-item" ng-repeat="x in alluser" ng-click="selectUser(x);">@{{x.username}}</a>
                                                </div>
                                              </div>
                                            
                                            </div>
                                        <div class="form-group">
                                          
                                          <div class="text-right">
                                            <button class="btn btn-success" ng-if="!loading.history ">check</button>
                                            <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                                        </div>
                                        </div>
                                        
    
                                        
                                    </form>
                                    
                                </div>
                                <div class="col-lg-2"></div>
                                
                            </div>
                            <div class="text-center" ng-if="walletBalance.length > 0">
                                    
                                        <h2>@{{walletBalance}}</h2>
                                        
                                      
                            </div>
                        
                          </div>
                        </div>
                        
              </div>
              <div class="tab-pane fade animated bounceIn" id="funds">
                     <div class="col-lg-1"></div>
                        <div class="col-lg-8" ng-show="currentData.admin != 1">
                            <form role="form" class="form-horizontal addstudent"  ng-submit="transferBalance();" >
                               
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">** Search Sender:</label>
                                    <div class="col-sm-3">
                                      <input class="form-control" type="search" ng-model="postDataS.username"  ng-change="adminSender(postDataS.username);">
                                      </div>
                                      <div class="col-sm-3">
                                        <input class="form-control" type="text" ng-model="transferDataS.username" readonly>
                                      </div>
                                    
                                </div>
                                <div class="form-group">
                                  <label  class="col-sm-4 control-label"></label>
                                  <div class="col-sm-4">
                                    <div>
                                      <a href="#" class="list-group-item" ng-repeat="x in userS" ng-click="selectSender(x);">@{{x.username}}</a>
                                    </div>
                                  </div>
                                
                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">** Search Recipient:</label>
                                    <div class="col-sm-3">
                                    <input class="form-control" type="search" ng-model="postDataR.username"  ng-change="adminRecipient(postDataR.username);">
                                    </div>
                                    <div class="col-sm-3">
                                      <input class="form-control" type="text" ng-model="transferDataR.username" readonly>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                  <label  class="col-sm-4 control-label"></label>
                                  <div class="col-sm-4">
                                    <div>
                                      <a href="#" class="list-group-item" ng-repeat="x in userR" ng-click="selectRecipient(x);">@{{x.username}}</a>
                                    </div>
                                  </div>
                                
                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-4 control-label">** Enter Amount:</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" type="text" ng-model="postData.amount">
                                    </div>
                                    
                                </div>
                                
                                <div class="text-right">
                                    <button class="btn btn-success" ng-if="!loading.transfer">transfer</button>
                                    <span class="btn btn-success" ng-if="loading.transfer"><i class="fa fa-spinner fa-spin"></i></span>
                                </div>
                            </form>
                            
                        </div>
                         <div class="col-lg-8" ng-show="currentData.admin == 1">
                            <form role="form" class="form-horizontal addstudent"  ng-submit="transferBalance();" >
                               
                                
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">** Search Sender:</label>
                                    <div class="col-sm-3">
                                      <input class="form-control" type="search" ng-model="postDataS.username"  ng-change="superAdminSender(postDataS.username);">
                                      </div>
                                      <div class="col-sm-3">
                                        <input class="form-control" type="text" ng-model="transferDataS.username" readonly>
                                      </div>
                                    
                                </div>
                                <div class="form-group">
                                  <label  class="col-sm-4 control-label"></label>
                                  <div class="col-sm-4">
                                    <div>
                                      <a href="#" class="list-group-item" ng-repeat="x in userS" ng-click="selectSender(x);">@{{x.username}}</a>
                                    </div>
                                  </div>
                                
                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">** Search Recipient:</label>
                                    <div class="col-sm-3">
                                    <input class="form-control" type="search" ng-model="postDataR.username"  ng-change="superAdminRecipient(postDataR.username);">
                                    </div>
                                    <div class="col-sm-3">
                                      <input class="form-control" type="text" ng-model="transferDataR.username" readonly>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                  <label  class="col-sm-4 control-label"></label>
                                  <div class="col-sm-4">
                                    <div>
                                      <a href="#" class="list-group-item" ng-repeat="x in userR" ng-click="selectRecipient(x);">@{{x.username}}</a>
                                    </div>
                                  </div>
                                
                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">** Enter Amount:</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" type="text" ng-model="postData.amount">
                                    </div>
                                    
                                </div>
                                
                                <div class="text-right">
                                    <button class="btn btn-success" ng-if="!loading.transfer">transfer</button>
                                    <span class="btn btn-success" ng-if="loading.transfer"><i class="fa fa-spinner fa-spin"></i></span>
                                </div>
                            </form>
                            
                        </div>
                       
                      <div class="col-lg-1"></div>
              </div>
              <div class="tab-pane fade animated bounceIn " id="adminTransaction">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" >
                  <h4 class="text-center">Wallet History</h4>
                  <div class="row">
                    <div class="col-lg-12">
                      <form role="form" class="form-horizontal addstudent"  ng-submit="adminWHistoryWithDate();" >
                            <div class="form-group">
                              <label for="scl" class="col-sm-3 control-label">Start Date </label>
                              <div class="col-sm-7">
                                   <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                              </div>
                             
                            </div>
                            <div class="form-group">
                                <label for="scl" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-7">
                                     <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                </div>
                                

                            </div>
                            <div class="form-group">
                             
                              <div class="text-right">
                                <button class="btn btn-success" ng-if="!loading.history">submit</button>
                                <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                              </div>
                            </div>
                            

                            
                        </form>
                    </div>
                  </div>
           
                  <div class="table-responsive col-lg-12" ng-if="adminHistory.length > 0">
                          <form class="form-inline pull-right">
                              <div class="form-group">
                                  <label >Search</label>
                                  <input type="text" ng-model="search" class="form-control" placeholder="Search">
                              </div>
                          </form>
                          <dir-pagination-controls
                              max-size="5"
                              direction-links="true"
                              boundary-links="true" >
                          </dir-pagination-controls>
                          <table class="table table-striped table-bordered responsive">
                            <tr>
                              <th ng-click="sort('type')">Transaction Type
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('transfer')">Transfer
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('fullname')">Transaction For
                                       <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                              </th>
                              <th ng-click="sort('amount')">Amount
                                   <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                             
                              </th>
                              <th ng-click="sort('created_at')">Date
                                   <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                             
                              </th>
                              
                            </tr>
                            
                            
                            <tr  dir-paginate="roll in adminHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                              <td>@{{roll.type}} </td>
                              <td><span ng-if="roll.transfer == 1">Yes</span><span ng-if="roll.transfer == 0">No</span> <span ng-if="roll.transfer == 2">Withdrawal</span></td>
                              <td>@{{roll.accounts.user.fullname}} </td>
                              <td>@{{roll.amount}} </td>
                              <td>@{{roll.created_at}}</td>
                              
                            </tr>
                           
                          </table>
                          <dir-pagination-controls
                              max-size="5"
                              direction-links="true"
                              boundary-links="true" class="pull-right">
                          </dir-pagination-controls>
                          <div class="row" style="margin-top:50px;">
                                <button class="btn btn-success col-md-4" ng-click="loadMoreDate()" ng-if="full">Load More Data</button>

                            </div>
                  </div>
                  <div class="alert alert-danger" ng-if="adminHistory.length == 0">
                      <center>
                          <h3>No Records Found</h3>
                      </center>
                  </div>
                </div>
                        
                <div class="col-lg-1"></div>
              </div>
              <div class="tab-pane fade animated bounceIn " id="adminHistory">
                <div class="col-lg-1"></div>
                  <div class="col-lg-10" ng-show="currentData.admin == 1">
                    <h4 class="text-center">All Admins Transaction History</h4>
          
                    <div class="col-lg-12">
                        <form role="form" class="form-horizontal addstudent"  ng-submit="getAdminWalletHistoryWithDate();" >
                            <div class="form-group">
                              <label for="scl" class="col-sm-3 control-label">Start Date </label>
                              <div class="col-sm-7">
                                   <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                              </div>
                             
                            </div>
                            <div class="form-group">
                                <label for="scl" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-7">
                                     <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                </div>
                                

                            </div>
                            <div class="form-group">
                              <label  class="col-sm-4 control-label">** Select User:</label>
                              <div class="col-sm-6">
                                <select ng-model="postData.user_id">
                                  <option ng-repeat="p in admin" value="@{{p.user_id}}">@{{p.username}}</option>
                                </select>
                             <!--  <select chosen
                                  options="admin"
                                  ng-model="postData.user_id"
                                  ng-options="p.user_id as p.username for p in admin" >
                              </select> -->
                              </div>
                              <div class="text-right">
                                <button class="btn btn-success" ng-if="!loading.history">submit</button>
                                <span class="btn btn-success" ng-if="loading.history"><i class="fa fa-spinner fa-spin"></i></span>
                              </div>
                            </div>
                            

                            
                        </form>
                        
                    </div>
                    
                    <center ng-if="adminWalletHistory.length == 0 && !loading.history" ><h3>No Records</h3></center>
                    <div class="table-responsive col-lg-12" ng-if="adminWalletHistory.length > 0">
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label >Search</label>
                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" >
                            </dir-pagination-controls>
                            <table class="table table-striped table-bordered responsive">
                              <tr>
                                <th ng-click="sort('type')">Transaction Type
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')">Transfer
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('fullname')">Recipient
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('fullname')">Transaction By
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('amount')">Amount
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                               
                                </th>
                                <th ng-click="sort('created_at')">Date
                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                               
                                </th>
                                
                              </tr>
                              
                              
                              <tr  dir-paginate="roll in adminWalletHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                <td>@{{roll.type}} </td>
                                <td><span ng-if="roll.transfer == 1">Yes</span><span ng-if="roll.transfer == 0">No</span> <span ng-if="roll.transfer == 2">Withdrawal</span> </td>
                                <td>@{{roll.recipient}} </td>
                                <td>@{{roll.users.fullname}} </td>
                                <td>@{{roll.amount}} </td>
                                <td>@{{roll.created_at}}</td>
                                
                              </tr>
                             
                            </table>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" class="pull-right">
                            </dir-pagination-controls>
                    </div>
                  </div>
                 <div class="col-lg-1"></div>
              </div>
            </div>
            
        </div>
    </div>
       
  </div>
</div>
@endsection
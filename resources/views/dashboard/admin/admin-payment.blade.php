@extends('layouts.dash')
@section('content')
 <div class="row" ng-controller="AdminInsuranceCtrl">
            <div class="col-lg-12">
              
                <div class="tabbable">
                   <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                        <li class="active"><a href="#rank-pills" data-toggle="tab">Generate Monthly Upgrades </a>
                        </li>
                         <li ><a href="#home-pills" data-toggle="tab">Prepare monthly payment sheet </a>
                        </li>
                         <li ><a href="#email-pills" data-toggle="tab">Prepare Premium monthly payment sheet</a>
                        </li>

                        
                    </ul> 
                </div>
            </div>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    @{{messages.success}}
                </div>
                <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <ul ng-repeat="x in messages.error">
                        <li>@{{x}}</li>
                    </ul> 
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                    	<div class="tab-content">
                            <div class="tab-pane fade in active animated bounceIn" id="rank-pills">
                                 <center ng-if="loadRank"><button class="btn btn-primary" ng-click="getRankPayment()">View Rank List</button></center>
                                    
                                <div class="col-lg-12" ng-if="!loadRank && paymentRankList.length > 0">
                                    <form class="form-inline pull-right">
                                        <div class="form-group">
                                            <label >Search</label>
                                            <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" >
                                    </dir-pagination-controls>
                                    <table class="table table-striped table-bordered responsive">
                                      <tr>
                                        <th ng-click="sort('firstname')">Name
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='firstname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                       
                                        <th ng-click="sort('username')">Username
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('phone')">Phone
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">Total Airtime
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                         <th ng-click="sort('amt')">PR1
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">PR2
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                         <th ng-click="sort('amt')">PR3
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">PR4
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                         <th ng-click="sort('amt')">PR5
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                         <th ng-click="sort('amt')">Current Rank
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>

                                      </tr>
                                      
                                      
                                      <tr  dir-paginate="roll in paymentRankList|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                        <td>@{{roll.fullname}}</td>
                                        <td>@{{roll.username}}</td>
                                        <td>@{{roll.phone}}</td>
                                        <td>@{{roll.total}}</td>
                                        <td>@{{roll.leg1}}</td>
                                        <td>@{{roll.leg2}}</td>
                                        <td>@{{roll.leg3}}</td>
                                        <td>@{{roll.leg4}}</td>
                                        <td>@{{roll.leg5}}</td>
                                        <td>@{{roll.current_rank}}</td>
                                      </tr>
                                     
                                    </table>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" class="pull-right">
                                    </dir-pagination-controls>
                                   
                                    <div class="row" style="margin-top:50px;">
                                          <button class="btn btn-success col-md-4" ng-click="loadRanksMore()" ng-if="fullRank">Load More Data</button>
                                          <button class="btn btn-primary col-md-4" ng-click="generateRanksExcel()" ng-if="!fullRank">Generate Rank List</button>
                                    </div>
                                </div>
                                <div class="col-lg-12" ng-if="!loadRank && paymentRankList.length == 0 && currentpage == lastpage">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          No User Qualified for ranks this time month
                                      </button>
                                    </center>
                                </div>
                                
                             </div>
                               <div class="tab-pane fade  animated bounceIn" id="home-pills">
                                 <center ng-if="load"><button class="btn btn-primary" ng-click="getPayment()">View Payment List</button></center>
                                    
                                <div class="col-lg-12" ng-if="!load && paymentList.length > 0">
                                    <form class="form-inline pull-right">
                                        <div class="form-group">
                                            <label >Search</label>
                                            <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" >
                                    </dir-pagination-controls>
                                    <table class="table table-striped table-bordered responsive">
                                      <tr>
                                        <th ng-click="sort('firstname')">Name
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='firstname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                       
                                        <th ng-click="sort('username')">Username
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('country')">Country
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='country'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('phone')">Phone Number
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                       
                                        <th ng-click="sort('carrier')">Network Carrier
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='carrier'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">Amount due
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                      </tr>
                                      
                                      
                                      <tr  dir-paginate="roll in paymentList|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                        <td>@{{roll.fullname}}</td>
                                        <td>@{{roll.username}}</td>
                                        <td>@{{roll.country}}</td>
                                        <td>@{{roll.phone}}</td>
                            
                                        <td>@{{roll.carrier}}</td>
                                        <td>@{{roll.total}}</td>
                                        
                                      </tr>
                                     
                                    </table>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" class="pull-right">
                                    </dir-pagination-controls>
                                   
                                    <div class="row" style="margin-top:50px;">
                                          <button class="btn btn-success col-md-4" ng-click="loadMore()" ng-if="full">Load More Data</button>
                                           <button class="btn btn-primary col-md-4" ng-click="generateXl()" ng-if="!full">Generate Payment List</button>
                                           <div class="col-md-2" ng-if="!full"></div>
                                           <button class="btn btn-danger col-md-4" ng-click="resetInsurance()" ng-if="!full">Reset &  Dispatch Payment</button>
                                    </div>
                                </div>
                                <div class="col-lg-12" ng-if="!load && paymentList.length == 0">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          No User Qualified for payment at this time month
                                      </button>
                                    </center>
                                </div>
                                
                             </div>
                            <div class="tab-pane fade animated bounceIn" id="email-pills">
                              <div class="row">
                               <center ng-if="loadPremium"><button class="btn btn-primary" ng-click="getPremiumPayment()">View Premium Payment List</button></center>
                                    
                               <div class="col-lg-12" ng-if="!loadPremium && paymentListPremium.length > 0">
                                    <form class="form-inline pull-right">
                                        <div class="form-group">
                                            <label >Search</label>
                                            <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" >
                                    </dir-pagination-controls>
                                    <table class="table table-striped table-bordered responsive">
                                      <tr>
                                        <th ng-click="sort('firstname')">Name
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='firstname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                       
                                        <th ng-click="sort('username')">Username
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('country')">Country
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='country'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('phone')">Phone Number
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                       
                                        <th ng-click="sort('carrier')">Network Carrier
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='carrier'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">Amount due
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                      </tr>
                                      
                                      
                                      <tr  dir-paginate="roll in paymentListPremium|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                        <td>@{{roll.fullname}}</td>
                                        <td>@{{roll.username}}</td>
                                        <td>@{{roll.country}}</td>
                                        <td>@{{roll.phone}}</td>
                                        <td>@{{roll.carrier}}</td>
                                        <td>@{{roll.total}}</td>
                                        
                                      </tr>
                                     
                                    </table>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" class="pull-right">
                                    </dir-pagination-controls>
                                   
                                    <div class="row" style="margin-top:50px;">
                                          <button class="btn btn-success col-md-4" ng-click="loadMorePremium()" ng-if="fullPremium">Load More Premium Data</button>
                                           <button class="btn btn-primary col-md-4" ng-click="generateXlPremium()" ng-if="!fullPremium">Generate Premium Payment List</button>
                                           <div class="col-md-2" ng-if="!fullPremium"></div>
                                           <button class="btn btn-danger col-md-4" ng-click="resetPremiumInsurance()" ng-if="!fullPremium">Reset &  Dispatch Premium Payment</button>
                                    </div>
                                </div>
                                <div class="col-lg-12" ng-if="!loadPremium && paymentListPremium.length == 0">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          No User Qualified for payment at this time month
                                      </button>
                                    </center>
                                </div>
                                
                              </div>
                              
                            </div>
					    </div>
                            
                            
                            
                        </div>
                        
                    </div>
                </div>
                <!-- <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ng-show="showModal">
                    <div class="modal-header">
                        <h1>Processing...</h1>
                    </div>
                    <div class="modal-body">
                        <div class="progress progress-striped active">
                            <div class="bar" style="width: 100%;"></div>
                        </div>
                    </div>
                </div> -->
            </div>


@endSection               
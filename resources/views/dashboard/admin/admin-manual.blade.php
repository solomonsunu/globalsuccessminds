@extends('layouts.dash')
@section('content')
<div class="row" >
  <div class="col-lg-12">
    <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active">
                <a href="#credit" data-toggle="tab">Pay Regular</a>
              </li>
              <li >
                <a href="#credit1" data-toggle="tab">Pay Premium</a>
              </li>
              <li >
                <a href="#credit11" data-toggle="tab">Upgrade Notificatio </a>
              </li>
              <li >
                <a href="#comm" data-toggle="tab">Pay Agent Com</a>
              </li>
              <li >
                <a href="#commDirect" data-toggle="tab">Pay Direct Com</a>
              </li>
                
              
          </ul> 
      </div>
  </div>
  <div class="col-lg-12">

    
      <div class="panel ">
        <div class="panel-body">
           <div class="tab-content">
              <div class="tab-pane fade in active animated bounceIn" id="credit">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Upload Payfile</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  method="post" action="{{url('dashboard/manual-pay')}}"  enctype='multipart/form-data'>
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">File:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="file" name = "import_file" >
                                </div>
                                
                              </div>
                            
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success">save</button>
                                     
                                  </div>
                              </div>
                                  
                              

                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>
             
              <div class="tab-pane fade  animated bounceIn" id="credit1">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Upload Premium Payfile</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  method="post" action="{{url('dashboard/manual-pay-premium')}}" enctype='multipart/form-data'>
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">File:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="file" name = "import_file1" >
                                </div>
                                
                              </div>
                            
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success">save</button>
                                     
                                  </div>
                              </div>
                                  
                              

                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>
              <div class="tab-pane fade  animated bounceIn" id="credit11">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Upload Upgrade List</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  method="post" action="{{url('dashboard/manual-notification')}}" enctype='multipart/form-data'>
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">File:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="file" name = "import_file2" >
                                </div>
                                
                              </div>
                            
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success">save</button>
                                     
                                  </div>
                              </div>
                                  
         
                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>

              <div class="tab-pane fade  animated bounceIn" id="comm">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Upload Agent Commision List</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  method="post" action="{{url('dashboard/manual-comission')}}" enctype='multipart/form-data'>
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">File:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="file" name = "import_file_comm" >
                                </div>
                                
                              </div>
                            
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success">save</button>
                                     
                                  </div>
                              </div>
                                  
         
                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>
               <div class="tab-pane fade  animated bounceIn" id="commDirect">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Upload User Direct Commision List</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  method="post" action="{{url('dashboard/manual-direct-comission')}}" enctype='multipart/form-data'>
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">File:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="file" name = "import_file_direct_comm" >
                                </div>
                                
                              </div>
                            
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success">save</button>
                                     
                                  </div>
                              </div>
                                  
         
                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>
                          
            </div>
            
        </div>
    </div>
       
  </div>
</div>
@endsection






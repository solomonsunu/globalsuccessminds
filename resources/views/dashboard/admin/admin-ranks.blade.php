@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="adminRankCtrl">
  <div class="col-lg-12">
    <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              <li class="active">
                <a href="#credit" data-toggle="tab">Create Ranks</a>
              </li>
              <li>
                <a href="#transaction" data-toggle="tab" ng-click="getRanks();">List Ranks</a>
              </li>
              <li>
                <a href="#rank" data-toggle="tab" ng-click="getUserRanks();">List Ranks Attained</a>
              </li>
             
                
              
          </ul> 
      </div>
  </div>
  <div class="col-lg-12">

      <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        @{{messages.success}}
      </div>
      <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        <ul ng-repeat="x in messages.error">
          <li>@{{x}}</li>
        </ul>  
      </div>
      <div class="panel ">
        <div class="panel-body">
           <div class="tab-content">
              <div class="tab-pane fade in active animated bounceIn" id="credit">
                  <div class="col-lg-1"></div>
                    <div class="col-lg-8">
                      <h4 class="text-center">Create User Rank</h4>
                      <div class="col-lg-12">
                          <form role="form" class="form-horizontal addstudent"  ng-submit="saveRank();" >
                              
                              <div class="form-group">
                                <label  class="col-sm-4 control-label">Rank Name:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="text" ng-model="postData.rank_name" >
                                </div>
                                
                              </div>
                               <div class="form-group">
                                <label  class="col-sm-4 control-label">Number of People Req:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="text" ng-model="postData.required_user_num" >
                                </div>
                                
                              </div>
                               <div class="form-group">
                                <label  class="col-sm-4 control-label">Total Airtime Purchase Req:</label>
                              
                                <div class="col-sm-4">
                                  <input class="form-control" type="text" ng-model="postData.required_airtime" >
                                </div>
                                
                              </div>
                             
                             
                              <div class="form-group">
                                 <div class="col-md-4"></div>
                                  <div class="col-md-4 pull-right">
                                      <button class="btn btn-success" ng-if="!loading.ranks">save</button>
                                      <span class="btn btn-success" ng-if="loading.ranks"><i class="fa fa-spinner fa-spin"></i></span>
                                  </div>
                              </div>
                                  
                              

                              
                          </form>
                          
                      </div>
                    </div>
                    
                  <div class="col-lg-1"></div>
                                  
              </div>
              <div class="tab-pane fade animated bounceIn " id="transaction">
                
                <div class="col-lg-12" >
                    <h4 class="text-center">Rank Information</h4>
                  
                 
                    
             
                    <div class="table-responsive col-lg-12" ng-if="ranks.length > 0">
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label >Search</label>
                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" >
                            </dir-pagination-controls>
                            <table class="table table-striped table-bordered responsive">
                              <tr>
                                <th>#</th>
                                <th ng-click="sort('type')">Name
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')"> Required Number of Users 
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')"> Required Total Airtime 
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                               
                                
                              </tr>
                              
                              
                              <tr  dir-paginate="roll in ranks|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                <td>@{{$index +1 }}</td>
                                <td>@{{roll.rank_name}} </td>
                               
                                <td>@{{roll.required_user_number}} </td>
                                <td>@{{roll.required_airtime}} </td>
                                
                              </tr>
                             
                            </table>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" class="pull-right">
                            </dir-pagination-controls>
                    </div>
                </div>
               
                      
              </div>
              <div class="tab-pane fade animated bounceIn " id="rank">
                
                <div class="col-lg-12" >
                    <h4 class="text-center">Ranks Attained Information</h4>
                
             
                    <div class="table-responsive col-lg-12" ng-if="user_ranks.length > 0">
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label >Search</label>
                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" >
                            </dir-pagination-controls>
                            <table class="table table-striped table-bordered responsive">
                              <tr>
                                <th>#</th>
                                <th ng-click="sort('type')">Name
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('type')">Username
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('type')">Phone
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')"> Rank Attained
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th ng-click="sort('transfer')"> Status
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                               
                                
                              </tr>
                              
                              
                              <tr  dir-paginate="roll in user_ranks|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                <td>@{{$index +1 }}</td>
                                <td>@{{roll.user_name}} </td>
                                <td>@{{roll.name}} </td>
                                <td>@{{roll.phone}} </td>
                               
                                <td>@{{roll.upgrade_name}} </td>
                                <td><a href="#" ng-click="payRank(roll)" class="btn btn-danger">Pay Ammount</a> </td>
                                
                              </tr>
                             
                            </table>
                            <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" class="pull-right">
                            </dir-pagination-controls>


                    </div>
                    <div class="alert alert-danger" ng-if="user_ranks.length == 0">
                              <center>
                                  <h3>No Records Found</h3>
                              </center>
                          </div>
                </div>
               
                      
              </div>
                          
            </div>
            
        </div>
    </div>
       
  </div>
</div>
@endsection






@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="adminRankCtrl">
      <div class="col-lg-12">
                  
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                            <li class="active"><a href="#home-pills" data-toggle="tab">Regular User Transfer </b></a>
                            </li>

                            <li ><a href="#top-pills" data-toggle="tab">Premium User Transfer </b></a>
                            </li>
                           
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="tab-content">
                                <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                   <div class="col-lg-12">
                                    <div class="container-fluid">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="regularTransfer();" >
                                                <div class="row">
                                                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                    </div>
                                                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                     <div class="col-md-5">
                                                          <label for="scl" class=" control-label">User Phone Number </label>
                                                          <div class="">
                                                               <input type="text" ng-model="postData.originPhone" id="scl" class="form-control" required>
                                                          </div>


                                                      </div>
                                                       <div class="col-md-5">
                                                          <label for="scl" class=" control-label">New Sponsor Phone Number</label>
                                                          <div class="">
                                                               <input type="text" ng-model="postData.destinationPhone" id="scl" class="form-control"  required>
                                                          </div>
                                                          

                                                      </div>
                                                      <div class="col-md-2">
                                                          <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                          <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                      </div>
                                                </div>
                                             
                                          </form>
                                    </div>
                                       
                                   </div>
                                   
                                    
                                    
                                    
                                 </div>
                                 <div class="tab-pane fade  animated bounceIn" id="top-pills">
                                   <div class="col-lg-12">
                                    <div class="container-fluid">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="premiumTransfer();" >
                                                <div class="row">
                                                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                    </div>
                                                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-5">
                                                          <label for="scl" class=" control-label">User Phone Number </label>
                                                          <div class="">
                                                               <input type="text" ng-model="postData.originPhonePremium" id="scl" class="form-control" required>
                                                          </div>


                                                      </div>
                                                       <div class="col-md-5">
                                                          <label for="scl" class=" control-label">New Sponsor Phone Number</label>
                                                          <div class="">
                                                               <input type="text" ng-model="postData.destinationPhonePremium" id="scl" class="form-control"  required>
                                                          </div>
                                                          

                                                      </div>
                                                      <div class="col-md-2">
                                                          <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                          <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                      </div>
                                                </div>
                                             
                                          </form>
                                    </div>
                                       
                                   </div>
                                   
                                    
                                    
                                 </div>
                               
						              </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
@endSection               
@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="AirtelUserMgtCtrl">
	 <div class="col-lg-12">
        <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
           		<li  class="active"><a href="#home-pills" data-toggle="tab">Active Users</a>
                </li>
               <!--  <li><a href="#profile-pills" data-toggle="tab">Activate New Users</a>
                </li> -->
                
                
                <li><a href="#settings-pills" data-toggle="tab">Update User Account</a>
                </li>
                <li><a href="#notification" data-toggle="tab">Notifications</a>
                </li>
            </ul> 
        </div>
    </div>
    <div class="col-lg-12">

    	<div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
    		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
    		@{{messages.success}}
    	</div>
    	<div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
    		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
    		<ul ng-repeat="x in messages.error">
    			<li>@{{x}}</li>
    		</ul>
    		
    		
    	</div>
        <div class="panel panel-flat">
            <div class="panel-body">
            	 <div class="tab-content">
                    <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                        <h3>Active Users </h3>
                        <p>Summary Of active User Information (AIRTEL)</p>
                         
			            
			            	<div class="table-responsive col-lg-12" ng-if="activeUser.length > 0">
			                    <form class="form-inline pull-right">
                                    <div class="form-group">
                                        <label >Search</label>
                                        <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                    </div>
                                </form>
                                 <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true" >
                                </dir-pagination-controls>
			                    <table class="table table-striped table-bordered responsive">
			                     
			                      <tr>
			                       <th ng-click="sort('fullname')">Name
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='fullname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                    </th>
                                    
                                    <th ng-click="sort('username')">Username
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                    <th ng-click="sort('current_balance')">Current Balance
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='current_balance'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                     <th ng-click="sort('email')">Email
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='email'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                    <th ng-click="sort('carrier')">Mobile Network
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='carrier'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                     <th ng-click="sort('phone')">Phone
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                     <th ng-click="sort('country')">Country
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='country'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
                                    <th ng-click="sort('created_at')">Registration Date
                                         <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                   
                                    </th>
			                    
			                       
			                      </tr>
			                      
			                      
			                      <tr  dir-paginate="roll in activeUser|orderBy:sortKey:reverse|filter:search|filter:{carrier:'airtel'}|itemsPerPage:10">
			                        <td>@{{roll.fullname}} </td>
			                        
			                        <td><a  ng-href="@{{'/users/'+roll.user_id}}">@{{roll.username}} </a> </td>
			                        <td>@{{roll.account.current_balance}}  </td>
			                        
			                        <td>@{{roll.email}}</td>
			                        <td>@{{roll.carrier}}</td>
			                        <td>@{{roll.phone}}</td>
			                        <td>@{{roll.country}}</td>
			                        <td>@{{roll.created_at}}</td>
			                      
			                      </tr>
			                     
			                    </table>
			                     <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true" class="pull-right">
                                </dir-pagination-controls>

                                 <div class="row" style="margin-top:50px;">
	                            	<button class="btn btn-success col-md-4" ng-click="loadMore()" ng-if="full">Load More Data</button>

	                            </div>
			                  </div>
			            
			        </div>
			        <!-- <div class="tab-pane fade " id="profile-pills">
                        <h3>New User Registration</h3>
                        <p>A list of users pending activation (AIRTEL)</p>
                         
			            
			            	<div class="table-responsive col-lg-12" ng-if="newUser.length > 0">
			                    <table class="table table-striped table-bordered responsive">
			                      <tr>
			                        <th>Name</th>
			                        <th>Ussername</th>
			                        <th>Email</th>
			                        <th>Mobile Network</th>
			                        <th>Phone #</th>
			                        <th>Country</th>
			                        <th>Registration Date</th>
			                       	
			                      </tr>
			                      
			                      
			                      <tr ng-repeat="roll in newUser|filter:{carrier:'airtel'}" >
			                        <td>@{{roll.lastname}} @{{roll.firstname}} </td>
			                        <td>@{{roll.username}}</td>
			                        <td>@{{roll.email}}</td>
			                        <td>@{{roll.carrier}}</td>
			                        <td>@{{roll.phone}}</td>
			                        <td>@{{roll.country}}</td>
			                        <td>@{{roll.created_at}}</td>
			                        
			                        <td><button class="btn btn-warning" ng-click="activate(roll.user_id)">activate</button></td>
			                       	<td><button title="The process is NOT reversible" class="btn btn-danger" ng-click="removeUser(roll.user_id)">delete</button></td>
			                       
			                      </tr>
			                     
			                    </table>
			                  </div>
			            
			        </div> -->
                    
                    
                    <div class="tab-pane fade animated bounceIn" id="settings-pills">
                        <div class="col-lg-1"></div>
				    		<div class="col-lg-8">
					    		<form role="form" class="form-horizontal addstudent" ng-submit="updateUser();" >
					    			
					    			<div class="form-group">
					    				<label for="scl" class="col-sm-3 control-label">First Name </label>
					    				<div class="col-sm-9">
					    					<input type="text" ng-model="currentData.user_id" id="scl" class="form-control" style="display: none">
					    					<input type="text" ng-model="currentData.firstname" id="scl" class="form-control">
					    				</div>

					    			</div>
					    		
					    			<div class="form-group">
					    				<label for="mn" class="col-sm-3 control-label">Last Name </label>
					    				<div class="col-sm-9">
					    					<input type="text" ng-model="currentData.lastname" id="md" class="form-control">
					    				</div>
					    			</div>
					    		
		
					    			<div class="form-group">
						    			<label for="email" class="col-sm-3 control-label">Email</label>
										<div class="col-sm-9">
						    				<input type="email" ng-model="currentData.email" id="email" class="form-control">
						    			</div>
					    			</div>
									<div class="form-group">
						    			<label for="carrier" class="col-sm-3 control-label">Network Carrier</label>
						    			<div class="col-sm-9">
	                                        <select name="carrier" class=" form-control">
	                                            <option value="mtn">MTN</option>
	                                            <option value="vodaphone">VODAPHONE</option>
	                                            <option value="airtel">Airtel</option>
	                                            <option value="tigo">Tigo</option>
	                                            <option value="glo">Glo</option>
                  								<option value="expresso">Expresso</option>	
	                                        </select>
	                                    </div>
                            		</div>
               
					    			<div class="form-group">
						    			<label for="phone" class="col-sm-3 control-label">Phone Number</label>
						    			<div class="col-sm-9">
						    				<input type="text" ng-model="currentData.phone" id="phone" class="form-control">
						    			</div>
					    			</div>
					    			<div class="form-group">
						    			<label for="phone" class="col-sm-3 control-label">Want to Change Password</label>
						    			<div class="col-sm-9">
						    				<label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="0">No</label>
						    				<label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="1">Yes</label>
						    			</div>
					    			</div>
					    			<div class="form-group" ng-if="currentData.choice==1">
						    			<label for="pass" class="col-sm-3 control-label">Change Password</label>
						    			<div class="col-sm-9">
						    				<input type="password" ng-model="currentData.password" id="pass" class="form-control">
						    			</div>
					    			</div>
					    			<div class="form-group" ng-if="currentData.choice==1">
						    			<label for="pass1" class="col-sm-3 control-label">Repeat New Password</label>
						    			<div class="col-sm-9">
						    				<input type="password" ng-model="currentData.password_confirmation" id="pass1" class="form-control">
						    			</div>
					    			</div>
					    			<div class="form-group">
						    			<label for="gender" class="col-sm-3 control-label">Gender</label>
						    			<div class="col-sm-9">
						    				<select name="gender" id="gender" ng-model='currentData.gender' class='form-control'>
						    					<option></option>
						    					<option value="male">Male</option>
						    					<option value="female">Female</option>
						    				</select>
						    			</div>
						    		</div>
									<div  class="form-group">	
										<label for="" class="col-sm-3 control-label">upload Image</label>
										<div class="col-md-9">
											<div class="img-container">
												<img src="{{ asset('img/profile.png') }}" class="img-responsive profilePic">
											</div>
											<div class="img-control">
												<!--<label for="fInput" class="btn btn-default btn-outline text-center">Select Image</label>-->
												<div id="upld"><input type="file" file-model="currentData.file" accept="image/*" id="fInput"></div>
											</div>
										</div>
									</div>
									<hr>
									
						    		<hr>
					    			<div class="p-md">
					    				<button class="btn btn-success" ng-if="!loading.user">submit changes</button>
					    				<span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
					    			</div>
					    		</form>
					    	</div>
				    		<div class="col-lg-1"></div>
                    </div>
                    <div class="tab-pane fade " id="notification">
                        <ul class="nav nav-pills">
                            <li class="active"><a href="#unread" data-toggle="tab">Unread Notifications</a>
                            </li>
                            <li><a href="#read" data-toggle="tab">Read Notifications</a>
                            </li>
                        </ul> 
                        <div class="tab-content">
                            <div class="tab-pane fade  in active panel-body" id="unread">
                                <div class="list-group" ng-if="unRead.length > 0">
                                    <div class="list-group-item" ng-repeat="x in unRead">
                                        <i class="fa fa-comment fa-fw"></i>
                                        <b style="margin-right: 10px;">@{{x.title  }}</b>
                                        <span>@{{x.detail}}</span>
                                        <button title="Mark as Read" type="button" class="pull-right btn btn-primary btn-circle" ng-click="markRead(x.notification_id)"><i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                                <h4 ng-if="unRead.length == 0">Unread notification list empty</h4>   
                                
                            </div>
                            <div class="tab-pane fade panel-body" id="read">
                                
                                <div class="list-group" ng-if="read.length > 0">
                                    <div class="list-group-item" ng-repeat="x in read">
                                        <i class="fa fa-comment fa-fw"></i>
                                        <b style="margin-right: 10px;">@{{x.title  }}</b>
                                        <span>@{{x.detail}}</span>
                                    </div>
                                </div>
                                <h4 ng-if="read.length == 0">Read notification list empty</h4>   
                            </div>
                        </div>
			        </div>
                </div>
                
            </div>
        </div>
    </div>
     </div>
@endSection               
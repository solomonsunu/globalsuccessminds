@extends('layouts.dash')
@section('content')

	 <div class="row" ng-controller="InsuranceCtrl">
        <div class="col-lg-12">
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                            <li class="active"><a href="#register-insurance" data-toggle="tab">Manage Admin Access Level</a>
                            </li>
                           
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                  <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        @{{messages.success}}
                    </div>
                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <ul ng-repeat="x in messages.error">
                            <li>@{{x}}</li>
                        </ul>
                        
                        
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	 <div class="tab-content">
                                <div class="tab-pane fade in active animated zoomIn" id="register-insurance">
                                    <h3>Assign Admin Role</h3>
                                         
                                        <div class="table-responsive col-lg-12" ng-if="adminList.length > 0">
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('firstname')">Firstname
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='firstname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('lastname')">Lastname
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='lastname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th ng-click="sort('username')">Username
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th>Current Role
                                                     <span class="glyphicon sort-icon" ></span>
                                               
                                                </th>
                                                <th>Change Access Level
                                                     <span class="glyphicon sort-icon" ></span>
                                               
                                                </th>
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in adminList|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{roll.firstname}} </td>
                                                <td>@{{roll.lastname}} </td>
                                                <td>@{{roll.username}}</td>
                                                <td ng-if="roll.admin == 1"><span class="label label-lg label-danger">GSMadmin</span></td>
                                               
                                                <td ng-if="roll.admin == 2" ><span class="label label-lg label-warning" >GSM 1</span></td>
                                                <td ng-if="roll.admin == 3"><span class="label label-lg label-warning ">GSM 2</span></td>
                                               
                                                <td ng-if="roll.admin == 4"><span class="label label-lg label-warning" >GSM 3</span></td>
                                                <td ng-if="roll.admin == 5"><span class="label label-lg label-warning" >GSM 4</span></td>
                                                <td ng-if="roll.admin == 6"><span class="label label-lg label-warning" >GSM 5</span></td>
                                                <td ng-if="roll.admin == 7"><span class="label label-lg label-warning" >GSM 6</span></td>
                                                <td>
                                                  <select ng-model="selectedItem" ng-options="item as item.name for item in items">
                                                  </select>
                                                </td>
                                                <td>
                                                  <button class="btn btn-primary" ng-click="assignRole(selectedItem.id,roll.user_id)">Assign Role</button>
                                                </td>
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                          </div>
						                    </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>

               
      </div>
@endSection               
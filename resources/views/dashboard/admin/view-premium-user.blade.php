@extends('layouts.dash')
@section('content')
    <div ng-controller="ViewUserCtrl" class="view-stud">
		<div class="row ">
	        <div class="col-lg-10">
	            <h2 class="page-header">Premium User Profile</h2>
	        </div>
	        
	    </div>
        <div class="row">
    		<div class="col-lg-12">
    			<div class="ibox float-e-margins">
    		            <div class="ibox-content">
                             
                                    <div class="tab-body profile printme">
                                        <div class="row">
                                            <div class="col-lg-3" >
                                                <img ng-if="userPremiumInfo.profile_image != null" ng-src="@{{ userPremiumInfo.profile_image }}" class="img-responsive">
                                                 <img ng-if="userPremiumInfo.profile_image == null && userPremiumInfo.gender == 'male'" src="{{asset('uploads/man.png')}}" class="img-responsive">
                                                <img ng-if="userPremiumInfo.profile_image == null && userPremiumInfo.gender == 'female'" src="{{asset('uploads/girl.png')}}" class="img-responsive">
                                 
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                            @{{messages.success}}
                                                        </div>
                                                        <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                            <ul ng-repeat="x in messages.error">
                                                                <li>@{{x}}</li>
                                                            </ul>
                                                            
                                                            
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-user"></i> Personal Information
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th>First Name</th>
                                                                        <td>@{{ userPremiumInfo.firstname }}</td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th>Last Name</th>
                                                                        <td>@{{ userPremiumInfo.lastname }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Date of Birth</th>
                                                                        <td>@{{ userPremiumInfo.dob }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Phone number</th>
                                                                        <td>@{{ userPremiumInfo.phone }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Gender</th>
                                                                        <td>@{{ userPremiumInfo.gender }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Country</th>
                                                                        <td>@{{ userPremiumInfo.country }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email</th>
                                                                        <td>@{{ userPremiumInfo.email }}</td>
                                                                    </tr>

                                                                     <tr >
                                                                        <th><button ng-click="checkDirectRC();">Check Direct Referal</button></th>
                                                                        <td>
                                                                           <span ng-if="directRC">
                                                                               @{{directRC}}

                                                                           </span>                                                             </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-list"></i> premium_referrer Detail
                                                            </div>
                                                            <div class="panel-body">
                                                                    <a href="#"  title="click to view user info"  ng-click="open(premium_referrer)">
                                                                        <img ng-if="premium_referrer.profile_image != null " ng-src="@{{premium_referrer.profile_image}}" width="40" height="40">
                                                                         <img ng-if="premium_referrer.profile_image == null " src="{{asset('img/avatar.png')}}" width="80" height="80">
                                                   
                                                                        <span ng-if="premium_referrer.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                        <span ng-if="premium_referrer.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                        
                                                                    </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-list"></i> Bank Details
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th>Account Name</th>
                                                                        <td>@{{ userPremiumInfo.account.accountname }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Account Number</th>
                                                                        <td>@{{ userPremiumInfo.account.accountnumber }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Bank Name</th>
                                                                        <td>@{{ userPremiumInfo.account.bankname }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Bank Branch</th>
                                                                        <td>@{{ userPremiumInfo.account.branch }}</td>
                                                                    </tr>
                                                                    <tr></tr>
                                                                    <tr></tr>
                                                                    <tr></tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div  class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-list"></i> Premium Referal Details
                                                        
                                                    </div>
                                                    <!-- <center><button ng-click="getUser();" class="btn btn-danger">view referral tree</button></center> -->
                                                    <div class="panel-body" ng-if="premium_level1.length > 0">
                                                        <div class="list-group-item level">
                                                           <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                                           <span ng-if="premium_level1.length > 0">
                                                           <span  ng-repeat="x in premium_level1">
                                                                <a href="#"  title="click to view user info"  ng-click="open(x)">
                                                                    <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                     <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                                    <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                    <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                    
                                                                </a>
                                                               
                                                           </span>

                                                                <span class="pull-right">Total: @{{premium_t1}} </span>
                                                           </span>

                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                                            <span ng-if="premium_level2.length > 0">

                                                           
                                                              <span ng-repeat="x in premium_level2" class="bdf">
                                                                <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                    <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                    <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                    <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                    <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                    
                                                                </a>
                                                                
                                                            
                                                
                                                            </span>
                                                            <span class="pull-right">Total: @{{premium_t2}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                                            <span ng-if="premium_level3.length > 0">
                                                               
                                                           <span ng-repeat="x in premium_level3">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                           
                                                          </span>
                                                               
                                                                <span class="pull-right">Total: @{{premium_t3}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 4 </span><img src="{{asset('images/gold.png')}}" width="40" height="40">
                                                            <span ng-if="premium_level4.length > 0">
                                                                
                                                           <span ng-repeat="x in premium_level4">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                            
                                                          </span>
                                                            
                                                              
                                                                <span class="pull-right">Total: @{{premium_t4}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level" >
                                                            <span>Level 5 </span><img src="{{asset('images/diamond.jpg')}}" width="40" height="40">
                                                            <span ng-if="premium_level5.length > 0">
                                                                
                                                                    <span ng-repeat="x in premium_level5">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                              
                                                            </a>
                                                            
                                                          </span>    
                                                        
                                                              
                                                                <span class="pull-right">Total: @{{premium_t5}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level" >
                                                            <span>Level 6 </span><img src="{{asset('images/platinum.jpg')}}" width="40" height="40">
                                                            <span ng-if="premium_level6.length > 0">
                                                                
                                                                    <span ng-repeat="x in premium_level6">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                           
                                                          </span>    
                                                        
                                                               
                                                                <span class="pull-right">Total: @{{premium_t6}} </span>
                                                            </span>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
    		            </div>
    		    </div>
    		</div>
	    </div>
	   
    </div>
   
@endSection  
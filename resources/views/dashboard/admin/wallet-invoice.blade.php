@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="adminRankCtrl">
      <div class="col-lg-12">
                  
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                            <li class="active"><a href="#home-pills" data-toggle="tab">Self Load Wallet Service Invoice Checker </b></a>
                            </li>
                             <li ><a href="#app-pills" data-toggle="tab">Self Load Wallet Service Invoice Checker (GT)</b></a>
                            </li>
                           
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="tab-content">
                                <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                   <div class="col-lg-12">
                                    <div class="container-fluid">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="searchInvoice();" >
                                                <div class="row">
                                                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                    </div>
                                                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                     <div class="col-md-5">
                                                          <label for="scl" class=" control-label">Start Date </label>
                                                          <div class="">
                                                               <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                                                          </div>


                                                      </div>
                                                       <div class="col-md-5">
                                                          <label for="scl" class=" control-label">End Date</label>
                                                          <div class="">
                                                               <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                                          </div>
                                                          

                                                      </div>
                                                      <div class="col-md-2">
                                                          <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                          <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                      </div>
                                                </div>
                                             
                                          </form>
                                    </div>
                                       
                                   </div>
                                   <div class="table-responsive col-lg-12" ng-if="invoiceHistory.length > 0" style="margin-top: 20px;">
                                            <div class="btn btn-primary pull-left">
                                                <h6>Total Sum Received : @{{invoiceHistorySum}}</h6>
                                            </div>
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('type')">#
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('invoiceNo')">Invoice Number
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='invoiceNo'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('transfer')">Amount
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('fullname')">User
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('fullname')">Phone
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('fullname')">Transaction Code
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('amount')">Transaction Response
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                               
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in invoiceHistory|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{$index + 1}} </td>
                                                <td>@{{roll.invoiceNo}}</td>
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.user.firstname}} @{{roll.user.lastname}} </td>
                                                <td>@{{roll.user.phone}}  </td>
                                                <td>@{{roll.responseCode}} </td>
                                                <td>@{{roll.responseMessage}} </td>
                                                
                                                <td>@{{roll.created_at|date:fullDate}}</td>
                                                
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <div ng-if="invoiceHistory.length == 0" style="margin-top: 30px;" class="col-lg-12">
                                        <center><h4>No Records Available (Pick a date range)</h4></center>
                                    </div>
                                    
                                    
                                 </div>
                                 <div class="tab-pane fade  animated bounceIn" id="app-pills">
                                   <div class="col-lg-12">
                                    <div class="container-fluid">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="searchInvoice2();" >
                                                <div class="row">
                                                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      @{{messages.success}}
                                                    </div>
                                                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                      <ul ng-repeat="x in messages.error">
                                                          <li>@{{x}}</li>
                                                      </ul>
                                                      
                                                      
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                     <div class="col-md-5">
                                                          <label for="scl" class=" control-label">Start Date </label>
                                                          <div class="">
                                                               <input type="date" ng-model="postData.start_date" id="scl" class="form-control" required>
                                                          </div>


                                                      </div>
                                                       <div class="col-md-5">
                                                          <label for="scl" class=" control-label">End Date</label>
                                                          <div class="">
                                                               <input type="date" ng-model="postData.end_date" id="scl" class="form-control"  required>
                                                          </div>
                                                          

                                                      </div>
                                                      <div class="col-md-2">
                                                          <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                          <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                      </div>
                                                </div>
                                             
                                          </form>
                                    </div>
                                       
                                   </div>
                                   <div class="table-responsive col-lg-12" ng-if="invoiceHistory2.length > 0" style="margin-top: 20px;">
                                            <div class="btn btn-primary pull-left">
                                                <h6>Total Sum Received : @{{invoiceHistorySum2}}</h6>
                                            </div>
                                            <form class="form-inline pull-right">
                                                <div class="form-group">
                                                    <label >Search</label>
                                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                                </div>
                                            </form>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" >
                                            </dir-pagination-controls>
                                            <table class="table table-striped table-bordered responsive">
                                              <tr>
                                                <th ng-click="sort('type')">#
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('clientRef')">Reference Number
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='invoiceNo'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('transfer')">Amount
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('senderName')">Sender
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('senderPhone')">Phone(Recipient)
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                 <th ng-click="sort('mobileNumber')">Mobile Money Number
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('status')">Status
                                                         <span class="glyphicon sort-icon" ng-show="sortKey=='type'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                </th>
                                                <th ng-click="sort('amount')">Message
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='amount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                               
                                                <th ng-click="sort('created_at')">Date
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='created_at'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                               
                                                </th>
                                                <th>Action</th>
                                                
                                              </tr>
                                              
                                              
                                              <tr  dir-paginate="roll in invoiceHistory2|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                                <td>@{{$index + 1}} </td>
                                                <td>@{{roll.clientRef}}</td>
                                                <td>@{{roll.amount}} </td>
                                                <td>@{{roll.senderName}} </td>
                                                <td>@{{roll.senderPhone}}  </td>
                                                <td>@{{roll.mobileNumber}}  </td>

                                                <td>@{{roll.status}} </td>
                                                <td>@{{roll.Message}} </td>
                                                
                                                <td>@{{roll.created_at|date:fullDate}}</td>
                                                <td><button class="btn btn-danger" ng-click="removeInvoice(roll.clientRef);">Delete</button></td>
                                              </tr>
                                             
                                            </table>
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true" class="pull-right">
                                            </dir-pagination-controls>
                                    </div>
                                    <div ng-if="invoiceHistory2.length == 0" style="margin-top: 30px;" class="col-lg-12">
                                        <center><h4>No Records Available (Pick a date range)</h4></center>
                                    </div>
                                    
                                    
                                 </div>
                               
						    </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
@endSection               
@extends('layouts.dash')
@section('content')
	 <div class="row" ng-controller="adminRankCtrl">
      <div class="col-lg-12">
                  
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                            <li class="active"><a href="#home-pills" data-toggle="tab">Premium User Upgrade </a>
                            </li>
                           
                            
                        </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="tab-content">
                                <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                   <div class="col-lg-6">
                                    <div class="container-fluid">
                                        <div class="jumbotron">
                                           <h3>Instructions for Upgrade</h3>
                                           <ol>
                                               <li>Choose your premium sponsor (The person whose premium tree you would like to join)</li>

                                                <li>Ensure you have a minimum of 50 cedis in your GSM wallet</li>

                                                 <li>Your GSM wallet would be charged 45 cedis upon successful upgrade</li>
                                           </ol>
                                           
                                       </div>
                                    </div>
                                       
                                   </div>
                                    <div class="col-lg-6">
                                        <form role="form" class="form-horizontal addstudent"  ng-submit="saveUpgrade();" >
                                            <h2>
                                                Perform Account Upgrade Here
                                            </h2>
                                              <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                    @{{messages.success}}
                                                </div>
                                                <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                    <ul ng-repeat="x in messages.error">
                                                        <li>@{{x}}</li>
                                                    </ul>
                                                    
                                                    
                                                </div>
                                            <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Username of Referrer </label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.username" id="scl" class="form-control">
                                                </div>


                                            </div>
                                             <div class="form-group">
                                                <label for="scl" class="col-sm-5 control-label">Wallet Balance</label>
                                                <div class="col-sm-7">
                                                     <input type="text" ng-model="postData.wbalance" id="scl" class="form-control" readonly>
                                                </div>
                                                

                                            </div>
                                            <div class="p-md">
                                                <button class="btn btn-success pull-right" ng-if="!loading.user">submit</button>
                                                <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                            </div>
                                        </form>
                                        
                                    </div>
                                    
                                 </div>
                               
						    </div>
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
@endSection               
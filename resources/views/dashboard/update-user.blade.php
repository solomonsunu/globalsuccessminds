@extends('layouts.dash')
@section('content')

	 <div class="row" ng-controller="AppCtrl">
     <div class="col-lg-12">
                    <div class="tabbable">
                       <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                         
                       
                             <li class="active"><a href="#accounts" data-toggle="tab">Update User Account</a>
                            </li>
                          
                      </ul> 
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        @{{messages.success}}
                    </div>
                    <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <ul ng-repeat="x in messages.error">
                            <li>@{{x}}</li>
                        </ul>
                        
                        
                    </div>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                        	 <div class="tab-content">
                        
                      
                                <div class="tab-pane fade in active animated bounceIn" id="accounts">
                                     <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <form role="form" class="form-horizontal addstudent" ng-submit="updateUser();" >
                                                
                                                <div class="form-group">
                                                    <label for="scl" class="col-sm-3 control-label">First Name </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="currentData.user_id" id="scl" class="form-control" style="display: none">
                                                        <input type="text" ng-model="currentData.firstname" id="scl" class="form-control">
                                                    </div>

                                                </div>
                                            
                                                <div class="form-group">
                                                    <label for="mn" class="col-sm-3 control-label">Last Name </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" ng-model="currentData.lastname" id="md" class="form-control">
                                                    </div>
                                                </div>
                                              
                    
                                                <div class="form-group">
                                                    <label for="email" class="col-sm-3 control-label">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" ng-model="currentData.email" id="email" class="form-control">
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label for="carrier" class="col-sm-3 control-label">Network Carrier</label>
                                                    <div class="col-sm-9">
                                                        <select ng-model="currentData.carrier" class=" form-control">
                                                            <option value="mtn">MTN</option>
                                                            <option value="vodafone">VODAFONE</option>
                                                            <option value="airtel">Airtel</option>
                                                            <option value="tigo">Tigo</option>
                                                            <option value="glo">Glo</option>
                                                            <option value="expresso">Expresso</option>

                                                        </select>
                                                    </div>
                                                </div> -->
                           
                                                <!-- <div class="form-group">
                                                  <label for="phone" class="col-sm-3 control-label">Want to Change Phone Number</label>
                                                  <div class="col-sm-9">
                                                    <label class="radio-inline"><input type="radio" ng-model="currentData.choicePhone" value="0">No</label>
                                                    <label class="radio-inline"><input type="radio" ng-model="currentData.choicePhone" value="1">Yes</label>
                                                  </div>
                                                </div> -->
                                                <div class="form-group"  >
                                                  <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                                                  <div class="col-sm-9">
                                                    <input type="text" ng-model="currentData.phone" id="phone" class="form-control" readonly>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone" class="col-sm-3 control-label">Want to Change Password</label>
                                                    <div class="col-sm-9">
                                                        <label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="0">No</label>
                                                        <label class="radio-inline"><input type="radio" ng-model="currentData.choice" value="1">Yes</label>
                                                    </div>
                                                </div>
                                                <div class="form-group" ng-if="currentData.choice==1">
                                                    <label for="pass" class="col-sm-3 control-label">Change Password</label>
                                                    <div class="col-sm-9">
                                                        <input type="password" ng-model="currentData.password" id="pass" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group" ng-if="currentData.choice==1">
                                                    <label for="pass1" class="col-sm-3 control-label">Repeat New Password</label>
                                                    <div class="col-sm-9">
                                                        <input type="password" ng-model="currentData.password_confirmation" id="pass1" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="gender" class="col-sm-3 control-label">Gender</label>
                                                    <div class="col-sm-9">
                                                        <select name="gender" id="gender" ng-model='currentData.gender' class='form-control'>
                                                            <option></option>
                                                            <option value="male">Male</option>
                                                            <option value="female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div  class="form-group">   
                                                    <label for="" class="col-sm-3 control-label">upload Image</label>
                                                    <div class="col-md-9">
                                                        <div class="img-container">
                                                            <img src="{{ asset('img/profile.png') }}" class="img-responsive profilePic" height="150" width="150">
                                                        </div>
                                                        <div class="img-control">
                                                            <!--<label for="fInput" class="btn btn-default btn-outline text-center">Select Image</label>-->
                                                            <div id="upld"><input type="file" file-model="currentData.file" accept="image/*" id="fInput"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                
                                                <hr>
                                                <div class="p-md">
                                                    <button class="btn btn-success" ng-if="!loading.user">submit changes</button>
                                                    <span class="btn btn-success" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
     </div>
     
@endSection               
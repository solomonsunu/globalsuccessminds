@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="UserRanksMgtCtrl">
  <div class="col-lg-12">
    <div class="tabbable">
           <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
              
	            <li><a href="#profile-pills" data-toggle="tab" >New Premium User Upgrades (solo)</a>
	            </li>
	           
              
          </ul> 
      </div>
  </div>
  <div class="col-lg-12">

      <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        @{{messages.success}}
      </div>
      <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        <ul ng-repeat="x in messages.error">
          <li>@{{x}}</li>
        </ul>  
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
           <div class="tab-content">
					        <div class="tab-pane fade in active animated bounceIn" id="profile-pills">
                     <div class="col-lg-12" >
                      <h4 class="text-center">List Of Premium User Upgrades for Current Month</h4>
                    
	                            <center ng-show="!premiumUser">
                               <button class="btn btn-primary" ng-click="getPremiumUserListFix();">View List</button> 
                              </center>
					            
				            	<div class="table-responsive col-lg-12" ng-if="premiumUser.length > 0" style="height:500px;">
				                    <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label >Search</label>
                                    <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                             <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" >
                            </dir-pagination-controls>
				                    <table class="table table-striped table-bordered responsive" >
				                     <thead>
				                     	 <tr>
					                       <th ng-click="sort('fullname')">Name
                                                     <span class="glyphicon sort-icon" ng-show="sortKey=='fullname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                            </th>
                                            
                                            <th ng-click="sort('username')">Username
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                           
                                            </th>
                                          
                                             <th ng-click="sort('email')">Email
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='email'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                           
                                            </th>
                                            <th ng-click="sort('phone')">Phone
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                           
                                            </th>
                                            
                                             <th ng-click="sort('rank_name')">Rank
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='rank'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                           
                                            </th>
                                  
					                      </tr>
				                     </thead>
				                     <tbody>
					                     <tr  dir-paginate="roll in premiumUser|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
					                        <td>@{{roll.fullname}} </td>
					                        
					                        <td><a  ng-href="@{{'/premium-users/'+roll.user_id}}">@{{roll.username}}</a>  </td>
					                        
					                        <td>@{{roll.email}}</td>
					                        <td>@{{roll.phone}}</td>
					                        <td>@{{roll.ranks.rank_name}}</td>
					                        
					                      
					                      </tr>
				                     </tbody>
				                     
				                    </table>
				                    <dir-pagination-controls
                                max-size="5"
                                direction-links="true"
                                boundary-links="true" class="pull-right">
                            </dir-pagination-controls>
                            <div class="row" style="margin-top:50px;margin-bottom:50px;">
                            	<button class="btn btn-success col-md-4" ng-click="loadMorePremiumList()" ng-if="fullPremium">Load More Data</button>

                            </div>
                      </div>
                    	<div ng-if="premiumUser.length == 0">
  						            <div class="alert alert-danger alert-styled-right alert-arrow-right alert-bordered ">
  						                <center> No Data Found </center>
  						            </div>
  						        </div>
                    </div>
					            
					        </div>
				   
            </div>
            
        </div>
    </div>
       
  </div>
</div>
@endsection






@extends('layouts.dash')
@section('content')
    <div ng-controller="ViewUserCtrl" class="view-stud">
		<div class="row ">
	        <div class="col-lg-10">
	            <h2 class="page-header">User Profile</h2>
	        </div>
	        
	    </div>
        <div class="row">
    		<div class="col-lg-12">
    			<div class="ibox float-e-margins">
    		            <div class="ibox-content">
                             
                                    <div class="tab-body profile printme">
                                        <div class="row">
                                            <div class="col-lg-3" >
                                                <img ng-if="userInfo.profile_image != null" ng-src="@{{ userInfo.profile_image }}" class="img-responsive">
                                                 <img ng-if="userInfo.profile_image == null && userInfo.gender == 'male'" src="{{asset('uploads/man.png')}}" class="img-responsive">
                                                <img ng-if="userInfo.profile_image == null && userInfo.gender == 'female'" src="{{asset('uploads/girl.png')}}" class="img-responsive">
                                 
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                            @{{messages.success}}
                                                        </div>
                                                        <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                                            <ul ng-repeat="x in messages.error">
                                                                <li>@{{x}}</li>
                                                            </ul>
                                                            
                                                            
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-user"></i> Personal Information
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th>First Name</th>
                                                                        <td>@{{ userInfo.firstname }}</td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th>Last Name</th>
                                                                        <td>@{{ userInfo.lastname }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Date of Birth</th>
                                                                        <td>@{{ userInfo.dob }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Phone number</th>
                                                                        <td>@{{ userInfo.phone }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Gender</th>
                                                                        <td>@{{ userInfo.gender }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Country</th>
                                                                        <td>@{{ userInfo.country }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email</th>
                                                                        <td>@{{ userInfo.email }}</td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-list"></i> Referrer Detail
                                                            </div>
                                                            <div class="panel-body">
                                                                    <a href="#"  title="click to view user info"  ng-click="open(referrer)">
                                                                        <img ng-if="referrer.profile_image != null " ng-src="@{{referrer.profile_image}}" width="40" height="40">
                                                                         <img ng-if="referrer.profile_image == null " src="{{asset('img/avatar.png')}}" width="80" height="80">
                                                   
                                                                        <span ng-if="referrer.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                        <span ng-if="referrer.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                        
                                                                    </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-list"></i> Account Operations
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table">
                                                                    @if(Auth::user()->admin == 1)
                                                                    <tr >
                                                                        <th>Reset Password</th>
                                                                        <td><a href="#" ng-if="!loading.password" class="btn btn-danger" ng-click="resetPassword(userInfo.user_id)">Reset Password</a>
                                                                        <span class="btn btn-success" ng-if="loading.password"><i class="fa fa-spinner fa-spin"></i></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr >
                                                                        <th>Change Username</th>
                                                                        <td>
                                                                            <form  class="form-horizontal"   >
                                                                                 <div class="form-group">
                                                                                    <input type="text" ng-model="postData.username"  class="form-control" placeholder="New Username" required>
                                                                                </div>
                                                                                <div class="p-md">
                                                                                    <button class="btn btn-success pull-right" ng-if="!loading.user" type="submit" ng-click="changeUsername(userInfo.user_id)">submit</button>
                                                                                    <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span>
                                                                                </div>
                                                                                <!-- lol -->
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                     <tr >
                                                                        <th>Update Premium Bonus</th>
                                                                        <td>
                                                                            <form  class="form-horizontal"   >
                                                                                 <div class="form-group">
                                                                                    <input type="text" ng-model="postData.newBonus"  class="form-control" placeholder="New Bonus" required>
                                                                                </div>
                                                                                <div class="p-md">
                                                                                    <button class="btn btn-success pull-right" ng-if="!loading.bonus" type="submit" ng-click="changePremiumBonus(userInfo.user_id)">submit</button>
                                                                                    <span class="btn btn-success pull-right" ng-if="loading.bonus"><i class="fa fa-spinner fa-spin"></i></span>
                                                                                </div>
                                                                                <!-- lol -->
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    @endif
                                                                    <tr >
                                                                        <th>Update Phone Number</th>
                                                                        <td>
                                                                            <form  class="form-horizontal"   >
                                                                                 <div class="form-group">
                                                                                    <input type="text" ng-model="postData.phone"  class="form-control" placeholder="Phone Number" required>
                                                                                </div>
                                                                                <div class="p-md">
                                                                                    <button class="btn btn-success pull-right" ng-if="!loading.phone" type="submit" ng-click="updatePhone(userInfo.user_id)">submit</button>
                                                                                    <span class="btn btn-success pull-right" ng-if="loading.phone"><i class="fa fa-spinner fa-spin"></i></span>
                                                                                </div>
                                                                                <!-- lol -->
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div  class="row" >
                                            <div class="col-md-12 col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-list"></i> Referal Details
                                                        
                                                    </div>
                                                    <center><button ng-click="getUserRefferal();" class="btn btn-danger">view referral tree</button></center>
                                                    <div class="panel-body" ng-if="level1.length > 0">
                                                        <div class="list-group-item level">
                                                           <span>Level 1 </span><img src="{{asset('images/bronze.jpg')}}" width="40" height="40">
                                                           <span ng-if="level1.length > 0">
                                                           <span  ng-repeat="x in level1">
                                                                <a href="#"  title="click to view user info"  ng-click="open(x)">
                                                                    <img ng-if="x.profile_image != null " ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                     <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                               
                                                                    <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                    <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                    
                                                                </a>
                                                               
                                                           </span>

                                                                <span class="pull-right">Total: @{{t1}} </span>
                                                           </span>

                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 2 </span><img src="{{asset('images/silver.jpg')}}" width="40" height="40">
                                                            <span ng-if="level2.length > 0">

                                                           
                                                              <span ng-repeat="x in level2" class="bdf">
                                                                <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                    <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                    <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                    <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                    <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                                    
                                                                </a>
                                                                
                                                            
                                                
                                                            </span>
                                                            <span class="pull-right">Total: @{{t2}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 3 </span><img src="{{asset('images/peal.png')}}" width="40" height="40">
                                                            <span ng-if="level3.length > 0">
                                                               
                                                           <span ng-repeat="x in level3">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                           
                                                          </span>
                                                               
                                                                <span class="pull-right">Total: @{{t3}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level">
                                                            <span>Level 4 </span><img src="{{asset('images/gold.png')}}" width="40" height="40">
                                                            <span ng-if="level4.length > 0">
                                                                
                                                           <span ng-repeat="x in level4">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                            
                                                          </span>
                                                            
                                                              
                                                                <span class="pull-right">Total: @{{t4}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level" >
                                                            <span>Level 5 </span><img src="{{asset('images/diamond.jpg')}}" width="40" height="40">
                                                            <span ng-if="level5.length > 0">
                                                                
                                                                    <span ng-repeat="x in level5">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                 <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                              
                                                            </a>
                                                            
                                                          </span>    
                                                        
                                                              
                                                                <span class="pull-right">Total: @{{t5}} </span>
                                                            </span>
                                                        </div>
                                                        <div class="list-group-item level" >
                                                            <span>Level 6 </span><img src="{{asset('images/platinum.jpg')}}" width="40" height="40">
                                                            <span ng-if="level6.length > 0">
                                                                
                                                                    <span ng-repeat="x in level6">
                                                            <a  href="#"  title="click to view user info"  ng-click="open(x)">
                                                                <img ng-if='x.profile_image != null' ng-src="@{{x.profile_image}}" width="40" height="40">
                                                                <img ng-if="x.profile_image == null " src="{{asset('img/avatar.png')}}" width="40" height="40">
                                                   
                                                                <span ng-if="x.paymentstatus == 0" class="status-mark border-danger position-top "></span>
                                                                <span ng-if="x.paymentstatus == 1" class="status-mark border-success position-top "></span>
                                                            </a>
                                                           
                                                          </span>    
                                                        
                                                               
                                                                <span class="pull-right">Total: @{{t6}} </span>
                                                            </span>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
    		            </div>
    		    </div>
    		</div>
	    </div>
	   
    </div>
   
@endSection  
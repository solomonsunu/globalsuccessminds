@extends('layouts.dash')
@section('content')
   <div class="" id="app-pills">
   <div class="col-lg-6">
    <div class="container-fluid">
        <div class="jumbotron">
           <h3>Payment Confirmation </h3>
           <ol>
               <li>Please confirm the GSM number and click on submit to proceed to next level</li>
                 
           </ol>
           
       </div>
    </div>
       
   </div>
    <div class="col-lg-6">

        
        <form  class="form-horizontal addstudent"  action="https://myghpay.com/myghpayclient/" method="post" >
            <h2>
                Perform Wallet Load
            </h2>
              
            <div class="form-group">
                <label for="scl" class="col-sm-5 control-label">Amount </label>
                <div class="col-sm-7">
                     <input type="text" name="amount" id="scl" class="form-control" placeholder="eg. 2.00 (Amount must be in this format)" value="{{Session::get('data')['amount']}}" readonly>
                </div>


            </div>
            
             <div class="form-group">
                <label for="scl" class="col-sm-5 control-label">GSM Recipient Account Phone # </label>
                <div class="col-sm-7">
                     <input type="text" name="gsm_phone" id="scl" value="{{Session::get('data')['phone']}}" class="form-control" readonly>
                </div>
                

            </div>
             <input type="hidden" class="form-control" name="clientid" id="inputAmount" name="description" value="{{Session::get('data')['clientid']}}">
            <input type="hidden" class="form-control" id="inputMerchantId" name="clientsecret" value="{{Session::get('data')['clientsecret']}}">
            <input type="hidden" class="form-control" id="inputItemCode" name="itemname" value="{{Session::get('data')['itemname']}}">
            <input type="hidden" class="form-control" id="inputItemCode" name="clientref" value="{{Session::get('data')['clientref']}}">
            
            <input type="hidden" name="returnurl" value="{{Session::get('data')['returnurl']}}" />
            <div class="p-md">
                <button class="btn btn-success pull-right" " type="submit">Save & Continue</button>
                <!-- <span class="btn btn-success pull-right" ng-if="loading.user"><i class="fa fa-spinner fa-spin"></i></span> -->
            </div>
        </form>

         
    </div>
    
 </div>
                               
@endSection               
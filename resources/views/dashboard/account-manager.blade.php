@extends('layouts.dash')
@section('content')
<div class="row" ng-controller="AdminInsuranceCtrl">
            <div class="col-lg-12">
              
                <div class="tabbable">
                   <ul class="nav nav-tabs nav-tabs-highlight nav-tabs-top top-divided nav-justified">
                        <li class="active"><a href="#home-pills" data-toggle="tab">Prepare monthly payment sheet </a>
                        </li>
                         <li ><a href="#email-pills" data-toggle="tab">Reset All Payment Status</a>
                        </li>
                        
                    </ul> 
                </div>
            </div>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissable" ng-if="messages.success.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    @{{messages.success}}
                </div>
                <div class="alert alert-danger alert-dismissable" ng-if="messages.error.length > 0">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <ul ng-repeat="x in messages.error">
                        <li>@{{x}}</li>
                    </ul> 
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="tab-content">
                            <div class="tab-pane fade in active animated bounceIn" id="home-pills">
                                 <center ng-if="load"><button class="btn btn-primary" ng-click="getPayment()">View Payment List</button></center>
                                    
                                <div class="col-lg-12" ng-if="!load && paymentList.length > 0">
                                    <form class="form-inline pull-right">
                                        <div class="form-group">
                                            <label >Search</label>
                                            <input type="text" ng-model="search" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" >
                                    </dir-pagination-controls>
                                    <table class="table table-striped table-bordered responsive">
                                      <tr>
                                        <th ng-click="sort('firstname')">Name
                                                 <span class="glyphicon sort-icon" ng-show="sortKey=='firstname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                        </th>
                                       
                                        <th ng-click="sort('username')">Username
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='username'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('country')">Country
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='country'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('phone')">Phone Number
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='phone'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <!-- <th ng-click="sort('accountname')">Account Name
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='accountname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('accountnumber')">Account Number
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='accountnumber'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('bankname')">Bank Name
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='bankname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('branch')">Bank Branch
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='branch'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th> -->
                                        <th ng-click="sort('carrier')">Network Carrier
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='carrier'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                        <th ng-click="sort('amt')">Amount due
                                             <span class="glyphicon sort-icon" ng-show="sortKey=='amt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                       
                                        </th>
                                      </tr>
                                      
                                      
                                      <tr  dir-paginate="roll in paymentList|orderBy:sortKey:reverse|filter:search|itemsPerPage:10">
                                        <td>@{{roll.fullname}}</td>
                                        <td>@{{roll.username}}</td>
                                        <td>@{{roll.country}}</td>
                                        <td>@{{roll.phone}}</td>
                                       <!--  <td>@{{roll.account.accountname}}</td>
                                        <td>@{{roll.account.accountnumber}}</td>
                                        <td>@{{roll.account.bankname}}</td>
                                        <td>@{{roll.account.branch}}</td> -->
                                        <td>@{{roll.carrier}}</td>
                                        <td>@{{roll.total}}</td>
                                        
                                      </tr>
                                     
                                    </table>
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true" class="pull-right">
                                    </dir-pagination-controls>
                                    <button class="btn btn-primary pull-right" ng-click="generate()">Generate Payment List</button>
                                    
                                </div>
                                <div class="col-lg-12" ng-if="!load && paymentList.length == 0">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          No User Qualified for payment at this time month
                                      </button>
                                    </center>
                                </div>
                                
                             </div>
                            <div class="tab-pane fade animated bounceIn" id="email-pills">
                              <div class="row">
                                <div class="col-lg-12" ng-if="paymentList.length > 0">
                                    <div class="row">
                                        <div class="col-md-8">
                                           <h2>This process is NOT reversible</h2>
                                           <p>This will notify all qualified users they have been paid their team sales commisions</p>
                                       </div>
                                       <div class="col-md-4">
                                           <button class="btn btn-lg btn-danger" ng-click="resetInsurance()">
                                               Reset &  Dispatch Payment
                                           </button>
                                       </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" ng-if="paymentList.length == 0">
                                    <center>
                                      <button class="btn btn-lg btn-danger" >
                                          No User Qualified for payment at this time month
                                      </button>
                                    </center>
                                </div>
                                
                              </div>
                              
                            </div>
              </div>
                            
                            
                            
                        </div>
                        
                    </div>
                </div>
                <!-- <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" ng-show="showModal">
                    <div class="modal-header">
                        <h1>Processing...</h1>
                    </div>
                    <div class="modal-body">
                        <div class="progress progress-striped active">
                            <div class="bar" style="width: 100%;"></div>
                        </div>
                    </div>
                </div> -->
            </div>

@endSection               

@extends('../layouts.default')

@section('content')
<div class="row-fluid">
    <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3" style="margin-top:-40px;">
        <div class="account-wall">
            <h3 class="text-center login-title">Request for pasword recovery succefully completed</h3>
            <h4 class="text-center login-title">Check your inbox for reset link</h4>
        </div>
    </div>
</div>

@endSection()
@section('footer')

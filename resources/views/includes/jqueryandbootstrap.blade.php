<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css')}}">
<link href="{{ asset('bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">
<link href="{{ asset('dist/css/timeline.css')}}" rel="stylesheet">
<link href="{{ asset('dist/css/sb-admin-2.css')}}" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
<link href="{{asset('css/jquery-ui.structure.min.css')}}" rel="stylesheet">
<link href="{{asset('css/jquery-ui.theme.min.css')}}" rel="stylesheet">
<link href="{{asset('css/jquery-ui-timepicker-addon.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/jquery-ui-timepicker-addon.js')}}"></script>

<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/myscript.js')}}"></script>
<script src="{{ asset('bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>
<script src="{{ asset('dist/js/sb-admin-2.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/mystyles.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
   $(function() {
        $('#datetimepicker2').datepicker({
        	yearRange: "1970:2020",
        	showButtonPanel: true,
        	changeMonth: true,
      		changeYear: true
        });
       
    });
</script>

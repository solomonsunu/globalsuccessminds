<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css')}}">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/mystyles2.css')}}">
<link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/myscript.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/smoothscroll.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/nivo-slider.css')}}" type="text/css" />
<script src="{{ asset('js/jquery.nivo.slider.pack.js')}}" type="text/javascript"></script>

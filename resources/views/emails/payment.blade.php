<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    @include('includes.jqueryandbootstrap2')
</head>
<body>
<h2>Global Success Minds </h2>

<div>
    <h3> Hi <b>{{ $name }}</b> , you have been paid <b>{{ $amount }}</b> Ghana Cedis as team sales commision</h3>
    <h3>Date: <?php echo date('d-F-Y'); ?></h3>
    <span>Thank you</span><br>
    <span>Global Success Minds</span>
</div>

</body>
</html>
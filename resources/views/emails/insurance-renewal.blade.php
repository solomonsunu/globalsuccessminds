<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    @include('includes.jqueryandbootstrap2')
</head>
<body>
<h2>Monthly Insurance Activation</h2>

<div>
    <h3> Hi <b>{{ $name }}</b> , your insurance has been renewed for <?php echo date("F j, Y, g:i a"); ?>.</h3>
    <a class="btn btn-primary" href="http://www.globalsuccessminds.com/">visit dashboard</a>
    <br>
    <span>The GlobalSuccessMinds team</span>
</div>

</body>
</html>
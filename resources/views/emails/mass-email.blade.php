<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    @include('includes.jqueryandbootstrap2')
</head>
<body>
<h2>{{ $title }}</h2>

<div>
    <h3> Hi <b>{{ $name }}</b> , {{ $content }}.</h3>
    <span>Sent: <?php echo date("F j, Y, g:i a"); ?></span>
    <!-- <a class="btn btn-primary" href="http://www.globalsuccessminds.com/">visit dashboard</a> -->
    <br>
    <span>The GlobalSuccessMinds team</span>
</div>

</body>
</html>
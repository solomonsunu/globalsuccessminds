<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    @include('includes.jqueryandbootstrap2')
</head>
<body>
<h2>Welcome to GSM</h2>

<div>
    <h3> Hi <b>{{ $user->firstname }}</b> , you have successfully registered on global success minds platform.</h3>
    <h3>You can now logon with your username/phone number and password to access your dashboard</h3>
    <span>Thank you</span><br>
    <span>Global Success Minds</span>
</div>

</body>
</html>
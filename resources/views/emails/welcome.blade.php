<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    @include('includes.jqueryandbootstrap2')
</head>
<body>
<h2>Welcome to GSM</h2>

<div>
    <h3> Hi <b>{{ $name }}</b> , global success minds is glad to have you as part of the team. Your account has been activated.</h3>
    <a class="btn btn-primary" href="http://www.globalsuccessminds.com/">Get Started</a>
</div>

</body>
</html>
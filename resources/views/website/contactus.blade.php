<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 17:02:44 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Buildify">
    <meta name="author" content="Marketify">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Global Success Minds</title>
    <!-- STYLES -->
   <link rel="stylesheet" type="text/css" href="{{asset('website/css/fontello.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/skeleton.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/plugins.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/base.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/style.css')}}" />
<link href="{{asset('website/fonts.googleapis.com/cssf438.css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i')}}" rel="stylesheet">
    <!-- /STYLES -->
</head>

<body>

<!-- MAIN BACKGROUND -->
<div class="buildify_tm_mainbg">
	
	<!-- PATTERN -->
	<div class="marketify_pattern_overlay"></div>
	<!-- /PATTERN -->
	
</div>
<!-- /MAIN BACKGROUND -->

   <!-- WRAPPER ALL -->
    <div class="buildify_tm_wrapper_all">
        <div class="buildify_tm_wrapper">
            <div class="buildify_tm_animate_submenu"></div>
            <!-- LEFTPART -->
            <div class="buildify_tm_leftpart_wrap">
                <!-- LEFT PATTERN -->
                <div class="buildify_tm_build_pattern"></div>
                <!-- /LEFT PATTERN -->
                <!-- MENUBAR -->
                <div class="buildify_tm_menubar">
                    <div class="buildify_tm_menubar_in">
                        <div class="buildify_tm_menubar_content">
                            <div class="menu_logo" style="background-color: white;">
                                <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
                            </div>
                            <div class="menu_nav">
                                <div class="menu_nav_in">
                                    <div class="menu_nav_content scrollable">
                                        <ul class="vert_nav">
                                           <li><a href="{{url('/')}}">Home</a></li>
                                    
                                        <li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
                                        <li><a href="{{url('productsandservices')}}">Products & Services</a></li>
                                        
                                        <li><a href="{{url('/#company')}}">About Us</a></li>
                                        <li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
                                        <li><a href="{{url('contactus')}}">Contact</a></li>
                                        @if(Auth::user())
                                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
                                        @else
                                         <li><a href="{{url('login')}}">Login</a>
                                        @endif
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /MENUBAR -->
            </div>
            <!-- /LEFTPART -->
            <!-- RIGHTPART -->
            <div class="buildify_tm_rightpart_wrap">
                <div class="buildify_tm_rightpart">
                    <!-- CONTENT -->
                    <div class="buildify_tm_content_wrap">
                        <div class="buildify_tm_content">
                            <!-- TOPBAR -->
                            <div class="buildify_tm_topbar_info">
                                <div class="buildify_tm_connection">
                                    <div class="phone_numb">
                                        <div class="phone_numb_in">
                                            <img src="img/call.png" alt="" />
                                            <p>Hot Line: <span>030-254-1284 / 0303964373</span></p>
                                        </div>
                                    </div>
                                    <div class="send_msg">
                                        <a href="{{url('contactus')}}">
										<img class="svg" src="website/img/svg/message2.svg" alt="" />
									</a>
                                    </div>
                                </div>
                                <div class="buildify_tm_social_list">
                                    <ul>
                                        <li><a href="#"><i class="xcon-facebook"></i></a></li>
                                        <li><a href="#"><i class="xcon-twitter"></i></a></li>
                                        <li><a href="#"><i class="xcon-instagram"></i></a></li>
                                        <li><a href="#"><i class="xcon-pinterest"></i></a></li>
                                        <li><a href="#"><i class="xcon-gplus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /TOPBAR -->
                            <!-- HEADER -->
                            <div class="buildify_tm_mobile_header_wrap">
                                <div class="in">
                                    <div class="container">
                                        <div class="header_inner">
                                            <div class="logo">
                                                <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
                                            </div>
                                            <div class="buildify_tm_trigger">
                                                <div class="hamburger hamburger--collapse-r">
                                                    <div class="hamburger-box">
                                                        <div class="hamburger-inner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="navigation_wrap">
                                        <div class="container">
                                            <div class="inner_navigation">
                                                <ul class="nav">
                                                    <li><a href="{{url('/')}}">Home</a></li>
                                    
                                                    <li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
                                                    <li><a href="{{url('productsandservices')}}">Products & Services</a></li>
                                                    
                                                    <li><a href="{{url('/#company')}}">About Us</a></li>
                                                    <li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
                                                    <li><a href="{{url('contactus')}}">Contact</a></li>
                                                    @if(Auth::user())
                                                    <li><a href="{{url('/dashboard')}}">Dashboard</a>
                                                    @else
                                                     <li><a href="{{url('login')}}">Login</a>
                                                    @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /HEADER -->
					   	
					   	<div class="buildify_tm_universal_title_holder">
							<div class="container">
								<div class="title_holder_inner">
									<h3>Contact</h3>
								</div>
							</div>
						</div>		
						<div class="buildify_tm_section">
							<div class="container">
								<div class="buildify_tm_contact_wrap">
									<!-- <div class="leftbox sticky_sidebar">
										<div class="subtitle">
											<p>Buildify is a privately owned, internationally focussed engineering enterprise with world-class capabilities spanning the entire client value chain. We operate an integrated business model comprising the full range of engineering, construction and asset management services delivering single-source solutions for some of the world's most prestigious public and private organisations.</p>
										</div>
										<div class="get_in_touch_wrap">
											<div class="title">
												<h3>Get in Touch With Us</h3>
											</div>
											<div class="inner_wrap">
												<form action="http://frenify.com/" method="post" class="contact_form" id="contact_form">
													<div class="returnmessage" data-success="Your message has been received, We will contact you soon."></div>
													<div class="empty_notice"><span>Please Fill Required Fields</span></div>
													<div class="row">
														<label>Full Name<span></span></label>
														<input id="name" type="text" />
													</div>
													<div class="row">
														<label>Your E-mail<span></span></label>
														<input id="email" type="text" />
													</div>
													<div class="row">
														<label>Your Subject<span></span></label>
														<input id="subject" type="text" />
													</div>
													<div class="row">
														<label>Your Message<span></span></label>
														<textarea id="message"></textarea>
													</div>
													<div class="row">
														<a id="send_message" href="#">Send Message</a>
													</div>
												</form>
											</div>
										</div>
									</div> -->
									<div class="leftbox sticky_sidebar">
										<div class="rightbox_inner">
											<div class="in">
												<div class="service_type_list">
													<ul>
														<li>
															<div class="office">
																<h3>Main Office</h3>
															</div>
															<p class="definition">37 Anderson Close, Adenta Barrier, Accra</p>
															<p class="phone">
																<label>Phone:</label>
																<span>030-254-1284 / 0303964373</span>
															</p>
															<p class="mail">
																<label>Email:</label>
																<span><a href="#">info@globalsuccessminds.com</a></span>
															</p>
														</li>
														
													</ul>
												</div>
											</div>
										</div>
									</div>
										
								</div>
							</div>
						</div>				
						<div class="buildify_tm_section">
							<div class="buildify_tm_quotebox">
								<div class="container">
									<div class="inner">
										<h3 class="text">Ready To Make Money ? <a href="{{url('register')}}"> Register Now</a></h3>
										<span class="pattern"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
   				<!-- /CONTENT -->
   				<!-- FOOTER -->
				<footer class="buildify_tm_footer">
                        <div class="buildify_tm_footer_wrap" id="company">
                            <div class="container">
                                <div class="buildify_tm_list_wrap" data-column="2" data-space="40">
                                    <ul class="buildify_list">
                                        <li>
                                            <div class="inner">
                                                <div class="footer_section_title">
                                                    <h3>About Company</h3>
                                                </div>
                                                <div class="definition">
                                                    <p>We are christened to break barriers in people’s lives and offer limitless opportunities. Our goal is to create a community of successful individuals enjoying financial freedom, a community where we train and assist each other and share ideas. There by cushioning each other en route success. With GSM, you are never left alone as you are shielded by a team of astute business minds. You become a member of a team of airtime sales people where our corporate staff is always available to train and assist you to become successful. Our community of like minded people, with you inclusive would contribute in changing the world and making it a better place than we met it through this novel opportunity of airtime sales.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="inner">
                                                <div class="footer_section_title">
                                                    <h3>Business Hours</h3>
                                                </div>
                                                <div class="definition">
                                                    <p>We are happy to meet you during our working hours. Please make an appointment.</p>
                                                </div>
                                                <div class="inner_list">
                                                    <ul>
                                                        <li>
                                                            <div class="wrap">
                                                                <span class="left">Monday-Saturday:</span>
                                                                <span class="right">8;30am to 6pm</span>
                                                            </div>
                                                        </li>
                                                        <!-- <li>
														<div class="wrap">
															<span class="left">Saturday:</span>
															<span class="right">10am to 3pm</span>
														</div>
													</li> -->
                                                        <li>
                                                            <div class="wrap">
                                                                <span class="left">Sunday:</span>
                                                                <span class="right">Closed</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- 	<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Helpful Links</h3>
											</div>
											<div class="helpful_links">
												<div class="inner_list">
													<ul>
														<li><a href="#">Our services</a></li>
														<li><a href="#">Diclaimer</a></li>
														<li><a href="#">Showcase</a></li>
														<li><a href="#">Privacy Policy</a></li>
														<li><a href="#">Affliates</a></li>
													</ul>
												</div>
											</div>
										</div>
									</li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="buildify_tm_copyright">
                                <div class="container">
                                    <div class="qwe">
                                        <p>&copy; 2014 - 2019 Copyright Global Success Minds. All Rights Reserved.<a href="{{url('/termsandconditions')}}">Terms & Conditions</a> </p>
                                        <div class="totop">
                                            <span>To Top</span>
                                            <a class="buildify_tm_totop" href="#"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
				<!-- /FOOTER -->
			</div>
   		</div>
   		<!-- /RIGHTPART -->
   		
   </div>
      
       
</div>
<!-- / WRAPPER ALL -->


<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('website/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/plugins.js')}}"></script>
<!--[if lt IE 10]> <script type="text/javascript" src="{{asset('js/ie8.js')}}"></script> <![endif]-->   
<script type="text/javascript" src="{{asset('website/js/init.js')}}"></script>
<!-- /SCRIPTS -->

<script type="text/javascript">function add_chatinline(){var hccid=64164170;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
</body>

<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 17:02:44 GMT -->
</html>
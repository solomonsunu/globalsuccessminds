<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 16:59:49 GMT -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="description" content="Buildify">
<meta name="author" content="Marketify">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Global Success Minds</title>

<!-- STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('website/css/fontello.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/skeleton.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/plugins.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/base.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/style.css')}}" />
<link href="{{asset('website/fonts.googleapis.com/cssf438.css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i')}}" rel="stylesheet">

</head>

<body>

<!-- MAIN BACKGROUND -->
<div class="buildify_tm_mainbg">
	
	<!-- PATTERN -->
	<div class="marketify_pattern_overlay"></div>
	<!-- /PATTERN -->
	
</div>
<!-- /MAIN BACKGROUND -->

<!-- WRAPPER ALL -->
<div class="buildify_tm_wrapper_all">
   
   <div class="buildify_tm_wrapper">
   
   		
	   <div class="buildify_tm_animate_submenu"></div>
	   
   
  		<!-- LEFTPART -->
   		<div class="buildify_tm_leftpart_wrap">
   			
   			<!-- LEFT PATTERN -->
   			<div class="buildify_tm_build_pattern"></div>
   			<!-- /LEFT PATTERN -->
   			
   			<!-- MENUBAR -->
   			<div class="buildify_tm_menubar">
   				<div class="buildify_tm_menubar_in">
   					<div class="buildify_tm_menubar_content">
   						<div class="menu_logo" style="background-color: white;">
   							<a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
   						</div>
   						
   						<div class="menu_nav">
   							<div class="menu_nav_in">
   								<div class="menu_nav_content scrollable">
									<ul class="vert_nav">
										<li><a href="{{url('/')}}">Home</a></li>
									
										<li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
										<li><a href="{{url('productsandservices')}}">Products & Services</a></li>
										
										<li><a href="{{url('/#company')}}">About Us</a></li>
										<li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
										<li><a href="{{url('contactus')}}">Contact</a></li>
										@if(Auth::user())
				                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
				                    	@else
				                         <li><a href="{{url('login')}}">Login</a>
				                     	@endif
				                     	</li>
									</ul>
   								</div>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   			<!-- /MENUBAR -->
   			
   		</div>
   		<!-- /LEFTPART -->
   		
   		<!-- RIGHTPART -->
   		<div class="buildify_tm_rightpart_wrap">
   			<div class="buildify_tm_rightpart">
				
   				<!-- CONTENT -->
				<div class="buildify_tm_content_wrap">
					<div class="buildify_tm_content">
						
						<!-- TOPBAR -->
						<div class="buildify_tm_topbar_info">
							<div class="buildify_tm_connection">
								<div class="phone_numb">
									<div class="phone_numb_in">
										<img src="website/img/call.png" alt="" />
										<p>Hot Line: <span>030-254-1284 / 0303964373</span></p>
									</div>
								</div>
								<div class="send_msg">
									<a href="{{url('contactus')}}">
										<img class="svg" src="website/img/svg/message2.svg" alt="" />
									</a>
								</div>
							</div>
							<div class="buildify_tm_social_list">
								<ul>
									<li><a href="#"><i class="xcon-facebook"></i></a></li>
									<li><a href="#"><i class="xcon-twitter"></i></a></li>
									<li><a href="#"><i class="xcon-instagram"></i></a></li>
									<li><a href="#"><i class="xcon-pinterest"></i></a></li>
									<li><a href="#"><i class="xcon-gplus"></i></a></li>
								</ul>
							</div>
						</div>
						<!-- /TOPBAR -->
						
						<!-- HEADER -->
					   <div class="buildify_tm_mobile_header_wrap">
							<div class="in">
							   <div class="container">
								   <div class="header_inner">
									   <div class="logo">
										   <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
									   </div>
									   <div class="buildify_tm_trigger">
											<div class="hamburger hamburger--collapse-r">
												<div class="hamburger-box">
													<div class="hamburger-inner"></div>
												</div>
											</div>
										</div>
								   </div>
							   </div>
								<div class="navigation_wrap">
									<div class="container">
										<div class="inner_navigation">
											<ul class="nav">
												<li><a href="{{url('/')}}">Home</a></li>
									
										<li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
										<li><a href="{{url('productsandservices')}}">Products & Services</a></li>
										
										<li><a href="{{url('/#company')}}">About Us</a></li>
										<li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
										<li><a href="{{url('contactus')}}">Contact</a></li>
										@if(Auth::user())
				                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
				                    	@else
				                         <li><a href="{{url('login')}}">Login</a>
				                     	@endif
				                     	</li>
											</ul>
										</div>
									</div>
								</div>
						   </div>
					   </div>
						<!-- /HEADER -->
						
						<div class="container">
							<div class="buildify_tm_top_title_wrap">
								<div class="left">
									<h3 style="color:#3b2057;">Join the airtime business with GSM and make money from airtime sales</h3>
									<div style="padding: 4px auto; margin-left: 20px; margin-top: 20px;">
										<ol>
						              <li><b>Airtel: A97600 (code)</b></li>
						              <li><b>Mtn:  0549398254</b></li>
						              <li><b>Tigo: 0271099670</b></li>
						              <li><b>Vodafone: 109441 (till)</b></li>
						               <li><b>UT Bank 0011312086017</b></li>  
                						<li><b>Beige Capital 0280126876001</b> </li> 
						            </ol>
									</div>
									
								</div>
								<div class="right">
									<p>GSM Airtime for Cash is a unique opportunity for you to make money from airtime sales. How does it work? Sign up to GSM Airtime for Cash for free and become our independent airtime distributor: load your wallet with any amount to start selling airtime for any network (Airtel, Expresso, Glo, MTN, Tigo and Vodafone). GSM  has exciting commissions as an incentive package for meeting sales targets.  

						          This is a simple and easy business you can engage in to earn extra cash. You can do this business by selling airtime to your family, friends and co workers. What are your expectations in life? Extra money, freedom, security, and quality time for family, friends and the activities you love? Then first you would need to embrace this financial truth. <b style="color:#3b2057;">There’s a limit to how much you can save but there’s no limit to how much you can earn.</b> <br>
						          

						      	</p>
								</div>

								<p class="text-center">
						         
						              
						         
						        </p>
							</div>
						</div>
						
						<!-- /SHORT SERVICES -->
						<div class="buildify_tm_section" styl>
						<div class="about_list">
							<div class="buildify_tm_short_services_wrap">
								<div class="container">
									<div class="badge">
										<span>Satisfaction Guaranteed</span>
									</div>
								</div>
								<div class="container">
									<div class="prev_next">
										<div class="tm_next_button"></div>
										<div class="tm_prev_button"></div>
									</div>
								</div>
								<div class="container">
									<div class="swiper-pagination my_swiper_pagination"></div>
								</div>
								<div class="buildify_tm_universal_swiper_wrap">
									<div class="swiper-container">
										<div class="buildify_tm_articles_wrap swiper-wrapper">
											<div class="article_inner swiper-slide">
												<div class="article_image first">
													<div class="overlay"></div>
												</div>
											</div>
											<div class="article_inner swiper-slide">
												<div class="article_image second">
													<div class="overlay"></div>
												</div>
											</div>
											<div class="article_inner swiper-slide">
												<div class="article_image third">
													<div class="overlay"></div>
												</div>
											</div>
											<div class="article_inner swiper-slide">
												<div class="article_image fourth">
													<div class="overlay"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="buildify_tm_short_service_list">
								<div class="container">
									<div class="buildify_tm_list_wrap" data-column="3" data-space="30">
										<ul class="buildify_list">
											<li class="trailer">
												<div class="list_inner">
													<div class="image_holder">
														<img src="website/img/services/370x240.jpg" alt="" />
														<div class="main_image second"></div>
													</div>
													<div class="titles_holder">
														<div class="title">
															<h3><a href="service_single.html">Dial *161*444# on all Networks</a></h3>
														</div>
														<div class="definition">
															 <ul>
												               <li>To Buy Airtime</li>
												               <li>To Check the Money on your GSM Cash Wallet </li>
												               <li>To Change your PIN</li>
												               <li>To transfer Money from your Wallet to Others</li>
												             </ul>
															
														</div>
														
													</div>
												</div>
												
											</li>
											<li>
												<div class="list_inner">
													<div class="image_holder">
														<img src="website/img/services/370x240.jpg" alt="" />
														<div class="main_image second"></div>
													</div>
													<div class="titles_holder">
														<div class="title">
															<h3><a href="service_single.html">Airtime for Cash</a></h3>
														</div>
														<div class="definition">
															<p>GSM Airtime for cash allows you to directly recharge on all networks in Ghana at a 24/7 absolute convenience whilst being paid commissions on sales you make on all networks. Load Your GSM Cash Wallet to ensure an easy way to recharge and sell on all networks with a click. The ease, convenience and profitability make it the best means for airtime recharge. </p>
														</div>
														
													</div>
												</div>
											</li>
											<li>
												<div class="list_inner">
													<div class="image_holder">
														<img src="website/img/services/370x240.jpg" alt="" />
														<div class="main_image second"></div>
													</div>
													<div class="titles_holder">
														<div class="title">
															<h3><a href="#">AIRTIME 4 CASH  PRESENTS FOUR (4) STREAMS OF INCOME</a></h3>
														</div>
														<div class="definition">
															<ol>
												               <li>DIRECT AIRTIME COMMISSIONS</li>
												               <li>TEAM AIRTIME COMMISSIONS </li>
												               <li>DIRECT UPGRADE COMMISSIONS</li>
												               <li>TEAM UPGRADE COMMISSIONS</li>
												             </ol>
														</div>
														
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- /SHORT SERVICES -->
						
						<!-- FEATURED WORKS -->
							<div class="buildify_tm_section">
							<div class="buildify_tm_news_wrap">
								<div class="container">
									<div class="buildify_tm_universal_carousel_wrap">
										<div class="title_holder">
											<div class="span">
												<h3>Join Us</h3>
											</div>
										</div>
										<div class="buildify_tm_list_wrap" data-column="2" data-space="0">
											<ul class="buildify_list buildify_tm_miniboxes">
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">GSM AGENT</a></h3>
															</div>
															<div class="definition">
																<p>GSM offers the opportunity of starting airtime sales business either at home, office, hall of residence, hostel, school, a shop or by the road. It is that easy and simple; all you need is a phone and a simple card. No start up capital is required. Agents earn 4% instant commission on airtime sales. Agent account cannot however be used to refer others to join GSM. Agents who wish to earn Team Airtime Commissions for referring people has to register a regular account as well. It is the regular account that can be used to refer people to join GSM. To become an agent all you need to do is register, load your GSM wallet and start selling airtime to all networks 24/7.


																</p>
																<a href="{{url('register/agent')}}" style="background-color: #edb600;color: #fff; text-decoration: none; padding: 5px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Click Here For Agent Registration<!-- <span>FREE CONSULTATION FOR THE FIRST MONTH (450$ VALUE)</span> --></a>
																
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>March 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">GSM USER</a></h3>
															</div>
															<div class="definition">
																<p>Click on the Click Here To Registration button below. Fill the registration form and submit. After a successful registration, log into your page to view your pin. Load your wallet by sending the amount to any of these mobile money numbers or at any of these banks or with our authorised agents. </p>
															</div>
															<a href="{{url('register')}}" style="background-color: #edb600;color: #fff; text-decoration: none; padding: 5px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Click Here To Registration<!-- <span>FREE CONSULTATION FOR THE FIRST MONTH (450$ VALUE)</span> --></a>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>June 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<!-- <li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="blog_single.html">Buildify Plans $300m East NYC</a></h3>
															</div>
															<div class="definition">
																<p>More than 1,000 homes to be built at planned Barking waterfront ...</p>
															</div>
															<div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>April 01</span></p>
															</div>
														</div>
													</div>
												</li> -->
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /FEATURED WORKS -->

							<!-- NEWS -->
						<div class="buildify_tm_section">
							<div class="buildify_tm_news_wrap">
								<div class="container">
									<div class="buildify_tm_universal_carousel_wrap">
										<div class="title_holder">
											<div class="span">
												<h3>ENJOY ENORMOUS BENEFITS BY BECOMING A PREMIER MEMBER</h3>
											</div>
										</div>
										<div class="buildify_tm_list_wrap" data-column="3" data-space="0">
											<ul class="buildify_list buildify_tm_miniboxes">
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">UPGRADE PACKAGE</a></h3>
															</div>
															<div class="definition">
																<ol>
													              <li>Receive GSM branded watch</li>
													              <li>Recieve Ghc5 in your wallet for airtime purchase</li>
													              <li>Hit the first rank: GSM premier distributor.</li>
													              <li>Enjoy upgrade commissions</li>
													              <li>Advance GSM leadership ranks</li>
													            </ol>
																
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>March 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">UPGRADE REQUIREMENT</a></h3>
															</div>
															<div class="definition">
																 <ol>
													              <li>Load your wallet with Ghc50.00</li>
													              <li>Login to your back office </li>
													              <li>Click on upgrade</li>
													              <li>Ghc45 will be deducted from your wallet</li>
													              
													            </ol>
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>June 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="blog_single.html">GSM LEADERSHIP RANKS </a></h3>
															</div>
															<div class="definition">
																Members are rewarded as they advance the leadership ranks. <br>
												                RANK REWARDS ARE MEANT TO INCENTIVISE AND MOTIVATE MEMBERS FOR THEIR PERFORMANCE. <br>
												                <a href="images/gsm.pdf" target="_blank">(click here to view ranks and the rewards package) </a>
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>April 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /NEWS -->
						
						<!-- TESTIMONIALS -->
						<div class="buildify_tm_section">
							<div class="buildify_tm_testimonial_wrap">
								<div class="buildify_tm_universal_parallax_wrap">
									<span class="shape"></span>
									<div class="main_bg">
										<div class="overlay_image"></div>
										<div class="overlay_video">
											<video autoplay loop muted>
												<source src="wesite/video/1.mp4" type="video/mp4">
											</video>
										</div>
										<div class="overlay_color testimonial"></div>
									</div>
									<div class="main_content">
										<div class="container">
											<div class="buildify_tm_universal_carousel_wrap testimonial">
												<div class="title_holder light">
													<div class="span">
														<h3>Testimonials</h3>
													</div>
													<div class="carousel_nav">
														<div class="custom_nav">
															<a href="#" class="prev"></a>
															<a href="#" class="next"></a>
														</div>
													</div>
												</div>
												<div class="main_carousel_wrap">
													<ul class="owl-carousel">
														<li class="item">
															<div class="inner">
																<div class="testimonial_definition">
																	<p>Life on campus could get awful sometimes, deeming it vital to supplement the money your parents give you. GSM airtime for cash offers such an enviable opportunity to make money as a student just by joining the airtime business.</p>
																</div>
																<div class="name_holder_wrap">
																	<div class="image">
																		<img src="images/testi6.jpg" alt="" />
																	</div>
																	<div class="name">
																		<span>Joshua</span>
																		<!-- <span>CEO of Fasia Co.</span> -->
																	</div>
																</div>
																<div class="quote">
																	<img class="svg" src="website/img/svg/quotes.svg" alt="" />
																</div>
															</div>
														</li>
														<li class="item">
															<div class="inner">
																<div class="testimonial_definition">
																	<p> Increasingly and steadily my financial goals are being met, I took that decision to join GSM Airtime for Cash business, that gave me the opportunity to be my own boss and be a partner, it has been a life changing experience for me. </p>
																</div>
																<div class="name_holder_wrap">
																	<div class="image">
																		<img src="images/testi4.jpg" alt="" />
																	</div>
																	<div class="name">
																		<span>Daniel Osei Tuffour</span>
																		<!-- <span>Constructor</span> -->
																	</div>
																</div>
																<div class="quote">
																	<img class="svg" src="website/img/svg/quotes.svg" alt="" />
																</div>
															</div>
														</li>
														
														<li class="item">
															<div class="inner">
																<div class="testimonial_definition">
																	<p>I have become an advocate for earning extra income aside your salary. It creates a sound financial freedom. Using my phone to make money by selling airtime to my church members and family is very easy and totally worth it. My family is well taken care of and I sleep better now. I couldn’t have done this without Global Success Minds Airtime for cash.</p>
																</div>
																<div class="name_holder_wrap">
																	<div class="image">
																		<img src="images/testi8.jpg" alt="" />
																	</div>
																	<div class="name">
																		<span>Aunty Pomaa</span>
																		<!-- <span>Architector of Futuro Co.</span> -->
																	</div>
																</div>
																<div class="quote">
																	<img class="svg" src="website/img/svg/quotes.svg" alt="" />
																</div>
															</div>
														</li>
														
															<li class="item">
															<div class="inner">
																<div class="testimonial_definition">
																	<p>I love all the fine things I see .This comes with great expense and yes my job pays well but it could never give me the financial security I craved, with global success minds airtime for cash I make money from airtime sales: am on my way to living my dream. </p>
																</div>
																<div class="name_holder_wrap">
																	<div class="image">
																		<img src="images/testi2.jpg" alt="" />
																	</div>
																	<div class="name">
																		<span>Queen Talata</span>
																		<!-- <span>Constructor</span> -->
																	</div>
																</div>
																<div class="quote">
																	<img class="svg" src="website/img/svg/quotes.svg" alt="" />
																</div>
															</div>
														</li>
															<li class="item">
															<div class="inner">
																<div class="testimonial_definition">
																	<p>  I was sceptical initially but I am glad I joined the airtime for cash business. I get to leverage on the many friends I have in real life and even on social media to make money through airtime sales. </p>
																</div>
																<div class="name_holder_wrap">
																	<div class="image">
																		<img src="images/testi5.jpg" alt="" />
																	</div>
																	<div class="name">
																		<span>Antonio</span>
																		<!-- <span>Constructor</span> -->
																	</div>
																</div>
																<div class="quote">
																	<img class="svg" src="website/img/svg/quotes.svg" alt="" />
																</div>
															</div>
														</li>	
													</ul>
												</div>
											</div>
										<!-- 	<div class="buildify_tm_partners_wrap">
												<div class="buildify_tm_universal_carousel_wrap">
													<div class="main_carousel_wrap">
														<ul class="owl-carousel">
															<li class="item">
																<img src="img/partners/1.png" alt="" />
															</li>
															<li class="item">
																<img src="img/partners/4.png" alt="" />
															</li>
															<li class="item">
																<img src="img/partners/3.png" alt="" />
															</li>
															<li class="item">
																<img src="img/partners/4.png" alt="" />
															</li>
															<li class="item">
																<img src="img/partners/5.png" alt="" />
															</li>
															<li class="item">
																<img src="img/partners/2.png" alt="" />
															</li>
														</ul>
													</div>
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /TESTIMONIALS -->
						
						<!-- NEWS -->
						<div class="buildify_tm_section" id="opportunity">
							<div class="buildify_tm_news_wrap">
								<div class="container">
									<div class="buildify_tm_universal_carousel_wrap">
										<div class="title_holder">
											<div class="span">
												<h3>Opportunities</h3>
											</div>
										</div>
										<div class="buildify_tm_list_wrap" data-column="2" data-space="0">
											<ul class="buildify_list buildify_tm_miniboxes">
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">OPPORTUNITY</a></h3>
															</div>
															<div class="definition">
																<p>The Telecommunication industry is the gold mine of our time. Several billions of Ghana cedis is spent on airtime every month making it a gold mine. GSM gives you the opportunity to mine in this great gold reserve.

																</p>
																<p>It is our conviction that everyone has the right to live their dreams. With our unique distributor commissions coupled with our desired products and services, we are confident that you will enjoy the financial freedom to live your dream.</p>
																<p>By selling airtime to people on the GSM platform, you are invariably providing a great service to them. Remember the saying that if you want to go fast go alone but if you want to go far go with people. It’s not an individual effort; it takes the group effort made up of our individual small efforts to enjoying a resounding success.</p>
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>March 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">Get to Earn Forever</a></h3>
															</div>
															<div class="definition">
																<p>Are you making extra income right now? Earning income doesn’t have to stop at your salary. Extra income is not always about the money. It serves as a personal safety net against uncertainties. You would have more control over your income, save more and most importantly use your time productively. Global success minds, Airtime for cash gives you the opportunity to make that extra cash. With a little effort and dedication you can earn a lot more than you ever envisaged. Your phone is your work tool and sales of airtime to the pull of people around you would guarantee that success you deserve. This is no brainer; it is simple for anyone who wants to make that cash. Do not get comfortable with a steady pay cheque. Do something for yourself that makes your time worthwhile and promises a successful life .BE A PARTNER and a business owner and earn residual income every month.</p>
															</div>
															<!-- <div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>June 01</span></p>
															</div> -->
														</div>
													</div>
												</li>
												<!-- <li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="blog_single.html">Buildify Plans $300m East NYC</a></h3>
															</div>
															<div class="definition">
																<p>More than 1,000 homes to be built at planned Barking waterfront ...</p>
															</div>
															<div class="read_more_news">
																<p><a href="blog_single.html">Read more</a><span>April 01</span></p>
															</div>
														</div>
													</div>
												</li> -->
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /NEWS -->
						
						<!-- QUOTEBOX -->
						<div class="buildify_tm_section">
							<div class="buildify_tm_quotebox">
								<div class="container">
									<div class="inner">
										<h3 class="text">Ready To Make Money ? <a href="{{url('register')}}"> Register Now</a></h3>
										<span class="pattern"></span>
									</div>
								</div>
							</div>
						</div>
						<!-- /QUOTEBOX -->
						
					</div>
				</div>
   				<!-- /CONTENT -->
				
				<!-- FOOTER -->
				<footer class="buildify_tm_footer" >
					<div class="buildify_tm_footer_wrap" id="company">
						<div class="container">
							<div class="buildify_tm_list_wrap" data-column="2" data-space="40">
								<ul class="buildify_list">
									<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>About Company</h3>
											</div>
											<div class="definition">
												<p>We are christened to break barriers in people’s lives and offer limitless opportunities. Our goal is to create a community of successful individuals enjoying financial freedom, a community where we train and assist each other and share ideas. There by cushioning each other en route success. With GSM, you are never left alone as you are shielded by a team of astute business minds. You become a member of a team of airtime sales people where our corporate staff is always available to train and assist you to become successful. Our community of like minded people, with you inclusive would contribute in changing the world and making it a better place than we met it through this novel opportunity of airtime sales.
												</p>
											</div>
										</div>
									</li>
									<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Business Hours</h3>
											</div>
											<div class="definition">
												<p>We are happy to meet you during our working hours. Please make an appointment.</p>
											</div>
											<div class="inner_list">
												<ul>
													<li>
														<div class="wrap">
															<span class="left">Monday-Saturday:</span>
															<span class="right">8;30am to 6pm</span>
														</div>
													</li>
													<!-- <li>
														<div class="wrap">
															<span class="left">Saturday:</span>
															<span class="right">10am to 3pm</span>
														</div>
													</li> -->
													<li>
														<div class="wrap">
															<span class="left">Sunday:</span>
															<span class="right">Closed</span>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</li>
								<!-- 	<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Helpful Links</h3>
											</div>
											<div class="helpful_links">
												<div class="inner_list">
													<ul>
														<li><a href="#">Our services</a></li>
														<li><a href="#">Diclaimer</a></li>
														<li><a href="#">Showcase</a></li>
														<li><a href="#">Privacy Policy</a></li>
														<li><a href="#">Affliates</a></li>
													</ul>
												</div>
											</div>
										</div>
									</li> -->
								</ul>
							</div>
						</div>
						<div class="buildify_tm_copyright">
							<div class="container">
								<div class="qwe">
									<p>&copy; 2014 - 2019  Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> </p>
									<div class="totop">
										<span>To Top</span>
										<a class="buildify_tm_totop" href="#"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
				<!-- /FOOTER -->
			</div>
   		</div>
   		<!-- /RIGHTPART -->
   		
   </div>
      
</div>
<!-- / WRAPPER ALL -->



<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('website/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/plugins.js')}}"></script>
<!--[if lt IE 10]> <script type="text/javascript" src="{{asset('js/ie8.js')}}"></script> <![endif]-->	
<script type="text/javascript" src="{{asset('website/js/init.js')}}"></script>
<!-- /SCRIPTS -->

</div>
<script type="text/javascript">function add_chatinline(){var hccid=64164170;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>

</body>

<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 17:00:53 GMT -->
</html>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->


<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 16:59:49 GMT -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="description" content="Buildify">
<meta name="author" content="Marketify">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Global Success Minds</title>

<!-- STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('website/css/fontello.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/skeleton.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/plugins.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/base.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/style.css')}}" />
<link href="{{asset('website/fonts.googleapis.com/cssf438.css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i')}}" rel="stylesheet">
<!--[if lt IE 9]> <script type="text/javascript" src="js/modernizr.custom.js"></script> <![endif]-->
<!-- /STYLES -->

</head>

<body>

<!-- MAIN BACKGROUND -->
<div class="buildify_tm_mainbg">
	
	<!-- PATTERN -->
	<div class="marketify_pattern_overlay"></div>
	<!-- /PATTERN -->
	
</div>
<!-- /MAIN BACKGROUND -->

<!-- WRAPPER ALL -->
<div class="buildify_tm_wrapper_all">
   
   <div class="buildify_tm_wrapper">
   
   		
	   <div class="buildify_tm_animate_submenu"></div>
	   
   
  		<!-- LEFTPART -->
   		<div class="buildify_tm_leftpart_wrap">
   			
   			<!-- LEFT PATTERN -->
   			<div class="buildify_tm_build_pattern"></div>
   			<!-- /LEFT PATTERN -->
   			
   			<!-- MENUBAR -->
   			<div class="buildify_tm_menubar">
   				<div class="buildify_tm_menubar_in">
   					<div class="buildify_tm_menubar_content">
   						<div class="menu_logo" style="background-color: white;">
   							<a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
   						</div>
   						
   						<div class="menu_nav">
   							<div class="menu_nav_in">
   								<div class="menu_nav_content scrollable">
									<ul class="vert_nav">
										<li><a href="{{url('/')}}">Home</a></li>
									
										<li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
										<li><a href="{{url('productsandservices')}}">Products & Services</a></li>
										
										<li><a href="{{url('/#company')}}">About Us</a></li>
										<li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
										<li><a href="{{url('contactus')}}">Contact</a></li>
										@if(Auth::user())
				                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
				                    	@else
				                         <li><a href="{{url('login')}}">Login</a>
				                     	@endif
				                     	</li>
									</ul>
   								</div>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   			<!-- /MENUBAR -->
   			
   		</div>
   		<!-- /LEFTPART -->
   		
   		<!-- RIGHTPART -->
   		<div class="buildify_tm_rightpart_wrap">
   			<div class="buildify_tm_rightpart">
				
   				<!-- CONTENT -->
				<div class="buildify_tm_content_wrap">
					<div class="buildify_tm_content">
						
						<!-- TOPBAR -->
						<div class="buildify_tm_topbar_info">
							<div class="buildify_tm_connection">
								<div class="phone_numb">
									<div class="phone_numb_in">
										<img src="website/img/call.png" alt="" />
										<p>Hot Line: <span>030-254-1284 / 0303964373</span></p>
									</div>
								</div>
								<div class="send_msg">
									<a href="{{url('contactus')}}">
										<img class="svg" src="website/img/svg/message2.svg" alt="" />
									</a>
								</div>
							</div>
							<div class="buildify_tm_social_list">
								<ul>
									<li><a href="#"><i class="xcon-facebook"></i></a></li>
									<li><a href="#"><i class="xcon-twitter"></i></a></li>
									<li><a href="#"><i class="xcon-instagram"></i></a></li>
									<li><a href="#"><i class="xcon-pinterest"></i></a></li>
									<li><a href="#"><i class="xcon-gplus"></i></a></li>
								</ul>
							</div>
						</div>
						<!-- /TOPBAR -->
						
						<!-- HEADER -->
					   <div class="buildify_tm_mobile_header_wrap">
							<div class="in">
							   <div class="container">
								   <div class="header_inner">
									   <div class="logo">
										  <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
									   </div>
									   <div class="buildify_tm_trigger">
											<div class="hamburger hamburger--collapse-r">
												<div class="hamburger-box">
													<div class="hamburger-inner"></div>
												</div>
											</div>
										</div>
								   </div>
							   </div>
								<div class="navigation_wrap">
									<div class="container">
										<div class="inner_navigation">
											<ul class="nav">
												<li><a href="{{url('/')}}">Home</a></li>
									
										<li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
										<li><a href="{{url('productsandservices')}}">Products & Services</a></li>
										
										<li><a href="{{url('/#company')}}">About Us</a></li>
										<li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
										<li><a href="{{url('contactus')}}">Contact</a></li>
										@if(Auth::user())
				                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
				                    	@else
				                         <li><a href="{{url('login')}}">Login</a>
				                     	@endif
				                     	</li>
											</ul>
										</div>
									</div>
								</div>
						   </div>
					   </div>
						<!-- /HEADER -->
						
					

						   	<div class="buildify_tm_service_title_holder">
							 <div class="buildify_tm_universal_title_holder">
								<div class="container">
									<div class="title_holder_inner">
										<h3>Compensation Plan</h3>
									</div>
								</div>
							</div>	
						</div>
						<div class="buildify_tm_service_long_subtitle">
							<div class="container">
								<h3>TEAM SALES COMMISSION</h3>
								<p>
								
								By joining this business, you earn commissions on airtime sales and purchases.
								Just follow these simple steps:
								Sign up for free to become a distributor on our platform. Load your GSM wallet to start selling airtime at your convenience and pace. Recommend GSM airtime for cash to other distributors and consumers to increase your sales returns and earn more.</p>
							</div>
						</div>
						<!-- FEATURED WORKS -->
							<div class="buildify_tm_section">
							<div class="buildify_tm_news_wrap">
								<div class="container">
									<div class="buildify_tm_universal_carousel_wrap">
										
										<div class="buildify_tm_list_wrap" data-column="2" data-space="0">
											<ul class="buildify_list buildify_tm_miniboxes">
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">AIRTIME COMMISSIONS </a></h3>
															</div>
															<div class="definition">
																 
													          <p>This model enables airtime consumers to also become airtime retailers: that is, we pay commissions to our members for referring others to our platform and retailing to them. </p>
													          <ol>
													            <li>DIRECT AIRTIME COMMISSIONS: Members earn 3% instant commission when they buy airtime for themselves as well as buy or sell to other people</li>
													            <li>
													             TEAM AIRTIME COMMISSIONS: This is the commission you earn for recommending or referring people to join and buy their airtime with GSM or retailing to them as a GSM distributor. 0.10%, 0.15%, 0.20%, 0.25%, 0.30% and 0.50% commissions would be paid to you from level 1 to level 6 respectively on your team sales performance.
													            </li>

													           
													          </ol>
																
																
															</div>
															
														</div>
													</div>
												</li>
												<li class="buildify_tm_minibox">
													<div class="inner">
														<div class="image_news">
															<img class="svg" src="website/img/svg/open-book.svg" alt="" />
														</div>
														<div class="main_part">
															<div class="title_holder_news">
																<h3><a href="#">UPGRADE COMMISSIONS</a></h3>
															</div>
															<div class="definition">
																 <ol>
														             <li>DIRECT UPGRADE COMMISSIONS: This is the commission you earn for recommending people to upgrade to become GSM Premier distributors. Ghc10.00 would be paid to you on each recommendation.  </li>
														            <li>TEAM UPGRADE COMMISSIONS: this commission is paid whenever someone upgrades to become a GSM premier distributor in the team. Ghc3.00 is paid on each team member upgrade.</li>
														            

														          </ol>
															</div>
															
														</div>
													</div>
												</li>
												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /FEATURED WORKS -->
						
						<!-- QUOTEBOX -->
						<div class="buildify_tm_section">
							<div class="buildify_tm_quotebox">
								<div class="container">
									<div class="inner">
										<h3 class="text">Ready To Make Money ? <a href="{{url('register')}}"> Register Now</a></h3>
										<span class="pattern"></span>
									</div>
								</div>
							</div>
						</div>
						<!-- /QUOTEBOX -->
						
					</div>
				</div>
   				<!-- /CONTENT -->
				
				<!-- FOOTER -->
				<footer class="buildify_tm_footer" >
					<div class="buildify_tm_footer_wrap" id="company">
						<div class="container">
							<div class="buildify_tm_list_wrap" data-column="2" data-space="40">
								<ul class="buildify_list">
									<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>About Company</h3>
											</div>
											<div class="definition">
												<p>We are christened to break barriers in people’s lives and offer limitless opportunities. Our goal is to create a community of successful individuals enjoying financial freedom, a community where we train and assist each other and share ideas. There by cushioning each other en route success. With GSM, you are never left alone as you are shielded by a team of astute business minds. You become a member of a team of airtime sales people where our corporate staff is always available to train and assist you to become successful. Our community of like minded people, with you inclusive would contribute in changing the world and making it a better place than we met it through this novel opportunity of airtime sales.
												</p>
											</div>
										</div>
									</li>
									<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Business Hours</h3>
											</div>
											<div class="definition">
												<p>We are happy to meet you during our working hours. Please make an appointment.</p>
											</div>
											<div class="inner_list">
												<ul>
													<li>
														<div class="wrap">
															<span class="left">Monday-Saturday:</span>
															<span class="right">8;30am to 6pm</span>
														</div>
													</li>
													<!-- <li>
														<div class="wrap">
															<span class="left">Saturday:</span>
															<span class="right">10am to 3pm</span>
														</div>
													</li> -->
													<li>
														<div class="wrap">
															<span class="left">Sunday:</span>
															<span class="right">Closed</span>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</li>
								<!-- 	<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Helpful Links</h3>
											</div>
											<div class="helpful_links">
												<div class="inner_list">
													<ul>
														<li><a href="#">Our services</a></li>
														<li><a href="#">Diclaimer</a></li>
														<li><a href="#">Showcase</a></li>
														<li><a href="#">Privacy Policy</a></li>
														<li><a href="#">Affliates</a></li>
													</ul>
												</div>
											</div>
										</div>
									</li> -->
								</ul>
							</div>
						</div>
						<div class="buildify_tm_copyright">
							<div class="container">
								<div class="qwe">
									<p>&copy; 2014 - 2019  Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a></p>
									<div class="totop">
										<span>To Top</span>
										<a class="buildify_tm_totop" href="#"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
				<!-- /FOOTER -->
			</div>
   		</div>
   		<!-- /RIGHTPART -->
   		
   </div>
      
</div>
<!-- / WRAPPER ALL -->



<!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('website/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/plugins.js')}}"></script>
<!--[if lt IE 10]> <script type="text/javascript" src="{{asset('js/ie8.js')}}"></script> <![endif]-->	
<script type="text/javascript" src="{{asset('website/js/init.js')}}"></script>
<!-- /SCRIPTS -->

</div>
<script type="text/javascript">function add_chatinline(){var hccid=64164170;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>

</body>

<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 17:00:53 GMT -->
</html>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 16:59:49 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Buildify">
    <meta name="author" content="Marketify">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Global Success Minds</title>
    <!-- STYLES -->
   <link rel="stylesheet" type="text/css" href="{{asset('website/css/fontello.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/skeleton.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/plugins.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/base.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('website/css/style.css')}}" />
<link href="{{asset('website/fonts.googleapis.com/cssf438.css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i')}}" rel="stylesheet">
    <!--[if lt IE 9]> <script type="text/javascript" src="js/modernizr.custom.js"></script> <![endif]-->
    <!-- /STYLES -->
</head>

<body>
    <!-- MAIN BACKGROUND -->
    <div class="buildify_tm_mainbg">
        <!-- PATTERN -->
        <div class="marketify_pattern_overlay"></div>
        <!-- /PATTERN -->
    </div>
    <!-- /MAIN BACKGROUND -->
    <!-- WRAPPER ALL -->
    <div class="buildify_tm_wrapper_all">
        <div class="buildify_tm_wrapper">
            <div class="buildify_tm_animate_submenu"></div>
            <!-- LEFTPART -->
            <div class="buildify_tm_leftpart_wrap">
                <!-- LEFT PATTERN -->
                <div class="buildify_tm_build_pattern"></div>
                <!-- /LEFT PATTERN -->
                <!-- MENUBAR -->
                <div class="buildify_tm_menubar">
                    <div class="buildify_tm_menubar_in">
                        <div class="buildify_tm_menubar_content">
                            <div class="menu_logo" style="background-color: white;">
                                <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
                            </div>
                            <div class="menu_nav">
                                <div class="menu_nav_in">
                                    <div class="menu_nav_content scrollable">
                                        <ul class="vert_nav">
                                            <li><a href="{{url('/')}}">Home</a></li>
                                    
                                        <li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
                                        <li><a href="{{url('productsandservices')}}">Products & Services</a></li>
                                        
                                        <li><a href="{{url('/#company')}}">About Us</a></li>
                                        <li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
                                        <li><a href="{{url('contactus')}}">Contact</a></li>
                                        @if(Auth::user())
                                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
                                        @else
                                         <li><a href="{{url('login')}}">Login</a>
                                        @endif
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /MENUBAR -->
            </div>
            <!-- /LEFTPART -->
            <!-- RIGHTPART -->
            <div class="buildify_tm_rightpart_wrap">
                <div class="buildify_tm_rightpart">
                    <!-- CONTENT -->
                    <div class="buildify_tm_content_wrap">
                        <div class="buildify_tm_content">
                            <!-- TOPBAR -->
                            <div class="buildify_tm_topbar_info">
                                <div class="buildify_tm_connection">
                                    <div class="phone_numb">
                                        <div class="phone_numb_in">
                                            <img src="website/img/call.png" alt="" />
                                            <p>Hot Line: <span>030-254-1284 / 0303964373</span></p>
                                        </div>
                                    </div>
                                    <div class="send_msg">
                                        <a href="{{url('contactus')}}">
										<img class="svg" src="website/img/svg/message2.svg" alt="" />
									</a>
                                    </div>
                                </div>
                                <div class="buildify_tm_social_list">
                                    <ul>
                                        <li><a href="#"><i class="xcon-facebook"></i></a></li>
                                        <li><a href="#"><i class="xcon-twitter"></i></a></li>
                                        <li><a href="#"><i class="xcon-instagram"></i></a></li>
                                        <li><a href="#"><i class="xcon-pinterest"></i></a></li>
                                        <li><a href="#"><i class="xcon-gplus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /TOPBAR -->
                            <!-- HEADER -->
                            <div class="buildify_tm_mobile_header_wrap">
                                <div class="in">
                                    <div class="container">
                                        <div class="header_inner">
                                            <div class="logo">
                                               <a href="{{url('/')}}"><img src="img/logo.png" alt="" /></a>
                                            </div>
                                            <div class="buildify_tm_trigger">
                                                <div class="hamburger hamburger--collapse-r">
                                                    <div class="hamburger-box">
                                                        <div class="hamburger-inner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="navigation_wrap">
                                        <div class="container">
                                            <div class="inner_navigation">
                                                <ul class="nav">
                                                    <li><a href="{{url('/')}}">Home</a></li>
                                    
                                       <li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
                                        <li><a href="{{url('productsandservices')}}">Products & Services</a></li>
                                        
                                        <li><a href="{{url('/#company')}}">About Us</a></li>
                                        <li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
                                        <li><a href="{{url('contactus')}}">Contact</a></li>
                                        @if(Auth::user())
                                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
                                        @else
                                         <li><a href="{{url('login')}}">Login</a>
                                        @endif
                                        </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /HEADER -->
                            <div class="buildify_tm_service_title_holder">
                                <div class="buildify_tm_universal_title_holder">
                                    <div class="container">
                                        <div class="title_holder_inner">
                                            <h3>Terms and Conditions</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="buildify_tm_service_long_subtitle">
                                <div class="container">
                                    <p>
                                        Signing up signifies your agreement to adhere to the following terms and conditions governing GLOBAL SUCCESS MINDS (GSM).</p>
                                        <br>
                                        <br>
                                    <h4 class="text-left"><span>CHANGES TO BUSINESS PLAN AND COMMISSIONS</span></h4>
                                     <hr class="small" />
                                    <li>GSM may from time to time make changes to its benefits and opportunities, services, sign-up, marketing plan and incentives. All members will be updated as regards to any such changes via posting updates on their dashboard, email or SMS as per the email address or mobile phone number provided in their account details. Each member is therefore required to ensure that the email address and phone numbers they provide are valid, and that they check their emails regularly for the latest updates and accordingly, go by any such changes.
                                    </li>
                                    <br />
                                    <h4 class="text-left"><span>REQUIREMENTS TO BECOME A MEMBER</span></h4>
                                    <hr class="small" />
                                    <ul>
                                        <li>Each member is independent and is responsible for his or her own business and cannot bind the company to any obligations. Hence a member will not be treated as an employee.
                                        </li>
                                        <li>A registered phone number is required to register on the platform.</li>
                                        <li>There is NO SIGN-UP FEE to become a member. </li>
                                        <li>NO hidden payments, charges or cost of any kind to members.</li>
                                        <li>You agree that any updates, notifications and announcements about our programs and other services will be sent to you via email, SMS and or on your Dashboard.</li>
                                    </ul>
                                    <br />
                                    <h4 class="text-left"><span>CONDUCT OF MEMBERS</span></h4>
                                    <hr class="small" />
                                    <ul>
                                        <li>The website and materials must only be used for lawful purposes. Unauthorised login into another person’s account is not allowed. You must safeguard and promote the reputation of GSM and its services. You must refrain from all activities that may bring the name of GSM and its services into disrepute. </li>
                                        <li>The marketing of GSM, its opportunities and compensation plan must be void of all deceptive and misleading information. When you present GSM to others you should not make any misrepresentations or promises that are not contained originally in its business plan.</li>
                                        <li>You may not use GSM trade names/ Trademarks and Logos except to promote GSM. </li>
                                        <li>You may create your own marketing materials. Any marketing materials that use the name Global Success Minds or any of its logos, trademarks or trade names MUST be approved by GSM before they can be used. </li>
                                    </ul>
                                    <br />
                                    <h4 class="text-left"><span>PAYMENT OF REWARDS</span></h4>
                                    <hr class="small" />
                                    <ul>
                                        <li>You must be an active member and compliant with the Terms of the Agreement to qualify for rewards. So long as you comply with the terms of the Agreement, GSM shall pay rewards to you in accordance with the Rewards Plan.</li>
                                        <li>An ACTIVE member here refers to people who have registered and buy their airtime with GSM for the same registered number with a minimum of Ghc5.00</li>
                                        <li>Rewards would be paid at the end of every month.</li>
                                        <li>There is a 3% monthly deduction on the commission earned by members for the processing of reward payments. </li>
                                    </ul>
                                    <br />
                                    <h4 class="text-left"><span>VALUE ON TRANSACTIONS</span></h4>
                                    <hr class="small" />
                                    <ul>
                                        <li>The exact amount of airtime purchased shall be received by the buyer.</li>
                                        <li>The amount requested to be loaded on user wallets shall be loaded with the exact amount.</li>
                                        <li>There are NO CHARGES on wallet transactions.</li>
                                    </ul>
                                    <br />
                                    <h4 class="text-left"><span>SECURITY</span></h4>
                                    <hr class="small" />
                                    <p>
                                        Each member must keep their password and other secure access information confidential and notify GSM promptly if the member believes that the security of an account has been compromised. GSM has taken reasonable steps to protect the security of online transactions.
                                    </p>
                                    <p>However, GSM cannot and does not warrant such security and will not be liable for any losses or damages resulting from any security breaches.</p>
                                    <br />
                                    <h4><span>RANK AWARDS </span></h4>
                                    <hr class="small" />
                                    <ol>
                                        <li>Ranks shall be awarded to members who attain each rank requirement</li>
                                        <li>
                                            A member shall personally refer 5 upgrades as a primary requirement to attain ranks
                                        </li>
                                        <li>Rank awards shall be paid at the end of the preceding month.</li>
                                        <li>To attain more than one rank in a month, the user should have attained the requirements for each individual rank. </li>
                                        <li>Ranks are not skipped</li>
                                        <!-- <li>
              TEAM AIRTIME COMMISSIONS: This is the commission you earn for recommending or referring people to join and buy their airtime with GSM or retailing to them as a GSM distributor. Total amount of commission earning is dependent on team levels sales performance. That is: <b>Level 1: 0.10% ,</b> <b>Level 2: 0.15% ,</b> <b>Level 3: 0.20% ,</b> <b>Level 4: 0.25% ,</b> <b>Level 5:  0.30%</b> and <b>Level 6: 0.50%</b>            </li>
 -->
                                    </ol>
                                    <p class="text-center" style="color: #3b2057;">No one leg would contribute more than 40% of the total number of Premium members required to attain a rank. Eg. To hit the Silver Rank which requires 50 premium (upgraded) members, no one leg will contribute more than 10 Premium (upgraded) members in summing up to the 50 Premium (upgraded) members required to be awarded the Silver Rank. </p>
                                    <p>Leaders (members attaining ranks) have to train, encourage and assist their team members to attain ranks as well. <br>
                                        Therefore:
                                        <ol>
                                            <li>For <b>bronze</b> the number of premium members required is 10 plus 5 personal upgrades</li>
                                            <li>For <b>silver</b> there should be 2 bronze in your team</li>
                                            <li>For <b>pearl</b> there should be 2 silver in your team</li>
                                            <li>For <b>gold</b> there should be 2 pearl in your team</li>
                                            <li>For <b>sapphire</b> there should be 2 gold in your team</li>
                                            <li>For <b>diamond</b> there should be 2 sapphires in your team</li>
                                            <li>For <b>platinum</b> there should be 2 diamonds in your team</li>
                                            <li>For <b>platinum</b> sapphire there should be 2 platinum in your team</li>
                                            <li>For <b>president</b> there should be 2 platinum sapphire in your team</li>
                                        </ol>
                                    </p>
                                    <p style="color: #3b2057;" class="text-center"><b>NB:</b> Ranks and awards are vital motivation for members and a way to show appreciation for their hard work and to serve as a challenge to all members to aspire for the ultimate.</p>
                                    <h4 class="text-left"><span>ACKNOWLEDGEMENT</span></h4>
                                    <hr class="small" />
                                    <p>
                                        A Member acknowledges that he/she has read, understands and agrees to the terms set forth in this Agreement.
                                    </p>
                                </div>
                            </div>
                            <!-- QUOTEBOX -->
                            <div class="buildify_tm_section">
                                <div class="buildify_tm_quotebox">
                                    <div class="container">
                                        <div class="inner">
                                           <h3 class="text">Ready To Make Money ? <a href="{{url('register')}}"> Register Now</a></h3>
                                            <span class="pattern"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /QUOTEBOX -->
                        </div>
                    </div>
                    <!-- /CONTENT -->
                    <!-- FOOTER -->
                    <footer class="buildify_tm_footer">
                        <div class="buildify_tm_footer_wrap" id="company">
                            <div class="container">
                                <div class="buildify_tm_list_wrap" data-column="2" data-space="40">
                                    <ul class="buildify_list">
                                        <li>
                                            <div class="inner">
                                                <div class="footer_section_title">
                                                    <h3>About Company</h3>
                                                </div>
                                                <div class="definition">
                                                    <p>We are christened to break barriers in people’s lives and offer limitless opportunities. Our goal is to create a community of successful individuals enjoying financial freedom, a community where we train and assist each other and share ideas. There by cushioning each other en route success. With GSM, you are never left alone as you are shielded by a team of astute business minds. You become a member of a team of airtime sales people where our corporate staff is always available to train and assist you to become successful. Our community of like minded people, with you inclusive would contribute in changing the world and making it a better place than we met it through this novel opportunity of airtime sales.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="inner">
                                                <div class="footer_section_title">
                                                    <h3>Business Hours</h3>
                                                </div>
                                                <div class="definition">
                                                    <p>We are happy to meet you during our working hours. Please make an appointment.</p>
                                                </div>
                                                <div class="inner_list">
                                                    <ul>
                                                        <li>
                                                            <div class="wrap">
                                                                <span class="left">Monday-Saturday:</span>
                                                                <span class="right">8;30am to 6pm</span>
                                                            </div>
                                                        </li>
                                                        <!-- <li>
														<div class="wrap">
															<span class="left">Saturday:</span>
															<span class="right">10am to 3pm</span>
														</div>
													</li> -->
                                                        <li>
                                                            <div class="wrap">
                                                                <span class="left">Sunday:</span>
                                                                <span class="right">Closed</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- 	<li>
										<div class="inner">
											<div class="footer_section_title">
												<h3>Helpful Links</h3>
											</div>
											<div class="helpful_links">
												<div class="inner_list">
													<ul>
														<li><a href="#">Our services</a></li>
														<li><a href="#">Diclaimer</a></li>
														<li><a href="#">Showcase</a></li>
														<li><a href="#">Privacy Policy</a></li>
														<li><a href="#">Affliates</a></li>
													</ul>
												</div>
											</div>
										</div>
									</li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="buildify_tm_copyright">
                                <div class="container">
                                    <div class="qwe">
                                        <p>&copy; 2014 - 2019 Copyright Global Success Minds. All Rights Reserved.<a href="{{url('/termsandconditions')}}">Terms & Conditions</a> </p>
                                        <div class="totop">
                                            <span>To Top</span>
                                            <a class="buildify_tm_totop" href="#"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!-- /FOOTER -->
                </div>
            </div>
            <!-- /RIGHTPART -->
        </div>
    </div>
    <!-- / WRAPPER ALL -->
    <!-- SCRIPTS -->
<script type="text/javascript" src="{{asset('website/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/plugins.js')}}"></script>
<!--[if lt IE 10]> <script type="text/javascript" src="{{asset('js/ie8.js')}}"></script> <![endif]-->   
<script type="text/javascript" src="{{asset('website/js/init.js')}}"></script>
<!-- /SCRIPTS -->
    </div>
    <script type="text/javascript">function add_chatinline(){var hccid=64164170;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
</body>
<!-- Mirrored from frenify.com/envato/marketify/html/buildify/1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Jan 2019 17:00:53 GMT -->

</html>
<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{url('dashboard')}}">GSM</a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

			</ul>

			<p class="navbar-text">
			@if(Auth::user()->paymentstatus == 1)
			<span class="label bg-success-400">Status:Active</span>
			@else
			<span class="label bg-danger-400">Status:Inactive</span>
			@endif
			</p>

			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" >
					@if(Auth::user()->profile_image != null)
					<img src="{{asset(Auth::user()->profile_image)}}" alt="" class="" >
					@else	
					<img src="{{asset('assets/images/default_large.png')}}" alt="" class="" >
					@endif
					<span>{{Auth::user()->firstname}}</span>
					<i class="caret"></i>
					</a>

					
					<ul class="dropdown-menu dropdown-menu-right">
						
					<!-- 	<li class="divider"></li>
						<li><a href="{{url('/')}}"><i class="icon-user-plus"></i> My profile</a></li>
						<li class="divider"></li> -->
						<li><a href="{{url('changepassword')}}"><i class="icon-cog5"></i>Change Password</a></li>
						<li class="divider"></li>
						<li><a href="{{url('logout')}}"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
</div>

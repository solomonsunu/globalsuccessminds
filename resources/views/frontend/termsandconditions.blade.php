<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Global Success Minds</title>
  <link rel="shortcut icon" href="./images/favicon.png" />
  <!-- GOOGLE WEB FONTS -->
  <!-- <link href='../../../fonts.googleapis.com/css_f7b4c15e.css' rel='stylesheet' type='text/css' />
  <link href='../../../fonts.googleapis.com/css_a1b8c998.css' rel='stylesheet' type='text/css' />
   --><!-- END OF GOOGLE WEB FONTS -->

  <!-- BOOTSTRAP & STYLES -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/block_grid_bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/typicons.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/odometer-theme-default.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slider-pro.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/animate-custom.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slicknav.min.css')}}" rel="stylesheet" />
  <link href="{{asset('style.css')}}" rel="stylesheet" />
  <!-- END OF BOOTSTRAP & STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
  <!-- HEADER -->

  <div id="hw-hero">
    <!-- HEADER -->

    <div class="header header2">
      <!-- TOP BAR -->
      <div class="topbar">
        <div class="row">
          <div class="col-sm-6">
            <ul class="phone">
              <li><span class="typcn typcn-location-outline"></span> 7 Anderson Close, Adenta Barrier. Accra</li>
              <li><span class="typcn typcn-phone-outline"></span>030-254-1284 </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="toplinks">
              <li>MON - FRI 09:00 - 20:00</li>
              <li>E-MAIL US: info@globalsuccessminds.com</li>
             
            </ul>
          </div>
        </div>
      </div>
      <!-- END OF TOP BAR -->

      
      <!-- LOGO & NAVIGATION -->
      <div class="header-navigation">
      <div class="row">
        <div class="col-sm-12">
          <div class="logonav">
            <div class="row no-gutter">
              <div class="col-sm-2">
                <div class="logo">
                  <a href="./index.html"><img src="./images/logo1.png" alt="" /></a>
                </div>
              </div>
              <div class="col-sm-10">
                <nav id="desktop-menu">
                <ul class="sf-menu" id="navigation">
                    <li class="current"><a href="{{url('/')}}">Home</a>
                      
                    </li>
                    <li><a href="{{url('/#company')}}">Company</a>
                      
                    </li>
                    <li><a href="{{url('/#opportunity')}}">Opportunity</a>
                      
                    </li>
                    <li><a href="{{url('productsandservices')}}">Products and Services</a>
                      
                    </li>

                    <li><a href="{{url('compensationplan')}}">Compensation Plan</a>
                      
                    </li>

                    <li><a href="{{url('/#contactus')}}">Contact</a></li>
                    <li><a href="{{url('login')}}">Login</a>
                      
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- END OF LOGO & NAVIGATION -->
    </div>
    <!-- END OF HEADER -->
 <!-- PAGE HEADER -->
    <div id="page-header">
   <div class="title-breadcrumbs">
   <h1>TERMS AND CONDITIONS</h1>
    <h4 style="color: white">Signing up signifies your agreement to adhere to the following terms and conditions governing GLOBAL SUCCESS MINDS (GSM).</h4>
   </div>

    </div>
    <!-- END OF PAGE HEADER -->
  </div>

  <!-- SERVICES INTRO -->
  <section class="services2">
    <div class="row">
      <div class="col-md-6">
        
            <h4 class="text-left"><span>CHANGES TO BUSINESS PLAN AND COMMISSIONS</span></h4>
              <li>GSM may from time to time make changes to its benefits and opportunities, services, sign-up, marketing plan and incentives. All members will be updated as regards to any such changes via posting updates on their dashboard, email or SMS as per the email address or mobile phone number provided in their account details. Each member is therefore required to ensure that the email address and phone numbers they provide are valid, and that they check their emails regularly for the latest updates and accordingly, go by any such changes.
              </li>
            <br />

            <h4 class="text-left"><span>REQUIREMENTS TO BECOME A MEMBER</span></h4>
            <ul>
              <li>Each member is independent and is responsible for his or her own business and cannot bind the company to any obligations. Hence a member will not be treated as an employee.
              
              </li>
              <li>A registered phone number is required to register on the platform.</li>
              <li>There is NO SIGN-UP FEE to become a member. </li>
             
              <li>NO hidden payments, charges or cost of any kind to members.</li>
              <li>You agree that any updates, notifications and announcements about our programs and other services will be sent to you via email, SMS and or on your Dashboard.</li>
            </ul>
            <br />

            <h4 class="text-left"><span>CONDUCT OF MEMBERS</span></h4>
            <ul>
              <li>The website and materials must only be used for lawful purposes. Unauthorised login into another person’s account is not allowed. You must safeguard and promote the reputation of GSM and its services. You must refrain from all activities that may bring the name of GSM and its services into disrepute. </li>
              <li>The marketing of GSM, its opportunities and compensation plan must be void of all deceptive and misleading information. When you present GSM to others you should not make any misrepresentations or promises that are not contained originally in its business plan.</li>
              <li>You may not use GSM trade names/ Trademarks and Logos except to promote GSM. </li>
              <li>You may create your own marketing materials. Any marketing materials that use the name Global Success Minds or any of its logos, trademarks or trade names MUST be approved by GSM before they can be used. </li>

            </ul>

            <br />
           
      </div>
      <div class="col-md-6">
         <h4 class="text-left"><span>PAYMENT OF REWARDS</span></h4>
            <ul>
              <li>You must be an active member and compliant with the Terms of the Agreement to qualify for rewards. So long as you comply with the terms of the Agreement, GSM shall pay rewards to you in accordance with the Rewards Plan.</li>
              <li>An ACTIVE member here refers to people who have registered and buy their airtime with GSM for the same registered number with a minimum of Ghc5.00</li>
              <li>Rewards would be paid at the end of every month.</li>
              <li>There is a 3% monthly deduction on the commission earned by members for the processing of reward payments.      </li>
            </ul>

            
            <br />
            <h4 class="text-left"><span>VALUE ON TRANSACTIONS</span></h4>
            <ul>
              <li>The exact amount of airtime purchased shall be received by the buyer.</li>
              <li>The amount requested to be loaded on user wallets shall be loaded with the exact amount.</li>
              <li>There are NO CHARGES on wallet transactions.</li>
            </ul>

            <br />
            
            <h4 class="text-left"><span>SECURITY</span></h4>
              <p>
                Each member must keep their password and other secure access information confidential and notify GSM promptly if the member believes that the security of an account has been compromised. GSM has taken reasonable steps to protect the security of online transactions.

              </p>
              <p>However, GSM cannot and does not warrant such security and will not be liable for any losses or damages resulting from any security breaches.</p>
              <br />
            <h4 class="text-left"><span>ACKNOWLEDGEMENT</span></h4>
            <p>
              A Member acknowledges that he/she has read, understands and agrees to the terms set forth in this Agreement. 
            </p>
            
      </div>
      
      </div>
    <div class="row">
        <div class="col-md-12">
          <h2><span>RANK AWARDS </span></h2>
            <hr class="small" />
          <ol>
            <li>Ranks shall be awarded to members who attain each rank requirement</li>
            <li>
             A member shall personally refer 5 upgrades as a primary requirement to attain ranks
            </li>
            <li>Rank awards shall be paid at the end of the preceding month.</li>
            <li>To attain more than one rank in a month, the user should have attained the requirements for each individual rank. </li>
            <li>Ranks are not skipped</li>


            <!-- <li>
              TEAM AIRTIME COMMISSIONS: This is the commission you earn for recommending or referring people to join and buy their airtime with GSM or retailing to them as a GSM distributor. Total amount of commission earning is dependent on team levels sales performance. That is: <b>Level 1: 0.10% ,</b> <b>Level 2: 0.15% ,</b> <b>Level 3: 0.20% ,</b> <b>Level 4: 0.25% ,</b> <b>Level 5:  0.30%</b> and <b>Level 6: 0.50%</b>            </li>
 -->

          </ol>
          <p class="text-center" style="color: #3b2057;">No one leg would contribute more than 40% of the total number of Premium members required to attain a rank. Eg. To hit the Silver Rank which requires 50 premium (upgraded) members, no one leg will contribute more than 10 Premium (upgraded) members in summing up to the 50 Premium (upgraded) members required to be awarded the Silver Rank. </p>

          <p>Leaders (members attaining ranks) have to train, encourage and assist their team members to attain ranks as well. <br>
            Therefore:

            <ol>
              <li>For <b>bronze</b> the number of premium members required is 10 plus 5 personal upgrades</li>
              <li>For <b>silver</b> there should be 2 bronze in your team</li>
              <li>For <b>pearl</b> there should be 2 silver in your team</li>
              <li>For <b>gold</b> there should be 2 pearl in your team</li>
              <li>For <b>sapphire</b> there should be 2 gold in your team</li>
              <li>For <b>diamond</b> there should be 2 sapphires in your team</li>
              <li>For <b>platinum</b> there should be 2 diamonds in your team</li>
              <li>For <b>platinum</b> sapphire there should be 2 platinum in your team</li>
              <li>For <b>president</b> there should be 2 platinum sapphire in your team</li>
            </ol>
          </p>


          <p style="color: #3b2057;" class="text-center"><b>NB:</b> Ranks and awards are vital motivation for members and a way to show appreciation for their hard work and to serve as a challenge to all members to aspire for the ultimate.</p>
          
        </div>
       
        
     
    </div>
  </section>

 
<div class="spacing-45"></div>
  <!-- FOOTER -->
     <!-- FOOTER -->
  <section class="footer">

    <div class="footer-elements">
      <div class="row">
        <div class="col-sm-12">
          <div class="row no-gutter">

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-location-outline"></span>
                <p>Mission<span>To create a community of limitless opportunities through individual partnerships, desirable products and services and irresistible rewards, an avenue where dreams become realities.
                  </span></p>
              </div>
            </div>
            <div class="col-sm-2">
              
            </div>
            <!-- FOOTER TOP ELEMENT -->

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-watch"></span>
                <p>Vision<span><a href="./index2.html">As a global player, we envisage leading a team that will be the architects of designing the needed change in the world through collective efforts, creativity and innovation. </p>
</a></span></p>
              </div>
            </div>
            <!-- FOOTER TOP ELEMENT -->

            
          </div>
        </div>
      </div>
    </div>

    <div class="footer-widgets">
      <div class="row">

        

        

       

        

      </div>
    </div>
  </section>
  <!-- END OF FOOTER -->

  <div class="copyright">
    <div class="row">
      <div class="col-sm-8">
        <p class="text-center">Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> 
        
      </p>
      </div>
      <div class="col-sm-4">
        <p class="text-center">
        <a href="https://www.facebook.com/Global-Success-Minds-Airtime-For-Cash-963597237021542/" style="margin-right:20px;">
          <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://twitter.com/gsmairtime" style="margin-right:20px;">
          <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://www.youtube.com/channel/UCeOhFXLKuagDY2LZwQA6tDQ" style="margin-right:20px;">
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </a>  
      </p>
      </div>

    </div>
  </div>

  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- JAVASCRIPT FILES -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.hoverIntent.js')}}"></script>
  <script src="{{asset('js/superfish.min.js')}}"></script>
  <script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/odometer.min.js')}}"></script>
  <script src="{{asset('js/waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
  <script src="{{asset('js/wow.min1.js')}}"></script>
  <script src="{{asset('js/retina.min.js')}}"></script>
  <script src="{{asset('js/custom1.js')}}"></script>
  <!-- END OF JAVASCRIPT FILES -->

</body>
</html>

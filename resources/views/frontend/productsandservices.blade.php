<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Global Success Minds</title>
  <link rel="shortcut icon" href="./images/favicon.png" />
  <!-- GOOGLE WEB FONTS -->
  <!-- <link href='../../../fonts.googleapis.com/css_f7b4c15e.css' rel='stylesheet' type='text/css' />
  <link href='../../../fonts.googleapis.com/css_a1b8c998.css' rel='stylesheet' type='text/css' />
   --><!-- END OF GOOGLE WEB FONTS -->

  <!-- BOOTSTRAP & STYLES -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/block_grid_bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/typicons.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/odometer-theme-default.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slider-pro.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/animate-custom.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slicknav.min.css')}}" rel="stylesheet" />
  <link href="{{asset('style.css')}}" rel="stylesheet" />
  <!-- END OF BOOTSTRAP & STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
  <!-- HEADER -->

  <div id="hw-hero">
    <!-- HEADER -->

    <div class="header header2">
      <!-- TOP BAR -->
      <div class="topbar">
        <div class="row">
          <div class="col-sm-6">
            <ul class="phone">
              <li><span class="typcn typcn-location-outline"></span> 30 Abese loop Aviation down Adenta, Accra</li>
              <li><span class="typcn typcn-phone-outline"></span>030-254-1284 </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="toplinks">
              <li>MON - FRI 09:00 - 20:00</li>
              <li>E-MAIL US: info@globalsuccessminds.com</li>
             
            </ul>
          </div>
        </div>
      </div>
      <!-- END OF TOP BAR -->

      
      <!-- LOGO & NAVIGATION -->
      <div class="header-navigation">
      <div class="row">
        <div class="col-sm-12">
          <div class="logonav">
            <div class="row no-gutter">
              <div class="col-sm-2">
                <div class="logo">
                  <a href="./index.html"><img src="./images/logo1.png" alt="" /></a>
                </div>
              </div>
              <div class="col-sm-10">
                <nav id="desktop-menu">
                <ul class="sf-menu" id="navigation">
                    <li><a href="{{url('/')}}">Home</a>
                      
                    </li>
                    <li><a href="{{url('/#company')}}">Company</a>
                      
                    </li>
                    <li><a href="{{url('/#opportunity')}}">Opportunity</a>
                      
                    </li>
                    <li class="current"><a href="{{url('productsandservices')}}">Products and Services</a>
                      
                    </li>

                    <li><a href="{{url('compensationplan')}}">Compensation Plan</a>
                      
                    </li>

                    <li><a href="{{url('contactus')}}">Contact</a></li>
                    <li><a href="{{url('login')}}">Login</a>
                      
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- END OF LOGO & NAVIGATION -->
    </div>
    <!-- END OF HEADER -->
 <!-- PAGE HEADER -->
    <div id="page-header">
   <div class="title-breadcrumbs">
   <h1>PRODUCTS & SERVICES</h1>
   
   </div>

    </div>
    <!-- END OF PAGE HEADER -->
  </div>

  <!-- SERVICES INTRO -->
  <section class="services2">
    <div class="row">
      <div class="col-md-8">
        <h2 class="text-left"><span>Airtime</span></h2>
        <!-- <h5>SERVICES FOR STARTUPS AND SMALL BUSINESSES</h5> -->
        <p>
        Airtime is a predominant product used every day with limitless market. Imagine making money from something you use every day. The telecommunication industry is a multimillion one settling on the large number of mobile phone users and the enormous amount of money spent on airtime every month. There are over 33 million mobile phone users in Ghana with over 300 million Ghana cedis spent on airtime every month. With GSM, you paid commissions on all airtime sales, exciting? The GSM Airtime for Cash platform enables our customers become advocate customers where every consumer also sells airtime. 
        </p>
        <!-- <h2 class="text-left">Personal Accident <span> Insurance Cover</span></h2>
      <p>
        
        We provide our partners, distributors and customers with good value insurance products and services that they need and reward them on those products that result from their own purchases and referral sales. The best sales person of course is a satisfied loyal customer.


        </p>
       <blockquote class="blockquote-colored">
      <p>
      With GSM, you earn income as you subscribe and sale insurance, exciting? We are committed to ensuring that our marketers, distributors and customers make money as they concentrate on building their teams and growing their business crowned with whiles the risk of running into an accident is taken care of by GSM. 

      The insurance will be renewed every month. Members earn income as they renew and sell insurance to their referrals each month.
      </p>
    </blockquote> -->


      </div>

       <div class="col-md-4">
        <div class="image-padding-shadow"><img src="images/ps.jpg" alt="" /></div>
          <!-- <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Benefits</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Accidental death</td>
                <td>GHC 2,000</td>
              </tr>
              <tr>
                <td>Permanent disability due to accident</td>
                <td>GHC 1,000</td>
              </tr>

              <tr>
                <td>Hospitalization due to accident</td>
                <td>Up to GHC 500</td>
              </tr>
            </tbody>
           </table> -->
      </div>
      </div>
  </section>

 
<div class="spacing-45"></div>
  <!-- FOOTER -->
     <!-- FOOTER -->
  <section class="footer">

    <div class="footer-elements">
      <div class="row">
        <div class="col-sm-12">
          <div class="row no-gutter">

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-location-outline"></span>
                <p>Mission<span>To create a community of limitless opportunities through individual partnerships, desirable products and services and irresistible rewards, an avenue where dreams become realities.
                  </span></p>
              </div>
            </div>
            <div class="col-sm-2">
              
            </div>
            <!-- FOOTER TOP ELEMENT -->

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-watch"></span>
                <p>Vision<span><a href="./index2.html">As a global player, we envisage leading a team that will be the architects of designing the needed change in the world through collective efforts, creativity and innovation. </p>
</a></span></p>
              </div>
            </div>
            <!-- FOOTER TOP ELEMENT -->

            
          </div>
        </div>
      </div>
    </div>

    <div class="footer-widgets">
      <div class="row">

        

        

       

        

      </div>
    </div>
  </section>
  <!-- END OF FOOTER -->

  <div class="copyright">
    <div class="row">
      <div class="col-sm-8">
        <p class="text-center">Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> 
        
      </p>
      </div>
      <div class="col-sm-4">
        <p class="text-center">
        <a href="https://www.facebook.com/Global-Success-Minds-Airtime-For-Cash-963597237021542/" style="margin-right:20px;">
          <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://twitter.com/gsmairtime" style="margin-right:20px;">
          <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://www.youtube.com/channel/UCeOhFXLKuagDY2LZwQA6tDQ" style="margin-right:20px;">
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </a>  
      </p>
      </div>

    </div>
  </div>

  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- JAVASCRIPT FILES -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.hoverIntent.js')}}"></script>
  <script src="{{asset('js/superfish.min.js')}}"></script>
  <script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/odometer.min.js')}}"></script>
  <script src="{{asset('js/waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
  <script src="{{asset('js/wow.min1.js')}}"></script>
  <script src="{{asset('js/retina.min.js')}}"></script>
  <script src="{{asset('js/custom1.js')}}"></script>
  <!-- END OF JAVASCRIPT FILES -->

</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Global Success Minds</title>
  <!-- <link rel="shortcut icon" href="./images/favicon.png" /> -->
  <!-- GOOGLE WEB FONTS -->
  <!-- <link href='../../../fonts.googleapis.com/css_f7b4c15e.css' rel='stylesheet' type='text/css' />
  <link href='../../../fonts.googleapis.com/css_a1b8c998.css' rel='stylesheet' type='text/css' /> -->
  <!-- END OF GOOGLE WEB FONTS -->

  <!-- BOOTSTRAP & STYLES -->
  
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/block_grid_bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/typicons.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/odometer-theme-default.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slider-pro.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/animate-custom.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slicknav.min.css')}}" rel="stylesheet" />
  <link href="{{asset('style.css')}}" rel="stylesheet" />
  <!-- END OF BOOTSTRAP & STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
  <!-- HEADER -->

    <div class="header header2">
      <!-- TOP BAR -->
      <div class="topbar">
        <div class="row">
          <div class="col-sm-6">
            <ul class="phone">7 Anderson Close, Adenta Barrier. Accra</li>
              <li><span class="typcn typcn-phone-outline"></span>030-254-1284 / 0303964373  </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="toplinks">
              <li>MON - SAT 08:30 - 18:00</li>
              <li>E-MAIL US: info@globalsuccessminds.com</li>

            </ul>
          </div>
        </div>
      </div>
      <!-- END OF TOP BAR -->

      
      <!-- LOGO & NAVIGATION -->
      <div class="header-navigation">
      <div class="row">
        <div class="col-sm-12">
          <div class="logonav">
            <div class="row no-gutter">
              <div class="col-sm-2">
                <div class="logo">
                  <a href="./index.html"><img src="images/logo1.png" alt="" /></a>
                </div>
              </div>
              <div class="col-sm-10">
                <nav id="desktop-menu">
                <ul class="sf-menu" id="navigation">
                    <li class="current"><a href="{{url('/')}}">Home</a>
                      
                    </li>
                    <li><a href="{{url('/#company')}}">Company</a>
                      
                    </li>
                    <li><a href="{{url('/#opportunity')}}">Opportunity</a>
                      
                    </li>
                    <li><a href="{{url('productsandservices')}}">Products and Services</a>
                      
                    </li>

                    <li><a href="{{url('compensationplan')}}">Compensation Plan</a>
                      
                    </li>

                    <li><a href="{{url('contactus')}}">Contact</a></li>
                     @if(Auth::user())
                        <li><a href="{{url('/dashboard')}}">Dashboard</a>
                     @else
                         <li><a href="{{url('login')}}">Login</a>
                     @endif
                   
                    
                      
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- END OF LOGO & NAVIGATION -->
    </div>
    <!-- END OF HEADER -->

      <!-- SLIDER -->
<div class="slider-pro" id="index-slider2">
    <div class="sp-slides">
        <!-- Slide 1 -->
        <div class="sp-slide">
            <img class="sp-image" src="images/5.png" alt="" />
            <!-- <div class="hw-the-slider-content">
              <div class="row">
                <div class="col-sm-12">
                  <div class="hw-the-slider-container">
                    <div class="hw-the-slider-captions sp-layer" data-show-transition="up" data-hide-transition="down" data-show-delay="800" data-hide-delay="800" data-stay-duration="5500">
                    <div class="title-frame orange">
                    <span class="typcn typcn-chart-line"></span>
                      <h2>Live Your Dream</h2>
                      <hr class="small" />
                      <p>We are a team of network marketers with the main objective of reducing the rate of unemployment in our society and attaining financial independence. We believe everyone should have a fair share, live a normal and responsible life and this can be achieved by creating a reliable and sustainable global on-line business opportunity for all in search of life changing opportunities.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
        </div>
         <!-- End of Slide 1 -->
        <!-- Slide 2 -->
        <div class="sp-slide">
             <img class="sp-image" src="images/img2.jpg" alt="" />
             <div class="hw-the-slider-content">
              <!-- <div class="row">
                <div class="col-sm-12">
                  <div class="hw-the-slider-container">
                    <div class="hw-the-slider-captions sp-layer" data-show-transition="down" data-hide-transition="right" data-show-delay="800" data-hide-delay="800" data-stay-duration="5500">
                    <div class="title-frame orange">
                    <span class="typcn typcn-chart-area"></span>
                      <h2>Who We Are</h2>
                      <hr class="small" />
                      <p>Global Success Minds is a network of enthusiastic and determined individuals with a strong desire to effect change in the world through entrepreneurship and wealth creation. As a Multi – Level Marketing (MLM) company, made up of positive – minded people connected from across the globe working as a team to secure financial freedom and pay our worth for our individual and collective efforts, we will dream, work together and grow as a team. The World’s richest individuals share a common assertion that it is not about the job and salary, but a life changing opportunity. The successful ones are those who seize the moment to make good the opportunities that come their way.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
        </div>
          <!-- End of Slide 2 -->
        <!-- Slide 3 -->
        <div class="sp-slide">
             <img class="sp-image" src="images/6.jpg" alt="" />
           
        </div>
        <!-- End of Slide 3 -->
        <!-- Slide 4 -->
        <div class="sp-slide">
           <img class="sp-image" src="images/7.jpg" alt="" />
           <!--  <div class="hw-the-slider-content">
          <div class="row">
            <div class="col-sm-12">
              <div class="hw-the-slider-container">
                <div class="hw-the-slider-captions sp-layer" data-show-transition="left" data-hide-transition="up" data-show-delay="800" data-hide-delay="800" data-stay-duration="5500">
                <div class="title-frame orange">
                <span class="typcn typcn-adjust-brightness"></span>
                   <h2>Live Your Dream</h2>
                      <hr class="small" />
                      <p>We are a team of network marketers with the main objective of reducing the rate of unemployment in our society and attaining financial independence. We believe everyone should have a fair share, live a normal and responsible life and this can be achieved by creating a reliable and sustainable global on-line business opportunity for all in search of life changing opportunities.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        </div>
        <div class="sp-slide">
           <img class="sp-image" src="images/8.jpg" alt="" />
         
        </div>
        <div class="sp-slide">
           <img class="sp-image" src="images/9.jpg" alt="" />
            
        </div>
         <!-- End of Slide 4 -->
        
    </div>
</div>
 <!-- END OF SLIDER -->

  <!-- FEATURES -->
  
  <!-- END OF FEATURES -->

  <!-- PROJECTS -->
  <section class="projects">
    <div class="row">
      <div class="col-sm-12">
           <center><h1><span>Join the airtime business with GSM and make money from airtime sales</span></h1></center>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <h2>JUST <span>IMAGINE</span></h2>
        <p class="text-center">GSM Airtime for Cash is a unique opportunity for you to make money from airtime sales. How does it work? Sign up to GSM Airtime for Cash for free and become our independent airtime distributor: load your wallet with any amount to start selling airtime for any network (Airtel, Expresso, Glo, MTN, Tigo and Vodafone). GSM  has exciting commissions as an incentive package for meeting sales targets.  

          This is a simple and easy business you can engage in to earn extra cash. You can do this business by selling airtime to your family, friends and co workers. What are your expectations in life? Extra money, freedom, security, and quality time for family, friends and the activities you love? Then first you would need to embrace this financial truth. There’s a limit to how much you can save but there’s no limit to how much you can earn. 
          <h2><span>How to get started:</span></h2>
          
          Click on the Get Started Now button below. Fill the registration form and submit. After a successful registration, log into your page to view your pin. 
          Load your wallet by sending the amount to any of these mobile money numbers or at any of these banks or with our authorised agents. <br>
         
              
         
        </p>
        <div class="row">
          <div class="col-md-6">
            <ol>
              <li><b>Airtel: A97600 (code)</b></li>
              <li><b>Mtn:  0549398254</b></li>
              <li><b>Tigo: 0271099670</b></li>
              <li><b>Vodafone: 109441 (till)</b></li>
            </ol>
          </div>
           <div class="col-md-6">
              <ol>
                <li><b>UT Bank 0011312086017</b></li>  
                <li><b>Beige Capital 0280126876001</b> </li> 
              </ol>
          </div>
        </div>

        <br>
        <br>
        <br>

        <div class="calltoaction home3 cta-button">
         <a href="{{url('register')}}" class="hw-btn"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Click Here To Register<!-- <span>FREE CONSULTATION FOR THE FIRST MONTH (450$ VALUE)</span> --></a>
        </div>
        <br>
         <div class="row">
          <div class="col-md-12">
            <h2><span>GSM AGENT</span></h2>
            <p class="text-center">
              GSM offers the opportunity of starting airtime sales business either at home, office,
              hall of residence, hostel, school, a shop or by the road. It is that easy and simple; all
              you need is a phone and a simple card. No start up capital is required.
              Agents earn 4% instant commission on airtime sales. Agent account cannot however
              be used to refer others to join GSM. Agents who wish to earn Team Airtime
              Commissions for referring people has to register a regular account as well. It is the
              regular account that can be used to refer people to join GSM.
              To become an agent all you need to do is register, load your GSM wallet and start
              selling airtime to all networks 24/7.
            </p>
          </div>
          <br>
          <br>

          <div class="calltoaction home3 cta-button">
           <a href="{{url('register/agent')}}" class="hw-btn"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Click Here For Agent Registration<!-- <span>FREE CONSULTATION FOR THE FIRST MONTH (450$ VALUE)</span> --></a>
          </div>
          <br>
           
        </div>


      </div>
    </div>

    
  </section>
<!-- END OF PROJECTS -->

  <section class="testimonials home2">
    <div class="row">
         <div class="col-sm-12">
            GSM Airtime for cash allows you to directly recharge on all networks in Ghana at a 24/7 absolute convenience whilst being paid commissions on sales you make on all networks. Load Your GSM Cash Wallet to ensure an easy way to recharge and sell on all networks with a click. The ease, convenience and profitability make it the best means for airtime recharge. 
            
         </div>
         <div class="col-sm-4">
           <!-- <h4><span>How to Check the Money on your Wallet</span> </h4>
                        
             *161*444*500*PIN# -->

         </div>
         <div class="col-sm-6">
           <h2><span> Dial *161*444# on all networks</span> </h2>
             <ul>
               <li>To Buy Airtime</li>
               <li>To Check the Money on your GSM Cash Wallet </li>
               <li>To Change your PIN</li>
               <li>To transfer Money from your Wallet to Others</li>
             </ul>
         </div>
         <div class="col-sm-2">
           <!-- <h4><span>How to buy Airtime</span> </h4>
            *161*444*Amount*Phone Number*PIN# -->
         </div>
    </div>
    
  </section>
  <!-- TESTIMONIALS -->
  <section class="testimonials home2">
    <div class="row">
        <div class="col-md-4">
          <h2>AIRTIME 4 CASH  <span> PRESENTS FOUR (4)</span> STREAMS OF INCOME</h2>
            <hr class="small" />
        </div>
        <div class="col-md-2"></div>
        <div class="col-sm-6">
          <div style="margin-top:40px;">
            <ol>
               <li>DIRECT AIRTIME COMMISSIONS</li>
               <li>TEAM AIRTIME COMMISSIONS </li>
               <li>DIRECT UPGRADE COMMISSIONS</li>
               <li>TEAM UPGRADE COMMISSIONS</li>
             </ol>
          </div>
        </div>
     
    </div>
    <div class="row">
      <div class="col-md-12">
          <h2><span> ENJOY ENORMOUS BENEFITS BY BECOMING A PREMIER MEMBER </span> </h2>
          <p>
            Members are given the option at their back office to upgrade to become GSM premier distributors
          </p>
          <div class="row">
            <div class="col-md-6">
            <h3><span>UPGRADE PACKAGE </span> </h3>
            <ol>
              <li>Receive GSM branded watch</li>
              <li>Recieve Ghc5 in your wallet for airtime purchase</li>
              <li>Hit the first rank: GSM premier distributor.</li>
              <li>Enjoy upgrade commissions</li>
              <li>Advance GSM leadership ranks</li>
            </ol>

             <h3><span>UPGRADE REQUIREMENT </span> </h3>
            <ol>
              <li>Load your wallet with Ghc50.00</li>
              <li>Login to your back office </li>
              <li>Click on upgrade</li>
              <li>Ghc45 will be deducted from your wallet</li>
              
            </ol>
            <h3><span>GSM LEADERSHIP RANKS </span> </h3>
            <p>Members are rewarded as they advance the leadership ranks. <br>
                RANK REWARDS ARE MEANT TO INCENTIVISE AND MOTIVATE MEMBERS FOR THEIR PERFORMANCE. <br>
                <a href="images/gsm.pdf" target="_blank">(click here to view ranks and the rewards package) </a></p>
            


          </div>
          <div class="col-md-6">
            <img class="img img-responsive" src="images/gsmwatch.jpg" alt="" />
          </div>
          </div>
          
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <h2>WHAT <span>OUR MEMBERS</span> HAVE TO SAY</h2>
        <hr class="small" />
        <!-- <p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users.</p> -->
      </div>

      <div class="col-sm-8">
        <div id="customers-testimonials" class="owl-carousel">

          <!--TESTIMONIAL 1 -->
          <div class="item">
            <div class="shadow-effect">
              <p>Life on campus could get awful sometimes, deeming it vital to supplement the money your parents give you. GSM airtime for cash offers such an enviable opportunity to make money as a student just by joining the airtime business.</p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="./images/testi6.jpg" alt="" />
              <span class="name">Joshua</span>
           </div>
          </div>
          <!--END OF TESTIMONIAL 1 -->
          <!--TESTIMONIAL 2 -->
          <div class="item">
            <div class="shadow-effect">
              <p>
              I have become an advocate for earning extra income aside your salary. It creates a sound financial freedom. Using my phone to make money by selling airtime to my church members and family is very easy and totally worth it. My family is well taken care of and I sleep better now. I couldn’t have done this without Global Success Minds Airtime for cash.
              </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/testi8.jpg" alt="" />
              <span class="name">Aunty Pomaa</span>
              
           </div>
          </div>
          <!--END OF TESTIMONIAL 2 -->
          <!--TESTIMONIAL 3 -->
          <div class="item">
            <div class="shadow-effect">
              <p>
              Increasingly and steadily my financial goals are being met, I took that decision to join GSM Airtime for Cash business, that gave me the opportunity to be my own boss and be a partner, it has been a life changing experience for me.
              </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/testi4.jpg" alt="" />
              <span class="name">Daniel Osei Tuffour</span>
              <!-- <span class="company">CHAIRMAN AT RCL INC.</span> -->
           </div>
          </div>
          <!--END OF TESTIMONIAL 3 -->
          <!--TESTIMONIAL 4 -->
          <div class="item">
            <div class="shadow-effect">
              <p>
              I love all the fine things I see .This comes with great expense and yes my job pays well but it could never give me the financial security I craved, with global success minds airtime for cash I make money from airtime sales: am on my way to living my dream.
              </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/testi2.jpg" alt="" />
              <span class="name">Queen Talata</span>
              <!-- <span class="company">FOUNDER OF INDEX SA</span> -->
           </div>
          </div>

          <div class="item">
            <div class="shadow-effect">
              <p>

              I was sceptical initially but I am glad I joined the airtime for cash business. I get to leverage on the many friends I have in real life and even on social media to make money through airtime sales.
              </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/testi5.jpg" alt="" />
              <span class="name">Antonio</span>
              <!-- <span class="company">FOUNDER OF INDEX SA</span> -->
           </div>
          </div>

            <div class="item">
            <div class="shadow-effect">
              <p>

             
             If am making this much doing this as part time, then I wonder the returns on a full time basis.
               </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/t10.jpg" alt="" />
              <span class="name">Akosua Moore</span>
              <!-- <span class="company">FOUNDER OF INDEX SA</span> -->
           </div>
          </div>
          <div class="item">
            <div class="shadow-effect">
              <p>

             This is incredibly amazing!!! After over ten years of using a phone and buying airtime on the streets which earned me nothing. Never did I imagine the airtime business had such awesome returns. GSM airtime for cash! I will sell saaaaaa….
               </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/t9.jpg" alt="" />
              <span class="name">Kanda</span>
              <!-- <span class="company">FOUNDER OF INDEX SA</span> -->
           </div>
          </div>

          <div class="item">
            <div class="shadow-effect">
              <p>

             
              Buying airtime with GSM has become a way of life for me, my family and my friends because we get paid for buying airtime.
               </p>
            </div>
            <div class="testimonial-name home2">
              <img class="img-circle" src="images/testi7.jpg" alt="" />
              <span class="name">Alhaji Alhassan</span>
              <!-- <span class="company">FOUNDER OF INDEX SA</span> -->
           </div>
          </div>
           
          <!--END OF TESTIMONIAL 4 -->
          
        </div>
      </div>
    </div>
  </section>
  <!-- END OF TESTIMONIALS -->

  <!-- CALL TO ACTION -->
  <section class="calltoaction" id="company">
    <div class="row">
      <div class="col-sm-12">
        <h2><span>COMPANY</span></h2>
        <p class="text-center">
We are christened to break barriers in people’s lives and offer limitless opportunities. Our goal is to create a community of successful individuals enjoying financial freedom, a community where we train and assist each other and share ideas. There by cushioning each other en route success. With GSM, you are never left alone as you are shielded by a team of astute business minds. You become a member of a team of airtime sales people where our corporate staff is always available to train and assist you to become successful. Our community of like minded people, with you inclusive would contribute in changing the world and making it a better place than we met it through this novel opportunity of airtime sales.
      </div>
    </div>

    <div class="row">
      <div class="col-sm-9 center-block">
    <div class="cta-image"></div>
  </div>
  </div>
  </section>
    <!-- END OF CALL TO ACTION -->
<!-- TESTIMONIALS -->
  <section class="testimonials home2" id="opportunity">

    <div class="row">
      <div class="col-sm-6">
        <h2 class="text-center"><span>OPPORTUNITY</span></h2>
        <hr class="small" />
        <p>The Telecommunication industry is the gold mine of our time. Several billions of Ghana cedis is spent on airtime every month making it a gold mine. GSM gives you the opportunity to mine in this great gold reserve.
        </p>
        <p>
          It is our conviction that everyone has the right to live their dreams. With our unique distributor commissions coupled with our desired products and services, we are confident that you will enjoy the financial freedom to live your dream. 
        </p>
        <p>
           By selling airtime to people on the GSM platform, you are invariably providing a great service to them. Remember the saying that if you want to go fast go alone but if you want to go far go with people. It’s not an individual effort; it takes the group effort made up of our individual small efforts to enjoying a resounding success.
   
        </p>
      </div>
      <div class="col-sm-6">
        <h2 class="text-center"><span>Get to Earn Forever</span></h2>
        <hr class="small" />
        <p>Are you making extra income right now? Earning income doesn’t have to stop at your salary. Extra income is not always about the money. It serves as a personal safety net against uncertainties. You would have more control over your income, save more and most importantly use your time productively. Global success minds, Airtime for cash gives you the opportunity to make that extra cash. With a little effort and dedication you can earn a lot more than you ever envisaged. Your phone is your work tool and sales of airtime to the pull of people around you would guarantee that success you deserve. This is no brainer; it is simple for anyone who wants to make that cash. Do not get comfortable with a steady pay cheque. Do something for yourself that makes your time worthwhile and promises a successful life .BE A PARTNER and a business owner and earn residual income every month.<p>

            </div>

    </div>
  </section>
  <!-- END OF TESTIMONIALS -->
  

    <!-- FOOTER -->
  <section class="footer">

    <div class="footer-elements">
      <div class="row">
        <div class="col-sm-12">
          <div class="row no-gutter">

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-location-outline"></span>
                <p>Mission<span>To create a community of limitless opportunities through individual partnerships, desirable products and services and irresistible rewards, an avenue where dreams become realities.
                  </span></p>
              </div>
            </div>
            <div class="col-sm-2">
              
            </div>
            <!-- FOOTER TOP ELEMENT -->

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-watch"></span>
                <p>Vision<span><a href="./index2.html">As a global player, we envisage leading a team that will be the architects of designing the needed change in the world through collective efforts, creativity and innovation. </p>
</a></span></p>
              </div>
            </div>
            <!-- FOOTER TOP ELEMENT -->

            
          </div>
        </div>
      </div>
    </div>

    <div class="footer-widgets">
      <div class="row">

        

        

       

        

      </div>
    </div>
  </section>
  <!-- END OF FOOTER -->

  <div class="copyright">
    <div class="row">
      <div class="col-sm-8">
        <p class="text-center">Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> 
        
      </p>
      </div>
      <div class="col-sm-4">
        <p class="text-center">
        <a href="https://www.facebook.com/Global-Success-Minds-Airtime-For-Cash-963597237021542/" style="margin-right:20px;">
          <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://twitter.com/gsmairtime" style="margin-right:20px;">
          <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://www.youtube.com/channel/UCeOhFXLKuagDY2LZwQA6tDQ" style="margin-right:20px;">
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </a>  
      </p>
      </div>

    </div>
  </div>

  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- JAVASCRIPT FILES -->
  <!-- <script src="{{ asset('js/fileupload.js') }}"></script> -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.hoverIntent.js')}}"></script>
  <script src="{{asset('js/superfish.min.js')}}"></script>
  <script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/odometer.min.js')}}"></script>
  <script src="{{asset('js/waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
  <script src="{{asset('js/wow.min1.js')}}"></script>
  <script src="{{asset('js/retina.min.js')}}"></script>
  <script src="{{asset('js/custom1.js')}}"></script>
  <!-- END OF JAVASCRIPT FILES -->


  <script>
   jQuery(document).ready(function($) {
      "use strict";

       //  HEADER SLIDER HOOK
      jQuery('#index-slider2').fadeIn(1000);
      $('#index-slider2').sliderPro({
      width: 955,
      height: 610,
      visibleSize: '100%',
      forceSize: 'fullWidth',
      arrows: true,
      autoplay: true,
      autoplayDelay: 8500,
      autoplayOnHover: 'none',
      slideDistance: 0,
      breakpoints: {
        1025: {
             width: '75%'
        },
        500: {
            width: '100%',
            arrows: false
        }
    }
});
        //  TESTIMONIALS CAROUSEL HOOK
        $('#customers-testimonials').owlCarousel({
          loop: true,
          items: 1,
          margin: 0,
          autoplay: true,
          autoplayTimeout: 8500,
          animateIn: 'fadeIn',
          animateOut: 'rotateOutUpRight',
          lazyLoad: true,
        });
        //  CLIENTS CAROUSEL HOOK
        $('#clients-carousel').owlCarousel({
          loop: true,
          items: 5,
          margin: 30,
          autoplay: true,
          autoplayTimeout: 8500,
          smartSpeed: 450,
          responsive: {
            0: {
              items: 2
            },
            768: {
              items: 3
            },
            1170: {
              items: 5
            }
          }
        });

});
  </script>
</body>

</html>

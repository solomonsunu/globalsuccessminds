<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Global Success Minds</title>
  <link rel="shortcut icon" href="./images/favicon.png" />
  <!-- GOOGLE WEB FONTS -->
  <!-- <link href='../../../fonts.googleapis.com/css_f7b4c15e.css' rel='stylesheet' type='text/css' />
  <link href='../../../fonts.googleapis.com/css_a1b8c998.css' rel='stylesheet' type='text/css' />
   --><!-- END OF GOOGLE WEB FONTS -->

  <!-- BOOTSTRAP & STYLES -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/block_grid_bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/typicons.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/odometer-theme-default.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slider-pro.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/animate-custom.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slicknav.min.css')}}" rel="stylesheet" />
  <link href="{{asset('style.css')}}" rel="stylesheet" />
  <!-- END OF BOOTSTRAP & STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
  <!-- HEADER -->

  <div id="hw-hero">
    <!-- HEADER -->

    <div class="header header2">
      <!-- TOP BAR -->
      <div class="topbar">
        <div class="row">
          <div class="col-sm-6">
            <ul class="phone">
              <li><span class="typcn typcn-location-outline"></span> 30 Abese loop Aviation down Adenta, Accra</li>
              <li><span class="typcn typcn-phone-outline"></span>030-254-1284 </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="toplinks">
              <li>MON - FRI 09:00 - 20:00</li>
              <li>E-MAIL US: info@globalsuccessminds.com</li>
             
            </ul>
          </div>
        </div>
      </div>
      <!-- END OF TOP BAR -->

      
      <!-- LOGO & NAVIGATION -->
      <div class="header-navigation">
      <div class="row">
        <div class="col-sm-12">
          <div class="logonav">
            <div class="row no-gutter">
              <div class="col-sm-2">
                <div class="logo">
                  <a href="./index.html"><img src="./images/logo1.png" alt="" /></a>
                </div>
              </div>
              <div class="col-sm-10">
                <nav id="desktop-menu">
                <ul class="sf-menu" id="navigation">
                    <li ><a href="{{url('/')}}">Home</a>
                      
                    </li>
                    <li><a href="{{url('/#company')}}">Company</a>
                      
                    </li>
                    <li><a href="{{url('/#opportunity')}}">Opportunity</a>
                      
                    </li>
                    <li><a href="{{url('productsandservices')}}">Products and Services</a>
                      
                    </li>

                    <li class="current"><a href="{{url('compensationplan')}}">Compensation Plan</a>
                      
                    </li>

                    <li><a href="{{url('contactus')}}">Contact</a></li>
                    <li><a href="{{url('login')}}">Login</a>
                      
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- END OF LOGO & NAVIGATION -->
    </div>
    <!-- END OF HEADER -->
 <!-- PAGE HEADER -->
    <div id="page-header">
   <div class="title-breadcrumbs">
   <h1>COMPENSATION PLAN</h1>
    <h4 style="color: white">With an unprecedented rewards program, GSM enjoys the honour of parading an array of money making avenues which is responsive to our partners, distributors and customers.</h4>
   </div>

    </div>
    <!-- END OF PAGE HEADER -->
  </div>

  <!-- SERVICES INTRO -->
  <section class="services2">
    <div class="row">
      <div class="col-md-12">
        <h2 class="text-center"><span>TEAM SALES COMMISSION</span></h2>
        <!-- <h5>SERVICES FOR STARTUPS AND SMALL BUSINESSES</h5> -->
        <p class="text-center">
        By joining this business, you earn commissions on airtime sales and purchases.<br>
        <strong>Just follow these simple steps:</strong><br>
        </p>
        <p class="text-center">
          Sign up for free to become a distributor on our platform. Load your GSM wallet to start selling airtime at your convenience and pace. Recommend GSM airtime for cash to other distributors and consumers to increase your sales returns and earn more. 
        </p> <br/>
        
        

      </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <h2>AIRTIME 4 CASH  <span> PRESENTS FOUR (4)</span> STREAMS OF INCOME</h2>
            <hr class="small" />
        </div>
       
        <div class="col-sm-6">
          <div style="margin-top:40px;">
            <ol>
               <li>DIRECT AIRTIME COMMISSIONS</li>
               <li>TEAM AIRTIME COMMISSIONS </li>
               <li>DIRECT UPGRADE COMMISSIONS</li>
               <li>TEAM UPGRADE COMMISSIONS</li>
             </ol>
          </div>
        </div>
     
    </div>
    <div class="row">
        <div class="col-md-6">
          <h2><span>AIRTIME COMMISSIONS </span></h2>
            <hr class="small" />
          <p>This model enables airtime consumers to also become airtime retailers: that is, we pay commissions to our members for referring others to our platform and retailing to them. </p>
          <ol>
            <li>DIRECT AIRTIME COMMISSIONS: Members earn 3% instant commission when they buy airtime for themselves as well as buy or sell to other people</li>
            <li>
             TEAM AIRTIME COMMISSIONS: This is the commission you earn for recommending or referring people to join and buy their airtime with GSM or retailing to them as a GSM distributor. 0.10%, 0.15%, 0.20%, 0.25%, 0.30% and 0.50% commissions would be paid to you from level 1 to level 6 respectively on your team sales performance.
            </li>

            <!-- <li>
              TEAM AIRTIME COMMISSIONS: This is the commission you earn for recommending or referring people to join and buy their airtime with GSM or retailing to them as a GSM distributor. Total amount of commission earning is dependent on team levels sales performance. That is: <b>Level 1: 0.10% ,</b> <b>Level 2: 0.15% ,</b> <b>Level 3: 0.20% ,</b> <b>Level 4: 0.25% ,</b> <b>Level 5:  0.30%</b> and <b>Level 6: 0.50%</b>            </li>
 -->

          </ol>
        </div>
       
        <div class="col-md-6">
          <h2><span>UPGRADE COMMISSIONS  </span></h2>
            <hr class="small" />
         
          <ol>
             <li>DIRECT UPGRADE COMMISSIONS: This is the commission you earn for recommending people to upgrade to become GSM Premier distributors. Ghc10.00 would be paid to you on each recommendation.  </li>
            <li>TEAM UPGRADE COMMISSIONS: this commission is paid whenever someone upgrades to become a GSM premier distributor in the team. Ghc3.00 is paid on each team member upgrade.</li>
            

          </ol>
        </div>
     
    </div>
    
  </section>

 
<div class="spacing-45"></div>
  <!-- FOOTER -->
     <!-- FOOTER -->
  <section class="footer">

    <div class="footer-elements">
      <div class="row">
        <div class="col-sm-12">
          <div class="row no-gutter">

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-location-outline"></span>
                <p>Mission<span>To create a community of limitless opportunities through individual partnerships, desirable products and services and irresistible rewards, an avenue where dreams become realities.
                  </span></p>
              </div>
            </div>
            <div class="col-sm-2">
              
            </div>
            <!-- FOOTER TOP ELEMENT -->

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-watch"></span>
                <p>Vision<span><a href="./index2.html">As a global player, we envisage leading a team that will be the architects of designing the needed change in the world through collective efforts, creativity and innovation. </p>
</a></span></p>
              </div>
            </div>
            <!-- FOOTER TOP ELEMENT -->

            
          </div>
        </div>
      </div>
    </div>

    <div class="footer-widgets">
      <div class="row">

        

        

       

        

      </div>
    </div>
  </section>
  <!-- END OF FOOTER -->

 <div class="copyright">
    <div class="row">
      <div class="col-sm-8">
        <p class="text-center">Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> 
        
      </p>
      </div>
      <div class="col-sm-4">
        <p class="text-center">
        <a href="https://www.facebook.com/Global-Success-Minds-Airtime-For-Cash-963597237021542/" style="margin-right:20px;">
          <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://twitter.com/gsmairtime" style="margin-right:20px;">
          <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://www.youtube.com/channel/UCeOhFXLKuagDY2LZwQA6tDQ" style="margin-right:20px;">
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </a>  
      </p>
      </div>

    </div>
  </div>

  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- JAVASCRIPT FILES -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.hoverIntent.js')}}"></script>
  <script src="{{asset('js/superfish.min.js')}}"></script>
  <script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/odometer.min.js')}}"></script>
  <script src="{{asset('js/waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
  <script src="{{asset('js/wow.min1.js')}}"></script>
  <script src="{{asset('js/retina.min.js')}}"></script>
  <script src="{{asset('js/custom1.js')}}"></script>
  <!-- END OF JAVASCRIPT FILES -->

</body>
</html>

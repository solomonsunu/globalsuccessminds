<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Global Success Minds</title>
  <link rel="shortcut icon" href="./images/favicon.png" />
  <!-- GOOGLE WEB FONTS -->
 <!--  <link href='../../../fonts.googleapis.com/css_f7b4c15e.css' rel='stylesheet' type='text/css' />
  <link href='../../../fonts.googleapis.com/css_a1b8c998.css' rel='stylesheet' type='text/css' /> -->
  <!-- END OF GOOGLE WEB FONTS -->

  <!-- BOOTSTRAP & STYLES -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/block_grid_bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/typicons.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/odometer-theme-default.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slider-pro.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/animate-custom.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" />
  <link href="{{asset('css/owl.theme.default.min.css')}}" rel="stylesheet" />
  <link href="{{asset('css/slicknav.min.css')}}" rel="stylesheet" />
  <link href="{{asset('style.css')}}" rel="stylesheet" />
  <!-- END OF BOOTSTRAP & STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
  #page-header {
    min-height: 360px;
    background-size: cover;
    background-position: top center;
    background: url(./images/build.jpeg);
  }
  #page-header:after { background: #055AA8; opacity:0; bottom: 0px; content: ""; left: 0; position: absolute; right: 0; top: 0; z-index: 1; }

</style>
</head>


<body>

  <!-- HEADER -->

  <div id="hw-hero">
    <!-- HEADER -->

    <div class="header header2">
      <!-- TOP BAR -->
      <div class="topbar">
        <div class="row">
          <div class="col-sm-6">
            <ul class="phone">
              <li><span class="typcn typcn-location-outline"></span> 7 Anderson Close, Adenta Barrier. Accra</li>
              <li><span class="typcn typcn-phone-outline"></span>030-254-1284 / 0303964373 </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="toplinks">
              <li>MON - SAT 08:30 - 18:00</li>
              <li>E-MAIL US: info@globalsuccessminds.com</li>
             
            </ul>
          </div>
        </div>
      </div>
      <!-- END OF TOP BAR -->

      
      <!-- LOGO & NAVIGATION -->
      <div class="header-navigation">
      <div class="row">
        <div class="col-sm-12">
          <div class="logonav">
            <div class="row no-gutter">
              <div class="col-sm-2">
                <div class="logo">
                  <a href="./index.html"><img src="./images/logo1.png" alt="" /></a>
                </div>
              </div>
              <div class="col-sm-10">
                <nav id="desktop-menu">
                <ul class="sf-menu" id="navigation">
                    <li ><a href="{{url('/')}}">Home</a>
                      
                    </li>
                    <li><a href="{{url('/#company')}}">Company</a>
                      
                    </li>
                    <li><a href="{{url('/#opportunity')}}">Opportunity</a>
                      
                    </li>
                    <li><a href="{{url('productsandservices')}}">Products and Services</a>
                      
                    </li>

                    <li><a href="{{url('compensationplan')}}">Compensation Plan</a>
                      
                    </li>

                    <li class="current"><a href="{{url('contactus')}}">Contact</a></li>
                    <li><a href="{{url('login')}}">Login</a>
                      
                    </li>
                   
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- END OF LOGO & NAVIGATION -->
    </div>
    <!-- END OF HEADER -->
 <!-- PAGE HEADER -->
    <div id="page-header">
    <!-- <div class="title-breadcrumbs">
     <h1>CONTACT US</h1>
    </div> -->

    </div>
    <!-- END OF PAGE HEADER -->
  </div>

  <!--  ICON BOXES -->
<div class="about1-numbers contact">
      <div class="row">

        <div class="col-sm-4">
        <div class="shadow-effect">
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <div class="number lightgray"><span class="typcn typcn-mail"></span></div>
            </div>
            <div class="col-sm-12 col-md-9">
              <h4>DROP US A LINE</h4>
              <p>Collaboratively administrate empowered markets via plug-and-play networks.</p>
              <span><a href="./contact.html">info@globalsuccessminds.com</a></span>
            </div>
          </div>
          </div>
        </div>

        <div class="col-sm-4">
         <div class="shadow-effect">
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <div class="number blue"><span class="typcn typcn-heart"></span></div>
            </div>
            <div class="col-sm-12 col-md-9">
              <h4>ASK FOR SUPPORT</h4>
              <p>Feel free  to ask our able help desk for assistance anytime.</p>
              <span><a href="./contact.html">info@globalsuccessminds.com</a></span>
            </div>
          </div>
          </div>
        </div>

        <div class="col-sm-4">
         <div class="shadow-effect">
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <div class="number green"><span class="typcn typcn-location"></span></div>
            </div>
            <div class="col-sm-12 col-md-9">
              <h4>VISIT OUR OFFICE</h4>
              <p>37 Anderson Close, Adenta Barrier, Accra</p>
              <span>Call us: 030-254-1284 / 0303964373</span>
            </div>
            </div>
          </div>
        </div>
      </div>

      </div>
<!--  END OF ICON BOXES -->

 
<div class="spacing-45"></div>
  <!-- FOOTER -->
     <!-- FOOTER -->
  <section class="footer">

    <div class="footer-elements">
      <div class="row">
        <div class="col-sm-12">
          <div class="row no-gutter">

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-location-outline"></span>
                <p>Mission<span>To create a community of limitless opportunities through individual partnerships, desirable products and services and irresistible rewards, an avenue where dreams become realities.
                  </span></p>
              </div>
            </div>
            <div class="col-sm-2">
              
            </div>
            <!-- FOOTER TOP ELEMENT -->

            <!-- FOOTER TOP ELEMENT -->
            <div class="col-sm-5">
              <div class="footer-element">
                <span class="typcn typcn-watch"></span>
                <p>Vision<span><a href="./index2.html">As a global player, we envisage leading a team that will be the architects of designing the needed change in the world through collective efforts, creativity and innovation. </p>
</a></span></p>
              </div>
            </div>
            <!-- FOOTER TOP ELEMENT -->

            
          </div>
        </div>
      </div>
    </div>

    <div class="footer-widgets">
      <div class="row">

        

        

       

        

      </div>
    </div>
  </section>
  <!-- END OF FOOTER -->

 <div class="copyright">
    <div class="row">
      <div class="col-sm-8">
        <p class="text-center">Copyright Global Success Minds. All Rights Reserved. <a href="{{url('/termsandconditions')}}">Terms & Conditions</a> 
        
      </p>
      </div>
      <div class="col-sm-4">
        <p class="text-center">
        <a href="https://www.facebook.com/Global-Success-Minds-Airtime-For-Cash-963597237021542/" style="margin-right:20px;">
          <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://twitter.com/gsmairtime" style="margin-right:20px;">
          <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
        </a>  
        <a href="https://www.youtube.com/channel/UCeOhFXLKuagDY2LZwQA6tDQ" style="margin-right:20px;">
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
        </a>  
      </p>
      </div>

    </div>
  </div>

  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- JAVASCRIPT FILES -->
<script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.hoverIntent.js')}}"></script>
  <script src="{{asset('js/superfish.min.js')}}"></script>
  <script src="{{asset('js/jquery.sliderPro.min.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/odometer.min.js')}}"></script>
  <script src="{{asset('js/waypoints.min.js')}}"></script>
  <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
  <script src="{{asset('js/wow.min1.js')}}"></script>
  <script src="{{asset('js/retina.min.js')}}"></script>
  <script src="{{asset('js/custom1.js')}}"></script>
  <!-- END OF JAVASCRIPT FILES -->

</body>
</html>

<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="We are Asem, your solution for public and private oriented products and services evaluation." >
    <meta name="keywords" content=" services, public, private, mass, products." >
    <meta name="audience" content="all" />
    <meta name="distribution" content="global" />
    <meta name="rating" content="general" />
    <meta name="revisit-after" content="1 days" />
    <meta name="robots" content="index, follow">
    <title>GSM</title>

    <link href="{{asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <link href="{{asset('assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/angular-fancy-modal.css')}}" type="text/css" />

     <link rel="stylesheet" href="{{ asset('bower_components/angular-material/angular-material.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('bower_components/ng-toast/dist/ngToast.css')}}" type="text/css" />
     <!-- <link rel="stylesheet" href="{{ asset('bower_components/ng-toast/dist/ngToast-animations.css')}}" type="text/css" /> -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('/assets/js/ngStorage/ngStorage.min.js')}}"></script> -->
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/nicescroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <!-- // <script type="text/javascript" src="/assets/js/pages/dashboard.js"></script> -->
    <script type="text/javascript" src="{{asset('/assets/js/pages/layout_fixed_custom.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/custom.js')}}"></script>

    <link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/extras/animate.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}">
    <link rel="image_src" href="{{asset('assets/images/chosen-sprite.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/ngDatepicker.css')}}">
    <link rel="stylesheet" href="{{asset('js/acute/acute.select/acute.select.css')}}">

    <!-- /theme JS files -->

    <!-- custom Js files -->
    <script src="{{asset('/assets/lib/moment.min.js')}}"></script>
     <script src="{{asset('bower_components/angular/angular.min.js') }}"></script>
     <script src="{{asset('bower_components/angular-animate/angular-animate.min.js') }}"></script>
     <script src="{{asset('bower_components/angular-aria/angular-aria.min.js') }}"></script>
     <script src="{{asset('js/angular-sanitize.min.js') }}"></script>

     <script src="{{asset('js/angular-chosen.js')}}"></script>
     <script src="{{asset('js/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('js/angular-chosen.js')}}"></script>
    <script src="{{asset('js/angular-fancy-modal.js')}}"></script>
    <script src="{{ asset('js/dirPagination.js') }}"></script>
    <script src="{{ asset('js/acute/acute.select/acute.select.js') }}"></script>
    <script src="{{asset('js/ngStorage/ngStorage.min.js')}}"></script>

    <script src="{{asset('bower_components/angular-material/angular-material.js')}}"></script>
    <script src="{{asset('bower_components/ng-file-upload/dist/ng-file-upload-all.min.js') }}"></script>
    <script src="{{asset('bower_components/ng-toast/dist/ngToast.min.js') }}"></script>
    <script src="{{asset('bower_components/angular-socialshare/dist/angular-socialshare.min.js') }}"></script>
     
    <script src="{{ asset('js/angular/angular-app.js') }}"></script>
    <script src="{{ asset('js/angular/controllers.js') }}"></script>
    <script src="{{ asset('js/angular/angular-filter.js') }}"></script>
    <script src="{{ asset('js/angular/angular-services.js') }}"></script>
    <script src="{{ asset('js/angular/angular-factory.js') }}"></script>
    <!-- /theme JS files -->
    <!-- /theme JS files -->

</head>

<body class="navbar-top"  ng-app="gsmApp" ng-cloak ng-controller="AppCtrl">
<toast></toast>

<div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('dashboard')}}">GSM</a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            </ul>

            <p class="navbar-text">
           
            </p>

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" >
                    
                    <img src="{{asset('assets/images/default_large.png')}}" alt="" class="" >
                   
                    <i class="caret"></i>
                    </a>

                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        
                      
                        <li class="divider"></li>
                        <li><a href="{{url('logout')}}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
</div>
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

        <div class="sidebar sidebar-main">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                
                                <a href="#" class="media-left">
                                  
                                    <img src="{{asset('assets/images/default_large.png')}}" alt="" class="img-circle img-sm" >
                                  
                                </a>
                                

                               
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <!-- Main -->
                                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                                <li class="{{ Active::check('dashboard') }}"><a href="{{url('dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                               
                                
                                
                                
                                <!-- /page kits -->

                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>


            <!-- Main content -->
            <div class="content-wrapper">
            @yield('page-header')
                


                <!-- Content area -->
                <div class="content">
                    
                
                 @yield('content')
                

                    <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; 2016. <a href="#">Global Success Minds</a>  <a href="#" target="_blank">v.0.1.1</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    
    
    <!-- /page container -->
    <!-- <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('bower_components/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('bower_components/pace/pace.js')}}"></script>
    <script src="{{asset('js/siminta.js')}}"></script>
     Page-Level Plugin Scripts
    <script src="{{asset('bower_components/morris/raphael-2.1.0.min.js')}}"></script>

    <script src="{{ asset('js/fileupload.js') }}"></script>
    <script src="{{ asset('js/bootstrap-formhelpers.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-formhelpers-countries.js')}}" type="text/javascript"></script> -->
 <script src="{{ asset('js/fileupload.js') }}"></script>
 <script src="{{ asset('js/bootstrap-formhelpers.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-formhelpers-countries.js')}}" type="text/javascript"></script>
</body>
</html>

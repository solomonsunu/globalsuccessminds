<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>Global Success Minds</title>
    <meta name="description" content="GSM AIRTIME 4 CASH">
    <meta name="author" content="Dennis Ji">
    <meta name="keyword" content="">
    <!-- end: Meta -->
    
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->
    
    <!-- start: CSS -->
    <link id="bootstrap-style" href="{{ asset('css/bootstrap.min.css') }}"  rel="stylesheet">
    <link href="{{ asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
    <link id="base-style" href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link id="base-style-responsive" href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css')}}" type="text/css" />
    <link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">

    <!-- end: CSS -->
    
     <link  href="{{ asset('css/site-login.css') }}" rel="stylesheet">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="{{ asset('http://html5shim.googlecode.com/svn/trunk/html5.js') }}"></script>
        <link id="ie-style" href="{{ asset('css/ie.css" rel="stylesheet') }}">
    <![endif]-->
    
    <!--[if IE 9]>
        <link id="ie9style" href="{{ asset('css/ie9.css" rel="stylesheet') }}">
    <![endif]-->
        
    <!-- start: Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- end: Favicon -->
    
        
        
</head>

<body class="loginandregistrationbackground">
        <div class="container-fluid-full">
        <div class="row" style="margin-top: -40px;">
             <center style="margin-bottom:0px;">
                <img src="{{ asset('img/logo1.png')}}" class="img-responsive" >
            </center>
        </div>
           
                    @yield('content')
    
        </div><!--/fluid-row-->
    
    <!-- start: JavaScript-->

        <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery-migrate-1.0.0.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery-ui-1.10.0.custom.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.ui.touch-punch.js') }}"></script>
    
        <script src="{{ asset('js/modernizr.js') }}"></script>
    
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    
        <script src="{{ asset('js/fullcalendar.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

        <script src="{{ asset('js/excanvas.js') }}"></script>
    <script src="{{ asset('js/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('js/jquery.flot.resize.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.chosen.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
        
        <script src="{{ asset('js/jquery.cleditor.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.noty.js') }}"></script>
    
        <script src="{{ asset('js/jquery.elfinder.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.raty.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.iphone.toggle.js') }}"></script>
    
        <script src="{{ asset('js/jquery.uploadify-3.1.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.gritter.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.imagesloaded.js') }}"></script>
    
        <script src="{{ asset('js/jquery.masonry.min.js') }}"></script>
    
        <script src="{{ asset('js/jquery.knob.modified.js') }}"></script>
    
        <script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>
    
        <script src="{{ asset('js/counter.js') }}"></script>
    
        <script src="{{ asset('js/retina.js') }}"></script>

       <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/jquery-ui-timepicker-addon.js') }}"></script>
    <script type="text/javascript">
       $(function() {
            $('#datetimepicker2').datepicker({
                yearRange: "1970:2020",
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
           
        });

       
    </script>

<script src="{{ asset('js/bootstrap-formhelpers.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-formhelpers-countries.js')}}" type="text/javascript"></script>
    <!-- end: JavaScript-->
    
</body>
</html>

@extends('../layouts.default')

@section('content')
<div class="row-fluid">
    <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3" style="margin-top:-40px;">
        <div class="account-wall">
            <h3 class="text-center login-title">Please enter your email and new password</h3>
            
            <form method="POST" action="/password/reset">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                @if (count($errors) > 0)
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" name="password">
                </div>

                <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="password_confirmation">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Reset Password
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<style type="text/css">
    .form-group{
        margin-bottom: 0px;
    }
</style>
@endSection()
@section('footer')

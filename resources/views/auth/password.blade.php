
@extends('../layouts.default')

@section('content')
<div class="row-fluid">
    <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3" style="margin-top:-40px;">
        <div class="account-wall">
            <h3 class="text-center login-title">Enter your email to recover your password</h3>
            <form method="POST" action="/password/email">
                {!! csrf_field() !!}
                @if (Session::has('flash_notice'))
                    <div class="alert alert-info">{{Session::get('flash_notice')}}</div>
                @endif
                @if (Session::has('status'))
                    <div class="alert alert-info">{{Session::get('status')}}</div>
                @endif
                @if(Session::has('email'))
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('email') }}
                            </div>
                @endif
                @if (count($errors) > 0)
                    
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger ">{{ $error }}</div>
                        @endforeach
                   
                @endif

                

                <div class="form-group">
                    
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>

                <div>
                    <button type="submit" class="btn btn-success">
                        Send Password Reset Link
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endSection()
@section('footer')

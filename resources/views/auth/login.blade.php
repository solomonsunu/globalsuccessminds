@extends('../layouts.default')

@section('content')


 <div class="row-fluid">

                <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3" style="margin-top:-40px;">
                   
                    
                    <div class="account-wall">
                     
                        <a href="{{ asset('/login') }}"><i class="halflings-icon home pull-right"></i></a>
                  
                    <h3 class="text-center login-title">Sign in</h3>
                    	@if (Session::has('flash_notice'))
					    	<div class="alert alert-info">{{Session::get('flash_notice')}}</div>
					    @endif
					    @if (Session::has('message'))
							<div class="alert alert-info">{{Session::get('message')}}</div>
					    @endif
					    @if(Session::has('error'))
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {{ Session::get('error') }}
                                    </div>
                        @endif
                        @foreach($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        {{ $error }}
                                    </div>
                        @endforeach
                    	<form class="form-horizontal"  method="POST" action="login" role="form">
                     
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
	                        <div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="halflings-icon user"></i></span>
							  <input type="text" class="form-control" placeholder="Username/Phone Number" aria-describedby="basic-addon1" name="username">
							</div>
                           <div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="halflings-icon lock"></i></span>
							  <input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" name="password">
							</div>
                            
                            <div class="clearfix"></div>
                            
                            <div class="">  
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                            </div>
                            <div class="clearfix"></div>
                    </form>
                    <hr>
                    <span class="pull-right"><a href="{{url('register')}}">Register Here</a></span>
				     <span class="pull-left">Don't have an account?</span> 
				     <br/>
				     <span class="pull-right"><a href="{{url('password/email')}}">Have you forgotten your password?</a></span><br/>
				     <span class="pull-right"><a href="{{url('/')}}"> <-- Go back</a></span>  
                    </div>
                      
                </div><!--/span-->
            </div><!--/row-->
<!-- <div class="container" style="margin-bottom:100px;">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-12">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                
								<form id="login-form" method="POST" action="/auth/login" role="form" style="display: block;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group text-center">
										<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
										<label for="remember"> Remember Me</label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="http://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
												</div>
											</div>
										</div>
									</div>
								</form>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 -->
   
@endSection()
@section('footer')


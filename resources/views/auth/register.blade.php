@extends('../layouts.default')

@section('content')
 <div class="row-fluid" style="margin-top: -40px;">
        <form action="{{url('register')}}" method="POST" role="form" enctype="multipart/form-data">
            <div class="row">
              <div class="col-lg-6 col-lg-offset-3">
                <div class="panel registration-form">
                  <div class="panel-body reg">
                    <div class="text-center">
                      <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                      <h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
                       @if (Session::has('success'))
                            <div class="alert alert-info">{{Session::get('success')}}</div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger  ">{{Session::get('error')}}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{ Session::get('errors') }}
                            </div>

                        @endif
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="text" name="firstname" class="form-control" placeholder="First name" required>
                          <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="text" name="lastname" class="form-control" placeholder="Last name" required>
                          <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="password" name="password" class="form-control" placeholder="Create password" required>
                          <div class="form-control-feedback">
                            <i class="icon-user-lock text-muted"></i>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="password" name="password_confirmation" class="form-control" placeholder="Repeat password" required>
                          <div class="form-control-feedback">
                            <i class="icon-user-lock text-muted"></i>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="text" name="username" class="form-control" placeholder="Create Username" required>
                          <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="text" name="email" class="form-control" placeholder="Email" required>
                          <div class="form-control-feedback">
                            <i class="icon-envelop text-muted"></i>
                          </div>
                        </div>
                      </div>


                    </div>
                    <div class="row">
                       <div class="col-md-6">
                        <div class="form-group has-feedback">
                          
                          <select name="carrier" class=" form-control" required>
                            <option value="">Network Type</option>
                              <option value="mtn">MTN</option>
                              <option value="vodafone">VODAFONE</option>
                              <option value="airtel">AIRTEL</option>
                              <option value="tigo">TIGO</option>
                              <option value="glo">Glo</option>
                              <option value="expresso">Expresso</option>
                          </select>
                            <div class="form-control-feedback">
                            <i class="icon-station text-muted"></i>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <input type="text" name="phone" class="form-control" placeholder="Phone Number">
                          <div class="form-control-feedback">
                            <i class="icon-phone text-muted"></i>
                          </div>
                        </div>
                      </div>


                    </div>
                     <div class="row">
                       <div class="col-md-6">
                        <div class="form-group has-feedback">
                           <input type='text' class="form-control" name="dob" id='datetimepicker2' placeholder="Click to Enter Date of Birth" required/>

                          <div class="form-control-feedback">
                            <i class="icon-calendar text-muted"></i>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                          <select name="gender" class="form-control">
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                          </select>
                        <div class="form-control-feedback">
                            <i class="icon-man-woman text-muted"></i>
                          </div>
                        </div>
                      </div>


                    </div>
                    <div class="row">
                       <div class="col-md-6">
                        <div class=" bfh-selectbox bfh-countries" data-name="country" data-country="GH" data-available="GH,US,NG" data-flags="true">
                          <input type="hidden" value="" name="country">
                          <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                            <span class="bfh-selectbox-option input-medium" data-option=""></span>

                          </a>
                          <div class="bfh-selectbox-options">
                            <input type="text" name="country" class="form-control bfh-selectbox-filter">
                            <div role="listbox">
                                <ul role="option">
                                </ul>
                            </div>
                          </div>
                      </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group has-feedback">
                         <input type='text' class="form-control" name="brought_by"  placeholder="Reffered by Username" required/>

                        <div class="form-control-feedback">
                            <i class="icon-user-lock text-muted"></i>
                          </div>
                        </div>
                      </div>


                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" class="styled" name="termsandconditions" required checked="true">
                              Accept <a href="{{url('termsandconditions')}}" target="_blank">terms of service</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="col-md-6">
                        <div class="input-group ">
                                <div class="">
                                  <label>
                                    <input type="file" class="form-control" name="image">
                                    *Upload Image here
                                  </label>
                                </div>
                                
                              </div>
                      </div> -->
                      
                    </div>
                    

                    <div class="text-right">
                      <button type="submit" class="btn bg-teal-400 btn-success btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
                        <br>
                      <a href="{{url('login')}}" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Back to login form</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </form>


            </div><!--/row-->

   <style type="text/css">
        .selectbox-caret {
        float: right !important;
        margin-top: 8px !important;
        margin-right: -1px !important;
        margin-left: -10px !important;
    }
   </style>
@endSection()
@section('footer')

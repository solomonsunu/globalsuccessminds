<!Doctype html>
<html>
	<head>
		<title>Global Success Minds</title>
		@include('includes.jqueryandbootstrap')
	</head>
	<body>
		@yield('content')

	</body>
</html>

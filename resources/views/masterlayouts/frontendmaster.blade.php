<!Doctype html>
<html>
	<head>
		<title>Global Success Minds</title>
		@include('includes.jqueryandbootstrap2')
	</head>
	<body>
		<nav id="mynav" class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">G.S.M</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<!-- <ul class="nav navbar-nav">
					<li class="active"><a href="#">Link</a></li>
					<li><a href="#">Link</a></li>
				</ul> -->

				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{url('/')}}">Home</a></li>
					<li><a href="{{url('/#company')}}">Company</a></li>
					<li><a href="{{url('/#opportunity')}}">Opportunity</a></li>
					<li><a href="{{url('productsandservices')}}">Products and Services</a></li>
					<li><a href="{{url('compensationplan')}}">Compensation Plan</a></li>
					<li><a href="{{url('/#contactus')}}">Contact Us</a></li>
					<li><a href="{{url('login')}}">Login</a></li>

				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
		@yield('content')

	</body>
</html>

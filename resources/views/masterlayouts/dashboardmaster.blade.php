<!Doctype html>
<html>
	<head>
		<title>Network Marketing</title>
		@include('includes.jqueryandbootstrap')
	</head>
	<body>
		
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{URL::to('dashboard/')}}" style="letter-spacing:5px"><img class="img-responsive brand" src="{{asset('img/logo.png')}}"></a>
                	
                 </div>

                 
                 <ul class="nav pull-right top-nav" >
                 	<li class="dropdown purple">
                 		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome: @yield('headerusername')</i><b class="caret"></b></a>
                 		<ul class="dropdown-menu">
                 			<li class="divider"></li>
                 			<li><a href="{{ URL::to('logout') }}"><i class="fa fa-fw fa-power-off"></i>Log Out</a></li>
                 		</ul>
                 	</li>
                	
                </ul>
            <!-- /.navbar-header -->

            
            <div id="purple" class="navbar-default sidebar purple" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        
                        <li>
                            <a href="{{URL::to('dashboard/')}}"><i class="fa fa-dashboard fa-fw "></i> Dashboard</a>
                        </li>
                        @if(Auth::user()->admin == 0)
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw "></i>Account Manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/viewmessages')}}">View Notifications</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/viewaccounts')}}">View Accounts </a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/viewrefferals')}}">View Refferals </a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/addaccount')}}">Add Account Detail</a>
                                </li>
                                 <li>
                                    <a href="{{URL::to('dashboard/editaccount/'.Auth::user()->uid)}}">Edit Bank Account</a>
                                </li>

                                 <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                                
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw "></i> View Cycle<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/viewcycle/'.Auth::user()->uid)}}">View Cycle</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i> Add To Cycle<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/addtocycle/'.Auth::user()->uid)}}">Add To Cycle</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                         @if(Auth::user()->admin == 1)
                            <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>User Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/listpendingusers')}}">Activate New Users</a>
                                </li>
                                 <li>
                                    <a href="{{URL::to('dashboard/listactiveusers')}}">View All Active Users</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/create-su')}}">Create Super User</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>Accounts Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/registerinsurance')}}">Register Insurance payments</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/payment-sheet')}}"> Prepare monthly payment sheet</a>
                                </li>
                                 <li>
                                    <a href="#"  data-toggle="modal" data-target="#myModal">Reset All Insurance State</a>
                                </li>
                             
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                        @if(Auth::user()->admin == 2)
                            <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>User Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/listpendingusers')}}">Activate New Users</a>
                                </li>
                                
                                
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>Accounts Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/registerinsurance')}}">Register Insurance payments</a>
                                </li>
                             
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                         @if(Auth::user()->admin == 3)
                            <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>User Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('dashboard/listactiveusers')}}">View All Active Users</a>
                                </li>
                                
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        @endif
                         @if(Auth::user()->admin == 4)
                          <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>User Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                               
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>  
                        <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>Accounts Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                 <li>
                                    <a href="{{URL::to('dashboard/payment-sheet')}}"> Prepare monthly payment sheet</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                         @if(Auth::user()->admin == 5)
                       <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>User Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('dashboard/edituser/'.Auth::user()->uid)}}">Edit User Account</a>
                                </li>
                                
                                
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plus fa-fw "></i>Accounts Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <li>
                                    <a href="#"  data-toggle="modal" data-target="#myModal">Reset All Insurance State</a>
                                </li>
                                 
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                        
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper" style="margin-top: 30px;">
            <div class="row">
            @if(Auth::user()->admin == 0 && Auth::user()->insurancestatus == 0)
                <div class="panel panel-danger panel-heading pull-right">
                    <i class="fa fa-refresh fa-spin"></i>
                    <b>You are currently owing insurance</b><br>
                    <i>Paying insurance is required for payment claim</i>
                </div>
            @endif
            </div>
        @if(Auth::user()->admin == 5 || Auth::user()->admin == 1)
        
         <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h2 class="modal-title">Confirm Insurance Reset and Payment of Commisions   </h2></center>
                  </div>
                  <div class="modal-body">
                    <center><p>Do you really want to reset all insurance status on system to <b>" Owing Insurance " </b> and change the user commision's status to <b>" Paid " </b> ? <br>
                    <p>Has the payment sheet for this month already been generated ?</p>
                    </p></center>
                    <br>
                    <center>
                        <a href="{{URL::to('dashboard/reset-insurance')}}"  class="btn btn-primary" style="margin-right: 50px;">Yes</a> <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
                   
        @endif            

            @yield('content')
  
           
        </div>
  
    </div>
  
  


	</body>
</html>
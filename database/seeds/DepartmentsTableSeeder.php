<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('departments')->insert([
            'department_id' =>  '15-67-69',
            'school_id' =>  '35-67-21',
            'department_name' =>  'Department of Chemistry',
      
        ]);
          DB::table('departments')->insert([
            'department_id' =>  '51-76-70',
            'school_id' =>  '35-67-21',
            'department_name' =>  'Department of Computer Science',
      
        ]);
    }
}

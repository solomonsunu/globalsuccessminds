<?php

use Illuminate\Database\Seeder;
use App\User;
use Ramsey\Uuid\Uuid;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSMadmin',
            'lastname'         =>          'GSMadmin',
            'username'          =>          'GSMadmin',
            'dob'               =>          '1992/01/01',
            'gender'            =>          'male',
            'email'             =>          'admin@gmail.com',
            'password'          =>          bcrypt('GSMadmin'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          1,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 1',
            'lastname'         =>          'GSM 1',
            'username'          =>          'GSM1',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin1@gmail.com',
            'password'          =>          bcrypt('GSM1'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          2,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'airtel',
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 2',
            'lastname'         =>          'GSM 2',
            'username'          =>          'GSM2',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin2@gmail.com',
            'password'          =>          bcrypt('GSM2'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          3,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'mtn',
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 3',
            'lastname'         =>          'GSM 3',
            'username'          =>          'GSM3',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin3@gmail.com',
            'password'          =>          bcrypt('GSM3'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          4,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'tigo',
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 4',
            'lastname'         =>          'GSM 4',
            'username'          =>          'GSM4',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin@gmail.com',
            'password'          =>          bcrypt('GSM4'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          5,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'vodafone',
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 5',
            'lastname'         =>          'GSM 5',
            'username'          =>          'GSM5',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin@gmail.com',
            'password'          =>          bcrypt('GSM5'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          6,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'glo',
            
        ));
        User::create(array(
            'user_id'   =>  Uuid::uuid4()->toString(),
            'firstname'        =>          'GSM 6',
            'lastname'         =>          'GSM 6',
            'username'          =>          'GSM6',
            'dob'               =>          '1989/05/25',
            'gender'            =>          'male',
            'email'             =>          'admin@gmail.com',
            'password'          =>          bcrypt('GSM6'),
            'brought_by'        =>          '',
            'legend'            =>          0,
            'admin'             =>          7,
            'active'                =>      1,
            'profile_image'     =>          '',
            'status'                =>      2,
            'carrier'                =>      'expresso',
            
        ));
    }
}

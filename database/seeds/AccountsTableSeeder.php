<?php

use Illuminate\Database\Seeder;
use App\Accounts;
class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('accounts')->insert([
            'account_id' =>  '35-67-21',
            'user_id' =>  'ffdd-2345-6789-ttfr',
            'bankname'  => 'Unibank',
            'accountname'   => 'Admin Account',
            'accountnumber' => '10254136254',
            'branch' => 'Madina'
            
      
        ]);
          DB::table('accounts')->insert([
            'account_id' =>  '36-78-23',
            'user_id' =>  'ffad-1308-6709-aafr',
            'bankname'  => 'Ecobank',
            'accountname'   => 'Miki Account',
            'accountnumber' => '10884100254',
            'branch' => 'Madina'
      
        ]);
    }
}

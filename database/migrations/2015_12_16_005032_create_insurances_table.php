<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('insurance_d');
            $table->string('user_id');
            $table->string('invoicenumber',25);
            $table->double('amount', 15, 2);
            $table->string('paymonth',25)->nullable();
            $table->string('status',25)->default('Pending');
            $table->date('doc')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurances');
    }
}

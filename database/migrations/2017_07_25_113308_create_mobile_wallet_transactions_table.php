<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_wallet_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobile_wallet_id');
            $table->string('user_id');
            $table->string('invoiceNo')->nullable();
            $table->double('amount')->nullable();
            $table->string('responseCode')->nullable();
            $table->string('responseMessage')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mobile_wallet_transactions');
    }
}

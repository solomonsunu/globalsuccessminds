<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBagamDetailsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
             $table->string('bagam_brought')->default(0);
            $table->date('bagam_date')->nullable();
            $table->integer('bagam_direct_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->drop('bagam_brought');
            $table->drop('bagam_date');
            $table->drop('bagam_direct_count');
              

        });
    }
}

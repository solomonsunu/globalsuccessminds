<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('username', 25);
            $table->date('dob')->nullable();
            $table->string('gender');
            $table->string('password', 64);
            $table->integer('legend');
            $table->integer('admin');
            $table->string('brought_by')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('email')->nullable();
            $table->integer('active')->default(0);
            $table->string('password_temp')->nullable();
            $table->string('resetcode')->nullable();
            $table->string('link')->nullable();
            $table->string('status')->default(0);
            $table->string('paymentstatus')->default(0);
            $table->string('phone')->nullable();
            $table->string('country')->nullable();
            $table->string('carrier')->nullable();
            $table->date('activation_date')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('token')->nullable();

            //$table->double('insurance_amt')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNsanoCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nsano_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nsano_credit_id');
            $table->string('user_id');
            $table->double('amount', 15, 8);
            $table->string('number')->nullable();
            $table->string('network')->nullable();
            $table->string('refID')->nullable();
            $table->string('resp_msg')->nullable();
            $table->string('resp_reference')->nullable();
            $table->string('resp_code')->nullable();
            $table->string('resp_system_code')->nullable();
            $table->string('resp_transactionID')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nsano_credits');
    }
}

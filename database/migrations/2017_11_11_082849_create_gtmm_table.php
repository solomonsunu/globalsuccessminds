<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGtmmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gt_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gt_id');
            $table->string('user_id');
            $table->string('clientRef')->nullable();
            $table->double('amount')->nullable();
            $table->string('transRef')->nullable();
            $table->string('paymentMode')->nullable();
            $table->integer('statusCode')->nullable();
            $table->string('status')->default('pending');
            $table->string('senderPhone')->nullable();
            $table->string('senderName')->nullable();
            $table->string('Message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gt_transactions');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPremiumToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('premium_referrer')->nullable();
            $table->string('rank_id')->nullable();
            $table->integer('total_pre')->nullable();
            $table->double('total_bonus')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('premium_referrer');
            $table->dropColumn('rank_id');
             $table->dropColumn('total_pre');
             $table->dropColumn('total_bonus');
        });
    }
}

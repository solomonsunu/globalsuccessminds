<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class AirtimeCommision extends Model
{
     protected $table = 'user_airtime_commisions';
    protected $primaryKey = 'commision_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['college_id','college_name'];

   
     public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

     public function creditCommision($user_id, $amount)
    { 
        $this->commision_id = Uuid::uuid4();
        $this->user_id = $user_id;
        $this->amount = $amount;
        if($this->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}

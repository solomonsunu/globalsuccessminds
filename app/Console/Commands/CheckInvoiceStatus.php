<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Uuid;
use Carbon\Carbon;
use App\Helpers\LoadWallet as LoadW;
use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapClient\SoapClientOptionsBuilder;
use BeSimple\SoapCommon\SoapOptionsBuilder;

use Artisaninweb\SoapWrapper\SoapWrapper;

use SoapClient;
use App\MobileWalletTransaction;
use App\Account;
use DB;
use App\Notification;
class CheckInvoiceStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckInvoiceStatus:checkinvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CheckInvoiceStatus and credit users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // $record = (new MobileWalletTransaction)->where('status', 'pending')->orWhere('responseCode','')->orWhere('responseCode','21VD')->get();
        // $record = (new MobileWalletTransaction)->where('status', 'pending')->orWhere('responseCode','')->orWhere('responseCode','21VD')->get();
        $record = DB::select( DB::raw("SELECT * FROM mobile_wallet_transactions WHERE status = 'pending' AND (responseCode ='' OR responseCode = '21VD') AND Month(created_at) = Month(CURRENT_TIMESTAMP) AND Year(created_at) = Year(CURRENT_TIMESTAMP) ORDER BY created_at DESC") );
        // $record = (new MobileWalletTransaction)->where('status', 'pending')
        //     ->where(function($query) {
        //         $query->orWhere('responseCode',null)
        //             ->orWhere('responseCode','21DV');  
        //     })->get();
        if (count($record) > 0) {
           foreach ($record as $key => $value) {
                //$this->info($value);
                $wsdl = 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService?wsdl';
                $trace = true;
                $exceptions = true;

                $invoiceNo = $value->invoiceNo;
                $username = 'GSMLTD12';
                $password = '2i28U71fK'; 

                $xmlr = new \SimpleXMLElement("<checkInvStatus></checkInvStatus>");
                $xmlr->addChild('invoiceNo', $invoiceNo);
                $xmlr->addChild('username', $username);
                $xmlr->addChild('password', $password);


                $context = stream_context_create([
                    'ssl' => [
                        // set some SSL/TLS specific options
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    ]
                ]);
                try
                {
                   $client = new \SoapClient($wsdl,array('location' => 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService.InvoicingServiceHttpSoap11Endpoint/','trace' => $trace, 'exceptions' => $exceptions, 'soap_version' => SOAP_1_2,'stream_context' => $context));
                   $response = $client->checkInvStatus($xmlr);
                  
                }
                catch (Exception $e)
                {
                   echo "Error!";
                   echo $e -> getMessage ();
                   echo 'Last response: '. $client->__getLastResponse();
                }
                  $v = ['result'=>$response->return];
                  foreach ($v as $key => $value) {
                    $b = (array)$value;
                    break; 
                  }
                $responseCode = $b['responseCode'];
                $responseMessage = $b['responseMessage'];
                $trans = (new MobileWalletTransaction)->where('invoiceNo',$invoiceNo)->first();
                $trans->responseMessage = $responseMessage;
                $trans->responseCode = $responseCode;
                if ($responseCode == '0000') {
                    $trans->status = 'approved';
                }
                $trans->save();
                if ($responseCode == '0000') {
                    $amountCredit = 0.0;
                    if ($trans->amount < 50  ) {
                         $amountCredit = $trans->amount - 1;
                    }elseif ($trans->amount >= 50 && $trans->amount < 100 ) {
                         $amountCredit = $trans->amount - 0.5;
                    }else {
                         $amountCredit = $trans->amount;
                    }
                    // $amountCredit = $trans->amount - 1;
                    $result = (new MobileWalletTransaction)->creditMobileAccount($trans->user_id,$amountCredit);
                    if ($result) {
                         $message =new Notification;
                          $message->notification_id = Uuid::uuid4();
                          $message->user_id = $trans->user_id;
                          $message->title = 'Self Load Wallet Topup';
                          $message->detail = "Your Account has been credited with ".$amountCredit." Ghana Cedis as Wallet topup";
                          $message->status="unread";
                          $message->save();
                       
                        
                    $this->info('Account credited successfully!');
                    }
                }
           }
        }
        
         
    }
}

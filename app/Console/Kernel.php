<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Helpers\ActivateAccount;
use DB;
use App\User;
use App\Transaction;
use App\Account;
use App\Helpers\Insurance as ins;
use App\Helpers\ActivateAccount as activate;
use Illuminate\Http\Request;
use App\CompanyEarning;
use App\AccountDetail;
use App\Notification;
use App\Http\InsuranceController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
         \App\Console\Commands\CheckInvoiceStatus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('CheckInvoiceStatus:checkinvoice')->everyMinute();
        // $schedule->call(function() {
        //     $user = new User;
        //     $account = new Account;
        //     $transaction = new Transaction;

        //     (new ActivateAccount($user, $account, $transaction))->activate();

        // })->everyMinute();

        // $schedule->call(function () {
        //     $user = new User;
        //     $req  = new Request;
        //     $insurance = new Insurance;
        //     $ins = new ins;
        //     $notification = new Notification;
        //     $accountDetail = new AccountDetail;
        //     $userAccount = new Account;
        //     $activate = new activate;
        //     $earning = new CompanyEarning;

        //     $insurance_controller = new InsuranceController($user, $req, $insurance, $ins, $notification, $accountDetail, $userAccount, $activate, $earning);
        //     $insurance_controller->generate();
        // })->everyMinute();;//monthlyOn(1, '01:00');
    }
}

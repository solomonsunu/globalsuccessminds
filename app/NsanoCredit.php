<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\Account;
use Auth;
use Mail;
use App\AirtimeCommision;


class NsanoCredit extends Model
{
    protected $table = 'nsano_credits';
    protected $primaryKey = 'nsano_credit_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['college_id','college_name'];

   


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

   
  



    /**
     * Credit user's account
     *
     * @param string user_id
     * @param double $amount
     * @return boolean
     */
    // public function creditMoMoAccount($user_id, $amount,$phone, $usr = false)
    // {
    //     $transfer = 0;
    //     $transaction_balance = 0.0;
    //     $account = (new User)->where('user_id', $user_id)->first();
    //     $user = (new User)->where('user_id', $user_id)->first();
    //     if($account){
    //         if ( $account->current_balance < $amount) {
    //             $account->current_balance = $account->current_balance - $amount;
    //             $transaction_balance = $account->current_balance;
    //             if ($account->user->admin == 0) {
    //                 $transfer = 1;
    //             }
    //             if($account->save()){
    //                 $uid = ($usr) ? $usr : Auth::user()->user_id;

    //                 (new Transaction)->createTransaction([
    //                     'account_id' => $account->account_id,
    //                     'user_id'    => $uid,
    //                     'amount'     => $amount,
    //                     'type'       => 'debit',
    //                     'transfer'  =>  $transfer,
    //                     'description'=>'Withdrawal Debit',
    //                     'recipient' => $user->fullname,
    //                     'balance'   => $transaction_balance
    //                 ]);
                   
    //                 return "A withdrawal amount of $amount has been processed into your MOMO wallet on $phone. ";
    //             }
    //         }else{
    //             return 'Please You have Insufficient funds';
    //         }
           


    //     }
    //     return false;
    // }

    


    // public function transferMoney($sender_phone, $recipient_phone, $amount)
    // {
    //     $sender = (new User)->where('phone', formatPhoneNumber($sender_phone))->first();  #find user
    //     $sender_id = $sender->user_id;

    //     $recipient = (new User)->where('phone', $recipient_phone)->first();  #find recipient
    //     $recipient_id = $recipient->user_id;
    //     if($recipient){
    //         $balance = $this->getCurrentBalance($sender_id);
    //         if($balance >= $amount){
    //             if($this->debitAccount($sender_id, $amount, 1 ,$recipient->fullname, $sender_id)){
    //                 if($this->creditAccount($recipient_id, $amount, $sender_id)){
    //                     $new_balance = $balance - $amount;
    //                     $new_balance = number_format((float)$new_balance, 2, '.', '');
    //                     return 'Transaction Successful. Your balance is ' . $new_balance . 'GHS';
    //                 } 
    //                 $this->creditAccount($sender_id, $amount); //restore amount if crediting failed;                 
    //             }
    //             return 'Please we were unable to charge your account... Try again!';
    //         }
    //         return 'Please You have Insufficient funds';
    //     }
    //     return 'The Recipient is not a registered GSM member';
    // }

}

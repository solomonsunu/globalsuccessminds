<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWithdrawal extends Model
{
    protected $table = 'user_withdrawals';
    protected $primaryKey = 'user_withdrawal_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['college_id','college_name'];

   
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    
    /**
     * Get current balance for user
     *
     * @param string $user_id
     * @return string | boolean
     */
    public function getWithdrawal($user_id)
    {
        $withdrawal = $this->where('user_id', $user_id)->get();
        if ($withdrawal) return $withdrawal;
        return false;        
    }
     public function getPendingWithdrawalSuper()
    {
        $withdrawal = $this->where('paid', 0)->with(['user'=>function ($v)
        {
            $v->where('admin',0);
        }])->orderBy('created_at','DESC')->get()->take(20);
        if ($withdrawal) return $withdrawal;
        return false;        
    }
    public function getPendingWithdrawal($network)
    {
        $withdrawal = $this->where('paid', 0)->with(['user'=>function ($v) use($network)
        {
            $v->where(['carrier'=>$network,'admin'=>0]);
        }])->orderBy('created_at','DESC')->get()->take(20);
        if ($withdrawal) return $withdrawal;
        return false;        
    }
    public function getWithdrawalhistory($user,$network)
    {
        $withdrawalHistory = $this->where('user_id',$user)->with(['user'=>function ($v) use($network)
        {
            $v->where('carrier',$network);
        }])->orderBy('created_at','DESC')->get();
        if ($withdrawalHistory) return $withdrawalHistory;
        return false;        
    }

    /**
     * 
     *
     *
     *
     */
   
    
}

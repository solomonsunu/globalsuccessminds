<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;
use DB;
use Uuid;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $appends = ['fullname','amt','premium_status','referral_count','agent_ref_status','active','inactive'];

    public function generateLink()
    {


        $this->referral_link = Uuid::uuid4();
        $this->save();

    }
     public function getLinkAttribute()
    {
        return url('https://globalsuccessminds.com/register/user') . '?ref=' . $this->referral_link;
    }

    public function getPendingWithdrawal($network)
    {

        // $withdrawal = $this->where('carrier',$network)->with(['withdrawals'=>function ($v)
        // {
        //     $v->where('paid', 0);
        // }])->get();
        $withdrawal = $this->where(['carrier'=>$network,'admin'=>0])->with(['withdrawals'=>function ($v)
        {
            $v->where('paid', 0);
        }])->get();
        if ($withdrawal) return $withdrawal;
        return false;        
    }

     public function getPendingWithdrawalSuper()
    {   
        $withdrawal = $this->where('admin',0)->with(['withdrawals'=>function ($v)
        {
            $v->where('paid',0);
        }])->get();

        if ($withdrawal) return $withdrawal;
        return false;        
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['user_id','title','department_id','firstname','lastname','othername','dob','phone','address','gender','email', 'password','user_role','status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function account()
    {
        return $this->hasOne('App\Account','user_id');
    }

    public function cycle()
    {
        return $this->hasMany('App\Cycle','user_id');
    }

    public function insurance()
    {
        return $this->hasMany('App\Insurance','user_id');
    }

    public function accountdetail()
    {
        return $this->hasMany('App\AccountDetail','user_id');
    }
    public function withdrawals()
    {
        return $this->hasMany('App\UserWithdrawal','user_id');
    }
    public function transactions()
    {
        return $this->hasMany('App\Transaction','user_id');
    }
    public function commisions()
    {
        return $this->hasMany('App\AirtimeCommision','user_id');
    }


    /**public function getAmtAttribute()
    {
        $account = (new Account)->where('user_id',$this->user_id)->first();
        $total = 0;
        if ($account) {
            $transaction = (new Transaction)->where('account_id',$account->account_id)
                                ->where('type','debit')->where('transfer',0)
                                ->where('created_at', '>=', Carbon::now()->subMonth())
                                ->where('created_at','<',Carbon::now()->startOfMonth())
                                ->where(DB::raw("Year(created_at)"),date('Y'))
                                ->sum('amount');
            $total = $total + $transaction;
        }
        return $total;
    }**/


     public function getReferralCountAttribute()
    {
        $premiumUser =  $this->where('premium_referrer', $this->user_id)->count();
        if ($premiumUser >= 5) {
            return 1;
        }
        return 0;
    }

    //  public function getAmtAttribute()
    // {
    //     $account = (new Account)->where('user_id',$this->user_id)->first();
    //     $total = 0;
    //     if ($account) {
    //         $transaction = (new Transaction)->where('account_id',$account->account_id)
    //                             ->where('type','debit')->where('transfer',0)
    //                             ->where('created_at', '>=', Carbon::now()->startOfMonth())
    //                             ->where(DB::raw("Year(created_at)"),date('Y'))
    //                             ->sum('amount');
    //         $total = $total + $transaction;
    //     }
    //     return $total;
    // }


     public function getAmtAttribute()
     {
         $account = (new Account)->where('user_id',$this->user_id)->first();
         $total = 0;
         if ($account) {
             $transaction = (new Transaction)->where('account_id',$account->account_id)
                                 ->where('type','debit')->where('transfer',0)
                                 ->whereMonth('created_at', '=', 3)
                                 ->whereYear('created_at','=',2019)
                                 ->sum('amount');
             $total = $total + $transaction;
         }
         return $total;
     }
    public function getPremiumStatusAttribute()
    {
        if (Carbon::now()->month == Carbon::parse($this->premium_update_date)->month) {
            return 1;
        }
        return 0;

        
    }

    public function getAgentRefStatusAttribute()
    {
        $total = $this->where('brought_by',$this->user_id)->where('agent',1)->count();
        if ($total > 0) {
            return 1;
        }
        return 0;

        
    }

    public function getActiveAttribute()
    {
        $total = $this->where('paymentstatus',1)->count();
        
        return $total;

        
    }

public function getInactiveAttribute()
    {
        $total = $this->where('paymentstatus',0)->count();
        
        return $total;

        
    }




    // public function getPremiumStatusAttribute()
    // {
    //     if (Carbon::now()->subMonth()->month == Carbon::parse($this->premium_update_date)->month) {
    //         return 1;
    //     }
    //     return 0;
    // }
    public function ranks()
    {
        return $this->belongsTo('App\Rank', 'rank_id');
    }
    /**
     * Base64encode pin
     *
     */
    public function setPinAttribute($pin)
    {
        $this->attributes['pin'] = base64_encode($pin);
    }

    /**
     * Base64decode pin
     *
     *
     */
    public function getPinAttribute($pin)
    {
        return base64_decode($pin);
    }

    /**
     * Generate random pin for user
     *
     * @return Integer;
     */

    public function generatePin()
    {
        return mt_rand(1000, 9999);
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(30);
        });
    }

    /**
     * Set the password attribute.
     *
     * @param string $password
     */
    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['password'] = bcrypt($password);
    // }

    /**
     * Confirm the user.
     *
     * @return void
     */
    public function confirmEmail()
    {
        $this->verified = true;
        $this->active = 1;
        $this->token = null;

        $this->save();
    }


    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function validateUssdUser($phone, $pin)
    {
        $user = $this->where('phone', formatPhoneNumber($phone))->where('pin', base64_encode($pin))->first();
        if($user) return true;
        return false;
    }

    public function verifyPhone($phone)
    {
        $user = $this->where('phone', $phone)->first();
        if($user) return true;
        return false;
    }

    public function validatePin($phone, $pin)
    {
        $user = $this->where('phone', formatPhoneNumber($phone))->where('pin', base64_encode($pin))->first();
        if($user) return true;
        return false;
    }

    public function changePin($phone, $pin)
    {
        $user = User::where('phone', $phone)->first();
        $user->pin = $pin;
        $user->save();
        return true;
    }
}

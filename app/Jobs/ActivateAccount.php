<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Transaction;
use App\Account;
use Carbon\Carbon;

class ActivateAccount extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $balance = 0.0;
        $account = $this->account->where('user_id',$this->user->user_id)->first();
        if($account) {
            $transaction = Transaction::where('account_id',$account->account_id)->where('type','debit')->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
        
            $balance = $transaction;
            if ($balance >= 1.00) {
                $this->user->paymentstatus = 1;
                $this->user->save();
                return true;
            }
        }
    
    }
}

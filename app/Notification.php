<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $primaryKey = 'notification_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['log_id','user_id','arrival_time','departure_time','special_case','lat1','log1','lat2','log2'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function createNotification($notification)
    {
        
        // $user = (new User)->where('user_id', $notification['user_id'])->first();
        $this->notification_id = Uuid::uuid4();
        $this->user_id = $notification['user_id'];
        $this->title = $notification['title'];
        $this->detail = $notification['detail'];
        $this->status = $notification['status'];
        

        if($this->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\Account;
use App\User;
use Auth;
use Mail;


class MobileWalletTransaction extends Model
{
    protected $table = 'mobile_wallet_transactions';
    protected $primaryKey = 'mobile_wallet_id';


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

   
    public function creditMobileAccount($user_id, $amount)
    {
        $transfer = 0;
        $transaction_balance = 0.0;
        $account = (new Account)->where('user_id', $user_id)->with('user')->first();
        $user = (new User)->where('user_id', $user_id)->first();
        $admin = (new User)->where('admin', 1)->first();
        if($account){
            $account->current_balance += $amount;
            $transaction_balance = $account->current_balance;
            if ($account->user->admin == 0) {
                $transfer = 1;
            }
            if($account->save()){
                
                (new Transaction)->createTransaction([
                    'account_id' => $account->account_id,
                    'user_id'    => $admin->user_id,
                    'amount'     => $amount,
                    'type'       => 'credit',
                    'transfer'  =>  $transfer,
                    'description'=>'',
                    'recipient' => $user->fullname,
                    'balance'   => $transaction_balance
                ]);
                //  Mail::send('emails.creditNotification', ['account' => $account,'amount'=>$amount], function ($m) use ($account) {
                //     $m->from('admin@globalsuccessminds.com ', 'Global Success Minds');

                //     $m->to($account->user->email, $account->user->firstname)->subject('Account Credit Alert!');
                // });
                return true;
            }
        }
        return false;
    }


    

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;
class Rank extends Model
{
    protected $table = 'ranks';
    protected $primaryKey = 'rank_id';

    public function users()
    {
    	return $this->hasMany('App\User', 'rank_id', 'rank_id');
    }

    
}

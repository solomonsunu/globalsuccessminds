<?php

namespace App\Mailers;

use App\User;
use App\Account;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer
{

    /**
     * The Laravel Mailer instance.
     *
     * @var Mailer
     */
    protected $mailer;

    /**
     * The sender of the email.
     *
     * @var string
     */
    protected $from = 'admin@globalsuccessminds.com ';

    /**
     * The recipient of the email.
     *
     * @var string
     */
    protected $to;

    /**
     * The view for the email.
     *
     * @var string
     */
    protected $view;

    /**
     * The data associated with the view for the email.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Create a new app mailer instance.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Deliver the email confirmation.
     *
     * @param  User $user
     * @return void
     */

     
    //  public function creditNotification(Post $post)
    // {
        
    //     $this->to = $post->company->company_email;
    //     $this->view = 'emails.newPostNotification';
    //     $this->data = compact('post');

    //     $this->deliverNotification();
    // }

     public function debitNotification(Account $account,$mount)
    {
        
        $this->to = $account->user->email;
        $this->view = 'emails.debitNotification';
        $this->data = compact('account','amount');

        $this->deliver();
    }

    /**
     * Deliver the email.
     *
     * @return void
     */
    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function ($message) {
            $message->from($this->from, 'Administrator')
                    ->to($this->to)->subject('Accout Transaction Notification');;
        });
    }
     
}

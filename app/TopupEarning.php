<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;
use Carbon\Carbon;
use DB;

class TopupEarning extends Model
{
    protected $table = "topup_earning";

    public function saveCommision($network,$amount)
    {
    	$commision = 0.0;
    	if ($network == 4) {
    		$commision = $amount * 0.05;
    		 $earning = $this->where('network','mtn')->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->first();
    		 if ($earning) {
    		 	$earning->commision  = $earning->commision + $commision;
    		 	if($earning->save()){
		            return response()->json(['status' => true]);
		        }
		        return response()->json(['status' => false]);
    		 	
    		 }else{
    		 	$this->topup_id = Uuid::uuid4();
		        $this->network = 'mtn';
		        $this->commision = number_format((float)$commision, 2, '.', '');
		        if($this->save()){
		            return response()->json(['status' => true]);
		        }
		        return response()->json(['status' => false]);
    		 }
    		 

    	}else{
    		$commision = $amount * 0.07;
    		 $earning = $this->where('network','other')->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->first();
    		 if ($earning) {
    		 	$earning->commision  = $earning->commision + $commision;
    		 	if($earning->save()){
		            return response()->json(['status' => true]);
		        }
		        return response()->json(['status' => false]);
    		 	
    		 }else{
    		 	$this->topup_id = Uuid::uuid4();
		        $this->network = 'other';
		        $this->commision = number_format((float)$commision, 2, '.', '');
		        if($this->save()){
		            return response()->json(['status' => true]);
		        }
		        return response()->json(['status' => false]);
    		 }
    	}
        
    }
}

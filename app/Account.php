<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use Auth;
use Mail;
use App\AirtimeCommision;


class Account extends Model
{
    protected $table = 'accounts';
    protected $primaryKey = 'account_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['college_id','college_name'];

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'account_id', 'account_id');
    }


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function setCurrentBalanceAttribute($balance)
    {
        return $this->attributes['current_balance'] = number_format((float)$balance, 2, '.', '');
    }

    public function getCurrentBalanceAttribute($balance)
    {
        return number_format((float)$balance, 2, '.', '');
    }


    /**
     * Get current balance for user
     *
     * @param string $user_id
     * @return string | boolean
     */
    public function getCurrentBalance($user_id)
    {
        $account = $this->where('user_id', $user_id)->first();
        if ($account) return $account->current_balance;
        return false;
    }

    /**
     * Debit user's account
     *
     * @param string user_id
     * @param double $amount
     * @return boolean
     */
    public function debitAccountOld($user_id, $amount, $withdraw = 0, $recipient_name = null, $usr = false)
    {
        $transfer = 0;
        $transaction_balance = 0.0;
        $account = $this->where('user_id', $user_id)->with('user')->first();
        if($account){

            $account->current_balance -= $amount;
            $transaction_balance = $account->current_balance;

            if ($account->save()) {
                 // dd($transfer);
                if ($account->user->admin == 0 && $withdraw == 0) {
                    $transfer = 0;
                }
                if ($account->user->admin == 0 && $withdraw == 1) {
                    $transfer = 1;
                }
                if ($account->user->admin == 0 && $withdraw == 2) {
                    $transfer = 2;
                }
                $uid = ($usr) ? $usr : Auth::user()->user_id;

                // dd($recipient_name);
                (new Transaction)->createTransaction([
                    'account_id' => $account->account_id,
                    'user_id'    => $uid,
                    'amount'     => $amount,
                    'type'       => 'debit',
                    'transfer'  => $transfer,
                    'description'=>'',
                    'recipient'  => $recipient_name,
                    'balance'   => $transaction_balance
                ]);

                // Mail::send('emails.debitNotification', ['account' => $account,'amount'=>$amount], function ($m) use ($account) {
                //     $m->from('admin@globalsuccessminds.com ', 'Global Success Minds');

                //     $m->to($account->user->email, $account->user->firstname)->subject('Account Debit Alert!');
                // });
                // (new AppMailer())->debitNotification($account,$amount);

                return true;
            }
        }
        return false;
    }

    //modifications to accomodate 3% commision on instant topups
     public function debitAccount($user_id, $amount, $withdraw = 0, $recipient_name = null, $usr = false)
    {
        $a = [$user_id, $amount, $withdraw, $recipient_name];
         
        // return json_encode($a);
        $transfer = 0;
        $commision = 0.0;
        $transaction_balance = 0.0;
        $account = $this->where('user_id', $user_id)->with('user')->first();
        if($account){
            if ($account->user->admin == 0 && $withdraw == 0 && $account->user->agent == 0) {
                $account->current_balance = $account->current_balance - $amount;
                $transaction_balance = $account->current_balance;
                $commision = $amount * 0.01;
            }elseif($account->user->admin == 0 && $withdraw == 0 && $account->user->agent == 1){
                $account->current_balance = $account->current_balance - ($amount * 0.96);
                $transaction_balance = $account->current_balance;
            }
            else{
                $account->current_balance -= $amount;
                $transaction_balance = $account->current_balance;
            }
            

            if ($account->save()) {
                 // dd($transfer);
                if ($account->user->admin == 0 && $withdraw == 0) {
                    $transfer = 0;
                }
                if ($account->user->admin == 0 && $withdraw == 1) {
                    $transfer = 1;
                }
                if ($account->user->admin == 0 && $withdraw == 2) {
                    $transfer = 2;
                }
                // 3 = upgrade
                if ($account->user->admin == 0 && $withdraw == 3) {
                    $transfer = 3;
                }
                $uid = ($usr) ? $usr : Auth::user()->user_id;

                (new AirtimeCommision)->creditCommision($uid,$commision);
                // dd($recipient_name);
                (new Transaction)->createTransaction([
                    'account_id' => $account->account_id,
                    'user_id'    => $uid,
                    'amount'     => $amount,
                    'type'       => 'debit',
                    'transfer'  => $transfer,
                    'description'=>'',
                    'recipient'  => $recipient_name,
                    'balance'   => $transaction_balance
                ]);

                // Mail::send('emails.debitNotification', ['account' => $account,'amount'=>$amount], function ($m) use ($account) {
                //     $m->from('admin@globalsuccessminds.com ', 'Global Success Minds');

                //     $m->to($account->user->email, $account->user->firstname)->subject('Account Debit Alert!');
                // });
                // (new AppMailer())->debitNotification($account,$amount);

                return true;
            }
        }
        return false;
    }


    /**
     * Credit user's account
     *
     * @param string user_id
     * @param double $amount
     * @return boolean
     */
    public function creditAccount($user_id, $amount, $usr = false)
    {
        $transfer = 0;
        $transaction_balance = 0.0;
        $account = $this->where('user_id', $user_id)->with('user')->first();
        $user = (new User)->where('user_id', $user_id)->first();
        if($account){
            $account->current_balance += $amount;
            $transaction_balance = $account->current_balance;
            if ($account->user->admin == 0) {
                $transfer = 1;
            }
            if($account->save()){
                $uid = ($usr) ? $usr : Auth::user()->user_id;

                (new Transaction)->createTransaction([
                    'account_id' => $account->account_id,
                    'user_id'    => $uid,
                    'amount'     => $amount,
                    'type'       => 'credit',
                    'transfer'  =>  $transfer,
                    'description'=>'',
                    'recipient' => $user->fullname,
                    'balance'   => $transaction_balance
                ]);
                //  Mail::send('emails.creditNotification', ['account' => $account,'amount'=>$amount], function ($m) use ($account) {
                //     $m->from('admin@globalsuccessminds.com ', 'Global Success Minds');

                //     $m->to($account->user->email, $account->user->firstname)->subject('Account Credit Alert!');
                // });
                return true;
            }
        }
        return false;
    }

    //to remove over payment
        public function deductCreditAccount($user_id, $amount, $usr = false)
    {
        $transfer = 0;
        $transaction_balance = 0.0;
        $account = $this->where('user_id', $user_id)->with('user')->first();
        $user = (new User)->where('user_id', $user_id)->first();
        if($account){
            $account->current_balance = $account->current_balance - $amount;
            $transaction_balance = $account->current_balance;
            if ($account->user->admin == 0) {
                $transfer = 1;
            }
            if($account->save()){
                $uid = ($usr) ? $usr : Auth::user()->user_id;

                (new Transaction)->createTransaction([
                    'account_id' => $account->account_id,
                    'user_id'    => $uid,
                    'amount'     => $amount,
                    'type'       => 'debit',
                    'transfer'  =>  $transfer,
                    'description'=>'',
                    'recipient' => $user->fullname,
                    'balance'   => $transaction_balance
                ]);
                //  Mail::send('emails.creditNotification', ['account' => $account,'amount'=>$amount], function ($m) use ($account) {
                //     $m->from('admin@globalsuccessminds.com ', 'Global Success Minds');

                //     $m->to($account->user->email, $account->user->firstname)->subject('Account Credit Alert!');
                // });
                return true;
            }
        }
        return false;
    }


    public function transferMoney($sender_phone, $recipient_phone, $amount)
    {
        $sender = (new User)->where('phone', formatPhoneNumber($sender_phone))->first();  #find user
        $sender_id = $sender->user_id;

        $recipient = (new User)->where('phone', $recipient_phone)->first();  #find recipient
        $recipient_id = $recipient->user_id;
        if($recipient){
            $balance = $this->getCurrentBalance($sender_id);
            if($balance >= $amount){
                if($this->debitAccount($sender_id, $amount, 1 ,$recipient->fullname, $sender_id)){
                    if($this->creditAccount($recipient_id, $amount, $sender_id)){
                        $new_balance = $balance - $amount;
                        $new_balance = number_format((float)$new_balance, 2, '.', '');
                        return 'Transaction Successful. Your balance is ' . $new_balance . 'GHS';
                    } 
                    $this->creditAccount($sender_id, $amount); //restore amount if crediting failed;                 
                }
                return 'Please we were unable to charge your account... Try again!';
            }
            return 'Please You have Insufficient funds';
        }
        return 'The Recipient is not a registered GSM member';
    }

}

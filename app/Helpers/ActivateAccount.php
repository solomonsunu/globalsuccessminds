<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use Carbon\Carbon;


class ActivateAccount {
	public function __construct(User $user)
    {
        $this->user = $user;
        // $this->account = $account;
        // $this->transaction = $transaction;
    }

    public function activate(){
        $balance = 0.0;
        $account = Account::where('user_id',$this->user->user_id)->first();
        if($account) {
            $transaction = Transaction::where('account_id',$account->account_id)->where(['type'=>'debit','transfer'=>0])->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
        
            $balance = $transaction;
            //chamge amount from 1 cedi to 5 cedi
            if ($balance >= 5.00) {
                $this->user->paymentstatus = 1;
                $this->user->save();
                return 'true';
            }
        }
        return 'false';

        // $search = User::where('brought_by','!=',0)->where('admin',0)->where('paymentstatus',0)->where('phone',$phone)->first();
        // if ($search) {
        //     $balance = 0.0;
        //         $account = $this->account->where('user_id',$search->user_id)->first();
        //         if($account) {
        //             $transaction = Transaction::where('account_id',$account->account_id)->where('type','debit')->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                
        //             $balance = $transaction;
        //             if ($balance >= 10.00) {
        //                 $value->paymentstatus = 1;
        //                 $value->save();
        //                 return true;
        //             }
        //         }
        // }
        // return false;
    }
    // public function activate($phone){
    // 	$search = $this->user->where('brought_by','!=',0)->where('admin',0)->where('paymentstatus',0)->where('phone',$phone)->first();
    // 	if ($search) {
    // 		foreach ($search as $key => $value) {
    // 			$balance = 0.0;
    // 			$account = $this->account->where('user_id',$value->user_id)->first();
    // 			if ($account) {
				// 	$transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
				
				// 	$balance = $transaction;
				// 	if ($balance >= 10.00) {
				// 		$value->paymentstatus = 1;
				// 		$value->save();
				// 	}
				// }
    // 		}
    // 		return true;
    // 	}
    // 	return false;
    // }
	

}

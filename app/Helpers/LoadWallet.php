<?php
namespace App\Helpers;
use App\User;
use Cache;
use App\Account;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Uuid;
use Mail;
use Session;
use GuzzleHttp\Client;
use Ixudra\Curl\Facades\Curl;



class LoadWallet
{
    use DispatchesJobs;

    const NSANO_API_DOMAIN = "https://fs1.nsano.com:5001/api/fusion/tp/d219f17ec2ab45aa87c7f272126527e1";

  

    //Set up variables
    public static $kuwaita= "mikopo";
    public static $refID = 102202222;
    public static $msg = "success";
    // $amount = $_SESSION['amount'];
    // $mno = $_SESSION['mno'];
    // $msisdn = $_SESSION['msisdn'];
    // $_SESSION['response'] = '';
    

    
    public function validateUser($phone, $pin)
    {
        $user = (new User)->where('phone', $phone)->where('pin', $pin)->first();
        if($user) return true;
        return false;
    }

    public function verifyUser($phone)
    {
        $user = User::where('phone', formatPhoneNumber($phone))->first();
        if($user) return true;
        return false;
    }

     public function creditCurlRequest($data)
    {
      
        $response = Curl::to('https://fs1.nsano.com:5001/api/fusion/tp/d219f17ec2ab45aa87c7f272126527e1')
                    ->withData( array('kuwaita'=>'mikopo', 'msisdn' => $data['msisdn'],'mno'=>$data['mno'],'refID'=>$data['refID'],'amount'=>$data['amount']) )->post();
        // dd($response);
        return $response;   
    }
    public function makeCurlRequest($data)
    {
        // $response = Curl::to('https://fs1.nsano.com:5001/api/fusion/tp/d219f17ec2ab45aa87c7f272126527e1')
        //             ->withData( array('kuwaita'=>'mikopo', 'msisdn' => $data['msisdn'],'mno'=>$data['mno'],'refID'=>'341425665','amount'=>$data['amount']) )->post();
        //debit test
        $response = Curl::to('https://fs1.nsano.com:5001/api/fusion/tp/d219f17ec2ab45aa87c7f272126527e1')
                    ->withData( array('kuwaita'=>'malipo', 'msisdn' => $data['msisdn'],'mno'=>$data['mno'],'amount'=>$data['amount']) )->post();
    
        return $response;   
    }


    public function topUpWallet($phone, $amount, $network)
    {
        $user = (new User)->where('phone', $phone)->first();
        $recipient = ($user) ? $user->fullname : $phone;

            //TopUpApi request on success
            $nphone = "233".substr($phone, 1);
            $topup = $this->topUpRequest($nphone, $amount, $network);
           
            if($topup){
                $credit = (new Account)->creditAccount($user->user_id, $amount);
                if($credit){
                    return "You have successfully loaded your wallet  ".$amount."GHS"; // wallet load successful
                }
            }else{
                return "Unable to process transaction. Try again!!!";
            }
        
    }

 
}

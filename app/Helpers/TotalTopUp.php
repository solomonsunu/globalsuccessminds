<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use Carbon\Carbon;


class TotalTopUp  {
	public function __construct(User $user,Account $account,Transaction $transaction)
    {
        $this->user = $user;
        $this->account = $account;
        $this->transaction = $transaction;

    }

	
	// protected function paycount($set){
	// 	$total = 0.0;
	// 	foreach ($set as $key => $value) {
	// 		$account = $this->account->where('user_id',$value->user_id)->first();
	// 		if ($account) {
	// 			$transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
	// 			$total = $total + $transaction;
	// 			}
	// 	}

	// 	return $total;
	// }
	protected function paycount1($set,$dt){
		$total = 0.0;
		foreach ($set as $key => $value) {
			$account = $this->account->where('user_id',$value->user_id)->first();
			if ($account) {
				$transaction = $this->transaction->where('account_id',$account->account_id)->where('type','credit')->where('transfer',1)->whereRaw('date(created_at) = ?', [$dt])->sum('amount');
				$total = $total + $transaction;
				}
		}

		return $total;
	}
	public function getTotals($network,$dt){
		$total_credit = 0.0;
		$search = User::where('brought_by','!=',0)->where(['admin'=>0])->where('carrier',$network)->orderBy('user_id')->get();
		if (count($search) > 0) {
			// if ($dt) {
			// 	$total_credit = $this->paycount1($search,$dt);
			// 	return $total_credit;
			// }else{
			// 	$total_credit = $this->paycount($search);
			// 	return $total_credit;
			// }
			$total_credit = $this->paycount1($search,$dt);
			return $total_credit;
		}else{
			return 0.0;
		}
		

	}


}

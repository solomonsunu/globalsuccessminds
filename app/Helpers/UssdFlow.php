<?php
namespace App\Helpers;

use Cache;
use App\User;
use App\Account;
use Auth;
use duncan3dc\Forker\Fork;
use Icicle\Awaitable;
use Icicle\Coroutine\Coroutine;
use Icicle\Loop;

class UssdFlow
{
    protected $storedData = ['phone' => '','currentStep' => 1, 'prevans' => '', 
                            'option'=>'', 'fopt'=>'', 'amount'=>'', 'sopt' => null];

    protected $regData = [];
    protected $regForm = [1=>'First name',2=>'Last name',3=>'username',4=>'password (6 characters)',5=>'email',
                        6=>'Mobile number',7=>'Network type', 8=>'referred by'];
    public $ussdData;          
    private $cacheTimeout = 2; // mins

    public function selectActivity()
    {
        return ["Welcome to Global Success Minds, Please select an activity. \n1. Top up Airtime \n2. My Account \n3. Transfer Money \n4. Load wallet \n5. Other services", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }
     // Balance Check \n3. Change PIN


    public function checkBalance()
    {
        $pin = $this->ussdData->ussdinput;

        if((new User)->validatePin($this->ussdData->msisdn, $pin)){
            $user = User::where('phone', formatPhoneNumber($this->ussdData->msisdn))->first();

            $balance = (new Account)->getCurrentBalance($user->user_id);
            return ["Your current balance is " . $balance . "GHS", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        }
        return ["Wrong PIN... Try again!!!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
    }

    public function enterAmount()
    {
        return ["Enter Amount (digits)", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }
    public function enterPhone()
    {
        return ["Enter Mobile Number", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }
    public function confirmPhone()
    {
        return ["Repeat Mobile Number", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }
    
    public function currentPin()
    {
        return ["Enter your current PIN", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function choosePhoneOption()
    {
        return ["Number To Recharge. \n1. This Number \n2. Another Number", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function selectNetwork()
    {
        return ["Select network. \n1. AIRTEL \n2. GLO \n3. MTN \n4. TIGO \n5. VODAFONE", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }


    public function selectLoadNetwork()
    {
        return ["Select network. \n1. AIRTEL \n2. MTN \n3. TIGO \n4. VODAFONE", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }


    public function otherServices()
    {
        return ["Select a service. \n1. Purchase surfline data \n2. Purchase busy data", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function myAccount($value='')
    {
        return ["Select an option. \n1. Balance Check \n2. Change PIN", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }


    public function tCurrentPin()
    {
        $msg = "Enter PIN to confirm top up of ".$this->storedFlow->amount."GHS to ".$this->storedFlow->topupPhone;
        return [$msg, Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function newPin()
    {
        $cp = (new User)->validateUssdUser($this->ussdData->msisdn, $this->ussdData->ussdinput);
        if($cp)
            return ["Please Enter a new 4 digit PIN", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
        return ["Wrong PIN... Try again!!!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
    }

    public function changePin()
    {
        $this->storedFlow->transfer = false;
        $chgPin = (new User)->changePin(formatPhoneNumber($this->ussdData->msisdn), $this->storedFlow->option);
        if($chgPin)
            return ["You have successfully changed your PIN", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        return ["Unable to change PIN. Try again!!!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
    }

    public function recharge($amount)
    {
        // $this->storedFlow->amount = $amount;

        return ["Please Enter your PIN", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function verifyPin($recharge = false, $transfer = false)
    {
        // Cache::put('sf',[$this->storedFlow->phone,  $this->ussdData->ussdinput], 2);
        $vpin = (new User)->validateUssdUser($this->storedFlow->phone,  $this->ussdData->ussdinput);
        if(!$vpin){
            return ["Invalid PIN provided!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        }
        else{
            if($transfer){ 
                if($this->storedFlow->recipient_number == $this->storedFlow->repeat_recipient_number){
                    $msg = (new Account)->transferMoney($this->storedFlow->phone, $this->storedFlow->recipient_number, $this->storedFlow->amount);
                }else{
                    $msg = "Mobile numbers entered do not match"; //gl0balbanasc0
                }

                return [$msg, Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
            }
            if($recharge){
                if($this->storedFlow->topupPhone == $this->storedFlow->vtopupPhone){
                    // $vphone = (new User)->verifyPhone($this->storedFlow->topupPhone);
                    // if($vphone){

                		// remove this block if expresso network is added
                		if($this->storedFlow->network > 1){
                			$this->storedFlow->network++;
                		}
                        $msg = (new Ussd)->topUp($this->ussdData->msisdn, $this->storedFlow->topupPhone, $this->storedFlow->amount, $this->storedFlow->network);
                        return [$msg, Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
                    // }
                    // return ['The Recipient is not a registered GSM member', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];                    
                }
                return ['Mobile numbers entered do not match', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
            }
        }
        return true;
    }

    public function recipientNumber()
    {
        $this->storedFlow->transfer = true;
        return ["Enter recipient mobile number", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function amountTransfer()
    {
       
        return ["Enter amount to transfer", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
    }

    public function flow(){
        return  [
                [ 'step' => 1, 'options' => [ 1 => $this->enterAmount(), 2  => $this->currentPin(), 3 => $this->currentPin(), 4 => $this->recipientNumber()] ],

                [ 'step' => 2, 'prevans' => [ 1 => [ 1 => $this->enterAmount(), 2 => $this->enterPhone() ],
                                              3 => $this->newPin(),
                                              4 => $this->amountTransfer()
                                          ]
                                      ],

                [ 'step' => 3, 'prevans' => [ 1 => [1 => $this->recharge(2), 2 => $this->recharge(5), 3 => $this->recharge(10), 4 => $this->recharge(20), 5 => $this->recharge(50) ]
                                             ]
                                         ],

                [ 'step' => 4, 'options' => [1 => $this->verifyPin()] ],
                [ 'step' => 5, 'options' => []],
                [ 'step' => 6, 'options' => []],
                [ 'step' => 7, 'options' => []],
                [ 'step' => 8, 'options' => []],
                [ 'step' => 9, 'options' => []]

        ];
    }

    public function performLoadRequest($data)
    {
        if((new User)->validatePin($data->phone, $data->pin)){
            $data->phone = formatPhoneNumber($data->phone);
            //error_log(json_encode($data));
            if($data->loadNetwork == 2){
              
                $msg = (new Ussd)->mtn_ussd_load_wallet($data->loadAmount, $data->phone, $data->loadPhone);
               
                 
                return ['Your request is being processed', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
                //return ['Service Unavailable. Try Again Later', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
            }else{
                return ["Only MTN is currently supported", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
            }
        }else{
            return ["Invalid PIN provided!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        }
    }

    public function windFlow($currentStep, $option)
    {
        // error_log(json_encode($this->storedFlow));
        $aoption = (int) $option;
        $flows = $this->flow();
        foreach($flows as $flow){

            if ($currentStep == $flow['step']) {
                if($currentStep == 1 ){ #Current pin
                    $this->storedFlow->fopt = $option;
                }
                ################################################
                ##                                            ##
                ##                  STEP 1                    ##
                ##                                            ##
                ################################################

                // receive amount
                if ($this->storedFlow->fopt == 1 && $currentStep == 2) {
                    $this->storedFlow->amount = $option;
                    return $this->choosePhoneOption();
                    // return $this->enterPhone();
                }
                // recieves phone option (this/another)
                if ($this->storedFlow->fopt == 1 && $currentStep == 3) {
                    if($option == 1){
                        $this->storedFlow->thisNumber = true;
                        $this->storedFlow->topupPhone = formatPhoneNumber($this->ussdData->msisdn);
                        return $this->selectNetwork();
                    }
                    if($option == 2){
                        $this->storedFlow->thisNumber = false;
                        return $this->enterPhone();
                    }
                }
                if ($this->storedFlow->fopt == 1 && $currentStep == 4) {
                    if($this->storedFlow->thisNumber == true){
                        $this->storedFlow->network = $option;
                        $this->storedFlow->vtopupPhone = formatPhoneNumber($this->ussdData->msisdn);
                        return $this->tCurrentPin();
                    }else{
                        $this->storedFlow->topupPhone = $option;
                        return $this->confirmPhone();
                    }
                }
                if($this->storedFlow->fopt == 1 && $currentStep == 5){
                    if($this->storedFlow->thisNumber == true){
                        $this->storedFlow->vtopupPhone = formatPhoneNumber($this->ussdData->msisdn);
                        return $this->verifyPin(true);
                    }else{
                        $this->storedFlow->vtopupPhone = $option;
                        return $this->selectNetwork();
                    }
                }
                //receives confirm phone
                if ($this->storedFlow->fopt == 1 && $currentStep == 6) {
                    $this->storedFlow->network = $option;
                    return $this->tCurrentPin(); 
                }
                //receives pin 
                if ($this->storedFlow->fopt == 1 && $currentStep == 7) {
                    $this->storedFlow->cpin = $option;
                    return $this->verifyPin(true);
                }
                

                ################################################
                ##                                            ##
                ##                  STEP 2                    ##
                ##                                            ##
                ################################################

                //MENU OPTION 2: // MY ACCOUNT
                if($this->storedFlow->fopt == 2 && is_null($this->storedFlow->sopt)){
                    if($currentStep == 1){
                        return $this->myAccount();
                    }
                    if($currentStep == 2 ){
                        $this->storedFlow->sopt = $option;
                        // check balance
                        if($option == 1){
                            return $this->currentPin();
                        }else if ($option == 2) {
                            // error_log('change pin');
                            return $this->currentPin();
                        } 

                    }
                }
                if($this->storedFlow->fopt == 2){
                    // check balance 
                    if($this->storedFlow->sopt == 1 && $currentStep == 3){
                        return $this->checkBalance();
                    }


                    if($this->storedFlow->sopt == 2 && $currentStep == 3){
                        return $this->newPin();   
                    }
                    if($this->storedFlow->sopt == 2 && $currentStep == 4){
                        return $this->changePin();   
                    }
                    
                }



                ################################################
                ##                                            ##
                ##                  STEP 3                    ##
                ##                                            ##
                ################################################

                if($currentStep == 1 && $this->storedFlow->fopt == 3){ # Current pin
                    $this->storedFlow->transfer = false;
                    return $this->recipientNumber();
                }



                // recieves recipient phone number
                if($currentStep == 2 && $this->storedFlow->fopt == 3){
                     $this->storedFlow->recipient_number = $option;
                    return $this->confirmPhone();
                }

                // recieves recipient phone number
                if($currentStep == 3 && $this->storedFlow->fopt == 3){
                     $this->storedFlow->repeat_recipient_number = $option;
                    return $this->amountTransfer();
                }

                //recieves amount to transfer
                if($currentStep == 4 && $this->storedFlow->fopt == 3){
                    $this->storedFlow->amount = $this->storedFlow->option;
                    return $this->currentPin();
                }
                //recieves pin 
                if($currentStep == 5  && $this->storedFlow->fopt == 3){
                    // error_log('verify pin');
                    return $this->verifyPin(false, true);
                }
                
                // if($currentStep == 3 && $this->storedFlow->fopt == 3){//new pin
                //     $this->storedFlow->transfer = false;
                //     return $this->changePin();
                // }
                ################################################
                ##                                            ##
                ##                  STEP 4                    ##
                ##                                            ##
                ################################################

                if( $this->storedFlow->fopt == 4 && $currentStep == 1){
                    return $this->enterAmount();
                }
                if( $this->storedFlow->fopt == 4 && $currentStep == 2){
                    $this->storedFlow->loadAmount = $this->storedFlow->option;
                    return $this->enterPhone();
                }
                if( $this->storedFlow->fopt == 4 && $currentStep == 3){
                    $this->storedFlow->loadPhone = $this->storedFlow->option;
                    return $this->selectLoadNetwork();
                }
                if( $this->storedFlow->fopt == 4 && $currentStep == 4){
                    $this->storedFlow->loadNetwork = $this->storedFlow->option;
                    return $this->currentPin();
                }
                if( $this->storedFlow->fopt == 4 && $currentStep == 5){
                    $this->storedFlow->pin = $this->storedFlow->option;
                    return $this->performLoadRequest($this->storedFlow);

                }








                if($this->storedFlow->fopt == 5 && $currentStep == 1){
                    return $this->otherServices();
                }
                if($this->storedFlow->fopt == 5 && $currentStep == 2){
                    // return $this->serviceProcessStart();
                }





                // error_log(json_encode($flow));
                if (array_key_exists('prevans', $flow)) {
                    $this->storedFlow->transfer = false;
                    return  $flow['prevans'][$this->storedFlow->prevans][$aoption];
                }
                // load wallet
               



                return  $flow['options'][$aoption];
            }
        }
    }

    public function activity($ussdData)
    {
        $this->ussdData = $ussdData; //create object of ussd data  

        $ussdInput = (new Ussd)->parseUssdInput($this->ussdData->ussdinput); // split ussd input
        // error_log(json_encode($ussdInput));
        if($this->ussdData->ussdinput == '*161*444#'){
            Cache::forget($this->ussdData->msisdn);
        }

        $this->regKey = 'reg'.$this->ussdData->msisdn;

        //clear cache on first request
        // error_log(json_encode($this->ussdData->ussdinput));
        if (count($ussdInput) == 2) {
            // Cache::forget($this->ussdData->msisdn);
            // Cache::forget($this->regKey);
            //check if is reg or not
            if(!(new Ussd)->verifyUser($ussdData->msisdn)){
                // $newRegData = (object) $this->regData;
                // $newRegData->level = 0;
                // Cache::forever($this->regKey, $newRegData);
                // list($ussdmsg, $respcode, $respmsg, $resptype) = $this->registrationFlow();
                // return [$ussdmsg, $respcode, $respmsg, $resptype];
                return $this->buildXml(['ussdmsg'=> self::$msgs['noVerify'], 'respcode'=> self::$success_respcode, 'respmsg'=>self::$success_respmsg, 'resptype'=> self::$eresptype]);
            }
        }
        else if(count($ussdInput) > 2){
            //longstring parsing
            // *161*444*1*amount*numbertorecharge*network*pin
            switch ($ussdInput[2]) {
                case 1: //topup
                    if(count($ussdInput) == 7){
                       list($ussdmsg, $respcode, $respmsg, $resptype) = (new Ussd)->longStringTopup($ussdInput, $this->ussdData->msisdn);
                       return [$ussdmsg, $respcode, $respmsg, $resptype];
                    }
                    return ["Invalid topup request", Ussd::$fail_respcode, Ussd::$fail_respmsg, Ussd::$eresptype];
                    break;
                
                default:
                    # code...
                    break;
            }

        }


        if(Cache::has($this->regKey)){
            list($ussdmsg, $respcode, $respmsg, $resptype) = $this->registrationFlow();
            return [$ussdmsg, $respcode, $respmsg, $resptype];
        }
        else{
            $sf = Cache::get($this->ussdData->msisdn); // get stored flow
            $this->storedFlow = !$sf  ? (object)$this->storedData : $sf; // get stored flow or initial flow

            $this->storedFlow->phone = $this->ussdData->msisdn; // store phone number        

            #has stored flow
            if($sf && count($ussdInput) == 1){

                $this->storedFlow->prevans = $this->storedFlow->option;
                $this->storedFlow->option = $this->ussdData->ussdinput;

                list($ussdmsg, $respcode, $respmsg, $resptype)  = $this->windFlow($this->storedFlow->currentStep, $this->ussdData->ussdinput);
                $this->storedFlow->currentStep += 1;


                if ($resptype == 'Continue') {
                    Cache::put($ussdData->msisdn, $this->storedFlow, $this->cacheTimeout);
                }else{
                    Cache::forget($ussdData->msisdn);
                }
                return [$ussdmsg, $respcode, $respmsg, $resptype];
            }
            else{
                Cache::forget($ussdData->msisdn);
                Cache::put($ussdData->msisdn, $this->storedFlow, $this->cacheTimeout);

                return $this->selectActivity();
            }
        }
    }

    public function registrationFlow()
    {
        $this->regData = Cache::get($this->regKey);
        $level = $this->regData->level;
        switch ($level) {
            case 0:
                $this->regData->level = 1;
                Cache::forever($this->regKey, $this->regData);
                return [Ussd::$msgs['noVerify'], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
                break;
            case 1:
                if($this->ussdData->ussdinput == 0){
                    Cache::forget($this->regKey);
                    return ['Request cancelled', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
                }else if($this->ussdData->ussdinput == 1){
                    $this->regData->level = 2;
                    Cache::forever($this->regKey, $this->regData);
                    return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];
                }else{

                }
                break;
            case 2:
                $this->regData->level = 3;
                $this->regData->firstname = $this->ussdData->ussdinput; //firstname
                Cache::forever($this->regKey, $this->regData);
                return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;

            case 3:
                $this->regData->level = 4;
                $this->regData->lastname = $this->ussdData->ussdinput; //lastname
                Cache::forever($this->regKey, $this->regData);
                return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;

            case 4:
                $this->regData->level = 5;
                $this->regData->username = $this->ussdData->ussdinput; //username
                Cache::forever($this->regKey, $this->regData);
                return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;

            case 5:
                $this->regData->level = 6;
                $this->regData->password = $this->ussdData->ussdinput; //password
                Cache::forever($this->regKey, $this->regData);
                return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;

            case 6:
                $this->regData->level = 7;
                $this->regData->email = $this->ussdData->ussdinput; //email
                Cache::forever($this->regKey, $this->regData);
                return ['Please provide your '.$this->regForm[$level], Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;

            case 7:
                $this->regData->level = 8;
                $this->regData->phone = $this->ussdData->ussdinput;  //phone
                Cache::forever($this->regKey, $this->regData);
                return $this->selectNetwork();

                break;

            case 8:
                $this->regData->level = 9;
                $this->regData->carrier = $this->ussdData->ussdinput; //network
                Cache::forever($this->regKey, $this->regData);
                return ['Enter referrer username', Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$cresptype];

                break;


            case 9:
                $this->regData->level = 10;
                $this->regData->brought_by = $this->ussdData->ussdinput; //network
                // Cache::forever($this->regKey, $this->regData);
                $resp = (new Ussd)->registerUser($this->regData);
                // $this->regData->level = 8;
                // $this->regData->phone = $this->ussdData->ussdinput;
                // Cache::forever($this->regKey, $this->regData);
                Cache::forget($this->regKey);
                return [$resp, Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];

                break;
            case 10:
                

                break;
            default:
                # code...
                break;
        }
    }
}













// 

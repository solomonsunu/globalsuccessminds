<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use Carbon\Carbon;


class SingleReferralCount{
	public function __construct(User $user,Account $account,Transaction $transaction){
        $this->user = $user;
        $this->account = $account;
        $this->transaction = $transaction;

    }
	
	
	protected static function premiumReferralCount($set){
		$total = 0;
		foreach ($set as $key) {
				$total = $total + 1;
			
		}
		return $total ;
	}

	protected static function next_premium_user_level($key)
	{
		return User::where('premium_referrer', $key)->get();
	}

	///count number of premium referrals per user
	public static function getPremiumReferral($user)
	{
				
				$ranks_list = ['bronze'=>0,'silver'=>0,'silver'=>0,'pearl'=>0,'gold'=>0,'sapphire'=>0,'diamond'=>0];
				// $bronze = 0;
				// $silver = 0;
				// $pearl = 0;
				// $gold = 0;
				// $sapphire = 0;
				// $diamond = 0; 
				$main_set = [0,0,0,0,0];
			    $level1 = self::next_premium_user_level($user->user_id); #l1 users
			    
			    // $total += $user->total_bonus;
				if (count($level1) > 0){
					
					for ($i=0; $i < count($level1); $i++) { 
						$total = 0;
			    		$total ++; #total amt for l1 users
			    	
							$level2 = self::next_premium_user_level($level1[$i]->user_id);
							$total +=  self::premiumReferralCount($level2);

							foreach ($level2 as $l2) {
								$level3 = self::next_premium_user_level($l2->user_id);
								$total +=  self::premiumReferralCount($level3);

								foreach ($level3 as $l3) {
									$level4 = self::next_premium_user_level($l3->user_id);
									$total +=  self::premiumReferralCount($level4);
									
									foreach ($level4 as $l4) {
										$level5 = self::next_premium_user_level($l4->user_id);
										$total +=  self::premiumReferralCount($level5);
										
										foreach ($level5 as $l5) {
											$level6 = self::next_premium_user_level($l5->user_id);
											if(count($level6) > 0){
												$total +=  self::premiumReferralCount($level6);
											}
										}

									}
								}
							}

						$main_set[$i] = $total;
						
			    	}
					
				}
				
			
		return $main_set;
	}


	

}
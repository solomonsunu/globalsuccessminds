<?php
namespace App\Helpers;

use SoapBox\Formatter\Formatter;
use App\User;
use GuzzleHttp\Client;
use Cache;
use App\Account;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\TopupEarning;
use Uuid;
use Mail;
use Artisaninweb\SoapWrapper\SoapWrapper;

use SoapClient;
use App\MobileWalletTransaction;


class Ussd
{
    use DispatchesJobs;

    // const TOP_UP_API_DOMAIN = 'https://tpp.one4all.com.gh/TPP/TopUpApi';
    const TOP_UP_API_DOMAIN = 'tppgh.myone4all.com/api/TopUpApi';

    // public static $msgs  = ["noVerify" => "Welcome to GSM airtime for cash top up service. Enter
                                        // \n 1. To Register \n 0. To Cancel"];
    public static $msgs  = ["noVerify" => "Welcome to GSM airtime for cash top up service. please visit https://globalsuccessminds.com to register"];

    public static $fail_respcode = 1;
    public static $success_respcode = 0;
    public static $fail_respmsg = 'failed';
    public static $success_respmsg = 'success';
    public static $cresptype = 'Continue';
    public static $eresptype = 'End';


    /**
     * Process xml
     *
     * @param String $xmlInput
     * @return array
     */
    public function convertXml($xmlInput = null)
    {
        // if(!$xmlInput){
        //     $xmlInput = file_get_contents(public_path().'/test.xml');
        // }
        $formatter = Formatter::make($xmlInput, Formatter::XML);
        return $formatter->toArray();
    }

    /**
     * Split ussd input
     *
     * @param String $ussd_input
     * @return array
     */
    public function parseUssdInput($ussd_input)
    {
        $input = preg_split( "~[*]~", $ussd_input,null,PREG_SPLIT_NO_EMPTY);
        $index = count($input) - 1;
        $input[$index] = rtrim($input[$index], "#");

        return $input;
    }

    /**
     * Validate user credentials
     *
     * @param String    $phone
     * @param String    $pin
     * @param \App\user $user
     * @return boolean
     */
    public function validateUser($phone, $pin)
    {
        $user = (new User)->where('phone', $phone)->where('pin', $pin)->first();
        if($user) return true;
        return false;
    }

    public function verifyUser($phone)
    {
        $user = User::where('phone', formatPhoneNumber($phone))->first();
        if($user) return true;
        return false;
    }

    /**
     * build response xml data
     *
     * @param array $data
     * @return xml
     */
    public function buildXml($data)
    {
        // $output = [
        //             'pryussd_resp' => [
        //                         'ussdmsg' => $data['ussdmsg'],
        //                         'respcode'=> $data['respcode'],
        //                         'respmsg' => $data['respmsg'],
        //                         'resptype'=> $data['resptype']
        //                 ]
        //         ];
                $output =  "<pryussd_resp>";
                $output .= "<ussdmsg>".$data['ussdmsg']."</ussdmsg>";
                $output .= "<respcode>".$data['respcode']."</respcode>";
                $output .= "<respmsg>".$data['respmsg']."</respmsg>";
                $output .= "<resptype>".$data['resptype']."</resptype>";
                $output .= "</pryussd_resp>";

                return $output;

        // $formatter = Formatter::make($output, Formatter::ARR);
        // return $formatter->toXml();
    }

    /**
     * Perform topup request
     * @param string $phone
     * @param string
     * @param int $network
     *
     */
    public function topUpRequest($phone, $amount, $network)
    {
        $client = new \GuzzleHttp\Client();
        $trxn = rand(0, 99999999);

        $uri = "?username=".config('ussd.username')."&pin=".config('ussd.pin')."&service=001&retailer=".config('ussd.username')."&recipient=".$phone."&amount=".$amount."&network=".$network."&trxn=".$trxn; 

        // error_log($uri);

        $und = "?username=".config('ussd.username')."&pin=".config('ussd.pin')."&service=500";

        $res = $client->request('GET', self::TOP_UP_API_DOMAIN.$uri, ['headers' => ['Accept' => 'application/json', 'ApiKey' => 'ac8c4690f6dd11e8a372833fdf1e1ee8', 'ApiSecret' => 'KiyZ8Uh6Lk'] ]);

        $respdata = json_decode($res->getBody(), true);
        error_log(json_encode($respdata));

        if($respdata['status'] == 'OK'){
            return true;
        }
        return false;
    }

    /**
     * Process ussd data
     *
     * @param array $ussdData
     * @return xml response
     */
    public function process($ussdData)
    {
        $ussdData = (object) $ussdData; # json_decode(json_encode($ussdData), false);


        if(!$this->verifyUser($ussdData->msisdn)){
            return $this->buildXml(['ussdmsg'=> self::$msgs['noVerify'], 'respcode'=> self::$success_respcode, 'respmsg'=>self::$success_respmsg, 'resptype'=> self::$eresptype]);
        }

        // return (new UssdFlow)->activity($ussdData);

        list($ussdmsg, $respcode, $respmsg, $resptype) =  (new UssdFlow)->activity($ussdData);


        return $this->buildXml(['ussdmsg'=> $ussdmsg, 'respcode'=> $respcode, 'respmsg'=>$respmsg, 'resptype'=> $resptype]);
    }

    public static function getUssdCache($key)
    {
        return Cache::get($key);
    }

    /**
     * top up account
     * 
     * @param string $sender
     * @param String $phone
     * @param double $amount
     * @param int $network
     * @return string
     */
    public function topUp($sender, $phone, $amount, $network)
    {
        $user = (new User)->where('phone', $phone)->first();
        $recipient = ($user) ? $user->fullname : $phone;

        $user = (new User)->where('phone', formatPhoneNumber($sender))->first();
        $balance = (new Account)->getCurrentBalance($user->user_id);

        if ($amount <= $balance) {
            //TopUpApi request on success
            $nphone = "233".substr($phone, 1);
            // error_log('phone :'. $nphone. ' amount '.$amount.' network: '.$network);
            $topup = $this->topUpRequest($nphone, $amount, $network);
            // error_log(json_encode($topup));
           
            if($topup){
                $debit = (new Account)->debitAccount($user->user_id, $amount, 0, $recipient, $user->user_id);
                if($debit){
                    $cuser = (new User)->where('brought_by','!=',0)
                            ->where('admin', 0)
                            ->where('paymentstatus', 0)
                            ->where('phone', $phone)
                            ->first();
                    if($cuser){ 
                        $job = (new ActivateAccount($cuser))->activate();
                    }
                    (new TopupEarning)->saveCommision($network, $amount);


                    return "You have successfully recharged  ".$amount."GHS"; // topup successful
                }
            }else{
                return "Unable to process transaction. Try again!!!";
            }
        }else{
            return "You do not have sufficient funds.";
        }
    }

    public function registerUser($input)
    {
        $referredUser = (new User)->whereUsername($input->brought_by)->first();
        if(! $referredUser){
            return 'referrer not found... try again';
        }
        $user = (new User)->where('username', $input->username)->orWhere('phone', $input->phone)->first();
        if($user){
            if($user->username == $input->username){
                return 'username already exist';
            }
            if($user->phone == $input->phone){
                return 'phone number already exist';
            }
        }
        if(strlen($input->password) < 6){
            return 'password length must be greater than 5';
        }
        $net = [1 => 'airtel', 2=> 'glo', 3=> 'mtn', 4 => 'tigo', 5 => 'vodafone'];
        if(!isset($net[$input->carrier])){
            return 'Invalid network selected';
        }

        $user = new User;
        $user_id = Uuid::uuid4();
        $user->user_id = $user_id;
        $user->firstname = $input->firstname;
        $user->lastname = $input->lastname;
        $user->username = $input->username;
        $user->email = $input->email;
        $user->phone = $input->phone;
        // $user->dob = $input->dob;
        // $user->gender = $input->gender;
        $user->brought_by =  $referredUser->user_id;
        $user->password = bcrypt($input->password);
        $user->carrier = $net[$input->carrier];
        $user->pin = '0000';
        $user->verified = true;
        $user->active = 1;

        $country = '';

       

        if ($user->save()) {
            $user = $user->find($user_id);
            $account = new Account;
            $account_id = Uuid::uuid4();
            $account->account_id = $account_id;
            $account->user_id = $user->user_id;
            $account->current_balance = 0.0;
            $account->save();

            $data = array(
                'user' => $user
            );

            Mail::send('emails.registration', $data, function ($message) use($user){
                $message->from('admin@globalsuccessminds.com', 'Account Registration');

                $message->to($user->email)->subject('Account Registration');

            });
            return 'Registration Succesful';
        }
        return 'Registration Failed. Try again';
    }


    public function longStringTopup($input, $rawphone)      
    { 
        //   0   1  2   3      4      5      6
        // *161*444*1*amount*number*network*pin
        $phone = formatPhoneNumber($rawphone);
        // check phone
        if((new User)->verifyPhone($phone)){
            // check pin
            if((new User)->validatePin($rawphone, $input[6])){
                //set network
                if($input[5] > 1){
                    $input[5]+=1;
                }
                // set decimal
                // $input[3] = (float) str_replace('#', '.',  $input[3]);
                // $input[3] = (String)$input[3];
                $digits = str_split($input[3], 1);
                if($digits[0] == 0){
                    $input[3] = (int) $input[3];
                    $input[3] = $input[3] / 100;
                }
                $input[3] = (float) $input[3];

                // error_log(json_encode($input[3]));

                //            topUp($sender,   $phone,    $amount,   $network);
                $msg = $this->topUp($rawphone, $input[4], $input[3], $input[5]);

                return [$msg, Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
            }
            return ["Wrong PIN... Try again!!!", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        }
        return ["not registered", Ussd::$success_respcode, Ussd::$success_respmsg, Ussd::$eresptype];
        
        
        //success/fail
    }

    public function mtn_ussd_load_wallet($amt,$phone, $mtn_mobile)
    {
        // $amt = $amount;
        // $phone = $gsm_phone;
        // $mtn_mobile = $mtn_number;
        // $mo = '+2330545247030';

        if ($amt && $phone && $mtn_mobile) {
            $wsdl = 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService?wsdl';
            $trace = true;
            $exceptions = true;
            
            $userClient = (new User)->where('phone', $phone)->first();
            // error_log(json_encode($userClient));

            $name = 'Global Success Minds';
            $info = 'Load wallet service';
            $mobile = '+233'.substr($mtn_mobile, 1);
            $mesg = 'Kindly find attached the invoice number {$inv}';
            $billprompt = '1';
            $thirdpartyID = '233549398254';
            $username = 'GSMLTD12';
            $password = '2i28U71fK'; 
                


            $xmlr = new \SimpleXMLElement("<postInvoice></postInvoice>");
            $xmlr->addChild('name', $name);
            $xmlr->addChild('info', $info);
            $xmlr->addChild('amt', $amt);
            $xmlr->addChild('mobile', $mobile);
            $xmlr->addChild('mesg', $mesg);
            $xmlr->addChild('billprompt', $billprompt);
            $xmlr->addChild('thirdpartyID', $thirdpartyID);
            $xmlr->addChild('username', $username);
            $xmlr->addChild('password', $password);

            //dd($xmlr);
            $context = stream_context_create([
                'ssl' => [
                    // set some SSL/TLS specific options
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ]
            ]);
            try
            {
               $client = new \SoapClient($wsdl,array('location' => 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService.InvoicingServiceHttpSoap11Endpoint/','trace' => $trace, 'exceptions' => $exceptions, 'soap_version' => SOAP_1_2,'stream_context' => $context));
               $response = $client->postInvoice($xmlr);
              
            }
            catch (Exception $e)
            {
               echo "Error!";
               echo $e -> getMessage ();
               echo 'Last response: '. $client->__getLastResponse();
            }
            $v = ['result'=>$response->return];
            foreach ($v as $key => $value) {
                $b = (array)$value;
                break; 
            }
            $responseCode = $b['responseCode'];
            $responseMessage = $b['responseMessage'];
            $invoiceNo = $b['invoiceNo'];

            $mobile_wallet = new MobileWalletTransaction;
            $mobile_wallet->mobile_wallet_id = Uuid::uuid4();
            $mobile_wallet->user_id = $userClient->user_id;
            $mobile_wallet->invoiceNo = $invoiceNo;
            $mobile_wallet->amount = $amt;
            $mobile_wallet->responseCode = $responseCode;
            $mobile_wallet->responseMessage = $responseMessage;

            if ($mobile_wallet->save()) {
                return 'Request to load ' . $amt . 'GHS is being processed';
            }else{
                return 'Request failed';
            }
            //save transaction here into wallet loading transactions table 
        } 
    }

}

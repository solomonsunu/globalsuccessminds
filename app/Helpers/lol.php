<?php 
	public function getTotals(){
		$search = User::where('brought_by','!=',0)->where('admin',0)->where('paymentstatus',1)->orderBy('user_id')->get();
		$search_array = array();
		$array = array('uid' => 'total');
		$valueDefact;
				

		foreach ($search as $key => $value) {
			$search_array[] = $value->user_id; //returns an array of the uid of all users belonging to your cycle

		}
		// dd($search_array);
		foreach($search_array as $key => $value1){
			$total = 0.0;
		    $valueDefact = $value1;
		    $level1 = User::where('brought_by','=',$value1)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
		    if (count($level1) > 0){
		    	$level2=new \Illuminate\Database\Eloquent\Collection();
				foreach ($level1 as $value) {
					$dummy = User::where('brought_by','=',$value->user_id)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
					if ($dummy->isEmpty() == false) {
						$level2->add($dummy);
					}
				}
				if (count($level2) > 0) {
					$level3=new \Illuminate\Database\Eloquent\Collection();
					foreach ($level2 as $key => $value) {
						foreach ($value as $key => $value1) {
							$dummy = User::where('brought_by','=',$value1->user_id)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
							if ($dummy->isEmpty() == false) {
								$level3->add($dummy);
							}
						}
					}
					if (count($level3) > 0) {
						$level4=new \Illuminate\Database\Eloquent\Collection();
						foreach ($level3 as $key => $value) {
							foreach ($value as $key => $value1) {
								$dummy = User::where('brought_by','=',$value1->user_id)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
								if ($dummy->isEmpty() == false) {
									$level4->add($dummy);
								}
							}
						}
						if (count($level4) > 0) {
							$level5=new \Illuminate\Database\Eloquent\Collection();
							foreach ($level4 as $key => $value) {
								foreach ($value as $key => $value1) {
									$dummy = User::where('brought_by','=',$value1->user_id)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
									if ($dummy->isEmpty() == false) {
											$level5->add($dummy);
										}
								}
							}
							if (count($level5) > 0) {
								$level6=new \Illuminate\Database\Eloquent\Collection();
								foreach ($level5 as $key => $value) {
									foreach ($value as $key => $value1) {
										$dummy = User::where('brought_by','=',$value1->user_id)->where('brought_by','!=',0)->where('admin',0)->orderBy('user_id')->get();
										if ($dummy->isEmpty() == false) {
												$level6->add($dummy);
											}
									}
								}
								
							}else{
								$total = 0.0;
								$total +=  $this->paycount($level2) * 0.005;
								$total +=  $this->paycount($level3) * 0.007;
								$total +=  $this->paycount($level4) * 0.009;
								$total +=  $this->paycount1($level1) * 0.003;
								$array = array_add($array,$valueDefact,$total);
							}
						}else{
							$total = 0.0;
							$total +=  $this->paycount($level2) * 0.005;
							$total +=  $this->paycount($level3) * 0.007;
							$total +=  $this->paycount1($level1) * 0.003;		
							$array = array_add($array,$valueDefact,$total);
						}
					}else{
						$total = 0.0;
						$total +=  $this->paycount($level2) * 0.005;			
						$total +=  $this->paycount1($level1) * 0.003;
						$array = array_add($array,$valueDefact,$total);
					}
				}else{
					$total = 0.0;
					$total +=  $this->paycount1($level1) * 0.003;
					$array = array_add($array,$valueDefact,$total);
				}
		    }else{
		    	
		    	//count 1 == 0
		    }
		}

		$values = $array;
		unset($values['uid']);
		return $values;

	}
 ?>
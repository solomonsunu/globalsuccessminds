<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use App\Rank;
use Carbon\Carbon;
use App\Notification;
use App\UserUpgrade;
use Uuid;
use App\Helpers\SingleReferralCount as rCount;


class Insurance {
	public function __construct(User $user,Account $account,Transaction $transaction, Rank $rank, Notification $notification, UserUpgrade $upgrade, rCount $rcount){
        $this->user = $user;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->rank = $rank;
        $this->notification = $notification;
        $this->upgrade = $upgrade;
        $this->rcount = $rcount;

    }
	
	protected static function paycount($set){
		$total = 0.0;
		foreach ($set as $value) {
			$total = $total + $value->amt;
		}

		return $total;
	}

	// protected static function premiumPaycount($set, $percent){
	// 	$total = 0;
	// 	foreach ($set as $key) {
	// 		if (Carbon::now()->month == Carbon::parse($key->premium_update_date)->month && Carbon::now()->year == Carbon::parse($key->premium_update_date)->year) {
	// 			$total = $total + 1;
	// 		}
	// 	}
	// 	return $total * $percent;
	// }
	protected static function premiumPaycount($set, $percent){
		$total = 0;
		foreach ($set as $key) {
			if (Carbon::now()->subMonth()->month == Carbon::parse($key->premium_update_date)->month && Carbon::now()->year == Carbon::parse($key->premium_update_date)->year) {
				$total = $total + 1;
			}
		}
		return $total * $percent;
	}

	protected static function premiumReferralCount($set){
		$total = 0;
		foreach ($set as $key) {
				$total = $total + 1;
			
		}
		return $total ;
	}
	protected static function next_user_level($key)
	{
		return User::where('brought_by', $key)->get();
	}

	protected static function next_premium_user_level($key)
	{
		return User::where('premium_referrer', $key)->get();
	}


	public static function getNewTotals()
	{
		$amountDue = [];
		User::where(['paymentstatus'=> 1,'admin'=>0])->chunk(50,function ($users) use (&$amountDue)
		{
			foreach ($users as $user) {
				$total = 0;
			    $level1 = self::next_user_level($user->user_id); #l1 users

				if (count($level1) > 0){
					$total +=  self::paycount($level1) * 0.003; #total amt for l1 users

					foreach ($level1 as $l1) {
						$level2 = self::next_user_level($l1->user_id);
						$total +=  self::paycount($level2) * 0.005;

						foreach ($level2 as $l2) {
							$level3 = self::next_user_level($l2->user_id);
							$total +=  self::paycount($level3) * 0.007;

							foreach ($level3 as $l3) {
								$level4 = self::next_user_level($l3->user_id);
								$total +=  self::paycount($level4) * 0.009;
								
								foreach ($level4 as $l4) {
									$level5 = self::next_user_level($l4->user_id);
									$total +=  self::paycount($level5) * 0.01;
									
									foreach ($level5 as $l5) {
										$level6 = self::next_user_level($l5->user_id);
										if(count($level6) > 0){
											$total +=  self::paycount($level6) * 0.011;
										}
									}

								}
							}
						}
					}
				}

				array_push($amountDue,['total'=>$total,'fullname'=>$user->fullname,'username'=>$user->username,'country'=>$user->country,'phone'=>$user->phone,'carrier'=>$user->carrier,'email'=>$user->email,'user_id'=>$user->user_id]);
			}
			 // echo json_encode($amountDue);
			 // $res = json_encode($amountDue);
			// return $amountDue;
		});

		return $amountDue;
	}
	
	public static function getTf($res)
	{
		$amountDue = [];
		$referralCount = 0;
		$totalAirtime = 0.0;
			foreach ($res as $user) {

				$total = 0;
			    $level1 = self::next_user_level($user->user_id); #l1 users

				if (count($level1) > 0){
					$total +=  self::paycount($level1) * 0.001; #total amt for l1 users

					foreach ($level1 as $l1) {
						$level2 = self::next_user_level($l1->user_id);
						$total +=  self::paycount($level2) * 0.0015;

						foreach ($level2 as $l2) {
							$level3 = self::next_user_level($l2->user_id);
							$total +=  self::paycount($level3) * 0.002;

							foreach ($level3 as $l3) {
								$level4 = self::next_user_level($l3->user_id);
								$total +=  self::paycount($level4) * 0.0025;
								
								foreach ($level4 as $l4) {
									$level5 = self::next_user_level($l4->user_id);
									$total +=  self::paycount($level5) * 0.003;
									
									foreach ($level5 as $l5) {
										$level6 = self::next_user_level($l5->user_id);
										if(count($level6) > 0){
											$total +=  self::paycount($level6) * 0.005;
										}
									}

								}
							}
						}
					}
				}

				
			

				array_push($amountDue,['total'=>$total,'fullname'=>$user->fullname,'username'=>$user->username,'country'=>$user->country,'phone'=>$user->phone,'carrier'=>$user->carrier,'email'=>$user->email,'user_id'=>$user->user_id]);
			}
			 // echo json_encode($amountDue);
			 // $res = json_encode($amountDue);
			// return $amountDue;

		return $amountDue;
	}

	

	public function getPremiumAmount($users)
	{
		$amountDue = [];
		foreach ($users as $user) {
				$total = 0;
			    $level1 = self::next_premium_user_level($user->user_id); #l1 users
			    // $total += $user->total_bonus;
				if (count($level1) > 0){
					$total +=  self::premiumPaycount($level1, 3.0); #total amt for l1 users

					foreach ($level1 as $l1) {
						$level2 = self::next_premium_user_level($l1->user_id);
						$total +=  self::premiumPaycount($level2, 3.0);

						foreach ($level2 as $l2) {
							$level3 = self::next_premium_user_level($l2->user_id);
							$total +=  self::premiumPaycount($level3, 3.0);

							foreach ($level3 as $l3) {
								$level4 = self::next_premium_user_level($l3->user_id);
								$total +=  self::premiumPaycount($level4, 3.0);
								
								foreach ($level4 as $l4) {
									$level5 = self::next_premium_user_level($l4->user_id);
									$total +=  self::premiumPaycount($level5, 3.0);
									
									foreach ($level5 as $l5) {
										$level6 = self::next_premium_user_level($l5->user_id);
										if(count($level6) > 0){
											$total +=  self::premiumPaycount($level6, 3.0);
										}
									}

								}
							}
						}
					}
				}
				$finalTotal = $total + $user->total_bonus;
				array_push($amountDue,['total'=>$finalTotal,'fullname'=>$user->fullname,'username'=>$user->username,'country'=>$user->country,'phone'=>$user->phone,'carrier'=>$user->carrier,'email'=>$user->email,'user_id'=>$user->user_id]);
			}
		return $amountDue;
	}

	

	

}
<?php
namespace App\Helpers;
use App\User;
use App\Account;
use Cache;
use Log;
use Uuid;
use Auth;
use Carbon\Carbon;
use App\Helpers\LoadWallet as LoadW;
use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapClient\SoapClientOptionsBuilder;
use BeSimple\SoapCommon\SoapOptionsBuilder;

use Artisaninweb\SoapWrapper\SoapWrapper;

use SoapClient;
use App\MobileWalletTransaction;
use App\GtTransaction;

use App\Notification;
use Ixudra\Curl\Facades\Curl;
class GtLoad
{
	public function __construct(Ussd $ussd, Request $request, User $user, Account $account,LoadW $loadw,SoapWrapper $soapWrapper,MobileWalletTransaction $mobile_wallet,GtTransaction $gt)
    {
        $this->ussd = $ussd;
        $this->request = $request;
        $this->user = $user;
        $this->account = $account;
        $this->loadw = $loadw;
        $this->soapWrapper = $soapWrapper;
        $this->mobile_wallet = $mobile_wallet;
        $this->gt = $gt;
        
    }


    public static function sendPrompt($amt,$mm_phone,$gsm_phone_number)
    {
    
        $amount = $amt;
        $phone = $mm_phone;
        $gsm_phone = $gsm_phone_number;


        $currentUser = User::where('phone',$gsm_phone)->first();

        $user_id = $currentUser->user_id;
        
        
        $userClient = $currentUser;
      
        $ReferenceID = rand();
        $transId = rand();

          $this->gt->gt_id = Uuid::uuid4();
          $this->gt->user_id = $userClient->user_id;
          $this->gt->clientRef = $ReferenceID;
          $this->gt->transRef = $transId;
          $this->gt->amount = $amount;
          $this->gt->mobileNumber = $phone;
          $this->gt->senderPhone = $gsm_phone;
          $this->gt->senderName =$currentUser->lastname." ".$currentUser->firstname;
        
          if ($this->gt->save()) {

    
            $service = [
              "amount"=>$amount,
              "network"=>"mtn",
              "mobileNumber"=>$phone,
              "transId"=>"208624143",
              "ReferecnceID"=>"523529364",
              "remarks"=>"Mtn Mobile Money Test"
            ];
           $mechantreference = rand();
          
            $data= [
                  "serviename"=>"initiatedebitwallet",
                  "merchantToken"=>"065CC989-FB9F-4CD5-A3E2-804CADEA7484",
                  "merchantKey"=>"9C83FB9F-8006-4350-867C-1F85A3175E5D",
                  "merchantreference"=>"525365",
                  "serviceDetails"=>json_encode($service)

            ];
          
          //var_dump($data);

          
          \GuzzleHttp\RequestOptions::JSON;
          $url = 'https://196.216.228.129/apihub/api/myghpayextension/paymentRequest';
          $client = new \GuzzleHttp\Client(['verify' => false]);
          // $client->setDefaultOption('verify', false);
          $res = $client->request('POST', $url, ['json'=>$data]); 

          $respdata = json_decode($res->getBody(), true);
          //var_dump($respdata);
          $t = json_decode($respdata['Details'],true);
          //$t = (array) $t;
          //return $t["Status"];
          //return $respdata;

              $t = $this->gt->where('transRef',$transId)->first();

              $status = $respdata["Status"];
              if ($status == 1) {
                 
                  $result = (new GtTransaction)->creditMobileAccount($t->user_id,$t->amount);
                    if ($result) {
                      $amountCredit =$t->amount;
                      $message =new Notification;
                      $message->notification_id = Uuid::uuid4();
                      $message->user_id = $t->user_id;
                      $message->title = 'Self Load Wallet Topup';
                      $message->detail = "Your Account has been credited with ".$amountCredit." Ghana Cedis as Wallet topup";
                      $message->status="unread";
                      $message->save();


                      $t->status = "complete";
                      $t->Message = $t["Message"];
                      $t->save();

                          return ['status'=>true,'message'=>"Account credited successfully!"];
                    }
                    return ['status'=>false,'message'=>"An Error occured"];
              }else{
                      $t->status = "failed";
                      $t->Message = $t["Message"];
                      $t->save();
                      $amountCredit =$t->amount;
                      $message =new Notification;
                      $message->notification_id = Uuid::uuid4();
                      $message->user_id = $t->user_id;
                      $message->title = 'Self Load Wallet Topup Failure';
                      $message->detail = "Your Request to Credit your account with ".$amountCredit." Ghana Cedis as Wallet topup has failed";
                      $message->status="unread";
                      $message->save();
                       return ['status'=>false,'message'=>"Account credited Unsuccessful!"];
              }
       
    }

    

}

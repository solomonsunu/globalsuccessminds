<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use Carbon\Carbon;


class UpgradeCheck{
	public function __construct(User $user,Account $account,Transaction $transaction){
        $this->user = $user;
        $this->account = $account;
        $this->transaction = $transaction;

    }
	
	

	protected static function next_premium_user_level($key)
	{
		return User::where(['admin'=>0,'premium_referrer'=>$key])->whereNotNull('premium_referrer')->whereNotNull('total_pre')->get();
	}

	protected static function next_bagam_user_level($key)
	{
		return User::where(['admin'=>0,'bagam_brought'=>$key])->whereNotNull('premium_referrer')->get();
	}





	public function getAvailableUser($userId)
	{
		$candidate = [];
		$users = $this->user->where(['admin'=>0,'premium_referrer'=>$userId])->get();
		array_push($candidate,['group'=>$users]);
		foreach ($users as $user) {
			$level1 = self::next_premium_user_level($user->user_id); #l1 users
				if (count($level1) > 0){
					array_push($candidate,['group'=>$level1]);
					foreach ($level1 as $l1) {
						$level2 = self::next_premium_user_level($l1->user_id);
						array_push($candidate,['group'=>$level2]);
						foreach ($level2 as $l2) {
							$level3 = self::next_premium_user_level($l2->user_id);
							array_push($candidate,['group'=>$level3]);
							foreach ($level3 as $l3) {
								$level4 = self::next_premium_user_level($l3->user_id);
								array_push($candidate,['group'=>$level4]);
								foreach ($level4 as $l4) {
									$level5 = self::next_premium_user_level($l4->user_id);
									array_push($candidate,['group'=>$level5]);
									foreach ($level5 as $l5) {
										$level6 = self::next_premium_user_level($l5->user_id);
										array_push($candidate,['group'=>$level6]);
										
									}

								}
							}
						}
					}
				}
				
				// array_push($candidate,['user_id'=>$user->user_id,'total_pre'=>$user->total_pre]);
			}
		return $candidate;
	}
	
	public function getAvailableBagamUser($userId)
	{
		$candidate = [];
		$users = $this->user->where(['admin'=>0,'bagam_brought'=>$userId])->whereNotNull('premium_referrer')->get();
		array_push($candidate,['group'=>$users]);
		foreach ($users as $user) {
			$level1 = self::next_bagam_user_level($user->user_id); #l1 users
				if (count($level1) > 0){
					array_push($candidate,['group'=>$level1]);
					foreach ($level1 as $l1) {
						$level2 = self::next_bagam_user_level($l1->user_id);
						array_push($candidate,['group'=>$level2]);
						foreach ($level2 as $l2) {
							$level3 = self::next_bagam_user_level($l2->user_id);
							array_push($candidate,['group'=>$level3]);
							foreach ($level3 as $l3) {
								$level4 = self::next_bagam_user_level($l3->user_id);
								array_push($candidate,['group'=>$level4]);
								
							}
						}
					}
				}
				
				// array_push($candidate,['user_id'=>$user->user_id,'total_pre'=>$user->total_pre]);
			}
		return $candidate;
	}
	


	

}
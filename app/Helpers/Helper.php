<?php

	function set_active($path, $active = 'active selected') {
		return call_user_func_array('Request::is', (array)$path) ? $active : '';
	}

	function uploadImage($file)
	{
		if (!empty($file)) {
			$ext = $file->getClientOriginalExtension();
			$dir = public_path().'/uploads/';
			$filename = $this->request->input('username').'.'.$ext;
			$imagePath = '/uploads/';

			if (file_exists($dir.$filename)) {
				$filename = $this->request->input('username').uniqid().'.'.$ext;
				$file->move($dir,$filename);
			}else{
				$file->move($dir,$filename);
			}
			return $imagePath.$filename;
		}
	}

    function formatPhoneNumber($phone)
	{
		return "0".substr($phone, 3);
	}

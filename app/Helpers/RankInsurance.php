<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;
use App\Account;
use App\Rank;
use Carbon\Carbon;
use App\Notification;
use App\UserUpgrade;
use Uuid;
use App\Helpers\SingleReferralCount as rCount;


class RankInsurance {
	public function __construct(User $user,Account $account,Transaction $transaction, Rank $rank, Notification $notification, UserUpgrade $upgrade, rCount $rcount){
        $this->user = $user;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->rank = $rank;
        $this->notification = $notification;
        $this->upgrade = $upgrade;
        $this->rcount = $rcount;

    }
	
	protected static function paycount($set){
		$total = 0.0;
		foreach ($set as $value) {
			$total = $total + $value->amt;
		}

		return $total;
	}

	protected static function premiumPaycount($set, $percent){
		$total = 0;
		foreach ($set as $key) {
			if (Carbon::now()->subMonth()->month == Carbon::parse($key->premium_update_date)->month && Carbon::now()->year == Carbon::parse($key->premium_update_date)->year) {
				$total = $total + 1;
			}
		}
		return $total * $percent;
	}

	protected static function premiumReferralCount($set){
		$total = 0;
		foreach ($set as $key) {
				$total = $total + 1;
			
		}
		return $total ;
	}
	protected static function next_user_level($key)
	{
		return User::where('brought_by', $key)->get();
	}

	protected static function next_premium_user_level($key)
	{
		return User::where('premium_referrer', $key)->get();
	}


	public static function getTotalRank($res)
	{
		$amountDue = [];

		
			foreach ($res as $user) {
				$total = 0;
				$referralCount = 0;
				$totalAirtime = 0.0;
				$totalAirtimeFinal = 0.0;
				
				if ($user->referral_count == 1) {
					 $level1 = self::next_user_level($user->user_id); //level 1
					 $total +=  self::paycount($level1); //total amt for l1 users

						foreach ($level1 as $l1) {
							$level2 = self::next_user_level($l1->user_id);
							$total +=  self::paycount($level2);

							foreach ($level2 as $l2) {
								$level3 = self::next_user_level($l2->user_id);
								$total +=  self::paycount($level3);

								foreach ($level3 as $l3) {
									$level4 = self::next_user_level($l3->user_id);
									$total +=  self::paycount($level4);
									
									foreach ($level4 as $l4) {
										$level5 = self::next_user_level($l4->user_id);
										$total +=  self::paycount($level5);
										
										foreach ($level5 as $l5) {
											$level6 = self::next_user_level($l5->user_id);
											if(count($level6) > 0){
												$total +=  self::paycount($level6);
											}
										}

									}
								}
							}
						}

						$rank1 = "e7e4f81f-7af1-4b3d-b7f2-3ae35c34ecb3";
						$rank2 = "8dbb19fa-0e9d-40b1-9a40-17c3f72bf034";
						$rank3 = "fe243f08-17be-470f-8bfe-70274c9869ca";
						$rank4 = "22bdd0d7-4fd7-4dbe-9fca-76834db3f867";
						$rank5 = "90c35dd7-c96f-4cb5-b9e0-45970b8337fd";
						$rank6 = "2dabb201-2d8b-4c16-8389-33c1d081858c";

						$referralCount =  rCount::getPremiumReferral($user); 
						$leg1 =  $referralCount[0];	
						$leg2 =  $referralCount[1];	
						$leg3 =  $referralCount[2];	
						$leg4 =  $referralCount[3];	
						$leg5 =  $referralCount[4];	
						$totalAirtime = $total + $user->amt;
						$totalAirtimeFinal = $totalAirtime;


				}
				$add_req = User::where(['rank_id'=>$user->rank_id,'premium_referrer'=>$user->user_id])->get()->count();
				array_push($amountDue,['total'=>$totalAirtimeFinal,'leg1'=>$leg1,'leg2'=>$leg2,'leg3'=>$leg3,'leg4'=>$leg4,'leg5'=>$leg5,'fullname'=>$user->fullname,'username'=>$user->username,'user_id'=>$user->user_id,'email'=>$user->email,'phone'=>$user->phone,'rank_id'=>$user->rank_id,'current_rank'=>$user->ranks->rank_name,'add_req'=>$add_req]);
			}
			

		return $amountDue;
	}

	

	public function getPremiumAmount($users)
	{
		$amountDue = [];
		foreach ($users as $user) {
				$total = 0;
			    $level1 = self::next_premium_user_level($user->user_id); #l1 users
			    // $total += $user->total_bonus;
				if (count($level1) > 0){
					$total +=  self::premiumPaycount($level1, 3.0); #total amt for l1 users

					foreach ($level1 as $l1) {
						$level2 = self::next_premium_user_level($l1->user_id);
						$total +=  self::premiumPaycount($level2, 3.0);

						foreach ($level2 as $l2) {
							$level3 = self::next_premium_user_level($l2->user_id);
							$total +=  self::premiumPaycount($level3, 3.0);

							foreach ($level3 as $l3) {
								$level4 = self::next_premium_user_level($l3->user_id);
								$total +=  self::premiumPaycount($level4, 3.0);
								
								foreach ($level4 as $l4) {
									$level5 = self::next_premium_user_level($l4->user_id);
									$total +=  self::premiumPaycount($level5, 3.0);
									
									foreach ($level5 as $l5) {
										$level6 = self::next_premium_user_level($l5->user_id);
										if(count($level6) > 0){
											$total +=  self::premiumPaycount($level6, 3.0);
										}
									}

								}
							}
						}
					}
				}
				$finalTotal = $total + $user->total_bonus;
				array_push($amountDue,['total'=>$finalTotal,'fullname'=>$user->fullname,'username'=>$user->username,'country'=>$user->country,'phone'=>$user->phone,'carrier'=>$user->carrier,'email'=>$user->email,'user_id'=>$user->user_id]);
			}
		return $amountDue;
	}

	
	public static function getTotalRankUser($user)
	{
				$amountDue = [];
				$total = 0;
				$referralCount = 0;
				$totalAirtime = 0.0;
				$totalAirtimeFinal = 0.0;
				
				if ($user->referral_count == 1) {
					 $level1 = self::next_user_level($user->user_id); //level 1
					 $total +=  self::paycount($level1); //total amt for l1 users

						foreach ($level1 as $l1) {
							$level2 = self::next_user_level($l1->user_id);
							$total +=  self::paycount($level2);

							foreach ($level2 as $l2) {
								$level3 = self::next_user_level($l2->user_id);
								$total +=  self::paycount($level3);

								foreach ($level3 as $l3) {
									$level4 = self::next_user_level($l3->user_id);
									$total +=  self::paycount($level4);
									
									foreach ($level4 as $l4) {
										$level5 = self::next_user_level($l4->user_id);
										$total +=  self::paycount($level5);
										
										foreach ($level5 as $l5) {
											$level6 = self::next_user_level($l5->user_id);
											if(count($level6) > 0){
												$total +=  self::paycount($level6);
											}
										}

									}
								}
							}
						}

						

						$referralCount =  rCount::getPremiumReferral($user); 
						$leg1 =  $referralCount[0];	
						$leg2 =  $referralCount[1];	
						$leg3 =  $referralCount[2];	
						$leg4 =  $referralCount[3];	
						$leg5 =  $referralCount[4];	
						$totalAirtime = $total + $user->amt;
						$totalAirtimeFinal = $totalAirtime;


				}
				$add_req = User::where(['rank_id'=>$user->rank_id,'premium_referrer'=>$user->user_id])->get()->count();
				array_push($amountDue,['total'=>$totalAirtimeFinal,'leg1'=>$leg1,'leg2'=>$leg2,'leg3'=>$leg3,'leg4'=>$leg4,'leg5'=>$leg5,'fullname'=>$user->fullname,'username'=>$user->username,'user_id'=>$user->user_id,'email'=>$user->email,'phone'=>$user->phone,'rank_id'=>$user->rank_id,'current_rank'=>$user->ranks->rank_name,'add_req'=>$add_req]);
			
			

		return $amountDue;
	}

	

}
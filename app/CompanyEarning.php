<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyEarning extends Model
{
    protected $table = 'company_earnings';
    protected $primaryKey = 'company_earning_id';
}

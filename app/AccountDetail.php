<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountDetail extends Model
{
    protected $table = 'account_details';
    protected $primaryKey = 'account_detail_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['college_id','college_name'];

   
     public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}

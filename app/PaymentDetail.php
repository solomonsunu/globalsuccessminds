<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $table = 'payment_details';
    protected $primaryKey = 'payment_detail_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['order_id','user_id','status'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $primaryKey = 'transaction_id';

    public function users()
    {
    	return $this->belongsTo('App\User', 'user_id', 'user_id');
    }

    public function accounts()
    {
    	return $this->belongsTo('App\Account', 'account_id', 'account_id');
    }

    public function setAmountAttribute($amount)
    {
        return $this->attributes['amount'] = number_format((float)$amount, 2, '.', '');
    }

    public function getAmountAttribute($amount)
    {
        return number_format((float)$amount, 2, '.', '');
    }


    public function createTransaction($transaction)
    {
        
        // $user = (new User)->where('user_id', $transaction['user_id'])->first();
    	$this->transaction_id = Uuid::uuid4();
        $this->account_id = $transaction['account_id'];
        $this->user_id = $transaction['user_id'];
        $this->amount = $transaction['amount'];
        $this->type = $transaction['type'];
        $this->transfer = $transaction['transfer'];
        $this->description = $transaction['description'];
        $this->recipient =  $transaction['recipient'];
        $this->balance =  $transaction['balance'];

        if($this->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}

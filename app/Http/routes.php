<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Helpers\Ussd;
use App\Helpers\Insurance;
use App\Helpers\LoadWallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

Route::get('get-response-debit', function (Request $request) {
    if (!$request->session()->has('nsano-debit')) {
        return [];
    }
    return $request->session()->has('nsano-debit');

});
Route::get('get-response-credit', function (Request $request) {
    if (!$request->session()->has('nsano-credit')) {
         return [];
    }
    return $request->session()->has('nsano-credit');

});
// Route::get('clear-response', function () {
//    Cache::forget('mobile_money');

// });
Route::get('topup/{number}/{network}', 'AccountController@index');



// Route::get('topup/{number}/{network}', function ()
// {
//     // $safe = 233.$number;
//     // $sa = $network;
//     // $data = ['mno'=>$sa,'msisdn'=>$safe,'amount'=>2];
//     // return (new LoadWallet)->makeCurlRequest($data);

//     // return (new App\TopupEarning)->saveCommision(2, 4);
//     // $user = App\User::where('phone', '0209062306')->first();
//     // return (new App\Helpers\ActivateAccount($user))->activate();
//     // $a = (new Ussd)->topUpRequest('233208603871', '1');
//     // if($a) return 'true';
//     // return (array) Cache::get('reg233545247030');
//     // Cache::get('8a17aa58-e95e-4930-ad1c-b99a9d0e7e48_tree_l1_tree_l1');
    
// });

Route::get('mtn', 'UssdController@mtn_load_wallet');
Route::get('mtn_check/{invoiceId}', 'UssdController@checkInvoice');

Route::get('phpinfo', function ()
{
    phpinfo();
});

Route::get('ussd-stats', function ()
{
        
    // return Cache::get('cp');
    return [Cache::get('233209062306')];
    // return (array) Cache::get('XXXXXXXX');
});
Route::get('my-test-result', function ()
{
    return url();
     // return [Cache::get('mtn-response')];
});

Route::get('sendemail', function () {

    $data = array(
        'name' => "Learning Laravel",
    );

    Mail::queue('emails.welcome', $data, function ($message) {

        $message->from('solomonsunu@gmail.com', 'Learning Laravel');

        $message->to('solomonsunu@gmail.com')->subject('Learning Laravel test email');

    });

    return "Your email has been sent successfully";

});

Route::get('test', 'InsuranceController@generate');


//Website urls

Route::get('/', function () {
    return view('website.home');
});

Route::get('/productsandservices', ['as' => 'productsandservices', function()
{
    return view('website.productsandservices');
}]);
Route::get('/compensationplan', ['as' => 'compensationplan', function()
{
    return view('website.compensationplan');
}]);
Route::get('/contactus', ['as' => 'contactus', function()
{
    return view('website.contactus');
}]);

Route::get('termsandconditions', function (){
    return view('website.termsandconditions');
});

Route::get('privacy_policy', function (){
    return view('website.privacy_policy');
});

Route::get('success', function () {
    return view('dashboard.success');
});

//My authentication routes
Route::group(['middleware'=>'guest'],function(){
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'UserController@login');
});
Route::get('logout', 'UserController@logout');
Route::post('register', 'UserController@register');

Route::post('register/user', 'UserController@registerUser');


    // Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('changepassword', 'DashController@change_password');
Route::controllers([
   'password' => 'Auth\PasswordController',
]);


// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::get('register/confirm/{token}', 'UserController@confirmEmail');

Route::get('register/user', 'DashController@registerURL');
Route::get('register/agent', 'DashController@registerAgent');
Route::post('register/agent', 'UserController@registerAgent');


//User Log entry routes
Route::group(['middleware'=>'auth'], function (){
Route::get('dashboard', 'DashController@dashboard');
// Route::get('dashboard/admin-user-management', 'DashController@adminUserManager');
// Route::get('dashboard/admin-account-management', 'DashController@adminUserAccountManager');
Route::get('dashboard/super-admin-user-management', 'DashController@superAdminUserManager');
Route::get('dashboard/super-admin-user-rank-management', 'DashController@superAdminUserRankManager');
Route::get('dashboard/super-admin-account-management', 'DashController@superAdminUserAccountManager');
Route::get('dashboard/user-management', 'DashController@regularUserAccount');

Route::get('dashboard/admin-message', 'DashController@adminMessage');
Route::get('users/{id}', 'DashController@viewUser');
Route::get('premium-users/{id}', 'DashController@viewPremiumUser');
Route::get('dashboard/admin-role-management', 'DashController@adminRole');
Route::get('dashboard/admin-payment', 'DashController@admin_payment');
Route::get('dashboard/admin-profit', 'DashController@admin_profit');
Route::get('dashboard/ranks', 'DashController@ranks');
Route::get('premium-user/upgrade', 'DashController@premium_upgrade');
Route::get('bagam-user/upgrade', 'DashController@bagam_upgrade');
Route::get('dashboard/admin-summary', 'DashController@summary');

Route::get('dashboard/wallet-management', 'WalletController@index');
Route::get('dashboard/wallet-withdrawal', 'WalletController@withdrawal');
Route::get('dashboard/user-wallet', 'WalletController@managewallet');
Route::get('dashboard/user-wallet-withdrawal', 'WalletController@user_withdrawal');
Route::get('dashboard/update-user', 'DashController@update_user');
Route::get('dashboard/manual-pay', 'InsuranceController@pay_view');
Route::post('dashboard/manual-pay', 'InsuranceController@pay_process');
Route::post('dashboard/manual-pay-premium', 'InsuranceController@premium_process');
Route::post('dashboard/manual-notification', 'RankInsuranceController@premium_notification');
Route::post('dashboard/manual-comission', 'InsuranceController@agent_conmision_process');
Route::post('dashboard/manual-direct-comission', 'InsuranceController@direct_conmision_process');

//end of solid routes

//New Routes

Route::get('dashboard/admin-user-management', 'DashController@adminUserManager');
Route::get('dashboard/statement-request', 'DashController@statement_request');
Route::get('dashboard/load-wallet', 'DashController@load_wallet');
Route::get('dashboard/wallet-invoice', 'DashController@wallet_invoice');
Route::get('dashboard/movement-user', 'DashController@movement_user');

Route::get('dashboard/buy-airtime', 'DashController@buy_airtime');


Route::get('payment/confirmation', 'UssdController@confirm_payment');
Route::post('save-gtpayment','UssdController@gt_load_wallet');

Route::get('gt_callback', 'UssdController@gt_callback');



});

# New routes for RestApi
Route::group(['prefix'=>'api/v1','middleware'=>'cors'], function (){
    Route::resource('users', 'UserController');
    Route::get('getNewUser', 'AdminController@getUser');
    Route::get('getAdmin', 'AdminController@getAdmin');
    Route::get('/getAllUser', 'AdminController@get_active_user');
    Route::get('currentUser', 'UserController@currentUser');
     //Route::get('current', 'UserController@currentUser');
    Route::post('user-management/updateUser', 'UserController@updateUser');
    Route::post('user-management/createUser', 'UserController@createUser');
    //file upload route
    Route::post('files/upload', 'UploadsController@localUpload');
    //NOtification route
    Route::get('getNotification', 'NotificationController@getNotification');
    Route::post('markRead', 'NotificationController@markRead');
    Route::post('sendMessage', 'NotificationController@sendMessage');
    Route::post('sendMail', 'NotificationController@sendMail');
    //Referall route

    //Premium referal display

    Route::get('getPremiumReferral/level1', 'ReferralViewController@viewPLevel1');
    Route::post('getPremiumReferral/level2', 'ReferralViewController@viewPLevel2');
    Route::post('getPremiumReferral/level3', 'ReferralViewController@viewPLevel3');
    Route::post('getPremiumReferral/level4', 'ReferralViewController@viewPLevel4');
    Route::post('getPremiumReferral/level5', 'ReferralViewController@viewPLevel5');
    Route::post('getPremiumReferral/level6', 'ReferralViewController@viewPLevel6');

    Route::get('getReferral/level1', 'ReferralViewController@viewLevel1');
    Route::post('getReferral/level2', 'ReferralViewController@viewLevel2');
    Route::post('getReferral/level3', 'ReferralViewController@viewLevel3');
    Route::post('getReferral/level4', 'ReferralViewController@viewLevel4');
    Route::post('getReferral/level5', 'ReferralViewController@viewLevel5');
    Route::post('getReferral/level6', 'ReferralViewController@viewLevel6');

    Route::get('getBagamReferral/level1', 'ReferralViewController@viewBLevel1');
    Route::post('getBagamReferral/level2', 'ReferralViewController@viewBLevel2');
    Route::post('getBagamReferral/level3', 'ReferralViewController@viewBLevel3');
    Route::post('getBagamReferral/level4', 'ReferralViewController@viewBLevel4');

    //Payment related routes
    Route::get('getPayment', 'AdminController@getPaymentNew');
    Route::get('getUserRankUpgrades', 'AdminController@getTotalRankUpgrade');
    Route::get('getPaymentPremium', 'AdminController@getPaymentPremium');
    //interim
    Route::get('getUserPremiumFix', 'AdminController@getUserPremiumFix');
    Route::get('getUserPremium', 'AdminController@getUserPremium');
    Route::get('getProfit', 'AdminController@getProfit');

    Route::post('generate', 'InsuranceController@generate');
    Route::post('generatePremium', 'InsuranceController@generatePremium');
     Route::post('generate/ranks', 'InsuranceController@generateRank');
    Route::post('resetInsurance', 'InsuranceController@resetInsurance');
    Route::post('resetPremiumInsurance', 'InsuranceController@resetPremiumInsurance');

    Route::get('getPayHistory/{userID}', 'AdminController@getPayHistory');
    Route::post('activate-role', 'AdminController@activateRole');
    Route::get('totalProfit', 'AdminController@total_profit');
    Route::get('totalInsurance', 'AdminController@totalInsurance');
    Route::post('defActivity', 'AdminController@defActivity');
    Route::get('managedUser/{id}', 'AdminController@manageduser');
    Route::get('allUser/{id}', 'AdminController@managed_all_user');

    Route::get('users/{user_id}/pin', 'UserController@getPin');
    Route::post('users/{user_id}/pin', 'UserController@changePin');
    Route::get('wallets/{user_id}/transactions', 'WalletController@transactions');
    Route::get('admin/transactions', 'TransactionsController@adminTransactions');
    Route::get('admin/ranks', 'RankController@viewRank');
    Route::get('user-attained/ranks', 'UserController@getUserRanks');
    Route::get('user-attained/ranks/pay/{id}', 'UserController@ChangeUserRanksStatus');
    Route::post('admin/ranks', 'RankController@createRank');

     Route::post('admin/transactions/date', 'TransactionsController@adminTransactionsWithDate');

    Route::get('premium-user-upgrades', 'UserController@viewMonthlyUpgrades');
    Route::post('premium-user-upgrades', 'UserController@userPremiumUpgrade');
    Route::post('bagam-user-upgrades', 'UserController@userBagamUpgrade');
    Route::get('accounts/transactions/{user_id}', 'TransactionsController@showLocal');
    Route::post('accounts/transactions/admin', 'TransactionsController@showLocalAdmin');
    Route::post('accounts/transactions', 'TransactionsController@show');
    Route::get('admin/transactions/{user_id}', 'TransactionsController@adminShow');
    Route::post('admin/transactions/{user_id}', 'TransactionsController@adminShowWithDate');
    Route::get('accounts/{user_id}/balance', 'AccountController@checkBalance');
    Route::post('accounts/credit', 'AccountController@creditUserAccount');
    Route::post('accounts/transfer', 'AccountController@transferFunds');
    Route::post('accounts/usertransfer', 'AccountController@userTransferFunds');
    Route::post('accounts/withdrawal', 'AccountController@userWithdrawal');
    Route::get('accounts/withdrawal/{user_id}', 'UserWithdrawalController@checkHistory');
    Route::get('accounts/user/withdrawal', 'UserWithdrawalController@pendingWithdrawal');
    Route::post('accounts/user/withdrawal', 'UserWithdrawalController@checkUserHistory');

    Route::get('user/level/{id}', 'UserController@levelUser');//**
    Route::get('single-user/detail/{id}','UserController@levelUserDetail');
    Route::get('premium-user/level/{id}', 'UserController@levelPremiumUser');//**
    Route::get('cehck-direct-rc/{id}', 'AdminController@checkDirectRC');
    Route::post('ussd', 'UssdController@index');
    Route::post('ussd2', 'UssdController@index2');

    Route::get('accounts/withdrawal/paid/{id}', 'UserWithdrawalController@checkWithdrawal');

    Route::post('changepassword', 'UserController@save_password');
    Route::post('resetpassword', 'UserController@reset_password');

    Route::post('change-username', 'UserController@change_username');
    Route::post('change-bonus', 'UserController@change_bonus');
    Route::post('change-phone-number', 'UserController@change_phonenumber');

    Route::get('lol/{id}', 'UserController@test');

    Route::get('/carrier/{carrier}/getNetworkUser', 'AdminController@get_network_active_user');

    Route::post('mobile-transaction/debit', 'UssdController@mobileMoneyDebit');
    Route::post('mobile-transaction/credit', 'UssdController@mobileMoneyCredit');

     
    
    Route::post('generate-transaction-statement', 'TransactionsController@get_tran_statement');
    Route::post('generate-month-transaction-statement', 'TransactionsController@get_month_tran_statement');
    Route::post('generate-rank-attain-statement', 'TransactionsController@get_rank_tran_statement');

    Route::get('get-premium-qualification', 'AdminController@get_premium_qualification');
    Route::post('mtn-response', 'UssdController@get_mtn_response');

    Route::post('mtn-load-wallet', 'UssdController@mtn_load_wallet');
    Route::post('invoice-search', 'UssdController@invoice_history');
    Route::post('invoice-search-gt', 'UssdController@invoice_history_gt');
    Route::get('invoice-search-gt/remove/{id}', 'UssdController@invoice_history_gt_remove');

    Route::post('gt-load-wallet', 'UssdController@gt_load_wallet');
    Route::post('gt-direct-load-wallet', 'UssdController@gt_direct_load_wallet');

    //user transfer routes

    Route::post('regular-transfer', 'UssdController@regular_transfer');
    Route::post('premium-transfer', 'UssdController@premium_transfer');
    Route::post('topup-airtime', 'UssdController@topup_airtime');

    //callback gt
    Route::get('gt_callback', 'UssdController@gt_callback');
    Route::post('update-payment-status', 'UssdController@gt_callback_update');

     Route::get('nsano-credit-callback', 'UssdController@nsano_credit_callback');
     Route::get('nsano-debit-callback', 'UssdController@nsano_debit_callback');

    //agent referral
     Route::get('agent-referral', 'ReferralViewController@agent_referral');

     //total active and inactive
     //Route::get('total-active-inactive', 'UserController@total_active_inactive');

     Route::post('nsano-credit/momo', 'AccountController@credit');
});


//mobile app
Route::group(['prefix' => 'api/v2','namespace' => 'Api','middleware'=>'cors'], function () {
  Route::post('/auth/register', 'AuthController@register');
  Route::post('/auth/login', 'AuthController@login');
  Route::get('/account/balance', 'AccountController@get_balance');
  Route::post('/account/buy-airtime', 'AccountController@buy_credit');
  Route::get('/account/transactions', 'AccountController@transactions');
  Route::get('/account/notifications', 'AccountController@notifications');
  Route::post('/account/transfer', 'AccountController@transfer_money');
  Route::post('/account/change-password', 'AccountController@change_password');
  Route::post('/account/withdrawal', 'AccountController@withdrawal');
  // Route::post('/account/load-wallet', 'AccountController@load_wallet');
  Route::post('/account/change-pin', 'AccountController@change_pin');
  Route::post('/files/upload', 'AccountController@photoUpload');

  //transflow
  Route::post('/account/load-wallet/transflow', 'AccountController@load_wallet_transflow');

  //token validity check
   Route::get('/token/check', 'AccountController@tokenCheck');

});

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreMessageRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Notification;
use Auth;
use Uuid;
use Session;
use Cache;
use Mail;


class NotificationController extends Controller
{
    
    public function __construct(User $user, Request $request, Notification $notification)
    {
        $this->user = $user;
        $this->request = $request;
        $this->notification = $notification;
    }

    public function getNotification(){
        if (Auth::check())
        {
          $readNotification = $this->notification->where('user_id',Auth::user()->user_id)->where('status',"read")->get();
          $unReadNotification = $this->notification->where('user_id',Auth::user()->user_id)->where('status',"unread")->get();
          return ['readNotification'=>$readNotification,'unReadNotification'=>$unReadNotification];   
        }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
        }
      }
    public function markRead(){
        $notification_id = $this->request->input('notification_id');
        //dd($notification_id);
        $notification = $this->notification->where('notification_id',$notification_id)->first();
        $notification->status = "read";
        
        if($notification->save()) {
        
                 return ['status'=>true];
           
        }

   }

   public function sendMessage(StoreMessageRequest $req)
   {
      $check = 0;
        User::where(['active'=>1,'admin'=>0])->chunk(100,function ($users) use (&$check){
            foreach ($users as $user) {
                $notify = new Notification;
                 $notify->notification_id = Uuid::uuid4();
                 $notify->title = $this->request->input('title');
                 $notify->detail = $this->request->input('message');
                 $notify->status = "unread";
                 $notify->user_id = $user->user_id;
                 $notify->save();
                 $check = $check + 1;
              }
          });
       //dd(count($userlist))
       if ($check > 0) {
           return ['status'=>true];
       }
       return ['status'=>false];
       
   }

    public function sendMail(StoreMessageRequest $req)
   {
       $userlist = $this->user->where('admin',0)->get();
       $check = 0;
       foreach ($userlist as $key => $value) {
           $check = $check + 1;
           $data = array(
                        'name' => $value->firstname,
                        'title' => $this->request->input('title'),
                        'content' =>  $this->request->input('message')
                );

            Mail::queue('emails.mass-email', $data, function ($message) use($value){

                $message->from('info@globalsuccessminds.com', 'Global Success Minds');

                $message->to($value->email)->subject('Global Success Minds');

            });
       }

       if ($check == count($userlist)) {

           return ['status'=>true];
       }
       return ['status'=>false];
       
   }
    
}

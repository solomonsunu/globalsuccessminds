<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\User;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\BuyAirtimeRequest;
use App\Http\Requests\TransferRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\WithdrawalRequest;
use App\Http\Requests\LoadWalletRequest;
use App\Http\Requests\ChangePinRequest;
use App\Http\Controllers\Controller;
use App\Helpers\Ussd;

use Uuid;
use Session;
use Cache;

use Mail;

use Carbon\Carbon;
use Tymon\JWTAuth\JWTAuth;
//use JWTAuth;

use JWTAuthException;
use App\Account;
use App\Transaction;
use App\Notification;

use DB;

//none
use Auth;
use App\UserWithdrawal;
use App\GtTransaction;


use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapClient\SoapClientOptionsBuilder;
use BeSimple\SoapCommon\SoapOptionsBuilder;

use Artisaninweb\SoapWrapper\SoapWrapper;

use SoapClient;

use App\MobileWalletTransaction;

use Ixudra\Curl\Facades\Curl;

use paragraph1\phpFCM\Client as FCM;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification as FCMNotification;

class AccountController extends Controller
{
    private $user;
	private $jwtauth;
	public function __construct(User $user, JWTAuth $jwtauth,Request $request,Account $account,Ussd $ussd,Transaction $transaction,Notification $notification,UserWithdrawal $withdrawal,GtTransaction $gt,MobileWalletTransaction $mobile_wallet)
	{
		$this->ussd = $ussd;
	  $this->user = $user;
	  $this->jwtauth = $jwtauth;
	  $this->request = $request;
	  $this->account = $account;
	  $this->transaction = $transaction;
	  $this->notification = $notification;
	   $this->withdrawal = $withdrawal;
	   $this->gt = $gt;
	   $this->mobile_wallet = $mobile_wallet;
	  $this->middleware('jwt.auth', ['except' => ['authenticate']]);
	}
	   
	
	
	public function tokenCheck(Request $request)
    {
    	
    	try {

			if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
				return response()->json(['user_not_found'], 404);
			}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		// the token is valid and we have found the user via the sub claim
		
        return response()->json(['user_token_active'],200);
    }
	public function get_balance(Request $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		// the token is valid and we have found the user via the sub claim
		//return response()->json(compact('user'));
    	$currentUser = $this->jwtauth->parseToken()->authenticate();
    	//return [$currentUser];
        $balance = $this->account->getCurrentBalance($currentUser->user_id);
        return response()->json(compact('balance'));
    }

    public function buy_credit(BuyAirtimeRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
		// the token is valid and we have found the user via the sub claim
		//return response()->json(compact('user'));
    	$currentUser = $this->jwtauth->parseToken()->authenticate();
    	//return [$currentUser];

    	$phone = $this->request->input('phone');
        $amount = $this->request->input('amount');
        $network = $this->request->input('network');
        $pin = base64_encode($this->request->input('pin'));
       
       if ($this->ussd->validateUser($currentUser->phone,$pin)) {
          $userPhone = '233'.substr($currentUser->phone, 1);

          $result = $this->ussd->topUp($userPhone, $phone, $amount, $network);
         
            return response()->json(['status' => true, 'msg' => $result]);
          
         
       }else{
          return response()->json(['status' => false, 'msg' => 'Incorrect Pin']);
       }
        
    }

    public function transactions()
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$account = $this->account->where('user_id',$currentUser->user_id)->first();
        if ($account) {
             $users = DB::table('transactions')
            ->join('users', 'transactions.user_id', '=', 'users.user_id')
            ->select('transactions.*', 'users.firstname','users.lastname')
            ->where('transactions.account_id',$account->account_id)->orderBy('id','desc')
            ->take(10)->get();
            if ($users) { 
                return response()->json(['status' => true,'transactions' => $users]);
            }
            
        }
        return response()->json(['status' => true,'transactions' => []]);
       
      
        
    }
    public function notifications()
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$notifications = $this->notification->where('user_id',$currentUser->user_id)->where('status',"unread")->take(10)->get();
    	
        if ($notifications) {
            
                return response()->json(['status' => true,'notifications' => $notifications]);
            
            
        }
        return response()->json(['status' => true,'notifications' => []]);
       
      
        
    }

    public function transfer_money(TransferRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
		// the token is valid and we have found the user via the sub claim
		//return response()->json(compact('user'));
    	$currentUser = $this->jwtauth->parseToken()->authenticate();
    	//return [$currentUser];

    	
        
        $pin = base64_encode($this->request->input('pin'));
       
       if ($this->ussd->validateUser($currentUser->phone,$pin)) {
         	$sender_id = $currentUser->user_id;
            $recipient_phone = $request->input('phone');
            $amount = $request->input('amount');
            $recipient = $this->user->where('phone',$recipient_phone)->where('admin',0)->first();
            // dd($recipient->fullname);
            
             $withdraw = 1;
            if ($recipient) {
            	$recipient_name = $recipient->fullname; 
                if($this->account->getCurrentBalance($sender_id) >= $amount){
                    if($this->account->debitAccount($sender_id, $amount,$withdraw,$recipient_name)){
                        if ($this->account->creditAccount($recipient->user_id, $amount)) {
                            return response()->json(['status' => true,'msg'=>'Transfer Successfull']);
                        }          
                        $this->account->creditAccount($sender_id, $amount);
                        return response()->json(['status' => false,'msg' => 'Unable to complete transaction']);
                    }
                    return response()->json(['status'=>false, 'msg' => 'Unable to debit account']);
                }
                return response()->json(['status'=>false, 'msg' => 'Insufficient funds']);
            }else{
                return response()->json(['status'=>false, 'msg' => 'Recipient Account not found']);
            }
          
         
       }else{
          return response()->json(['status' => false, 'msg' => 'Incorrect Pin']);
       }
        
    }

    public function change_password(PasswordRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$old_pass = $this->request->input('old_password');
	    $new_pass = $this->request->input('new_password');
	    $new_pass1 = $this->request->input('repeat_password');
	    $user_id = $currentUser->user_id;
	    if ($new_pass === $new_pass1) { 
	        if (Auth::validate(['user_id'=>$user_id, 'password' => $old_pass])) {
	            $user = $this->user->where('user_id',$user_id)->first();
	            $user->password = bcrypt($new_pass);
	            if ($user->save()) {
	              return response()->json(['status'=>true,'message'=>"Password changed successfully"]);
	            }
	              return response()->json(['status'=>false,'message'=>'Password change failed']);
	        }else{

	          return response()->json(['status'=>false,'message'=>'Wrong Password provided']);
	        }

	    }else{
	      return response()->json(['status'=>false,'message'=>'Passwords do not match']);
	    }
        // return response()->json(['status' => true,'notifications' => []]);
       
      
        
    }

     public function change_pin(ChangePinRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$old_pass = $this->request->input('old_pin');
	    $new_pass = $this->request->input('new_pin');
	    $new_pass1 = $this->request->input('repeat_pin');
	    $user_id = $currentUser->user_id;
	    if ($new_pass === $new_pass1) { 
	    	$user = $this->user->where('user_id', $currentUser->user_id)->where('pin', base64_encode($old_pass))->first();
	        if ($user) {
	            $user = $this->user->where('user_id',$user_id)->first();
	            $user->pin = $new_pass;
	            if ($user->save()) {
	              return response()->json(['status'=>true,'message'=>"Pin changed successfully"]);
	            }
	              return response()->json(['status'=>false,'message'=>'Pin change failed']);
	        }else{

	          return response()->json(['status'=>false,'message'=>'Wrong Pin provided']);
	        }

	    }else{
	      return response()->json(['status'=>false,'message'=>'Pins do not match']);
	    }
        // return response()->json(['status' => true,'notifications' => []]);
       
      
        
    }
     public function withdrawal(WithdrawalRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$user_id = $currentUser->user_id;
        $amount = $request->input('amount');
        $withdraw = 2;
        if($this->account->getCurrentBalance($user_id) >= $amount){
             if($this->account->debitAccount($user_id, $amount,$withdraw)){
                $withdrawal_id = Uuid::uuid4();
                $this->withdrawal->user_withdrawal_id = $withdrawal_id;
                $this->withdrawal->user_id = $user_id;
                $this->withdrawal->amount = $amount;
                $this->withdrawal->phone_number =  $request->input('phone');
                
                if ($this->withdrawal->save()) {
                    return response()->json(['status'=>true,'msg'=>'Request Successfull']);
                }
            }


        }
        return response()->json(['status'=>false, 'msg' => 'Insufficient funds']);
        // return response()->json(['status' => true,'notifications' => []]);
       
      
        
    }

     public function photoUpload(Request $request)
    {
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();
    	$userClient = $this->user->where('user_id', $currentUser->user_id)->first();
        $file = $request->file('file');

        if (!empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/';
            $path = '/uploads/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";            
            if (file_exists($dir.$uploadedfile)) {
                    $filename = uniqid();
                    $file->move($dir,$uploadedfile);
                
            }else{
                    $file->move($dir,$uploadedfile);
            }
            $userClient->profile_image = $path.$uploadedfile;
            if ($userClient->save()) {
            	return response()->json(['status' => true,'imageUrl'=>$path.$uploadedfile]);
            }
            return response()->json(['status' => false,'imageUrl'=>'Unable To Upload image']);
                
            
        }
        return response()->json(['status' => false,'imageUrl'=>""]);
    }
     public function load_wallet(LoadWalletRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$user_id = $currentUser->user_id;
        $amount = $request->input('amount');

        $gsm_phone = $this->request->input('gsm_phone');
        $phone = $this->request->input('phone');
      
        $network = $this->request->input('network');
        
        $userClient = $this->user->where('phone', $gsm_phone)->first();
      
        $ReferenceID = rand();
        $transId = rand();

          $this->gt->gt_id = Uuid::uuid4();
          $this->gt->user_id = $userClient->user_id;
          $this->gt->clientRef = $ReferenceID;
          $this->gt->transRef = $transId;
          $this->gt->amount = $amount;
          $this->gt->senderPhone = $gsm_phone;
          $this->gt->mobileNumber = $phone;
          $this->gt->senderName =$currentUser->lastname." ".$currentUser->firstname;
        
          if ($this->gt->save()) {

		
            $service = [
              "amount"=>$amount,
              "network"=>$network,
              "mobileNumber"=>$phone,
              "transId"=>"208624143",
              "ReferecnceID"=>"523529364",
              "remarks"=>"Mtn Mobile Money Test"
           ];
	         $mechantreference = rand();
	        

            $data = [

                  "serviename"=>"initiatedebitwallet",
                  "merchantToken"=>"065CC989-FB9F-4CD5-A3E2-804CADEA7484",
                  "merchantKey"=>"9C83FB9F-8006-4350-867C-1F85A3175E5D",
                  "merchantreference"=>"525365",
                  "serviceDetails"=>json_encode($service)

            ];

        	
        	//var_dump($data);

        	
        	\GuzzleHttp\RequestOptions::JSON;
        	$url = 'https://196.216.228.129/apihub/api/myghpayextension/paymentRequest';
        	$client = new \GuzzleHttp\Client(['verify' => false]);
        	// $client->setDefaultOption('verify', false);
        	$res = $client->request('POST', $url, ['json'=>$data]); 
        	//return json_decode($res->getBody(), true);
        	$respdata = json_decode($res->getBody(), true);
        	//var_dump($respdata);
        	$tt = json_decode($respdata['Details'],true);
        	//$t = (array) $t;
        	//var_dump($t["Status"]);
        	//return $respdata;
        	 $status = $respdata["Status"];
        	 //var_dump($status);
              $t = $this->gt->where('transRef',$transId)->first();
              if ($status == 1) {
               
                $result = (new GtTransaction)->creditMobileAccount($t->user_id,$t->amount);
                    if ($result) {
                    	$amountCredit =$t->amount;
						$message =new Notification;
						$message->notification_id = Uuid::uuid4();
						$message->user_id = $t->user_id;
						$message->title = 'Self Load Wallet Topup';
						$message->detail = "Your Account has been credited with ".$amountCredit." Ghana Cedis as Wallet topup";
						$message->status="unread";
						$message->save();

                        
                     //$this->sendNotification($currentUser->deviceToken,$t->amount);
                          // $deviceToken = "demiM5xoHRs:APA91bH_agiIw5fa7M2JD7mGLK6fPrPf55uwx3_3YqL1U43z3eSD3JWVURbztEOXxxd6KwI_4_W06wLK9icnLLX53eUsWza-pCXoXATT3RSu7pb4ZVfUSc3e4QeUpugUbPRVnRsoEiFt";
						if (!is_null($userClient->deviceToken)) {
							$this->sendNotification($userClient->deviceToken,$t->amount);
						}
                          
                          //return ['status'=>true,'message'=>"Account credited successfully!"];
                    }
                    //return ['status'=>false,'message'=>"An Error occured"];
              }else{
                      $t->status = "failed";
                      $t->Message = $t["Message"];
                      $t->save();
                      $amountCredit =$t->amount;
                      $message =new Notification;
                      $message->notification_id = Uuid::uuid4();
                      $message->user_id = $t->user_id;
                      $message->title = 'Self Load Wallet Topup Failure';
                      $message->detail = "Your Request to Credit your account with ".$amountCredit." Ghana Cedis as Wallet topup has failed";
                      $message->status="unread";
                      $message->save();
                      if (!is_null($userClient->deviceToken)) {
                      	$this->sendFailedNotification($userClient->deviceToken,$t->amount);
                      }
                      
              }
       

             
            // return ['status'=>false,'message'=>"An Error occured"];
        }


      
   
      
       
      
        
    }

    //transflow
    public function load_wallet_transflow(LoadWalletRequest $request)
    {
    	
    	try {

		if (! $user =  $this->jwtauth->parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

			return response()->json(['token_absent'], $e->getStatusCode());

		}

		
    	$currentUser = $this->jwtauth->parseToken()->authenticate();

    	$user_id = $currentUser->user_id;
        // $amount = $request->input('amount');

        // $gsm_phone = $this->request->input('gsm_phone');
        // $phone = $this->request->input('phone');
      
        $network = $this->request->input('network');
        
       
      
        
        //from here
        $amt = $this->request->input('amount');
        $phone = $this->request->input('gsm_phone');
        $mtn_mobile = $this->request->input('phone');
        $mo = '+2330545247030';
         //$userClient = $this->user->where('phone', $phone)->first();


        if ($amt && $phone && $mtn_mobile) {
                 $wsdl = 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService?wsdl';
        $trace = true;
        $exceptions = true;
        
        $userClient = $this->user->where('phone', $phone)->first();

        $name = 'Global Success Minds';
        $info = 'Load wallet service';
        $mobile = '+233'.substr($mtn_mobile, 1);
        $mesg = 'Kindly find attached the invoice number {$inv}';
        $billprompt = '1';
        $thirdpartyID = '233549398254';
        $username = 'GSMLTD12';
        $password = '2i28U71fK'; 
            


        $xmlr = new \SimpleXMLElement("<postInvoice></postInvoice>");
        $xmlr->addChild('name', $name);
        $xmlr->addChild('info', $info);
        $xmlr->addChild('amt', $amt);
        $xmlr->addChild('mobile', $mobile);
        $xmlr->addChild('mesg', $mesg);
        $xmlr->addChild('billprompt', $billprompt);
        $xmlr->addChild('thirdpartyID', $thirdpartyID);
        $xmlr->addChild('username', $username);
        $xmlr->addChild('password', $password);

        //dd($xmlr);
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        try
        {
           $client = new \SoapClient($wsdl,array('location' => 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService.InvoicingServiceHttpSoap11Endpoint/','trace' => $trace, 'exceptions' => $exceptions, 'soap_version' => SOAP_1_2,'stream_context' => $context));
           $response = $client->postInvoice($xmlr);
          
        }
        catch (Exception $e)
        {
           echo "Error!";
           echo $e -> getMessage ();
           echo 'Last response: '. $client->__getLastResponse();
        }
         $v = ['result'=>$response->return];
          foreach ($v as $key => $value) {
            $b = (array)$value;
            break; 
          }
          $responseCode = $b['responseCode'];
          $responseMessage = $b['responseMessage'];
          $invoiceNo = $b['invoiceNo'];

          $this->mobile_wallet->mobile_wallet_id = Uuid::uuid4();
          $this->mobile_wallet->user_id = $userClient->user_id;
          $this->mobile_wallet->invoiceNo = $invoiceNo;
          $this->mobile_wallet->amount = $amt;
          $this->mobile_wallet->responseCode = $responseCode;
          $this->mobile_wallet->responseMessage = $responseMessage;

          if ($this->mobile_wallet->save()) {
           
            return response()->json(['status' => true,'message'=>'Request is being processed']);
          }
        }else{
          
            return response()->json(['status' => false,'message'=>'Unable to process request']);
        }
       


        //save transaction here into wallet loading transactions table 

             
            // return ['status'=>false,'message'=>"An Error occured"];
        


      
   
      
       
      
        
    }

    public function sendNotification($deviceId,$amount)
    {

    	$apiKey = 'AAAACF1LdRM:APA91bHYnA7ymDVD2SwYYcKdVF1MmHvqh1c0Jo-0lSn_ev9bM4RZW0boSDuKXxcNwUuWRLeTJfe-p-i5WQnN3QtDnHFkVi_2hq26iAB8z2XYQCqwAPmWQv6X-LZ0gwEnkFF-EKzbwyxj';
		$client = new FCM();
		$client->setApiKey($apiKey);
		$client->injectHttpClient(new \GuzzleHttp\Client());

		$note = new FCMNotification('Wallet Load', "Your Account has been loaded with $amount");
		$note->setIcon('notification_icon_resource_name')
		    ->setColor('#ffffff')
		    ->setBadge(1);

		$message = new Message();
		$message->addRecipient(new Device($deviceId));
		$id =  Uuid::uuid4();
		$message->setNotification($note)
		    ->setData(array('someId' => $id));

		$response = $client->send($message);
		return $response;
    }

    public function sendFailedNotification($deviceId,$amount)
    {

    	$apiKey = 'AAAACF1LdRM:APA91bHYnA7ymDVD2SwYYcKdVF1MmHvqh1c0Jo-0lSn_ev9bM4RZW0boSDuKXxcNwUuWRLeTJfe-p-i5WQnN3QtDnHFkVi_2hq26iAB8z2XYQCqwAPmWQv6X-LZ0gwEnkFF-EKzbwyxj';
		$client = new FCM();
		$client->setApiKey($apiKey);
		$client->injectHttpClient(new \GuzzleHttp\Client());

		$note = new FCMNotification('Wallet Load Failure', "Your Request to Credit your account with $amount Ghana Cedis as Wallet topup has failed");
		$note->setIcon('notification_icon_resource_name')
		    ->setColor('#ffffff')
		    ->setBadge(1);

		$message = new Message();
		$message->addRecipient(new Device($deviceId));
		$id =  Uuid::uuid4();
		$message->setNotification($note)
		    ->setData(array('someId' => $id));

		$response = $client->send($message);
		return $response;
    }

    function cvf_convert_object_to_array($data) {

	    if (is_object($data)) {
	        $data = get_object_vars($data);
	    }

	    if (is_array($data)) {
	        return array_map(__FUNCTION__, $data);
	    }
	    else {
	        return $data;
	    }
	}
	
   
}

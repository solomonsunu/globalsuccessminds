<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;

use Uuid;
use Session;
use Cache;

use Mail;

use Carbon\Carbon;
use Tymon\JWTAuth\JWTAuth;
//use JWTAuth;

use JWTAuthException;


class AuthController extends Controller
{
    private $user;
	private $jwtauth;
	public function __construct(User $user, JWTAuth $jwtauth,Request $request)
	{
	  $this->user = $user;
	  $this->jwtauth = $jwtauth;
	  $this->request = $request;
	}
	   
	public function register(RegisterRequest $request)
	{
		$user_id = Uuid::uuid4();
	    $this->user->user_id = $user_id;
	    $this->user->firstname = $this->request->input('firstname');
	    $this->user->lastname = $this->request->input('lastname');
	    $this->user->username = $this->request->input('username');
	    $this->user->email = $this->request->input('email');
	    $this->user->phone = $this->request->input('phone');
	    $this->user->dob = $this->request->input('dob');
	    $this->user->gender = $this->request->input('gender');
	    $this->user->admin = $this->request->input('role');
	    $this->user->brought_by =  Auth::user()->user_id;
	    $this->user->password = bcrypt($this->request->input('password'));
	    $this->user->carrier = $this->request->input('carrier');
	    $this->user->active = 1;
	    $this->user->country = 'Ghana';
  
	    if($user->save()) {
	         $user = $this->user->find($user_id);
	          
	        return response()->json([
		      'token' => $this->jwtauth->fromUser($newUser),
		      'user'=>$user
		    ]);
	    }else{
	    	return response()->json(['failed_to_create_new_user'], 500);
	    }
		
	   
	    
	}

	public function login(LoginRequest $request)
	{
		//return ['etst'];
	  // get user credentials: email, password
	  $credentials = $request->only('username', 'password');

	  $token = null;
	  try {

	    $token = $this->jwtauth->attempt($credentials);
	    //return $credentials;
	    if (!$token) {
	      return response()->json(['invalid_username_or_password'], 422);
	    }
	    $user = $this->user->where('username',$request->username)->first();
	    if(is_null($user->deviceToken)){
	    	$user->deviceToken = $this->request->input('deviceToken');
	    	$user->save();
	    } 
	  } catch (JWTAuthException $e) {
	    return response()->json(['failed_to_create_token'], 500);
	  }
	  return response()->json(compact(['token','user']));
	}
   
}

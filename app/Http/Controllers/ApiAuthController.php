<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use Uuid;
use Session;
use Cache;

use Mail;

use Carbon\Carbon;
use JWTAuth;
use JWTAuthException;



class ApiAuthController extends Controller
{

  private $user;
  private $jwtauth;
  public function __construct(User $user, JWTAuth $jwtauth,Request $request)
  {
    $this->user = $user;
    $this->jwtauth = $jwtauth;
     $this->request = $request;
  }
   
   public function register(RegisterRequest $request)
  {
    
    return response()->json([
      'token' => $this->jwtauth->fromUser($newUser)
    ]);
  }

  public function login(LoginRequest $request)
  {
    // get user credentials: email, password

    return 'test';
    // $credentials = $request->only('username', 'password');
    // $token = null;
    // try {
    //   $token = $this->jwtauth->attempt($credentials);
    //   if (!$token) {
    //     return response()->json(['invalid_email_or_password'], 422);
    //   }
    // } catch (JWTAuthException $e) {
    //   return response()->json(['failed_to_create_token'], 500);
    // }
    // return response()->json(compact('token'));
  }
   

}

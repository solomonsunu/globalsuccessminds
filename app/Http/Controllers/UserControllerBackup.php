<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Account;
use App\Notification;
use Auth;
use Uuid;
use Session;
use Cache;
use App\Helpers\Insurance;
use App\Helpers\RankInsurance;
use App\Helpers\UpgradeCheck as Upgrade;
use App\Helpers\TotalTopUp as TopUp;
use Mail;
use App\InsuranceLog;
use App\Transaction;
use App\CompanyEarning;
use App\TopupEarning;
use App\Rank;
use Carbon\Carbon;
use DB;
use App\UserUpgrade;


/*
   active

*/




class UserController extends Controller
{

   public function __construct(CompanyEarning $earning,TopupEarning $earningTopUp,TopUp $topup,InsuranceLog $log, Account $account,Insurance $insurance,RankInsurance $rankInsurance,  User $user, Request $request, Notification $notification,Transaction $transaction, Rank $rank, Upgrade $upgrade, UserUpgrade $userUpgrade)
   {
        $this->user = $user;
        $this->request = $request;
        $this->notification = $notification;
        $this->insurance = $insurance;
        $this->log = $log;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->topup = $topup;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
        $this->rank = $rank;
        $this->upgrade = $upgrade;
        $this->userUpgrade = $userUpgrade;
        $this->rankInsurance = $rankInsurance;
   }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
   */
  public function dashboard()
  {
      return view('dashboard.home');
  }
  public function loginPage()
  {
      return view('dashboard.auth.login');
  }

  public function superAdminUserManager()
 {
      return view('dashboard.user-management');
 }
  public function superAdminUserRankManager()
 {
      return view('dashboard.user-rank-check');
 }
 //cehck this
 public function adminUserManager()
 {
      return view('dashboard.admin-user-management');
 }

 public function admin4AccountManager()
 {
      return view('dashboard.admin4-account-management');

 }
 public function admin3AccountManager()
 {
      return view('dashboard.admin3-account-manager');
 }
 public function admin2AccountManager()
 {
     return view('dashboard.admin2-account-manager');
 }
 public function adminUserAccountManager()
 {
      return view('dashboard.account-manager');
 }
 public function regularUserAccount()
 {
      return view('dashboard.regular-user-management');

 }
 public function summary()
 {
      return view('dashboard.user-summary');

 }
 public function admin_payment()
 {
      return view('dashboard.admin-payment');

 }
 public function admin_profit()
 {
      return view('dashboard.admin-profit');

 }
  public function ranks()
 {
      return view('dashboard.admin-ranks');

 }
  public function update_user()
 {
      return view('dashboard.update-user');

 }
  public function premium_upgrade()
 {
      return view('dashboard.premium-upgrade');

 }
 public function regularAccountManager()
 {
      return view('dashboard.regular-account-management');

 }
 public function adminRole()
 {
      return view('dashboard.role-management');

 }
 public function adminMessage()
 {
      return view('dashboard.admin-message');
 }
  public function test($val)
  {
  return $this->insurance->getTotal($val);
  }
  public function change_password(){
    return view('dashboard.change-password');
  }
  public function save_password()
  {
    $old_pass = $this->request->input('old_password');
    $new_pass = $this->request->input('new_password');
    $new_pass1 = $this->request->input('new_password_repeat');
    $user_id = $this->request->input('user_id');
    if ($new_pass === $new_pass1) { 
        if (Auth::validate(['user_id'=>$user_id, 'password' => $old_pass])) {
            $user = $this->user->where('user_id',$user_id)->first();
            // dd($user);
            $user->password = bcrypt($new_pass);
            if ($user->save()) {
              return ['status'=>true];
            }
              return ['message'=>'Password change failed'];
        }else{

          return ['message'=>'Wrong Password provided'];
        }

    }else{
      return ['message'=>'Password do not match'];
    }
  }
  public function reset_password()
  {
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $new_pass = "gsm_default";
    if ($user) {
      $user->password = bcrypt($new_pass);
            if ($user->save()) {
              return ['status'=>true];
            }
              return ['message'=>'Password change failed'];
    }else{
        return ['message'=>'User not found'];
    }

  }

   public function getPayHistory($id){
    $getPayHistory = $this->user->with('accountdetail')->where('user_id',$id)->first();
    return ['payHistory'=>$getPayHistory];
   }
   public function get_active_user(){
    
    $activeUser = $this->user->where(['active'=>1,'admin'=>0])->with('account')->paginate(10);
    return response()->json(['status'=>true,'activeUser'=>$activeUser->toArray(),'paginate'=>$activeUser->toArray()]);
     // return $activeUser->toArray();
   }
  public function get_network_active_user($carrier){
  
    $activeUser = $this->user->where(['active'=>1,'admin'=>0,'carrier'=>$carrier])->with('account')->paginate(50);
     return $activeUser->toArray();
  }
  public function getUser(){
    $count = 0;
    User::where(['active'=>1,'admin'=>0])->chunk(100,function ($users) use (&$count){
      foreach ($users as $user) {
          $count = $count + $user->account->current_balance;
      }
    });
     return ['totalBalance'=>$count];
     unset($count);
  }



   public function manageduser($id){
     
        $manageUser = $this->user->where(['carrier'=>Auth::user()->carrier,'active'=>1,'admin'=>0])->where('phone','like','%'.$id.'%')->take(10)->get();
        return ['manageUser'=>$manageUser];
      
   }
   public function managed_all_user($id){
     
        $allusers = $this->user->where(['active'=>1,'admin'=>0])->where('phone','like','%'.$id.'%')->take(5)->get();
        return ['allusers'=>$allusers];
      
   }

   public function currentUser(){
       $currentUser = $this->user->with(['account','ranks'])->where('user_id',Auth::user()->user_id)->first();
       $admin = $this->user->whereIn('admin',[2,3,4,5,6,7])->get();

       return ['currentUser' => $currentUser,'admin'=>$admin];
   }

   

   public function specificUser()
   {


       //end of if level1 > 0
       //dd($getPayment->toArray());
       // if ($getPayment) {
       //     return ['status'=>true,'getPayment'=>json_encode($insurance),'getPaymentUser'=>json_encode($getPayment)];
       // }
       // return ['status'=>false];

   }
   protected function users_with_pay($collection){
      $all_users = $this->user->with('account')->where(['active'=>1,'paymentstatus'=>1,'admin'=>0])->orderBy('id')->get();
      $amt = 0.0;
        foreach ($all_users as  $value) {
          foreach ($collection as $v => $va) {
            if($v == $value->user_id){
              $value->amt = $va;
              break;
            }
          }
        }
      return $all_users;
    }
    public function getPaymentNew()
    {  
      $payUser = $this->user->where(['paymentstatus'=>1,'admin'=>0])->paginate(2);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getTf($payUser),'paginate'=>$payUser->toArray()]);
    }

    public function getTotalRankUpgrade()
    {  
      $payUser = $this->user->where(['paymentstatus'=>1,'admin'=>0])->whereNotNull('premium_referrer')->paginate(20);

      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->rankInsurance->getTotalRank($payUser),'paginate'=>$payUser->toArray()]);
    }

   public function getPayment()
   {
      
       return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getNewTotals()]);


   }
    public function getPaymentPremium()
   {
      $payUser = $this->user->where(['admin'=>0])->whereNotNull('premium_referrer')->paginate(20);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount($payUser),'paginate'=>$payUser->toArray()]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }
   //interim fix
    public function getUserPremiumFix()
   {
     $payUser = $this->user->where(['admin'=>0])->whereNotNull('premium_referrer')->whereMonth('premium_update_date', '=', date('m'))->whereYear('premium_update_date', '=', date('Y'))->with(['ranks'])->get();
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user'=>$payUser->toArray()]);
   }

    public function getUserPremium()
   {
      $payUser = $this->user->where(['admin'=>0])->whereNotNull('premium_referrer')->whereMonth('premium_update_date', '=', date('m'))->whereYear('premium_update_date', '=', date('Y'))->with(['ranks'])->paginate(10);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user'=>$payUser->toArray(),'paginate'=>$payUser->toArray()]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }
     public function getUserRanks()
   {
      $rankUser = $this->userUpgrade->where(['status'=>0])->get();
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user_ranks'=>$rankUser]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }

   public function ChangeUserRanksStatus($id)
   {
      $rankUser = $this->userUpgrade->where('upgrade_id',$id)->first();
      // dd($rankUser);
      $rankUser->status = 1;
      if ($rankUser->save()) {
        return ['status'=>true];
      }
      return ['status'=>false];
   }
   public function getProfit()
   {
       $earning = $this->earning->get();
       if ($earning) {
           return ['status'=>true,'earning'=>$earning];
       }
       return ['status'=>false,'earning'=>[]];

   }
   public function allUser()
   {
       $registerInsurance = $this->user->where('active',1)->where('admin',0)->orderBy('paymentstatus')->get();
       if ($registerInsurance) {
           return ['status'=>true,'registerInsurance'=>$registerInsurance];
       }
       return ['status'=>false];

   }
   public function getAdmin()
   {
       $adminList = $this->user->where('active',1)->where('admin','!=',0)->orderBy('admin')->get();
       if ($adminList) {
           return ['status'=>true,'adminList'=>$adminList];
       }
       return ['status'=>false];

   }
  
 public function activateRole(){
      $user_id = $this->request->input('user_id');
      //dd($user_id);
      $user = $this->user->where('user_id',$user_id)->first();
      $user->admin = $this->request->input('role_id');

      if($user->save()) {

               return ['status'=>true];
          }

     return ['status'=>false];

 }
   
public function removeUser(){
  $user_id = $this->request->input('user_id');
  $user = $this->user->where('user_id',$user_id)->first();
  if($user->delete()) {
      return ['status'=>true];
  }
  return['status'=>false];

}

  public function total_profit(){
    $profit = $this->earning->get();
    if ($profit) {
      return ['status'=>true, 'profit'=>$profit];
    }
      return['status'=>false];
  }

  public function totalInsurance(){
    $admins = $this->user->where('admin','!=',0)->where('admin','!=',1)->get();
    $dt = date('Y-m-d');
    $profit_topup = $this->earningTopUp->whereRaw('date(created_at) = ?', [$dt])->get();
    if ($admins) {
        foreach ($admins as  $value) {
            if ($value->admin == 2) {
               $admin1 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 3) {
               $admin2 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 4) {
               $admin3 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 5) {
               $admin4 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 6) {
               $admin5 = $this->topup->getTotals($value->carrier,$dt);
            }else{
               $admin6 = $this->topup->getTotals($value->carrier,$dt);
            }
        }
        return ['status'=>true, 'profit_topup'=>$profit_topup,'admin1'=>$admin1, 'admin2'=>$admin2, 'admin3'=>$admin3, 'admin4'=>$admin4,'admin5'=>$admin5,'admin6'=>$admin6];

    }

    return['status'=>false];

  }

  public function defActivity(){
    $admins = $this->user->where('admin','!=',0)->where('admin','!=',1)->get();
    $dt = $this->request->input('setDate');
    $profit_topup = $this->earningTopUp->whereRaw('date(created_at) = ?', [$dt])->get();
    if ($admins) {
        foreach ($admins as  $value) {
            if ($value->admin == 2) {
               $admin1 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 3) {
               $admin2 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 4) {
               $admin3 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 5) {
               $admin4 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 6) {
               $admin5 = $this->topup->getTotals($value->carrier,$dt);
            }else{
               $admin6 = $this->topup->getTotals($value->carrier,$dt);
            }
        }
        return ['status'=>true,'profit_topup'=>$profit_topup, 'admin1'=>$admin1, 'admin2'=>$admin2, 'admin3'=>$admin3, 'admin4'=>$admin4,'admin5'=>$admin5,'admin6'=>$admin6];

      }

      return['status'=>false];

 }

  public function updateUser(UpdateUserRequest $req){
    $user_id = $this->request->input('user_id');
    $choice = $this->request->input('choice');
    $choicePhone = $this->request->input('choicePhone');
    $user = $this->user->where('user_id',$user_id)->first();
    $user->firstname = $this->request->input('firstname');
    $user->lastname = $this->request->input('lastname');
    $user->email = $this->request->input('email');
    $user->dob = $this->request->input('dob');
    $user->gender = $this->request->input('gender');
    if ($choicePhone == 1) {
        $check = $this->user->where('phone',$this->request->input('phone'))->first();
        if (!$check) {
          $user->phone = $this->request->input('phone');
        } 
    }
    if ($choice == 1) {
      $user->password = bcrypt($this->request->input('password'));
    }
    if ($this->request->input('carrier')) {
      $user->carrier = $this->request->input('carrier');
    }
    if ($this->request->input('image') !== 0) {
      $user->profile_image = $this->request->input('image');
    }
    if($user->save()) {
        return ['status'=>true];
    }
      return ['status'=>false];
  }
  public function createUser(CreateUserRequest $req){
    $user_id = Uuid::uuid4();
    $this->user->user_id = $user_id;
    $this->user->firstname = $this->request->input('firstname');
    $this->user->lastname = $this->request->input('lastname');
    $this->user->username = $this->request->input('username');
    $this->user->email = $this->request->input('email');
    $this->user->phone = $this->request->input('phone');
    $this->user->dob = $this->request->input('dob');
    $this->user->gender = $this->request->input('gender');
    $this->user->admin = $this->request->input('role');
    $this->user->brought_by =  Auth::user()->user_id;
    $this->user->password = bcrypt($this->request->input('password'));
    $this->user->carrier = $this->request->input('carrier');
    $this->user->active = 1;
    $country = $this->request->input('country');
    if ($country == 'GH') {
        $this->user->country = 'Ghana';
    }elseif ($country == 'NG') {
         $this->user->country = 'Nigeria';
    }else{
         $this->user->country = 'United States of Ameria';
    }

    if ($this->request->input('image') !== 0) {
       $this->user->profile_image = $this->request->input('image');
    }
    if($user->save()) {
         $user = $this->user->find($user_id);
             $data = array(
                    'name' => $user->firstname
            );

            Mail::send('emails.registration', $data, function ($message) use($user){

                $message->from('admin@globalsuccessminds.com', 'Account Registration');

                $message->to($user->email)->subject('Account Registration');

            });
        return ['status'=>true];
    }
    return ['status'=>false];

 }


    
public function viewUser()
{
    return view('dashboard.view-user');
}
public function viewPremiumUser()
{
    return view('dashboard.view-premium-user');
}

public function levelUser($id)
{
    $getPayment = $this->user->with('account')->where('user_id',$id)->first();
    $referrer = $this->user->with('account')->where('user_id',$getPayment->brought_by)->first();
    $level1 = User::where('brought_by','=',$getPayment->user_id)->where('active','=',1)->get();
    $total1 = 0.0;
    $total2 = 0.0;
    $total3 = 0.0;
    $total4 = 0.0;
    $total5 = 0.0;
    $total6 = 0.0;

          //read l1 from cache
      $level1 = Cache::get($id.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image', 'active','paymentstatus')->where('brought_by','=',$id)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }
      
      
      //read l2
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }
             
            $balance = 0.0;
            
            $account = $this->account->where('user_id',$value->user_id)->first();
             //dd($account);
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value->amt = $balance;
               $total1 = $total1 + $balance;

               if (count($dummy) > 0) {
                  // $level2->add($dummy); 
                  array_push($level2, $dummy);   
               }
            }
         }

         $level2 = array_collapse($level2);
         // dd($level2);
        
         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total2 = $total2 + $balance;
                  if (count($dummy) > 0) {
                     array_push($level3, $dummy);                     
                  }
     
            }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
               $balance = 0.0;
               $account = $this->account->where('user_id',$value1->user_id)->first();
                 //dd($account);
               if ($account) {
                  $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                  $balance = $transaction;
                  $value1->amt = $balance;
                  $total3 = $total3 + $balance;
                  if (count($dummy) > 0) {                
                     array_push($level4, $dummy);
                     
                  }
                   
               }      
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  $balance = 0.0;
                  $account = $this->account->where('user_id',$value1->user_id)->first();
                  //dd($account);
                  if ($account) {
                     $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                     $balance = $transaction;
                     $value1->amt = $balance;
                     $total4 = $total4 + $balance;
                     if (count($dummy)> 0) {
                        array_push($level5, $dummy);
                     } 
                  
                  }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     $balance = 0.0;
                     $account = $this->account->where('user_id',$value1->user_id)->first();
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total5 = $total5 + $balance;
                        if (count($dummy)> 0) {
                           array_push($level6, $dummy);
                        }
                     }
                  }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
             
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total6 = $total6 + $balance;    
                     }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
          }
      }else{
         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
      }

   }else{
      return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
   }
    
}
public function levelPremiumUser($id){
   if (Auth::check()){

      $search = $id;
      $getPayment = $this->user->with('account')->where('user_id',$id)->first();
      $referrer = $this->user->with('account')->where('user_id',$getPayment->premium_referrer)->first();
      //dd();
      $total1 = 0.0;
      $total2 = 0.0;
      $total3 = 0.0;
      $total4 = 0.0;
      $total5 = 0.0;
      $total6 = 0.0;

      //read l1 from cache
      $level1 = Cache::get($search.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::where('premium_referrer','=',$search)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }     
      //read l2
      
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::where('premium_referrer','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }

            if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total1 = $total1 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level2, $dummy);   
                 }
         }
         
         $level2 = array_collapse($level2);

         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
             if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total2 = $total2 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level3, $dummy);   
                 }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
              if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total3 = $total3 + 1;
                  
              }  
              if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level4, $dummy);   
                 }    
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total4 = $total4 + 1;
                  
                  }
                  if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level5, $dummy);   
                 }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total5 = $total5 + 1;
                        
                      }
                      if (count($dummy) > 0) {
                              // $level2->add($dummy); 
                              array_push($level6, $dummy);   
                           }
                      }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                    if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                          $total6 = $total6 + 1;
                         
                    }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
          }
      }else{

         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
      }

   }else{
      return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
   }
   }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   //return View::make('dashboard.viewrefferals')->with('message',$message);

}

    
    
    //perform login
public function login(UserLoginRequest $req)
{
    //return $this->request->all();
    $username = trim($this->request->input('username'));
    $password = trim($this->request->input('password'));

    $user = $this->user->where('username', $username)->orWhere('phone', $username)->first();
    if ($user) {
        if(Auth::attempt(['username' => $user->username,'password'=>$password])){
            return redirect('dashboard');
        }
    }
    return redirect()->back()->withInput()->with('error', 'Wrong username/password combination');
}

public function logout()
{
  Auth::logout();
  return redirect('/');
}

public function register(StoreUserRequest $req){
   //dd($this->request->all());

   $filepath = '';
    $referredUser = $this->user->whereUsername($this->request->input('brought_by'))->first();
    //dd($referredUser->toArray());
    if(empty($referredUser)) {
            return redirect('register')->with('error', 'Referred User Does Not Exist !');
    }elseif($referredUser->active == 0){
            return redirect('register')->with('error', 'Referred User is pending Activation !');
    }else{

        // $file = $this->request->file('image');
        // if (!empty($file)) {
        //     $ext  = $file->getClientOriginalExtension();
        //     $dir = public_path().'/uploads/';
        //     $path = '/uploads/';

        //     $filename = uniqid();
        //     $uploadedfile = $filename.".{$ext}";            
        //     if (file_exists($dir.$uploadedfile)) {
        //             $filename = uniqid();
        //             $file->move($dir,$uploadedfile);
                
        //     }else{
        //             $file->move($dir,$uploadedfile);
        //     }
            

        // }

        $user_id = Uuid::uuid4();
        $this->user->user_id = $user_id;
        $this->user->firstname = $this->request->input('firstname');
        $this->user->lastname = $this->request->input('lastname');
        $this->user->username = $this->request->input('username');
        $this->user->email = $this->request->input('email');
        $this->user->phone = $this->request->input('phone');
        $this->user->dob = $this->request->input('dob');
        $this->user->gender = $this->request->input('gender');
        $this->user->brought_by =  $referredUser->user_id;
        $this->user->password = bcrypt($this->request->input('password'));
        $this->user->carrier = $this->request->input('carrier');
        // $this->user->pin = $this->user->generatePin();
        $this->user->pin = '0000';
        $this->user->verified = true;
        $this->user->active = 1;

        $country = $this->request->input('country');

        if ($country == 'GH') {
            $this->user->country = 'Ghana';
        }elseif ($country == 'NG') {
             $this->user->country = 'Nigeria';
        }else{
             $this->user->country = 'United States of Ameria';
        }

        // if ($this->request->file('image')) {
        //     $this->user->profile_image = $path.$uploadedfile;
        // }

        if ($this->user->save()) {
            $user = $this->user->find($user_id);

            $account_id = Uuid::uuid4();
            $this->account->account_id = $account_id;
            $this->account->user_id = $user->user_id;
            $this->account->current_balance = 0.0;
            $this->account->save();

            $data = array(
                'user' => $user
            );

            Mail::send('emails.registration', $data, function ($message) use($user){
                $message->from('admin@globalsuccessminds.com', 'Account Registration');

                $message->to($user->email)->subject('Account Registration');

            });
            return redirect('/login')->with('flash_notice', 'Registration Succesful');
        }
        return redirect()->back()->with('error', 'Registration Failed. Try again');
    }

}

public function confirmEmail($token){
  $user = User::whereToken($token)->first();
  if ($user) {
      $user->confirmEmail();
      $account_id = Uuid::uuid4();
      $this->account->account_id = $account_id;
      $this->account->user_id = $user->user_id;
      $this->account->current_balance = 0.0;
      if ($this->account->save()) {
          return redirect('/login')->with('flash_notice', 'You are now confirmed. Please login');

      } 
  }
}

public function viewLevel1(){
   if (Auth::check()){
        $search = Auth::user()->user_id;
        $total1 = 0.0;
        //read l1 from cache
        $level1 = Cache::get($search.'_tree_l1');

        if(!$level1){
           // error_log('no cache');
           $level1 = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image', 'active','paymentstatus','email','phone')->where('brought_by','=',$search)->where('active','=',1)->get();
           // Cache::forever($search.'_tree_l1', $level1);
        }
        $nextLevel = [];
         if (count($level1) > 0) {
             foreach ($level1 as $value) {
                $balance = 0.0;
                $dummy = Cache::get($value->user_id.'_tree_l1');
                if(!$dummy){
                   $dummy = User::where('brought_by','=',$value->user_id)->where('active','=',1)->get(); 
                   // Cache::forever($value->user_id.'_tree_l1', $dummy);
                }  
                $account = $this->account->where('user_id',$value->user_id)->first();
                if ($account) {
                   $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   $balance = $transaction;
                   $value->amt = $balance;
                   $total1 = $total1 + $balance;
                   if (count($dummy) > 0) {
                      array_push($nextLevel, $dummy);   
                   }
                }
             }

             $nextLevel = array_collapse($nextLevel);
         }

         return ['status'=>true,'level1'=>$level1,'nextLevel'=>$nextLevel,'total1'=>$total1];
    }
}
public function viewLevel2(){
   if (Auth::check()){
      $level2 = json_encode($this->request->input('l2'));
        // dd(json_decode($level2));
        $total2 = 0.0;
        if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $nextLevel = [];
         foreach (json_decode($level2) as $key => $value1) {
            // dd($value1->user_id);
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total2 = $total2 + $balance;
                  if (count($dummy) > 0) {
                     array_push($nextLevel, $dummy);                     
                  }
     
            }
         }
         $nextLevel = array_collapse($nextLevel);
         return ['status'=>true,'level2'=>json_decode($level2),'nextLevel'=>$nextLevel,'total2'=>$total2];
       }
         
    }
}

public function viewLevel3(){
   if (Auth::check()){
      $level3 = json_encode($this->request->input('l3'));
        // dd(json_decode($level2));
        $total3 = 0.0;
        if (count($level3) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $nextLevel = [];
         foreach (json_decode($level3) as $key => $value1) {
            // dd($value1->user_id);
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total3 = $total3 + $balance;
                  if (count($dummy) > 0) {
                     array_push($nextLevel, $dummy);                     
                  }
     
            }
         }
         $nextLevel = array_collapse($nextLevel);
         return ['status'=>true,'level3'=>json_decode($level3),'nextLevel'=>$nextLevel,'total3'=>$total3];
       }
         
    }
}

public function viewLevel4(){
   if (Auth::check()){
      $level4 = json_encode($this->request->input('l4'));
        // dd(json_decode($level2));
        $total4 = 0.0;
        if (count($level4) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $nextLevel = [];
         foreach (json_decode($level4) as $key => $value1) {
            // dd($value1->user_id);
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total4 = $total4 + $balance;
                  if (count($dummy) > 0) {
                     array_push($nextLevel, $dummy);                     
                  }
     
            }
         }
         $nextLevel = array_collapse($nextLevel);
         return ['status'=>true,'level4'=>json_decode($level4),'nextLevel'=>$nextLevel,'total4'=>$total4];
       }
         
    }
}

public function viewLevel5(){
   if (Auth::check()){
      $level5 = json_encode($this->request->input('l5'));
        // dd(json_decode($level2));
        $total5 = 0.0;
        if (count($level5) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $nextLevel = [];
         foreach (json_decode($level5) as $key => $value1) {
            // dd($value1->user_id);
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total5 = $total5 + $balance;
                  if (count($dummy) > 0) {
                     array_push($nextLevel, $dummy);                     
                  }
     
            }
         }
         $nextLevel = array_collapse($nextLevel);
         return ['status'=>true,'level5'=>json_decode($level5),'nextLevel'=>$nextLevel,'total5'=>$total5];
       }
         
    }
}

public function viewLevel6(){
   if (Auth::check()){
      $level6 = json_encode($this->request->input('l6'));
        // dd(json_decode($level2));
        $total6 = 0.0;
        if (count($level6) > 0) {
         foreach (json_decode($level6) as $key => $value1) {
            // dd($value1->user_id);
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total6 = $total6 + $balance;
            }
         }
         return ['status'=>true,'level6'=>json_decode($level6),'total6'=>$total6];
       }
         
    }
}
public function viewreferral(){
   if (Auth::check()){
      $search = Auth::user()->user_id;
      //dd();
      $total1 = 0.0;
      $total2 = 0.0;
      $total3 = 0.0;
      $total4 = 0.0;
      $total5 = 0.0;
      $total6 = 0.0;

      //read l1 from cache
      $level1 = Cache::get($search.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image', 'active','paymentstatus')->where('brought_by','=',$search)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }
      
      
      //read l2
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }
             
            $balance = 0.0;
            
            $account = $this->account->where('user_id',$value->user_id)->first();
             //dd($account);
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value->amt = $balance;
               $total1 = $total1 + $balance;

               if (count($dummy) > 0) {
                  // $level2->add($dummy); 
                  array_push($level2, $dummy);   
               }
            }
         }

         $level2 = array_collapse($level2);
         // dd($level2);
        
         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total2 = $total2 + $balance;
                  if (count($dummy) > 0) {
                     array_push($level3, $dummy);                     
                  }
     
            }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
               $balance = 0.0;
               $account = $this->account->where('user_id',$value1->user_id)->first();
                 //dd($account);
               if ($account) {
                  $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                  $balance = $transaction;
                  $value1->amt = $balance;
                  $total3 = $total3 + $balance;
                  if (count($dummy) > 0) {                
                     array_push($level4, $dummy);
                     
                  }
                   
               }      
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  $balance = 0.0;
                  $account = $this->account->where('user_id',$value1->user_id)->first();
                  //dd($account);
                  if ($account) {
                     $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                     $balance = $transaction;
                     $value1->amt = $balance;
                     $total4 = $total4 + $balance;
                     if (count($dummy)> 0) {
                        array_push($level5, $dummy);
                     } 
                  
                  }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     $balance = 0.0;
                     $account = $this->account->where('user_id',$value1->user_id)->first();
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total5 = $total5 + $balance;
                        if (count($dummy)> 0) {
                           array_push($level6, $dummy);
                        }
                     }
                  }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
             
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total6 = $total6 + $balance;    
                     }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
          }
      }else{
         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
      }

   }else{
      return ['status'=>false];
   }
   }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   //return View::make('dashboard.viewrefferals')->with('message',$message);

}

public function viewPremiumReferral(){
   if (Auth::check() && Auth::user()->premium_referrer){

      $search = Auth::user()->user_id;
      //dd();
      $total1 = 0.0;
      $total2 = 0.0;
      $total3 = 0.0;
      $total4 = 0.0;
      $total5 = 0.0;
      $total6 = 0.0;

      //read l1 from cache
      $level1 = Cache::get($search.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::where('premium_referrer','=',$search)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }     
      //read l2
      
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::where('premium_referrer','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }

            if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total1 = $total1 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level2, $dummy);   
                 }
         }
         
         $level2 = array_collapse($level2);

         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
             if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total2 = $total2 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level3, $dummy);   
                 }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
              if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total3 = $total3 + 1;
                  
              }  
              if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level4, $dummy);   
                 }    
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total4 = $total4 + 1;
                  
                  }
                  if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level5, $dummy);   
                 }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total5 = $total5 + 1;
                        
                      }
                      if (count($dummy) > 0) {
                              // $level2->add($dummy); 
                              array_push($level6, $dummy);   
                           }
                      }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                    if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                          $total6 = $total6 + 1;
                         
                    }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
          }
      }else{

         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
      }

   }else{
      return ['status'=>false];
   }
   }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   //return View::make('dashboard.viewrefferals')->with('message',$message);

}
public function displayPremiumTree()
{
    if (Auth::check() && Auth::user()->premium_referrer){
      $total1 = 0.0;
      $level1 = User::where('premium_referrer','=',Auth::user()->user_id)->where('active','=',1)->get();
      
      if (count($level1) > 0) {
            foreach ($level1 as $value) {
                if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                      $total1 = $total1 + 1;            
                }            
            }
            
            return ['status'=>true,'level1'=>$level1,'t1'=>$total1];            
      }else{
        return ['status'=>false]; 
      }

   }else{
      return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
}

public function viewPremiumReferralNew(){
   if (Auth::check() && Auth::user()->premium_referrer){
      $total1 = 0.0;
      $level1 = User::where('premium_referrer','=',Auth::user()->user_id)->where('active','=',1)->get();
      
      if (count($level1) > 0) {
            foreach ($level1 as $value) {
                if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                      $total1 = $total1 + 1;            
                }            
            }
            $fullLevel = [];
            foreach ($level1 as $level) {
                $key = $level->user_id;
                $fullLevel[$key] = $level;
            }
            return ['status'=>true,'level1'=>$fullLevel,'t1'=>$total1];            
      }else{
        return ['status'=>false]; 
      }

   }else{
      return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   
}
public function viewPremiumReferralNextLevel(){
   if (Auth::check() && Auth::user()->premium_referrer){
      $level = $this->request->input('level');
      $total = 0.0;
     
      if (count($level) > 0) {
          $nextLevel = [];
          $userList = User::select('username','user_id','premium_referrer','profile_image','phone','firstname','lastname')
                            ->whereIn('premium_referrer',$level)->get();

          $userList = $userList->groupBy('premium_referrer');
          // User::referrerGroupBy($userList, $level);

           // foreach ($level as $value) {
           //    $dummy = User::where('premium_referrer','=',$value->user_id)->where('active','=',1)->get();
           //    if (count($dummy) > 0) {
           //      foreach ($dummy as $value) {

           //        if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
           //              $total = $total + 1;            
           //        }
                  
           //     }
           //      array_push($nextLevel, $dummy);  
           //    }
           // }
           // $nextLevel = array_collapse($nextLevel);
        
          return ['status'=>true,'level'=>$userList,'t'=>$total];            
      }else{
        return ['status'=>false]; 
      }

   }else{
      return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   
}
public function viewPremiumReferralOld(){
  if (Auth::check()){
    if (Auth::user()->premium_referrer) {
      $search = Auth::user()->user_id;
      //dd();
      $level1 = User::where('premium_referrer','=',$search)->where('active','=',1)->get();
      $total1 = 0.0;
      $total2 = 0.0;
      $total3 = 0.0;
      $total4 = 0.0;
      $total5 = 0.0;
      $total6 = 0.0;
   
      if ($level1->count() > 0) {
        $level2=new \Illuminate\Database\Eloquent\Collection();
          foreach ($level1 as $value) {
             
              $dummy = User::where('premium_referrer','=',$value->user_id)->where('active','=',1)->get();  
              if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total1 = $total1 + 1;
              }
             if ($dummy->count() > 0) {
                   $level2->add($dummy);    
                }
                 
          }
          if ($level2->count() > 0) {
          $level3=new \Illuminate\Database\Eloquent\Collection();
          foreach ($level2 as $key => $value) {
              foreach ($value as $key => $value1) {
                  $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                  if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total2 = $total2 + 1;
                  }
                  if ($dummy->count() > 0) {
                    $level3->add($dummy);
                   
                  }
               
              }
            }
            if ($level3->count() > 0) {
              $level4=new \Illuminate\Database\Eloquent\Collection();

              foreach ($level3 as $key => $value) {
                  foreach ($value as $key => $value1) {
                      $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                       if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total3 = $total3 + 1;
                        }
                      if ($dummy->count() > 0) {
                          $level4->add($dummy);
                      }

                  }
              }
              if ($level4->count() > 0) {
                  $level5=new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level4 as $key => $value) {
                    foreach ($value as $key => $value1) {
                      $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                      if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total4 = $total4 + 1;
                        }
                      if (count($dummy)> 0) {
                            $level5->add($dummy);
                           
                         
                          }
                    

                    }


                  }
                  if ($level5->count() > 0) {
                    $level6=new \Illuminate\Database\Eloquent\Collection();
                    foreach ($level5 as $key => $value) {
                      foreach ($value as $key => $value1) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total5 = $total5 + 1;
                        }
                        if (count($dummy)> 0) {
                            $level6->add($dummy);
                          }

                      }


                    }
                    if ($level6->count() > 0) {
                    foreach ($level6 as $key => $value) {
                      foreach ($value as $key => $value1) {
                        if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total6 = $total6 + 1;
                        }
                      }


                    }
                  }
                    if ($level6->count() > 0) {
                        return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                    }
                    else{
                        return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                    }
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                  }
                }else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
                }
            }else{
              return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
            }
        }else{
           return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6];
        }

      }else{
        return ['status'=>false];
      }
    }
 }else{
        return redirect('/login')->with('flash_notice', 'Your Session has expired');
  }
    
    


    //return View::make('dashboard.viewrefferals')->with('message',$message);

}

public function getPin($user_id)
{ 
    if($user = $this->user->find($user_id)){
        if(is_null($user->pin)){
            $pin = $this->user->generatePin();
            $user->pin = $pin;
            if($user->save()){
                return response()->json(['pin' => $pin, 'status'=>true]);
            }
        }
        return response()->json(['pin' => $user->pin, 'status'=>true]);
    }
    return response()->json(['status' => false]);
}

    
public function changePin(Request $request, $user_id){
  $oldpin = $request->input('old_pin');
  $newpin = $request->input('new_pin');
  $password = $request->input('password');

  $user = $this->user->where('user_id', $user_id)->where('pin', base64_encode($oldpin))->first();
  if($user){
      if(Auth::validate(['username'=>$user->username, 'password' => $password])){
          $user->pin = $newpin;
          if($user->save()){
              return response()->json(['status'=>true]);
          }
      }
      return response()->json(['status'=>false]);
  }
    
    return response()->json(['status'=>false, 'message'=>'Wrong details Old pin provided']);
}

public function change_username(){
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $new_username = $this->request->input('username');
    $check = $this->user->where('username',$new_username)->first();
    if (!$check) {
      if ($user) {
        $user->username = $new_username;
              if ($user->save()) {
                return ['status'=>true];
              }
                return ['message'=>'Username change failed'];
      }else{
          return ['message'=>'User not found'];
      }
    }else{
      return ['message'=>'Username Already taken'];
    }
    

  }

  //premium bonus update
  public function change_bonus(){
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $newBonus = $this->request->input('newBonus');

     if ($user) {
        $user->total_bonus = $newBonus;
        if ($user->save()) {
          return ['status'=>true];
        }
          return ['message'=>'Bonus change failed'];
      }else{
          return ['message'=>'User not found'];
      }
      

  }

//check for user referal shit
protected function checkUserReferral ($userId){
    $res = $this->upgrade->getAvailableUser($userId);
    if ($res) {
      foreach ($res as $key) {
          if (count($key) > 0) {
            foreach ($key as $value) {
              foreach ($value as $v) {
                if($v->total_pre < 5) {
                  return $v;
                }
              }
            }
          }
        }
      }else{
        return false;
    }
}

public function checkDirectRC($id){
  $total = User::where('premium_referrer',$id)->count();
  return ['total'=>$total];
}



public function userPremiumUpgrade(Request $request){
  $username = $request->input('username');
  $balance = $request->input('wbalance');
  $user = $this->user->where('username',$username)->first();
  $rank = $this->rank->first();
  $owner = $this->user->where('user_id',Auth::user()->user_id)->first();
  $withdraw = 3; //indicatiing transfer
  $amount = 45.00;
  $recipient_name = 'GSM';
  $bonus = 7.00;
  if ($user) {
     if($user->premium_referrer ){
        if ($user->total_pre < 5) {
          if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Premium User Account";
                        $message->status="unread";
                        $message->save();
                        $owner->premium_referrer = $user->user_id;
                        $owner->rank_id = $rank->rank_id;
                        $owner->total_pre = 0;
                        $owner->premium_update_date = date('Y-m-d');

                        if ($owner->save()) {
                          $user->total_pre = $user->total_pre + 1;
                          $user->total_bonus = $user->total_bonus + $bonus;
                            if ($user->save()) {
                            return response()->json(['status' => true,'message' => $user]);
                          }
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
          return response()->json(['status'=>false,'message'=>'Insufficient Funds for upgrade']);
        }else{
          $res = $this->checkUserReferral($user->user_id);
          if ($res) {
             if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Premium User Account";
                        $message->status="unread";
                        $message->save();
                       
                        $owner->premium_referrer = $res->user_id;
                        $owner->rank_id = $rank->rank_id;
                        $owner->total_pre = 0;
                        $owner->premium_update_date = date('Y-m-d');
                        if ($owner->save()) {
                          $user->total_bonus = $user->total_bonus + $bonus;
                          if ($user->save()) {
                            $res->total_pre = $res->total_pre + 1;
                            if ($res->save()) {
                              return response()->json(['status' => true,'message' => $res]);
                            }
                            return response()->json(['status'=>false, 'message' => 'something went wrong']);
                          }                   
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
            
          }else{
             return response()->json(['status'=>false, 'message'=>'Error. Try another user']);
          }
         
        }
      }
        
        return response()->json(['status'=>false, 'message'=>'Selected Sponsor is not a Premium User']);
    }
      return response()->json(['status'=>false, 'message'=>'Selected Sponsor does not exist']);
  }

}

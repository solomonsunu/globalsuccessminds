<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SaveAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Controllers\Controller;
use App\Account;
use App\User;
use App\UserWithdrawal;
use Uuid;
use Auth;
use Mail;
use Session;
use Cache;

class UserWithdrawalController extends Controller
{
     public function __construct(Account $account, UserWithdrawal $withdrawal, User $user,Request $request)
    {
        // $this->middleware('auth');
        $this->account = $account;
        $this->request = $request;
        $this->user = $user;
        $this->withdrawal = $withdrawal;
        
    }
    
    public function checkHistory($user_id)
    {
        $withdrawal = $this->withdrawal->getWithdrawal($user_id);

        return response()->json(compact('withdrawal'));
    }

    public function pendingWithdrawal()
    {
        $user = Auth::user();
        if ($user->admin == 1) {
            $withdrawal = $this->withdrawal->getPendingWithdrawalSuper();
        }else{
            $network = $user->carrier;
            $withdrawal = $this->withdrawal->getPendingWithdrawal($network);
        }
        
        return response()->json(compact('withdrawal'));
    }

    public function checkUserHistory(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = $this->user->where('user_id',$user_id)->first();
        
        if ($user) {
             $withdrawalHistory = $this->withdrawal->getWithdrawalHistory($user->user_id,$user->carrier);
             return response()->json(compact('withdrawalHistory'));
        }
       
    }

    public function checkWithdrawal($id)
    {
        $withdrawal = $this->withdrawal->where('user_withdrawal_id',$id)->first();
        if ($withdrawal) {
            $withdrawal->paid = true;
            $withdrawal->paid_date = date("Y-m-d H:i:s");
            if ($withdrawal->save()) {
               return ['status'=>true];
            }
             return ['status'=>false];
        }
            return ['status'=>false];
    }


    
}

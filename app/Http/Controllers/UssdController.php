<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Ussd;
use App\User;
use App\Account;
use Cache;
use Log;
use Uuid;
use Auth;
use Carbon\Carbon;
use App\Helpers\LoadWallet as LoadW;
use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapClient\SoapClientOptionsBuilder;
use BeSimple\SoapCommon\SoapOptionsBuilder;

use Artisaninweb\SoapWrapper\SoapWrapper;

use SoapClient;
use App\MobileWalletTransaction;
use App\GtTransaction;

use App\Notification;
use Ixudra\Curl\Facades\Curl;
// use Illuminate\Support\Facades\Log;


class UssdController extends Controller
{
    public function __construct(Ussd $ussd, Request $request, User $user, Account $account,LoadW $loadw,SoapWrapper $soapWrapper,MobileWalletTransaction $mobile_wallet,GtTransaction $gt)
    {
        $this->ussd = $ussd;
        $this->request = $request;
        $this->user = $user;
        $this->account = $account;
        $this->loadw = $loadw;
        $this->soapWrapper = $soapWrapper;
        $this->mobile_wallet = $mobile_wallet;
        $this->gt = $gt;
        
    }
    /**
     * Process ussd request
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $xml = $request->getContent();
        $ussdData = $this->ussd->convertXml($xml);
        // Cache::put('req', $ussdData, 2);
        $content = $this->ussd->process($ussdData);    

        return response($content, '200')->header('Content-Type', 'text/xml');
    }

    /**
     * Process ussd request
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index2(Request $request)
    {
        
    }


    public function mobileMoneyDebit(Request $request){
        $data = ['mno'=>'MTN','msisdn'=>'233241443624','amount'=>1];
   
        $this->loadw->makeCurlRequest($data);



        $expiresAt = Carbon::now()->addMinutes(1);
        // error_log(json_encode( $request->getContent() ));
        $request->session()->put('nsano-debit',$request->getContent());
        // Cache::put('new_mobile_money_debit', $request->getContent());
        return ['code' => '00', 'msg' => 'successful'];
    }

    public function mobileMoneyCredit(Request $request){
        $data = ['mno'=>'MTN','msisdn'=>'233241443624','amount'=>1];
   
       $va =  $this->loadw->creditCurlRequest($data);



        // $expiresAt = Carbon::now()->addMinutes(1);
        // error_log(json_encode( $request->getContent() ));
        // $request->session()->put('nsano-credit',$request->getContent());
        return ['code' => '00', 'msg' => $va];
    }


    public function mtn_load_wallet()
    {
        $amt = $this->request->input('amount');
        $phone = $this->request->input('gsm_phone');
        $mtn_mobile = $this->request->input('mtn_number');
        $mo = '+2330545247030';

        if ($amt && $phone && $mtn_mobile) {
            $wsdl = 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService?wsdl';

        $trace = true;
        $exceptions = true;
        
        $userClient = $this->user->where('phone', $phone)->first();

        $name = 'Global Success Minds';
        $info = 'Load wallet service';
        $mobile = '+233'.substr($mtn_mobile, 1);
        $mesg = 'Kindly find attached the invoice number {$inv}';
        $billprompt = '1';
        $thirdpartyID = '233549398254';
        $username = 'GSMLTD12';
        $password = '2i28U71fK'; 
            


        $xmlr = new \SimpleXMLElement("<postInvoice></postInvoice>");
        $xmlr->addChild('name', $name);
        $xmlr->addChild('info', $info);
        $xmlr->addChild('amt', $amt);
        $xmlr->addChild('mobile', $mobile);
        $xmlr->addChild('mesg', $mesg);
        $xmlr->addChild('billprompt', $billprompt);
        $xmlr->addChild('thirdpartyID', $thirdpartyID);
        $xmlr->addChild('username', $username);
        $xmlr->addChild('password', $password);

        //dd($xmlr);
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        try
        {
           $client = new \SoapClient($wsdl,array('location' => 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService.InvoicingServiceHttpSoap11Endpoint/','trace' => $trace, 'exceptions' => $exceptions, 'soap_version' => SOAP_1_2,'stream_context' => $context));
           $response = $client->postInvoice($xmlr);
          
        }
        catch (Exception $e)
        {
           echo "Error!";
           echo $e -> getMessage ();
           echo 'Last response: '. $client->__getLastResponse();
        }
         $v = ['result'=>$response->return];
          foreach ($v as $key => $value) {
            $b = (array)$value;
            break; 
          }
          $responseCode = $b['responseCode'];
          $responseMessage = $b['responseMessage'];
          $invoiceNo = $b['invoiceNo'];

          $this->mobile_wallet->mobile_wallet_id = Uuid::uuid4();
          $this->mobile_wallet->user_id = $userClient->user_id;
          $this->mobile_wallet->invoiceNo = $invoiceNo;
          $this->mobile_wallet->amount = $amt;
          $this->mobile_wallet->responseCode = $responseCode;
          $this->mobile_wallet->responseMessage = $responseMessage;

          if ($this->mobile_wallet->save()) {
            return ['success'=>true];
          }
        }else{
            return ['success'=>false];
        }
       


        //save transaction here into wallet loading transactions table 
    }

     

    // public function checkInvoice($invoiceId)
    // {   
    //     $wsdl = 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService?wsdl';
    //     $trace = true;
    //     $exceptions = true;

    //     $invoiceNo = $invoiceId;
    //     $username = 'GSMLTD12';
    //     $password = '2i28U71fK'; 

    //     $xmlr = new \SimpleXMLElement("<checkInvStatus></checkInvStatus>");
    //     $xmlr->addChild('invoiceNo', $invoiceNo);
    //     $xmlr->addChild('username', $username);
    //     $xmlr->addChild('password', $password);


    //     $context = stream_context_create([
    //         'ssl' => [
    //             // set some SSL/TLS specific options
    //             'verify_peer' => false,
    //             'verify_peer_name' => false,
    //             'allow_self_signed' => true
    //         ]
    //     ]);
    //     try
    //     {
    //        $client = new \SoapClient($wsdl,array('location' => 'http://68.169.57.64:8080/transflow_webclient/services/InvoicingService.InvoicingServiceHttpSoap11Endpoint/','trace' => $trace, 'exceptions' => $exceptions, 'soap_version' => SOAP_1_2,'stream_context' => $context));
    //        $response = $client->checkInvStatus($xmlr);
          
    //     }
    //     catch (Exception $e)
    //     {
    //        echo "Error!";
    //        echo $e -> getMessage ();
    //        echo 'Last response: '. $client->__getLastResponse();
    //     }

    //     //update transaction here in wallet loading transactions table and credit user less 1 cedi
        
    //       $v = ['result'=>$response->return];
    //       foreach ($v as $key => $value) {
    //         $b = (array)$value;
    //         break; 
    //       }
    //       $responseCode = $b['responseCode'];
    //       $responseMessage = $b['responseMessage'];
    //       if ($responseCode == '0000') {
    //         $trans = $this->mobile_wallet->where('invoiceNo',$invoiceNo)->first();
    //         $trans->responseMessage = $responseMessage;
    //         $trans->status = 'approved';

    //         if ($trans->save()) {
    //           return ['success'=>true];
    //         }
    //       }
    //      //return $b['responseCode'];
    //       return ['success'=>true];
       
    // }

    public function get_mtn_response(Request $request){
        Cache::put('mtn-response',$request->getContent(), 5);
    }

    public function invoice_history()
    {
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');

        $invoiceHistory = $this->mobile_wallet->with('user')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->orderBy('created_at','asc')->get();
        $invoiceHistorySum = $this->mobile_wallet->where('responseCode','0000')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->sum('amount');
        if ($invoiceHistory) {
            return ['status'=>true,'invoiceHistory'=>$invoiceHistory,'invoiceHistorySum'=>$invoiceHistorySum];
        }else{
            return ['status'=>false,'invoiceHistory'=>[],'invoiceHistorySum'=>0.0];
        }

    }

    public function invoice_history_gt()
    {
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');

        $invoiceHistory = $this->gt->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('status','!=','Admin_Cancelled')->orderBy('created_at','asc')->get();
        $invoiceHistorySum = $this->gt->where('statusCode',1)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->sum('amount');
        if ($invoiceHistory) {
            return ['status'=>true,'invoiceHistory'=>$invoiceHistory,'invoiceHistorySum'=>$invoiceHistorySum];
        }else{
            return ['status'=>false,'invoiceHistory'=>[],'invoiceHistorySum'=>0.0];
        }

    }

    public function invoice_history_gt_remove($id)
    {
         

        $invoiceRemove = $this->gt->where('clientref',$id)->first();
        if ($invoiceRemove) {
           $invoiceRemove->status = "Admin_Cancelled";
           if ($invoiceRemove->save()) {
              return ['status'=>true];
           }else{
             return ['status'=>false];
           }
        }
         
         

    }


    //regular tree user transfer
     public function regular_transfer(){
        $userA = $this->request->input('originPhone');
         $userB = $this->request->input('destinationPhone');
       $newSponsorId = $this->user->where('phone',$userB)->first();
       if ($newSponsorId) {
         $user = $this->user->where('phone',$userA)->first();
         if ($user) {
           $user->brought_by = $newSponsorId->user_id;
           if ($user->save()) {
              return ['status' => true, 'msg' => 'Transfer Complete'];
           }else{
              return ['status' => false, 'msg' => 'Transfer Unsuccessful'];
           }
         }else{
            return ['status' => false, 'msg' => 'User not found'];
         }
       }else{
          return ['status' => false, 'msg' => 'Sponsor not found'];
       }
        
    }

    //premium tree user transfer

     public function premium_transfer(){
        $userA = $this->request->input('originPhonePremium');
        $userB = $this->request->input('destinationPhonePremium');
       $newSponsorId = $this->user->where('phone',$userB)->first();
       if ($newSponsorId) {
        if ($newSponsorId->total_pre < 5) {
         $user = $this->user->where('phone',$userA)->first();
            if ($user) {
            
              $currentSponsor = $user->premium_referrer;
                $user->premium_referrer = $newSponsorId->user_id;
               if ($user->save()) {
                  $newSponsorId->total_pre = $newSponsorId->total_pre + 1;
                  $newSponsorId->save();
                  $cUser = $this->user->where('user_id',$currentSponsor)->first();
                  if ($cUser) {
                    $cUser->total_pre = $cUser->total_pre - 1; 
                  }
                  return ['status' => true, 'msg' => 'Premium Transfer Complete'];
               }else{
                  return ['status' => false, 'msg' => 'Premium Transfer Unsuccessful'];
               }
             }else{
                return ['status' => false, 'msg' => 'User not found'];
             }
          }else{
            
            return ['status' => false, 'msg' => 'Sponsor has exhausted direct referrals'];
          }
            
       }else{
          return ['status' => false, 'msg' => 'Sponsor not found'];
       }
        
    }
 

    //top up airtime backoffice
     public function topup_airtime(){
        $phone = $this->request->input('number');
        $amount = $this->request->input('amount');
        $network = $this->request->input('network');
        $pin = base64_encode($this->request->input('pin'));
       
       if ($this->ussd->validateUser(Auth::user()->phone,$pin)) {
          $userPhone = '233'.substr(Auth::user()->phone, 1);

          $result = $this->ussd->topUp($userPhone, $phone, $amount, $network);
         
            return ['status' => true, 'msg' => $result];
          
         
       }else{
          return ['status' => false, 'msg' => 'Incorrect Pin'];
       }
        
    }

    public function _gt_load_wallet()
    {   

        // $amt = $this->request->input('amount');
        $gsm_phone = $this->request->input('gsm_phone');
        $phone = $this->request->input('phone');
      
        $amount = $this->request->input('amount');
        $network = $this->request->input('network');
        
        $userClient = $this->user->where('phone', $gsm_phone)->first();
      
        $ReferenceID = rand();
        $transId = rand();
          $this->gt->gt_id = Uuid::uuid4();
          $this->gt->user_id = $userClient->user_id;
          $this->gt->clientRef = $ReferenceID;
          $this->gt->transRef = $transId;
          $this->gt->amount = $amount;
          $this->gt->senderPhone = $gsm_phone;
          $this->gt->senderName = Auth::user()->lastname." ".Auth::user()->firstname;
        
          if ($this->gt->save()) {

		
            $service = [
              "amount"=>$amount,
              "network"=>$network,
              "mobileNumber"=>$phone,
              "transId"=>$transId,
              "ReferecnceID"=>$ReferenceID,
              "remarks"=>"Mtn Mobile Money Test"
            ];
	         $mechantreference = rand();
            $data= [
                  "serviename"=>"initiatedebitwallet",
                  "merchantToken"=>"065CC989-FB9F-4CD5-A3E2-804CADEA7484",
                  "merchantKey"=>"9C83FB9F-8006-4350-867C-1F85A3175E5D",
                  "merchantreference"=>"525365",
                  "serviceDetails"=>json_encode($service)

            ];
        	//$ldata = json_encode($data);
        	var_dump($data);
                   
                   //sksj

        	$curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://196.216.228.23/apihubtest/api/myghpayextension/paymentRequest",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
            ));

         $response = curl_exec($curl);
          $err = curl_error($curl);

         curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            print_r(json_decode($response));
        }


             $response = Curl::to('https://196.216.228.23/apihubtest/api/myghpayextension/paymentRequest')
                        ->withData($data)
			->asJson()
                        ->post();
              var_dump($response);          
              if ($response->status == 1) {
                $t = $this->gt->where('transRef',$response->transactionId)->first();
                $result = (new GtTransaction)->creditMobileAccount($t->user_id,$t->amount);
                    if ($result) {
                         $message =new Notification;
                          $message->notification_id = Uuid::uuid4();
                          $message->user_id = $t->user_id;
                          $message->title = 'Self Load Wallet Topup';
                          $message->detail = "Your Account has been credited with ".$amountCredit." Ghana Cedis as Wallet topup";
                          $message->status="unread";
                          $message->save();
                       
                        
                     $this->info('Account credited successfully!');
                          return ['status'=>true,'message'=>"Account credited successfully!"];
                    }
                    return ['status'=>false,'message'=>"An Error occured"];
              }

             return redirect('/payment/confirmation')->with('data',$data);
            return ['status'=>false,'message'=>"An Error occured"];
           

          }
          else{
              return ['success'=>false];
          }
       



        
        //save transaction here into wallet loading transactions table 
         
       
    }



    //GT Bank api

     public function confirm_payment()
     {
          return view('dashboard.confirm-payment');

     }

    public function gt_load_wallet()
    {   

        // $amt = $this->request->input('amount');
        $phone = $this->request->input('gsm_phone');
       

        $clientid = "e716d2b3-c25d-430d-893f-c5d1ad1c77c7";
        $clientsecret = "e21db698-f406-410f-9b74-843955997770";
        $itemname = "GSM Wallet Load";
        $clientref = Uuid::uuid4();
        $amount = $this->request->input('amount');
        $returnurl = "https://globalsuccessminds.com/api/v1/gt_callback";
         // $returnurl = "http://localhost:8000/gt_callback";
        $baseurl = "https://myghpay.com/myghpayclient/";
        // $baseurl = "https:// 196.216.228.28/myghpayclient/";

        $userClient = $this->user->where('phone', $phone)->first();
        if ($userClient) {
          $this->gt->gt_id = Uuid::uuid4();
          $this->gt->user_id = $userClient->user_id;
          $this->gt->clientRef = $clientref;
          $this->gt->amount = $amount;
          $this->gt->senderName = Auth::user()->fullname;
          $this->gt->senderPhone = $phone;
        
          

          if ($this->gt->save()) {
            $data = [
                        'clientid' => $clientid,
                        'clientsecret' => $clientsecret,
                        'itemname' => $itemname,
                        'clientref' => $clientref,
                        'amount' => $amount,
                        'returnurl' => $returnurl,
                        'phone'=>$phone
                  ];

            return redirect('/payment/confirmation')->with('data',$data);
            // return ['status'=>true,'response'=>$response];
           

          }
          else{
              return ['success'=>false];
          }
        }
        else{
          return redirect()->back()->with('error', 'GSM Number Not found');
        }

          
       


        
        //save transaction here into wallet loading transactions table 
         
       
    }


    public function gt_callback(Request $request)
    {

        // $x = $request->getContent();
      return view('dashboard.gt-success');
             

        // return response($content, '200')->header('Content-Type', 'text/xml');
    }

    public function gt_callback_update(Request $request)
    {

        $clientref = $this->request->input('clientref');
        $statusCode = $this->request->input('statusCode');
        $refCode = $this->request->input('refCode');
        $paymentMode = $this->request->input('paymentMode');
        $Message = $this->request->input('Message');


        $client = $this->gt->where("clientRef",$clientref)->where('status','pending')->first();
        if ($client) {
          if ($statusCode == 1) {
            $client->status = "complete";
             $client->paymentMode = $paymentMode;
              $client->statusCode = $statusCode;
              $client->Message = $Message;
            $client->save();

            $result = (new GtTransaction)->creditMobileAccount($client->user_id,$client->amount);
              if ($result) {
                   $message =new Notification;
                    $message->notification_id = Uuid::uuid4();
                    $message->user_id = $client->user_id;
                    $message->title = 'Self Load Wallet Topup';
                    $message->detail = "Your Account has been credited with ".$client->amount." Ghana Cedis as Wallet topup";
                    $message->status="unread";
                    $message->save();
                 
                  
               // $this->info('Account credited successfully!');
                    return ['status'=>true,'message'=>"Account credited successfully!"];
              }
              return ['status'=>false,'message'=>"Error Occured "];
          }else{
              $client->status = "failed";
              $client->paymentMode = $paymentMode;
              $client->statusCode = $statusCode;
              $client->Message = $Message;
              $client->save();
            return ['status'=>false,'message'=>"Error Occured "];
          }
          
         


        }

        return ['status'=>false,'message'=>"Error Occured "];

        // return response($content, '200')->header('Content-Type', 'text/xml');
    }

    public function gt_direct_load_wallet(Request $request)
    {

        $amount = $this->request->input('amount');
        //$phone = $this->request->input('mtn_number');
        $gsm_phone = $this->request->input('gsm_phone');


        $currentUser = Auth::user();

        $user_id = $currentUser->user_id;
        
        
        $userClient = $this->user->where('phone', $gsm_phone)->first();
      
        $ReferenceID = rand();
        $transId = rand();

          $this->gt->gt_id = Uuid::uuid4();
          $this->gt->user_id = $userClient->user_id;
          $this->gt->clientRef = $ReferenceID;
          $this->gt->transRef = $transId;
          $this->gt->amount = $amount;
          $this->gt->mobileNumber = $currentUser->phone;
          $this->gt->senderPhone = $currentUser->phone;
          $this->gt->senderName =$currentUser->lastname." ".$currentUser->firstname;
        
          if ($this->gt->save()) {

    
            $service = [
              "amount"=>$amount,
              "network"=>"mtn",
              "mobileNumber"=>$phone,
              "transId"=>"208624143",
              "ReferecnceID"=>"523529364",
              "remarks"=>"Mtn Mobile Money Test"
            ];
           $mechantreference = rand();
          
            $data= [
                  "serviename"=>"initiatedebitwallet",
                  "merchantToken"=>"065CC989-FB9F-4CD5-A3E2-804CADEA7484",
                  "merchantKey"=>"9C83FB9F-8006-4350-867C-1F85A3175E5D",
                  "merchantreference"=>"525365",
                  "serviceDetails"=>json_encode($service)

            ];
          
          //var_dump($data);

          
          \GuzzleHttp\RequestOptions::JSON;
          $url = 'https://196.216.228.129/apihub/api/myghpayextension/paymentRequest';
          $client = new \GuzzleHttp\Client(['verify' => false]);
          // $client->setDefaultOption('verify', false);
          $res = $client->request('POST', $url, ['json'=>$data]); 
          

          $respdata = json_decode($res->getBody(), true);
        //  var_dump($respdata);
          $t = json_decode($respdata['Details'],true);
          //$t = (array) $t;
          //return $t["Status"];
          //return $respdata;

              $t = $this->gt->where('transRef',$transId)->first();

              $status = $respdata["Status"];
              if ($status == 1) {
                 
                  $result = (new GtTransaction)->creditMobileAccount($t->user_id,$t->amount);
                    if ($result) {
                      $amountCredit =$t->amount;
                      $message =new Notification;
                      $message->notification_id = Uuid::uuid4();
                      $message->user_id = $t->user_id;
                      $message->title = 'Self Load Wallet Topup';
                      $message->detail = "Your Account has been credited with ".$amountCredit." Ghana Cedis as Wallet topup";
                      $message->status="unread";
                      $message->save();


                      $t->status = "complete";
                      $t->Message = $t["Message"];
                      $t->save();

                          return ['status'=>true,'message'=>"Account credited successfully!"];
                    }
                    return ['status'=>false,'message'=>"An Error occured"];
              }else{
                      $t->status = "failed";
                      $t->Message = $t["Message"];
                      $t->save();
                      $amountCredit =$t->amount;
                      $message =new Notification;
                      $message->notification_id = Uuid::uuid4();
                      $message->user_id = $t->user_id;
                      $message->title = 'Self Load Wallet Topup Failure';
                      $message->detail = "Your Request to Credit your account with ".$amountCredit." Ghana Cedis as Wallet topup has failed";
                      $message->status="unread";
                      $message->save();
                       return ['status'=>false,'message'=>"Account credited Unsuccessful!"];
              }
       



        // return response($content, '200')->header('Content-Type', 'text/xml');
    }

  }

   public function nsano_credit_callback(Request $request){
       
        Log::info('NSANO Credit CALLBACK'.$request->getContent());
       return $request->all();
    }

    public function nsano_debit_callback(Request $request){
       
        Log::info('NSANO Debit CALLBACK'.$request->getContent());
       return $request->all();
    }




}

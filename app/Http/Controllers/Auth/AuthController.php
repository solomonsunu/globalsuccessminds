<?php
namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Uuid;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class AuthController extends Controller
{
   /*
   |--------------------------------------------------------------------------
   | Registration & Login Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles the registration of new users, as well as the
   | authentication of existing users. By default, this controller uses
   | a simple trait to add these behaviors. Why don't you explore it?
   |
   */


   use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    // private $redirectTo = '/';
   protected $redirectPath = '/dashboard';

   public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password, 'user_role'=>1])) {
            // Authentication passed...
            return redirect()->intended('dashboard/log');
        }elseif (Auth::attempt(['email' => $email, 'password' => $password, 'user_role'=>0])) {
            // Authentication passed...
            return redirect()->route('dashboard/allhistory');
        }
    }
   /**
    * Create a new authentication controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('guest', ['except' => 'getLogout']);
   }

   /**
    * Get a validator for an incoming registration request.
    *
    * @param  array  $data
    * @return \Illuminate\Contracts\Validation\Validator
    */
   protected function validator(array $data)
   {
       return Validator::make($data, [
          'title' => 'required|max:255',
           'firstname' => 'required|max:255',
           'lastname' => 'required|max:255',
           'gender' => 'required',
           'email' => 'required|email|max:255|unique:users',
           'password' => 'required|confirmed|min:6',
           'dob' => 'required|max:255',
          'phone' => 'required|digits:10',

       ]);
   }

   /**
    * Create a new user instance after a valid registration.
    *
    * @param  array  $data
    * @return User
    */
   protected function create(array $data)
   {
       $id = Uuid::uuid4();
       //dd($id);
       return User::create([
           'user_id' => $id->toString(),
           'department_id' => $data['department'],
          'title' => $data['title'],
           'firstname' => $data['firstname'],
           'lastname' => $data['lastname'],
           'othername' => $data['othername'],
           'gender' => $data['gender'],
           'email' => $data['email'],
           'address' => $data['address'],
           'phone' => $data['phone'],
           'dob' => $data['dob'],
           'user_role' => 1,
           'password' => bcrypt($data['password']),
       ]);
   }
}


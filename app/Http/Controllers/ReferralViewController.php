<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Account;
use App\Notification;
use Auth;
use Uuid;
use Session;
use Cache;
use App\Helpers\Insurance;
use App\Helpers\RankInsurance;
use App\Helpers\UpgradeCheck as Upgrade;
use App\Helpers\TotalTopUp as TopUp;
use Mail;
use App\InsuranceLog;
use App\Transaction;
use App\CompanyEarning;
use App\TopupEarning;
use App\Rank;
use Carbon\Carbon;
use DB;
use App\UserUpgrade;


/*
   active

*/




class ReferralViewController extends Controller
{

   public function __construct(CompanyEarning $earning,TopupEarning $earningTopUp,TopUp $topup,InsuranceLog $log, Account $account,Insurance $insurance,RankInsurance $rankInsurance,  User $user, Request $request, Notification $notification,Transaction $transaction, Rank $rank, Upgrade $upgrade, UserUpgrade $userUpgrade)
   {
        $this->user = $user;
        $this->request = $request;
        $this->notification = $notification;
        $this->insurance = $insurance;
        $this->log = $log;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->topup = $topup;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
        $this->rank = $rank;
        $this->upgrade = $upgrade;
        $this->userUpgrade = $userUpgrade;
        $this->rankInsurance = $rankInsurance;
   }


  public function viewLevel1(){
     if (Auth::check()){
          $search = Auth::user()->user_id;
          $total1 = 0.0;
          $level1 = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image', 'active','paymentstatus','email','phone')->where('brought_by','=',$search)->where('active','=',1)->where('agent','=',0)->get();
             
          $nextLevel = [];
          foreach ($level1 as $key => $value) {
            $total1 += $value->amt;
          }
           
           return ['status'=>true,'level1'=>$level1,'nextLevel'=>$nextLevel,'total1'=>$total1];
      }
  }
  public function viewLevel2(){
     if (Auth::check()){

        $upliner = $this->request->l2;
          // dd(json_decode($level2));
          $total2 = 0.0;
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level2 = [];
           foreach ($upliner as $key => $value1) {
               //var_dump($value1['user_id']);
              
             
             $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1['user_id'])->where('active','=',1)->where('agent','=',0)->get();
             if (count($dummy) > 0) {
                   array_push($level2, $dummy);                     
                }
             
             
           }
           $level = array_collapse($level2);
           return ['status'=>true,'level2'=>$level];
         }
           
      }
  }


public function viewLevel3(){
     if (Auth::check()){

        $upliner = $this->request->l3;
          // dd(json_decode($level2));
          
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level3 = [];
           foreach ($upliner as $key => $value1) {
               //var_dump($value1['user_id']);
              
             
             $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1['user_id'])->where('active','=',1)->where('agent','=',0)->get();
             if (count($dummy) > 0) {
                   array_push($level3, $dummy);                     
                }
             
             
           }
           $level = array_collapse($level3);
           return ['status'=>true,'level3'=>$level];
         }
           
      }
  }

  public function viewLevel4(){
     if (Auth::check()){

        $upliner = $this->request->l4;
          // dd(json_decode($level2));
          
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level4 = [];
           foreach ($upliner as $key => $value1) {
               //var_dump($value1['user_id']);
              
             
             $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1['user_id'])->where('active','=',1)->where('agent','=',0)->get();
             if (count($dummy) > 0) {
                   array_push($level4, $dummy);                     
                }
             
             
           }
           $level = array_collapse($level4);
           return ['status'=>true,'level4'=>$level];
         }
           
      }
  }

public function viewLevel5(){
     if (Auth::check()){

        $upliner = $this->request->l5;
          // dd(json_decode($level2));
          
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level5 = [];
           foreach ($upliner as $key => $value1) {
               //var_dump($value1['user_id']);
              
             
             $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1['user_id'])->where('active','=',1)->where('agent','=',0)->get();
             if (count($dummy) > 0) {
                   array_push($level5, $dummy);                     
                }
             
             
           }
           $level = array_collapse($level5);
           return ['status'=>true,'level5'=>$level];
         }
           
      }
  }

  public function viewLevel6(){
     if (Auth::check()){

        $upliner = $this->request->l6;
          // dd(json_decode($level2));
          
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level6 = [];
           foreach ($upliner as $key => $value1) {
               //var_dump($value1['user_id']);
              
             
             $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus','email','phone')->where('brought_by','=',$value1['user_id'])->where('active','=',1)->where('agent','=',0)->get();
             if (count($dummy) > 0) {
                   array_push($level6, $dummy);                     
                }
             
             
           }
           $level = array_collapse($level6);
           return ['status'=>true,'level6'=>$level];
         }
           
      }
  }

  

  //Premium referal tree
  public function viewPLevel1(){
     if (Auth::check()){
          $search = Auth::user()->user_id;
          $total1 = 0.0;
          $level1 = User::where('premium_referrer','=',$search)->where('active','=',1)->with('ranks')->get();
         
           return ['status'=>true,'level1'=>$level1];
      }
  }

  public function viewPLevel2(){
     if (Auth::check()){
      $upliner = $this->request->pl2;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level2 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('premium_referrer','=',$value1['user_id'])->where('active','=',1)->with('ranks')->get();
              if (count($dummy) > 0) {   
                      array_push($level2, $dummy);   
              }
           }
           $level = array_collapse($level2);
           return ['status'=>true,'level2'=>$level];
         }
           
      }
  }

  public function viewPLevel3(){
     if (Auth::check()){
      $upliner = $this->request->pl3;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level3 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('premium_referrer','=',$value1['user_id'])->where('active','=',1)->with('ranks')->get();
              if (count($dummy) > 0) {   
                      array_push($level3, $dummy);   
              }
           }
           $level = array_collapse($level3);
           return ['status'=>true,'level3'=>$level];
         }
           
      }
  }

  public function viewPLevel4(){
     if (Auth::check()){
      $upliner = $this->request->pl4;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level4 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('premium_referrer','=',$value1['user_id'])->where('active','=',1)->with('ranks')->get();
              if (count($dummy) > 0) {   
                      array_push($level4, $dummy);   
              }
           }
           $level = array_collapse($level4);
           return ['status'=>true,'level4'=>$level];
         }
           
      }
  }

  public function viewPLevel5(){
     if (Auth::check()){
      $upliner = $this->request->pl5;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level5 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('premium_referrer','=',$value1['user_id'])->where('active','=',1)->with('ranks')->get();
              if (count($dummy) > 0) {   
                      array_push($level5, $dummy);   
              }
           }
           $level = array_collapse($level5);
           return ['status'=>true,'level5'=>$level];
         }
           
      }
  }

  public function viewPLevel6(){
     if (Auth::check()){
      $upliner = $this->request->pl6;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level6 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('premium_referrer','=',$value1['user_id'])->where('active','=',1)->with('ranks')->get();
              if (count($dummy) > 0) {   
                      array_push($level6, $dummy);   
              }
           }
           $level = array_collapse($level6);
           return ['status'=>true,'level6'=>$level];
         }
           
      }
  }

    //Bagam referal tree
  public function viewBLevel1(){
     if (Auth::check()){
          $search = Auth::user()->user_id;
          $total1 = 0.0;
          $level1 = User::where('bagam_brought','=',$search)->where('active','=',1)->get();       
           return ['status'=>true,'level1'=>$level1];
      }
  }

  public function viewBLevel2(){
     if (Auth::check()){
      $upliner = $this->request->l2;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level2 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('bagam_brought','=',$value1['user_id'])->where('active','=',1)->get();
              if (count($dummy) > 0) {   
                      array_push($level2, $dummy);   
              }
           }
           $level = array_collapse($level2);
           return ['status'=>true,'level2'=>$level];
         }
           
      }
  }

  public function viewBLevel3(){
     if (Auth::check()){
      $upliner = $this->request->l3;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level3 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('bagam_brought','=',$value1['user_id'])->where('active','=',1)->get();
              if (count($dummy) > 0) {   
                      array_push($level3, $dummy);   
              }
           }
           $level = array_collapse($level3);
           return ['status'=>true,'level3'=>$level];
         }
           
      }
  }

  public function viewBLevel4(){
     if (Auth::check()){
      $upliner = $this->request->l4;
        
          if (count($upliner) > 0) {
           // $level3=new \Illuminate\Database\Eloquent\Collection();
           $level4 = [];
           foreach ($upliner as $key => $value1) {
              $dummy = User::where('bagam_brought','=',$value1['user_id'])->where('active','=',1)->get();
              if (count($dummy) > 0) {   
                      array_push($level4, $dummy);   
              }
           }
           $level = array_collapse($level4);
           return ['status'=>true,'level4'=>$level];
         }
           
      }
  }

   public function agent_referral(){
     if (Auth::check()){
          $search = Auth::user()->user_id;
          
          $ref = User::where('brought_by','=',$search)->where(['active'=>1,'agent'=>1])->with(['commisions'=>function($v)
          {
            $v->where(DB::raw("Month(created_at)"),date('m'))->where(DB::raw("Year(created_at)"),date('Y'));
          }])->get();
             
          
           
           return ['status'=>true,'ref'=>$ref];
      }
  }

}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SaveAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Http\Controllers\Controller;
use App\Account;
use App\User;
use App\Transaction;
use App\UserWithdrawal;
use App\NsanoCredit;
use Uuid;
use Auth;
use Mail;
use Session;
use Cache;
use App\Helpers\LoadWallet as LoadW;
use App\Notification;


class AccountController extends Controller
{
     public function __construct(Account $account, UserWithdrawal $withdrawal, User $user,Request $request, Transaction $transaction, LoadW $loadw,NsanoCredit $nsanoCredit,Notification $notification)
    {
        // $this->middleware('auth');
        $this->account = $account;
        $this->request = $request;
        $this->user = $user;
        $this->transaction = $transaction;
        $this->withdrawal = $withdrawal;
        $this->loadw = $loadw;
        $this->nsanoCredit = $nsanoCredit;
         $this->notification = $notification;
    }

    public function index($number,$network)
    {
        $data = ['mno'=>$network,'msisdn'=>$number,'amount'=>1];
    // return (new LoadWallet);
       return $this->loadw->makeCurlRequest($data);
    }
     public function credit()
    {
        // return $this->request;

        $amount = $this->request->input('amount');
        $number = $this->request->input('number');
        $gsm_number = $this->request->input('gsm_number');
        $network = strtoupper($this->request->input('network'));
        $refID = mt_rand(1000000,9999999);
        // return $gsm_number;
        $transfer = 1;
        $transaction_balance = 0.0;
        
        $user = $this->user->where('phone', $gsm_number)->first();

        if ($user) {
            // return $user;
            $account = $this->account->where('user_id', $user->user_id)->first();
             // return $account;
            if($account){
                if ( $account->current_balance > $amount) {

                    
                    $nsano_credit_id = Uuid::uuid4();    
                    $this->nsanoCredit->nsano_credit_id = $nsano_credit_id;
                    $this->nsanoCredit->user_id = $user->user_id;      
                    $this->nsanoCredit->amount = $amount;      
                    $this->nsanoCredit->number = $number;   
                    $this->nsanoCredit->network = $network; 
                    $this->nsanoCredit->refID = $refID;  

                    if ($this->nsanoCredit->save()) {
                        $number = '233'.substr($number, 1);
                        $data = ['mno'=>$network,'msisdn'=>$number,'amount'=>$amount,'refID'=>$refID];
                        
                        $resp =  $this->loadw->creditCurlRequest($data);
                        if ($resp) {
                            $resp = json_decode($resp,true);
                            $resp_msg = $resp['msg'];
                            $resp_reference=$resp['reference'];
                            $resp_code=$resp['code'];
                            $resp_system_code=$resp['system_code'];
                            $resp_transactionID=$resp['transactionID'];

                            $trans = $this->nsanoCredit->where('nsano_credit_id',$nsano_credit_id)->first();
                            if ($trans) {
                                $trans->resp_msg = $resp_msg;
                                $trans->resp_reference = $resp_reference;
                                $trans->resp_code = $resp_code;
                                $trans->resp_system_code = $resp_system_code;
                                $trans->resp_transactionID = $resp_transactionID;
                                if ($trans->save()) {
                                    $account->current_balance = $account->current_balance - $amount;
                                    $transaction_balance = $account->current_balance;
                                    
                                    if($account->save()){
                                        

                                        (new Transaction)->createTransaction([
                                            'account_id' => $account->account_id,
                                            'user_id'    => $user->user_id,
                                            'amount'     => $amount,
                                            'type'       => 'debit',
                                            'transfer'  =>  $transfer,
                                            'description'=>'Withdrawal Debit',
                                            'recipient' => $user->fullname,
                                            'balance'   => $transaction_balance
                                        ]);
                                           $message =new Notification;
                                            $message->notification_id = Uuid::uuid4();
                                            $message->user_id = $user->user_id;
                                            $message->title = 'Mobile Money Withdrawal';
                                            $message->detail = "You have successfully withdrawn an amount of  ".$amount." Ghana Cedis from your GSM wallet";
                                            $message->status="unread";
                                            $message->save();

                                        return ["success"=>false,"message"=> "A withdrawal amount of GHC $amount has been processed into your MOMO wallet on $number. "];
                                    }
                                }
                            }

                            
                        }else{
                            return ['success'=>false,'message'=>'Error Occured. Transaction Failed'];
                        }
                    }else{
                        return ['success'=>false,'message'=>'Error Occured. Transaction Failed'];
                    }  

                   
                }else{
                    return ['success'=>false,'message'=>'Please You have Insufficient funds'];
                }
           


        }
        }
        


      
    }


    public function saveAccount(SaveAccountRequest $req){

        $this->account->account_id = Uuid::uuid4();
        $this->account->accountname = $this->request->input('accountname');
        $this->account->accountnumber = $this->request->input('accountnumber');
        $this->account->bankname = $this->request->input('bankname');
        $this->account->branch = $this->request->input('branch');
        $this->account->user_id = Auth::user()->user_id;


        if($this->account->save()) {
            return ['status'=>true];
        }
        return ['status'=>false];

   }
    public function updateAccount(UpdateAccountRequest $req){
        $account = $this->account->where('user_id',Auth::user()->user_id)->first();
        if ($account) {
            $account->accountname = $this->request->input('accountname');
            $account->accountnumber = $this->request->input('accountnumber');
            $account->bankname = $this->request->input('bankname');
            $account->branch = $this->request->input('branch');

            if($account->save()) {
                return ['status'=>true];
            }
        }

        return ['status'=>false];

   }
    public function checkBalance($user_id)
    {
        $balance = $this->account->getCurrentBalance($user_id);
        return response()->json(compact('balance'));
    }
    public function creditUserAccount(Request $request)
    {
        if (Auth::check()) {
            $amount = $request->input('amount');
            $user_id = $request->input('user_id');

            $credit = $this->account->creditAccount($user_id, $amount);
            if($credit){
                $balance = $this->account->getCurrentBalance($user_id);
                return response()->json(['status'=>true, 'current_balance'=>$balance]);
            }
            return response()->json(['status'=>false]);
        }else{
            Auth::logout();
            return redirect('/');
        }
        
    }
    public function transferFunds(Request $request)
    {
        if (Auth::check()) {
            $sender_id = $request->input('sender_id');
            $recipient_id = $request->input('recipient_id');
            $amount = $request->input('amount');
            $recipient = $this->user->where('user_id',$recipient_id)->where('admin',0)->first();
            $recipient_name = $recipient->fullname;
            //dd($recipient_id);
            $withdraw = 1;
            if($this->account->getCurrentBalance($sender_id) >= $amount){
                if($this->account->debitAccount($sender_id, $amount,$withdraw,$recipient_name)){
                    $this->account->creditAccount($recipient_id, $amount);

                    return response()->json(['status' => true]);
                }
                return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
            return response()->json(['status'=>false, 'message' => 'Insufficient funds']);
        }else{
            Auth::logout();
            return redirect('/');
        }
        
    }

    public function userWithdrawal(Request $request)
    {
        $user_id = $request->input('user_id');
        $amount = $request->input('amount');
        $withdraw = 2;
        if($this->account->getCurrentBalance($user_id) >= $amount){
             if($this->account->debitAccount($user_id, $amount,$withdraw)){
                $withdrawal_id = Uuid::uuid4();
                $this->withdrawal->user_withdrawal_id = $withdrawal_id;
                $this->withdrawal->user_id = $user_id;
                $this->withdrawal->amount = $amount;
                if (!$request->input('mobile')) {
                    $this->withdrawal->phone_number =  $request->input('phone');
                }else{
                    $this->withdrawal->cash_pickup =  false;
                    $this->withdrawal->account_name =  $request->input('account_name');
                    $this->withdrawal->account_number =  $request->input('account_number');
                    $this->withdrawal->bank_name =  $request->input('bank_name');
                    $this->withdrawal->branch_name =  $request->input('branch_name');
                }
                if ($this->withdrawal->save()) {
                    return response()->json(['status'=>true]);
                }
            }


        }
        return response()->json(['status'=>false, 'message' => 'Insufficient funds']);
    }

     public function userTransferFunds(Request $request)
    {
         if (Auth::check()) {
            $sender_id = $request->input('sender_id');
            $recipient_phone = $request->input('phone');
            $amount = $request->input('amount');
            $recipient = $this->user->where('phone',$recipient_phone)->where('admin',0)->first();
            // dd($recipient->fullname);
            $recipient_name = $recipient->fullname;
             $withdraw = 1;
            if ($recipient) {
                if($this->account->getCurrentBalance($sender_id) >= $amount){
                    if($this->account->debitAccount($sender_id, $amount,$withdraw,$recipient_name)){
                        if ($this->account->creditAccount($recipient->user_id, $amount)) {
                            return response()->json(['status' => true]);
                        }          
                        $this->account->creditAccount($sender_id, $amount);
                        return response()->json(['status' => false,'message' => 'Unable to complete transaction']);
                    }
                    return response()->json(['status'=>false, 'message' => 'Unable to debit account']);
                }
                return response()->json(['status'=>false, 'message' => 'Insufficient funds']);
            }else{
                return response()->json(['status'=>false, 'message' => 'Error Occured']);
            }
        }else{
            Auth::logout();
            return redirect('/');
        }

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Rank;
use Uuid;
use Auth;

class RankController extends Controller
{
   public function __construct(Rank $rank, User $user,Request $request)
    {
        $this->rank = $rank;
        $this->user = $user;
        $this->request = $request;
    }
    public function createRank()
    {
    
        $this->rank->rank_id = Uuid::uuid4();
        $this->rank->rank_name = $this->request->input('rank_name');
        $this->rank->required_user_number = $this->request->input('required_user_num');
        $this->rank->required_airtime = $this->request->input('required_airtime');

        if($this->rank->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRank()
    {
        $ranks = $this->rank->get();
        if($ranks){
            //dd($user);
            return ['ranks' => $ranks];
        }
        return ['ranks' => false];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UploadsController extends Controller
{

    public function awsUpload(Request $request)
    {
        # code...
    }

    /**
     * Upload files to local dir
     *
     * @param $request Instance of Request
     * @return String
    */
    public function localUpload(Request $request)
    {
        $file = $request->file('file');

        if (!empty($file)) {
            $ext  = $file->getClientOriginalExtension();
            $dir = public_path().'/uploads/';
            $path = '/uploads/';

            $filename = uniqid();
            $uploadedfile = $filename.".{$ext}";            
            if (file_exists($dir.$uploadedfile)) {
                    $filename = uniqid();
                    $file->move($dir,$uploadedfile);
                
            }else{
                    $file->move($dir,$uploadedfile);
            }
            
                return ['imageUrl'=>$path.$uploadedfile];
            
        }
        return '';
    }
}

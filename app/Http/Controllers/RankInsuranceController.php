<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Http\Requests\StoreMessageRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Insurance;
use App\Notification;
use App\Rank;
use App\AccountDetail;
use App\Account;
use App\CompanyEarning;
use Auth;
use Uuid;
use Session;
use Cache;
use Excel;
use App\TopupEarning;
use App\Helpers\Insurance as ins;
use App\Helpers\ActivateAccount as activate;
use Carbon\Carbon;
use Mail;
use DB;
use Input;
use App\UserUpgrade;


class RankInsuranceController extends Controller
{
    
    public function __construct(TopupEarning $earningTopUp,User $user, Request $request, Insurance $insurance, ins $ins,  Notification $notification, AccountDetail $account, Account $userAccount, activate $activate, CompanyEarning $earning)
    {
        $this->user = $user;
        $this->request = $request;
        $this->insurance = $insurance;
        $this->ins = $ins;
        $this->notification = $notification;
        $this->account = $account;
        $this->userAccount = $userAccount;
        $this->activate = $activate;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
    }
    

        //Rank functions



    
    public function premium_notification()
    {

        if(Input::hasFile('import_file2')){
          $path = Input::file('import_file2')->getRealPath();
          $data = Excel::load($path, function($reader) {
          })->get();
          if(!empty($data) && $data->count()){
            
            foreach ($data as  $value) {

               $insert[] = ['user_id' => $value->id,'rank' => $value->rank,'rank_id' => $value->idrank];
             }

            if(!empty($insert)){
              //dd($insert);
              foreach ($insert as $value) {
                $user = User::where('user_id',$value['user_id'])->first();
                $rank = Rank::where('id',$value['rank_id'])->first();
                //dd($user);
                if ($rank && $user) {           
                    $user->rank_id = $rank->rank_id;
                    $user->save();
                    $message =new Notification;
                    $message->notification_id = Uuid::uuid4();
                    $message->user_id = $user->user_id;
                    $message->title = 'User Account Upgraded';
                    $message->detail = "You Have Succesfully Upgraded to ".$value['rank']." User Account";
                    $message->status="unread";
                    $message->save();

                    $upgrade =new UserUpgrade;
                    $upgrade->upgrade_id = Uuid::uuid4();
                    $upgrade->user_name = $user->fullname;
                    $upgrade->name = $user->username;
                    $upgrade->phone = $user->phone;
                    $upgrade->upgrade_name = $rank->rank_name;
                    $upgrade->save();
                 
                }
                

              }

             
             
            }
          }
        }

        return ['status'=>true];
        
      
    }
    
}

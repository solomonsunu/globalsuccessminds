<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Account;
use App\Notification;
use Auth;
use Uuid;
use Session;
use Cache;
use App\Helpers\Insurance;
use App\Helpers\RankInsurance;
use App\Helpers\UpgradeCheck as Upgrade;
use App\Helpers\TotalTopUp as TopUp;
use Mail;
use App\InsuranceLog;
use App\Transaction;
use App\CompanyEarning;
use App\TopupEarning;
use App\Rank;
use Carbon\Carbon;
use DB;
use App\UserUpgrade;


/*
   active

*/




class AdminController extends Controller
{

   public function __construct(CompanyEarning $earning,TopupEarning $earningTopUp,TopUp $topup,InsuranceLog $log, Account $account,Insurance $insurance,RankInsurance $rankInsurance,  User $user, Request $request, Notification $notification,Transaction $transaction, Rank $rank, Upgrade $upgrade, UserUpgrade $userUpgrade)
   {
        $this->user = $user;
        $this->request = $request;
        $this->notification = $notification;
        $this->insurance = $insurance;
        $this->log = $log;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->topup = $topup;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
        $this->rank = $rank;
        $this->upgrade = $upgrade;
        $this->userUpgrade = $userUpgrade;
        $this->rankInsurance = $rankInsurance;
   }
  

   public function getPayHistory($id){
    $getPayHistory = $this->user->with('accountdetail')->where('user_id',$id)->first();
    return ['payHistory'=>$getPayHistory];
   }
   public function get_active_user(){
    
    $activeUser = $this->user->where(['active'=>1,'admin'=>0])->with('account')->paginate(10);
    return response()->json(['status'=>true,'activeUser'=>$activeUser->toArray(),'paginate'=>$activeUser->toArray()]);
     // return $activeUser->toArray();
   }
  public function get_network_active_user($carrier){
  
    $activeUser = $this->user->where(['active'=>1,'admin'=>0,'carrier'=>$carrier])->with('account')->paginate(50);
     return $activeUser->toArray();
  }
  public function getUser(){
    // $count = 0;
    $account = Account::where('current_balance','!=',0)->sum('current_balance');
    // User::where(['active'=>1,'admin'=>0])->chunk(100,function ($users) use (&$count){
    //   foreach ($users as $user) { 
    //       $count = $count + $user->account->current_balance;
    //   }
    // });
     return ['totalBalance'=>$account];
     // unset($count);
  }



   public function manageduser($id){
     if (Auth::user()->admin == 1) {
       $allusers = $this->user->where(['active'=>1,'admin'=>0])->where('phone','like','%'.$id.'%')->take(5)->get();
        return ['manageUser'=>$allusers];
      
     }else{
      $manageUser = $this->user->where(['carrier'=>Auth::user()->carrier,'active'=>1,'admin'=>0])->where('phone','like','%'.$id.'%')->take(10)->get();
        return ['manageUser'=>$manageUser];
     }
        
      
   }
   public function managed_all_user($id){
     
        $allusers = $this->user->where(['active'=>1,'admin'=>0])->where('phone','like','%'.$id.'%')->orWhere('username','like','%'.$id.'%')->orWhere('firstname','like','%'.$id.'%')->orwhere('lastname','like','%'.$id.'%')->with('account')->take(5)->get();
        return ['allusers'=>$allusers];
      
   }

   public function currentUser(){
       $currentUser = $this->user->with(['account','ranks'])->where('user_id',Auth::user()->user_id)->first();
       $admin = $this->user->whereIn('admin',[2,3,4,5,6,7])->get();

       return ['currentUser' => $currentUser,'admin'=>$admin];
   }

   

   protected function users_with_pay($collection){
      $all_users = $this->user->with('account')->where(['active'=>1,'paymentstatus'=>1,'admin'=>0])->orderBy('id')->get();
      $amt = 0.0;
        foreach ($all_users as  $value) {
          foreach ($collection as $v => $va) {
            if($v == $value->user_id){
              $value->amt = $va;
              break;
            }
          }
        }
      return $all_users;
    }
    public function getPaymentNew()
    {  
      $payUser = $this->user->where(['paymentstatus'=>1,'admin'=>0])->paginate(1);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getTf($payUser),'paginate'=>$payUser->toArray()]);
    }

    public function getTotalRankUpgrade()
    {  
      $payUser = $this->user->where(['paymentstatus'=>1,'admin'=>0])->whereNotNull('premium_referrer')->with('ranks')->paginate(1);
      return $payUser;
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->rankInsurance->getTotalRank($payUser),'paginate'=>$payUser->toArray()]);
    }

   public function getPayment()
   {
      
       return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getNewTotals()]);


   }
    public function getPaymentPremium()
   {
      $payUser = $this->user->where(['admin'=>0,'paymentstatus'=>1])->whereNotNull('premium_referrer')->paginate(1);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount($payUser),'paginate'=>$payUser->toArray()]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }
   //interim fix
    public function getUserPremiumFix()
   {
     $payUser = $this->user->where(['admin'=>0])->whereNotNull('premium_referrer')->whereMonth('premium_update_date', '=', date('m'))->whereYear('premium_update_date', '=', date('Y'))->with(['ranks'])->paginate(10);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user'=>$payUser->toArray(),'paginate'=>$payUser->toArray()]);
   }

    public function getUserPremium()
   {
      $payUser = $this->user->where(['admin'=>0])->whereNotNull('premium_referrer')->whereMonth('premium_update_date', '=', date('m'))->whereYear('premium_update_date', '=', date('Y'))->with(['ranks'])->paginate(10);
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user'=>$payUser->toArray(),'paginate'=>$payUser->toArray()]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }
     public function getUserRanks()
   {
      $rankUser = $this->userUpgrade->where(['status'=>0])->get();
      // $insurance = $this->insurance->getTf($payUser);
      return response()->json(['status'=>true,'user_ranks'=>$rankUser]);
       // return response()->json(['status'=>true,'paymentUser'=>$this->insurance->getPremiumAmount()]);
   }

   public function ChangeUserRanksStatus($id)
   {
      $rankUser = $this->userUpgrade->where('upgrade_id',$id)->first();
      // dd($rankUser);
      $rankUser->status = 1;
      if ($rankUser->save()) {
        return ['status'=>true];
      }
      return ['status'=>false];
   }
   public function getProfit()
   {
       $earning = $this->earning->get();
       if ($earning) {
           return ['status'=>true,'earning'=>$earning];
       }
       return ['status'=>false,'earning'=>[]];

   }
   public function allUser()
   {
       $registerInsurance = $this->user->where('active',1)->where('admin',0)->orderBy('paymentstatus')->get();
       if ($registerInsurance) {
           return ['status'=>true,'registerInsurance'=>$registerInsurance];
       }
       return ['status'=>false];

   }
   public function getAdmin()
   {
       $adminList = $this->user->where('active',1)->where('admin','!=',0)->orderBy('admin')->get();
       if ($adminList) {
           return ['status'=>true,'adminList'=>$adminList];
       }
       return ['status'=>false];

   }
  
 public function activateRole(){
      $user_id = $this->request->input('user_id');
      //dd($user_id);
      $user = $this->user->where('user_id',$user_id)->first();
      $user->admin = $this->request->input('role_id');

      if($user->save()) {

               return ['status'=>true];
          }

     return ['status'=>false];

 }
   
public function removeUser(){
  $user_id = $this->request->input('user_id');
  $user = $this->user->where('user_id',$user_id)->first();
  if($user->delete()) {
      return ['status'=>true];
  }
  return['status'=>false];

}

  public function total_profit(){
    $profit = $this->earning->get();
    if ($profit) {
      return ['status'=>true, 'profit'=>$profit];
    }
      return['status'=>false];
  }

  public function totalInsurance(){
    $admins = $this->user->where('admin','!=',0)->where('admin','!=',1)->get();
    $dt = date('Y-m-d');
    $profit_topup = $this->earningTopUp->whereRaw('date(created_at) = ?', [$dt])->get();
    if ($admins) {
        foreach ($admins as  $value) {
            if ($value->admin == 2) {
               $admin1 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 3) {
               $admin2 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 4) {
               $admin3 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 5) {
               $admin4 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 6) {
               $admin5 = $this->topup->getTotals($value->carrier,$dt);
            }else{
               $admin6 = $this->topup->getTotals($value->carrier,$dt);
            }
        }
        return ['status'=>true, 'profit_topup'=>$profit_topup,'admin1'=>$admin1, 'admin2'=>$admin2, 'admin3'=>$admin3, 'admin4'=>$admin4,'admin5'=>$admin5,'admin6'=>$admin6];

    }

    return['status'=>false];

  }

  public function defActivity(){
    $admins = $this->user->where('admin','!=',0)->where('admin','!=',1)->get();
    $dt = $this->request->input('setDate');
    $profit_topup = $this->earningTopUp->whereRaw('date(created_at) = ?', [$dt])->get();
    if ($admins) {
        foreach ($admins as  $value) {
            if ($value->admin == 2) {
               $admin1 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 3) {
               $admin2 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 4) {
               $admin3 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 5) {
               $admin4 = $this->topup->getTotals($value->carrier,$dt);
            }elseif ($value->admin == 6) {
               $admin5 = $this->topup->getTotals($value->carrier,$dt);
            }else{
               $admin6 = $this->topup->getTotals($value->carrier,$dt);
            }
        }
        return ['status'=>true,'profit_topup'=>$profit_topup, 'admin1'=>$admin1, 'admin2'=>$admin2, 'admin3'=>$admin3, 'admin4'=>$admin4,'admin5'=>$admin5,'admin6'=>$admin6];

      }

      return['status'=>false];

 }

    
public function viewUser()
{
    return view('dashboard.view-user');
}
public function viewPremiumUser()
{
    return view('dashboard.view-premium-user');
}

public function levelUserDetail($id)
{
  $getPayment = $this->user->with('account')->where('user_id',$id)->first();
  $referrer = $this->user->with('account')->where('user_id',$getPayment->brought_by)->first();
  return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
}

    






  //premium bonus update
  public function change_bonus(){
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $newBonus = $this->request->input('newBonus');

     if ($user) {
        $user->total_bonus = $newBonus;
        if ($user->save()) {
          return ['status'=>true];
        }
          return ['message'=>'Bonus change failed'];
      }else{
          return ['message'=>'User not found'];
      }
      

  }



 public function get_premium_qualification()
    {  
      
      return response()->json(['status'=>true,'paymentUser'=>$this->rankInsurance->getTotalRankUser(Auth::user())]);
    }

}

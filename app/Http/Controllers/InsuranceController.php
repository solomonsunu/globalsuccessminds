<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Http\Requests\StoreMessageRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Insurance;
use App\Notification;
use App\Rank;
use App\AccountDetail;
use App\Account;
use App\CompanyEarning;
use Auth;
use Uuid;
use Session;
use Cache;
use Excel;
use App\TopupEarning;
use App\Helpers\Insurance as ins;
use App\Helpers\ActivateAccount as activate;
use Carbon\Carbon;
use Mail;
use DB;
use Input;
use App\UserUpgrade;


class InsuranceController extends Controller
{
    
    public function __construct(TopupEarning $earningTopUp,User $user, Request $request, Insurance $insurance, ins $ins,  Notification $notification, AccountDetail $account, Account $userAccount, activate $activate, CompanyEarning $earning)
    {
        $this->user = $user;
        $this->request = $request;
        $this->insurance = $insurance;
        $this->ins = $ins;
        $this->notification = $notification;
        $this->account = $account;
        $this->userAccount = $userAccount;
        $this->activate = $activate;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
    }
    

    public function pay_view()
    {
      return view('dashboard.admin.admin-manual');
    }
  public function pay_process()
    {

        if(Input::hasFile('import_file')){
          $path = Input::file('import_file')->getRealPath();
          $data = Excel::load($path, function($reader) {
          })->get();
          if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                // $insert[] = ['user_id' => $value->id,'username'=>$value->username, 'phone' => $value->phone,'sales'=>$value->ts,'charge'=>$value->ac,'amount'=>$value->tm];

                 $insert[] = ['user_id' => $value->id,'username'=>$value->username, 'phone' => $value->phone,'sales'=>$value->sales,'charge'=>$value->charge,'amount'=>$value->amount];
               
              }
            // foreach ($data as $key => $value) {
              
              
              
            // }
            // dd($insert);
            if(!empty($insert)){
                $totalEarning = 0.0;
                foreach ($insert as $value) {

                  $pay = $value["amount"];
                  
                  // $this->userAccount->creditAccount($value["user_id"], $pay);
                  //   $message =new Notification;
                  // $message->notification_id = Uuid::uuid4();
                  // $message->user_id = $value["user_id"];
                  // $message->title = 'Wrongful deduction of funds (October)';
                  // $message->detail = "You Have been Paid ".$pay." Ghana Cedis as refund for wrongful deduction made on your account on 5-11-2017. ";
                  // $message->status="unread";
                  // $message->save();
                  
                  $this->userAccount->creditAccount($value["user_id"], $pay);
                  $message =new Notification;
                  $message->notification_id = Uuid::uuid4();
                  $message->user_id = $value["user_id"];

                  $message->title = 'Team Sales Commision (February)';
                  $message->detail = "You Have been Paid ".$pay." Ghana Cedis as team sales commision payment";
                  $message->status="unread";
                  $message->save();
                  $totalEarning =  $totalEarning + $value["charge"];

                }

                $earning_id = Uuid::uuid4();
                $this->earning->company_earning_id = $earning_id;
                $this->earning->amount = $totalEarning;
                // $this->earning->month = date("F j, Y, g:i a");

                $this->earning->month = 'February, 2019';
                $this->earning->save();

                // User::where(['active'=>1,'paymentstatus'=>1,'admin'=> 0])->update(['paymentstatus' => 0]);
              }
            }
         }

       
        return ['status'=>true];
      
    }
     public function premium_process()
    {

        if(Input::hasFile('import_file1')){
          $path = Input::file('import_file1')->getRealPath();
          $data = Excel::load($path, function($reader) {
          })->get();
          if(!empty($data) && $data->count()){
            // foreach ($data as $key => $value) {
            //   foreach ($value as $key => $value) {
            //     $insert[] = ['user_id' => $value->id,'username'=>$value->username, 'phone' => $value->phone,'sales'=>$value->sales,'charge'=>$value->charge,'amount'=>$value->amount];
               
            //   }
              
              
            // }
            foreach ($data as $value) {
              // dd($value);
              $insert[] = ['user_id' => $value->id,'username'=>$value->username, 'phone' => $value->phone,'sales'=>$value->sales,'charge'=>$value->charge,'amount'=>$value->amount];
            }
            if(!empty($insert)){
              $totalEarning = 0.0;
              foreach ($insert as $value) {

                $pay = $value["amount"];
                $this->userAccount->creditAccount($value["user_id"], $pay);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = $value["user_id"];
                $message->title = 'Premium Upgrade/Team Commision (February)';
                $message->detail = "You Have been Paid ".$pay." Ghana Cedis as Premium team sales commision";
                $message->status="unread";
                $message->save();
                $totalEarning =  $totalEarning + $value["charge"];

                // $this->userAccount->deductCreditAccount($value["user_id"], $pay);
                //   $message =new Notification;
                //   $message->notification_id = Uuid::uuid4();
                //   $message->user_id = $value["user_id"];
                //   $message->title = 'February Double Premium Commision Payment Clawback';
                //   $message->detail = "You Have been deducted ".$pay." Ghana Cedis as clawback for double premium commision payment made on your account on 5-01-2019. ";
                //   $message->status="unread";
                //   $message->save();

              }

              $earning_id = Uuid::uuid4();
              $this->earning->company_earning_id = $earning_id;
              $this->earning->amount = $totalEarning;
              $this->earning->month = 'February, 2019';
              $this->earning->save();

              User::where(['admin'=>0])->whereNotNull('premium_referrer')->update(['total_bonus' => 0]);
             
            }
          }
        }

        return ['status'=>true];
        
      
    }


  public function agent_conmision_process()
    {

        if(Input::hasFile('import_file_comm')){
          $path = Input::file('import_file_comm')->getRealPath();
          $data = Excel::load($path, function($reader) {
          })->get();
          if(!empty($data) && $data->count()){
           
            foreach ($data as $value) {
              // dd($value);
              $insert[] = ['user_id' => $value->phone,'total'=>$value->total,'fname' => $value->fname,'lname'=>$value->lname];
            }
            if(!empty($insert)){
              $totalEarning = 0.0;
              foreach ($insert as $value) {
                $user = $this->user->where('phone',$value["user_id"])->first();
                if ($user) {
                   //calculate 0.5% of total sale amount
                  $comm = $value["total"]* 0.005;
                  $pay = $comm * 0.97;
                  $this->userAccount->creditAccount($user->user_id, $pay);
                  $name = $value["fname"]." ".$value["lname"];
                  $message =new Notification;
                  $message->notification_id = Uuid::uuid4();
                  $message->user_id = $user->user_id;
                  $message->title = 'Agent Referral Commision (February)';
                  $message->detail = "You Have been Paid ".$pay." Ghana Cedis as Agent Sales commision on Agent $name";
                  $message->status="unread";
                  $message->save();
                  $totalEarning =  $totalEarning + $comm * 0.03;
                }
               

              }

              $earning_id = Uuid::uuid4();
              $this->earning->company_earning_id = $earning_id;
              $this->earning->amount = $totalEarning;
              $this->earning->month = 'February, 2019';
              $this->earning->save();

              // User::where(['admin'=>0])->whereNotNull('premium_referrer')->update(['total_bonus' => 0]);
             
            }
          }
        }

        return ['status'=>true];
        
      
    }

 public function direct_conmision_process()
    {

        if(Input::hasFile('import_file_direct_comm')){
          $path = Input::file('import_file_direct_comm')->getRealPath();
          $data = Excel::load($path, function($reader) {
          })->get();
          if(!empty($data) && $data->count()){
           
            foreach ($data as $value) {
              //dd($value);
              $insert[] = ['user_id' => $value->id,'total'=>$value->total];
            }
            //dd($insert);
            if(!empty($insert)){
              $totalEarning = 0.0;
              foreach ($insert as $value) {
                //$user = DB::table('users')->where('username', $value['user_id'])->first();
                //$actualId = $this->user->where('username',$value["user_id"])->first();
               // dd( $value['user_id']);
                
                  $pay = $value["total"] * 0.97;
                  // $this->userAccount->deductCreditAccount($value["user_id"], $paydiff);

               	  $this->userAccount->creditAccount($value['user_id'], $pay);

                  $message =new Notification;
                  $message->notification_id = Uuid::uuid4();

                  $message->user_id = $value['user_id'];
                  $message->title = 'Instant Purchase Commision (February)';

                  $message->detail = "You Have been Paid ".$pay." Ghana Cedis as Instant Purchase commision.";
                  $message->status="unread";
                  $message->save();
                  //$totalEarning =  $totalEarning + $value["total"] * 0.03;
                
               

              }

             // $earning_id = Uuid::uuid4();
             // $this->earning->company_earning_id = $earning_id;
             // $this->earning->amount = $totalEarning;
             // $this->earning->month = 'February, 2019';
             // $this->earning->save();

              // User::where(['admin'=>0])->whereNotNull('premium_referrer')->update(['total_bonus' => 0]);
             
            }
          }
        }

        return ['status'=>true];
        
      
    }
    public function generate()
    {
        $ins = $this->request->input('list');
        if ($ins) {
            $path = public_path().'/excel/exports/';
            Excel::create('GSM-Payment-Sheet', function($excel) use($ins){
                 $excel->sheet('New sheet', function($sheet) use($ins){
                    $sheet->setStyle(array(
                        'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  12,
                    )
                    ));
                    $sheet->loadView('dashboard.payment-sheet')->with(['data'=>$ins]);

                 });

            })->store('xls', $path);
        }

    }

    public function generatePremium()
    {
        $ins = $this->request->input('listPremium');
        if ($ins) {
            $path = public_path().'/excel/exports/';
            Excel::create('GSM-Payment-Premium-Sheet', function($excel) use($ins){
                 $excel->sheet('New sheet', function($sheet) use($ins){
                    $sheet->setStyle(array(
                        'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  12,
                    )
                    ));
                    $sheet->loadView('dashboard.payment-sheet')->with(['data'=>$ins]);

                 });

            })->store('xls', $path);
        }



    }
    
    public function generateRank()
    {
        $ins = $this->request->input('listRank');
        if ($ins) {
            $path = public_path().'/excel/exports/';
            Excel::create('GSM-Rank-Sheet', function($excel) use($ins){
                 $excel->sheet('New sheet', function($sheet) use($ins){
                    $sheet->setStyle(array(
                        'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  12,
                    )
                    ));
                    $sheet->loadView('dashboard.rank-sheet')->with(['data'=>$ins]);

                 });

            })->store('xls', $path);
        }

    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\Account;
use Uuid;
use Auth;
use Excel;
use App\Notification;
use DateTime;
use DateInterval;
use DatePeriod;

class TransactionsController extends Controller
{

    public function __construct(Transaction $transaction, User $user, Account $account, Request $request, Notification $notification)
    {
        $this->transaction = $transaction;
        $this->user = $user;
        $this->account = $account;
        $this->request = $request;
        $this->notification = $notification;
      
    }
    public function store()
    {
        $user = $this->user->where('user_id',$this->request->input('user_id'))->first();
        $this->transaction->transaction_id = Uuid::uuid4();
        $this->transaction->account_id = $request->input('account_id');
        $this->transaction->user_id = $request->input('user_id');
        $this->transaction->amount = $request->input('amount');
        $this->transaction->type = $request->input('type');
        $this->transaction->description = $request->input('description');
        

        if($this->transaction->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
    public function show()
    {
        $user_id = $this->request->input('user_id');
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $monthCount = $this->nb_mois($start_date,$end_date);

        $account = $this->account->where('user_id',$user_id)->first();
        if ($account) {
            $user = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
                   
            // $user = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('id','desc')->take(30)->get();
            if ($user) { 
                return response()->json(['transactions' => $user]);
            }
            
        }
        return response()->json(['transactions' => false]);
        // $user = $this->user->with(['account'=>function($query){
        //             $query->with(['transactions'=>function ($value)
        //             {
        //                $value->with('users')->orderBy('id','desc');
        //             }])->orderBy('id','desc');
        //         }])->find($user_id);
        // if($user){
        //     //dd($user);
        //     return response()->json(['transactions' => $user->account->transactions]);
        // }
        // return response()->json(['transactions' => false]);
    }
        public function showLocalAdmin()
    {
        $user_id = $this->request->input('user_id');
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $monthCount = $this->nb_mois($start_date,$end_date);

        $account = $this->account->where('user_id',$user_id)->first();
        if ($account) {
            $user = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','desc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
                   
            if ($user) { 
                return response()->json(['transactions' => $user]);
            }
            
        }
        return response()->json(['transactions' => false]);
        
    }
    public function showLocal($user_id)
    {
        

        $account = $this->account->where('user_id',$user_id)->first();
        if ($account) {
            $user = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('id','desc')->take(30)->get();
                   
            // $user = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('id','desc')->take(30)->get();
            if ($user) { 
                return response()->json(['transactions' => $user]);
            }
            
        }
        return response()->json(['transactions' => false]);
        
    }
    public function adminShow($user_id)
    {

        

        $adminTrans = $this->transaction->where("user_id",$user_id)->with('users')->orderBy('id','desc')->take(100)->get();
        if($adminTrans){
            //dd($user);
            return response()->json(['transactions' => $adminTrans]);
        }
        return response()->json(['transactions' => false]);
    }

    public function adminShowWithDate($user_id)
    {

        //$user_id = $this->request->input('user_id');
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        //$monthCount = $this->nb_mois($start_date,$end_date);

            $adminTrans = $this->transaction->where("user_id",$user_id)->with('users')->orderBy('created_at','desc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();

            //var_dump($end_date);
                   
       if($adminTrans){
            //dd($user);
            return ['transactions' => $adminTrans];
        }
        //old

        // $adminTrans = $this->transaction->where("user_id",$user_id)->with('users')->orderBy('id','desc')->take(100)->get();
        // if($adminTrans){
        //     //dd($user);
        //     return response()->json(['transactions' => $adminTrans]);
        // }
        return response()->json(['transactions' => false]);
    }

     public function adminTransactionsWithDate()
    {
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $user = $this->transaction->with(['accounts'=>function($query){
                    $query->with('user');
                }])->where('user_id',Auth::user()->user_id)->orderBy('created_at','desc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->paginate(20);
        return $user->toArray();
    }

     public function adminTransactions()
    {
        $user = $this->transaction->with(['accounts'=>function($query){
                    $query->with('user');
                }])->where('user_id',Auth::user()->user_id)->orderBy('created_at','desc')->paginate(20);
        return $user->toArray();
        
    }

    protected function nb_mois($date1, $date2)
    {
        $begin = new DateTime( $date1 );
        $end = new DateTime( $date2 );
        $beginNew = $begin->format('Y-m-d');
        $endNew = $end->format('Y-m-d');

        // dd($result);
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $beginNew);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $endNew);
        $diff_in_months = $to->diffInMonths($from);
        
      

        return $diff_in_months;
    }
    public function get_tran_statement()
    {


        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $monthCount = $this->nb_mois($start_date,$end_date);
        // dd( $monthCount);
         
        if (Auth::check()){
            if ($monthCount <= 0) {
                return ['status'=>false,'message'=>'Incorrect Range Selected'];
            }
            if ($monthCount > 0 && $monthCount <= 3) {
                $amount = 0.5;
                $withdraw = 1;
                $recipient_name = 'GSM Administrative Account';
                if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                     $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                    $message =new Notification;
                    $message->notification_id = Uuid::uuid4();
                    $message->user_id = Auth::user()->user_id;
                    $message->title = 'Statement request charge';
                    $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                    $message->status="unread";
                    $message->save();

                    $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                    if ($account) {
                        $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
                        // $transaction->groupBy('type');
                        // dd($transaction->toArray());
                        if ($transaction) {
                            $path = public_path().'/excel/exports/';
                            $name = 'transaction_statement_'.Auth::user()->user_id;
                            Excel::create($name, function($excel) use($transaction){
                                $excel->setTitle('Account Statement');
                                $excel->setCreator('System')->setCompany('Global Success Minds');
                                $excel->setDescription('Transaction Statement');

                                 $excel->sheet('New sheet', function($sheet) use($transaction){
                                    $sheet->setStyle(array(
                                        'font' => array(
                                        'name'      =>  'Calibri',
                                        'size'      =>  12,
                                    )
                                    ));
                                    $sheet->loadView('dashboard.client.transaction-statement-sheet')->with('data',$transaction);

                                 });

                            })->store('xlsx', $path);

                            return['status'=>true,'message'=>'Statement Generated'];
                        }
                        
                    }
                }else{
                    return ['status'=>false, 'message'=>'Insufficient Funds'];
                }
           
        }
        if ($monthCount > 0 && $monthCount <= 12 && $monthCount > 3) {
            $amount = 1.0;
            $withdraw = 1;
            $recipient_name = 'GSM Administrative Account';
            if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                 $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = Auth::user()->user_id;
                $message->title = 'Statement request charge';
                $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                $message->status="unread";
                $message->save();

                 $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                if ($account) {
                    $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
                    // $transaction->groupBy('type');
                    // dd($transaction->toArray());
                    if ($transaction) {
                        $path = public_path().'/excel/exports/';
                        $name = 'transaction_statement_'.Auth::user()->user_id;
                        Excel::create($name, function($excel) use($transaction){
                            $excel->setTitle('Account Statement');
                            $excel->setCreator('System')->setCompany('Global Success Minds');
                            $excel->setDescription('Transaction Statement');

                             $excel->sheet('New sheet', function($sheet) use($transaction){
                                $sheet->setStyle(array(
                                    'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  12,
                                )
                                ));
                                $sheet->loadView('dashboard.client.transaction-statement-sheet')->with('data',$transaction);

                             });

                        })->store('xlsx', $path);

                        return['status'=>true,'message'=>'Statement Generated'];
                    }
                    
                }
            }else{
                return ['status'=>false, 'message'=>'Insufficient Funds'];
            }


        }
        if ($monthCount > 0 && $monthCount > 12) {
            $amount = 2.0;
            $withdraw = 1;
            $recipient_name = 'GSM Administrative Account';
           if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = Auth::user()->user_id;
                $message->title = 'Statement request charge';
                $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                $message->status="unread";
                $message->save();

                $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                if ($account) {
                    $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
                    // $transaction->groupBy('type');
                    // dd($transaction->toArray());
                    if ($transaction) {
                        $path = public_path().'/excel/exports/';
                        $name = 'transaction_statement_'.Auth::user()->user_id;
                        Excel::create($name, function($excel) use($transaction){
                            $excel->setTitle('Account Statement');
                            $excel->setCreator('System')->setCompany('Global Success Minds');
                            $excel->setDescription('Transaction Statement');

                             $excel->sheet('New sheet', function($sheet) use($transaction){
                                $sheet->setStyle(array(
                                    'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  12,
                                )
                                ));
                                $sheet->loadView('dashboard.client.transaction-statement-sheet')->with('data',$transaction);

                             });

                        })->store('xlsx', $path);

                        return['status'=>true,'message'=>'Statement Generated'];
                    }
                    
                }
            }else{
                return ['status'=>false, 'message'=>'Insufficient Funds'];
            }
        }
           
        
            
        }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
        }
    }
    public function get_month_tran_statement()
    {


        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $monthCount = $this->nb_mois($start_date,$end_date);
        // dd( $monthCount);
         
        if (Auth::check()){
            if ($monthCount <= 0) {
                return ['status'=>false,'message'=>'Incorrect Range Selected'];
            }
            if ($monthCount > 0 && $monthCount <= 3) {
            $amount = 0.5;
            $withdraw = 1;
            $recipient_name = 'GSM Administrative Account';
            if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                 $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = Auth::user()->user_id;
                $message->title = 'Statement request charge';
                $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                $message->status="unread";
                $message->save();

                $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                if ($account) {
                    $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
                 
                        $credit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->get();
                        $debit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->get();
                        $creditTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->sum('amount');
                        $debitTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->sum('amount');
                       
                        $path = public_path().'/excel/exports/';
                        $name = 'transaction_statement_'.Auth::user()->user_id;
                        Excel::create($name, function($excel) use($credit,$debit,$creditTotal,$debitTotal){
                            $excel->setTitle('Account Statement');
                            $excel->setCreator('System')->setCompany('Global Success Minds');
                            $excel->setDescription('Transaction Statement');

                             $excel->sheet('New sheet', function($sheet) use($credit,$debit,$creditTotal,$debitTotal){
                                $sheet->setStyle(array(
                                    'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  12,
                                )
                                ));
                                 // dd($credit);
                                $sheet->loadView('dashboard.client.month-transaction-statement-sheet')->with('dataCredit',$credit)->with('dataDebit',$debit)->with('creditTotal',$creditTotal)->with('debitTotal',$debitTotal);

                             });

                        })->store('xlsx', $path);

                        return['status'=>true,'message'=>'Statement Generated'];
                    
                    
                }
            }else{
                return ['status'=>false, 'message'=>'Insufficient Funds'];
            }
           
        }
        if ($monthCount >0 && $monthCount <= 12 && $monthCount > 3) {
            $amount = 1.5;
            $withdraw = 1;
            $recipient_name = 'GSM Administrative Account';
            if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                 $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = Auth::user()->user_id;
                $message->title = 'Statement request charge';
                $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                $message->status="unread";
                $message->save();

                 $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                if ($account) {
                    $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
                 
                        $credit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->get();
                        $debit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->get();
                        $creditTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->sum('amount');
                        $debitTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->sum('amount');
                       // dd($debit);
                        $path = public_path().'/excel/exports/';
                        $name = 'transaction_statement_'.Auth::user()->user_id;
                        Excel::create($name, function($excel) use($credit,$debit,$creditTotal,$debitTotal){
                            $excel->setTitle('Account Statement');
                            $excel->setCreator('System')->setCompany('Global Success Minds');
                            $excel->setDescription('Transaction Statement');

                             $excel->sheet('New sheet', function($sheet) use($credit,$debit,$creditTotal,$debitTotal){
                                $sheet->setStyle(array(
                                    'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  12,
                                )
                                ));
                                $sheet->loadView('dashboard.client.month-transaction-statement-sheet')->with('dataCredit',$credit)->with('dataDebit',$debit)->with('creditTotal',$creditTotal)->with('debitTotal',$debitTotal);

                             });

                        })->store('xlsx', $path);

                        return['status'=>true,'message'=>'Statement Generated'];
                    
                    
                }
            }else{
                return ['status'=>false, 'message'=>'Insufficient Funds'];
            }


        }
        if ($monthCount >0 &&  $monthCount > 12) {
            $amount = 3.0;
            $withdraw = 1;
            $recipient_name = 'GSM Administrative Account';
           if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                $this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name);

                $message =new Notification;
                $message->notification_id = Uuid::uuid4();
                $message->user_id = Auth::user()->user_id;
                $message->title = 'Statement request charge';
                $message->detail = "You Have been charged ".$amount." Ghana Cedis as statement request charge";
                $message->status="unread";
                $message->save();

               $account = $this->account->where('user_id',Auth::user()->user_id)->first();
                if ($account) {
                    $transaction = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
                 
                        $credit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->get();
                        $debit = $this->transaction->with('users')->where('account_id',$account->account_id)->orderBy('created_at','asc')->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->get();
                        $creditTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','credit')->sum('amount');
                        $debitTotal = $this->transaction->where('account_id',$account->account_id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->where('type','debit')->sum('amount');
                       // dd($debit);
                        $path = public_path().'/excel/exports/';
                        $name = 'transaction_statement_'.Auth::user()->user_id;
                        Excel::create($name, function($excel) use($credit,$debit,$creditTotal,$debitTotal){
                            $excel->setTitle('Account Statement');
                            $excel->setCreator('System')->setCompany('Global Success Minds');
                            $excel->setDescription('Transaction Statement');

                             $excel->sheet('New sheet', function($sheet) use($credit,$debit,$creditTotal,$debitTotal){
                                $sheet->setStyle(array(
                                    'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  12,
                                )
                                ));
                                $sheet->loadView('dashboard.client.month-transaction-statement-sheet')->with('dataCredit',$credit)->with('dataDebit',$debit)->with('creditTotal',$creditTotal)->with('debitTotal',$debitTotal);

                             });

                        })->store('xlsx', $path);

                        return['status'=>true,'message'=>'Statement Generated'];
                    
                    
                }
            }else{
                return ['status'=>false, 'message'=>'Insufficient Funds'];
            }
        }
           
        
            
        }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
        }
    }

    public function get_rank_tran_statement()
    {
        $data = $this->request->input('listRank');
         
        if (Auth::check()){
        
         
        $path = public_path().'/excel/exports/';
        $name = 'rank_statement_'.Auth::user()->user_id;
        Excel::create($name, function($excel) use($data){
            $excel->setTitle('Account Statement');
            $excel->setCreator('System')->setCompany('Global Success Minds');
            $excel->setDescription('Transaction Statement');

             $excel->sheet('New sheet', function($sheet) use($data){
                $sheet->setStyle(array(
                    'font' => array(
                    'name'      =>  'Calibri',
                    'size'      =>  12,
                )
                ));
                $sheet->loadView('dashboard.rank-attain-statement-sheet')->with(['data'=>$data]);

             });

        })->store('xlsx', $path);

        return['status'=>true,'message'=>'Statement Generated'];
                    
        
            
        }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
        }
    }
}

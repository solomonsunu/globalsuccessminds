<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Controllers\Controller;
use App\User;

use Auth;
use Uuid;
use Session;
use Cache;
;
use Mail;

use Carbon\Carbon;


/*
   active

*/




class DashController extends Controller
{

   public function __construct(User $user, Request $request)
   {
        $this->user = $user;
        $this->request = $request;
        
   }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
   */

    public function registerURL()
  {
      return view('auth.registerURL');
  }

   public function registerAgent()
  {
      return view('auth.registerAgent');
  }
  public function dashboard()
  {
      return view('dashboard.home');
  }
  public function loginPage()
  {
      return view('dashboard.auth.login');
  }

  public function superAdminUserManager()
 {
      return view('dashboard.admin.user-management');
 }
  public function superAdminUserRankManager()
 {
      return view('dashboard.admin.user-rank-check');
 }
 //cehck this
 public function adminUserManager()
 {
      return view('dashboard.admin.admin-user-management');
 }

 public function admin4AccountManager()
 {
      return view('dashboard.admin.admin4-account-management');

 }
 public function admin3AccountManager()
 {
      return view('dashboard.admin.admin3-account-manager');
 }
 public function admin2AccountManager()
 {
     return view('dashboard.admin.admin2-account-manager');
 }
 public function adminUserAccountManager()
 {
      return view('dashboard.admin.account-manager');
 }
 public function regularUserAccount()
 {
      return view('dashboard.client.regular-user-management');

 }
 public function summary()
 {
      return view('dashboard.admin.user-summary');

 }
 public function admin_payment()
 {
      return view('dashboard.admin.admin-payment');

 }
 public function admin_profit()
 {
      return view('dashboard.admin.admin-profit');

 }
  public function ranks()
 {
      return view('dashboard.admin.admin-ranks');

 }
  public function update_user()
 {
      return view('dashboard.update-user');

 }
  public function premium_upgrade()
 {
      return view('dashboard.admin.premium-upgrade');

 }
  public function bagam_upgrade()
 {
      return view('dashboard.admin.bagam-upgrade');

 }
 public function regularAccountManager()
 {
      return view('dashboard.client.regular-account-management');

 }
 public function adminRole()
 {
      return view('dashboard.admin.role-management');

 }
 public function adminMessage()
 {
      return view('dashboard.admin.admin-message');
 }
  public function test($val)
  {
  return $this->insurance->getTotal($val);
  }
  public function change_password(){
    return view('dashboard.change-password');
  }
  public function statement_request(){
    return view('dashboard.client.statement');
  }
  public function load_wallet(){
    return view('dashboard.client.load-wallet');
  }
  public function wallet_invoice(){
    return view('dashboard.admin.wallet-invoice');
  }
  public function movement_user(){
    return view('dashboard.admin.user-transfer');
  }

  public function buy_airtime(){
    return view('dashboard.client.buy-airtime');
  }
  
 public function viewUser()
{
    return view('dashboard.view-user');
}

}

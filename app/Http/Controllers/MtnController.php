<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Ussd;
use App\User;
use App\Account;
use Cache;
use Log;
use Carbon\Carbon;
use App\Helpers\LoadWallet as LoadW;

class UssdController extends Controller
{
    public function __construct(Ussd $ussd, User $user, Account $account,LoadW $loadw)
    {
        $this->ussd = $ussd;
        $this->user = $user;
        $this->account = $account;
        $this->loadw = $loadw;
    }
    /**
     * Process ussd request
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */
   

    public function mtn_response(Request $request){
       
       return json_encode( $request->getContent() );
        // error_log(json_encode( $request->getContent() ));
        $request->session()->put('nsano-debit',$request->getContent());
        // Cache::put('new_mobile_money_debit', $request->getContent());
        return ['code' => '00', 'msg' => 'successful'];
    }

    
}

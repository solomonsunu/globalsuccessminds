<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\StoreLinkUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Account;
use App\Notification;
use Auth;
use Uuid;
use Session;
use Cache;
use App\Helpers\Insurance;
use App\Helpers\RankInsurance;
use App\Helpers\UpgradeCheck as Upgrade;
use App\Helpers\TotalTopUp as TopUp;
use Mail;
use App\InsuranceLog;
use App\Transaction;
use App\CompanyEarning;
use App\TopupEarning;
use App\Rank;
use Carbon\Carbon;
use DB;
use App\UserUpgrade;


/*
   active

*/




class UserController extends Controller
{

   public function __construct(CompanyEarning $earning,TopupEarning $earningTopUp,TopUp $topup,InsuranceLog $log, Account $account,Insurance $insurance,RankInsurance $rankInsurance,  User $user, Request $request, Notification $notification,Transaction $transaction, Rank $rank, Upgrade $upgrade, UserUpgrade $userUpgrade)
   {
        $this->user = $user;
        $this->request = $request;
        $this->notification = $notification;
        $this->insurance = $insurance;
        $this->log = $log;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->topup = $topup;
        $this->earning = $earning;
        $this->earningTopUp = $earningTopUp;
        $this->rank = $rank;
        $this->upgrade = $upgrade;
        $this->userUpgrade = $userUpgrade;
        $this->rankInsurance = $rankInsurance;
   }

   public function save_password()
  {
    $old_pass = $this->request->input('old_password');
    $new_pass = $this->request->input('new_password');
    $new_pass1 = $this->request->input('new_password_repeat');
    $user_id = $this->request->input('user_id');
    if ($new_pass === $new_pass1) { 
        if (Auth::validate(['user_id'=>$user_id, 'password' => $old_pass])) {
            $user = $this->user->where('user_id',$user_id)->first();
            $user->password = bcrypt($new_pass);
            if ($user->save()) {
              return ['status'=>true,'message'=>"Password changed successfully"];
            }
              return ['status'=>false,'message'=>'Password change failed'];
        }else{

          return ['status'=>false,'message'=>'Wrong Password provided'];
        }

    }else{
      return ['status'=>false,'message'=>'Password do not match'];
    }
  }

  public function reset_password()
  {
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $new_pass = "gsmdefault";
    if ($user) {
      $user->password = bcrypt($new_pass);
      $user->pin = '0000';
            if ($user->save()) {
              return ['status'=>true,'Password/Pin change successful'];
            }
              return ['status'=>false,'message'=>'Password/Pin change failed'];

    }else{
        return ['status'=>false,'message'=>'User not found'];
    }

  }


   public function updateUser(UpdateUserRequest $req)
   {
      $user_id = $this->request->input('user_id');
      $choice = $this->request->input('choice');
      $choicePhone = $this->request->input('choicePhone');
      $user = $this->user->where('user_id',$user_id)->first();
      $user->firstname = $this->request->input('firstname');
      $user->lastname = $this->request->input('lastname');
      $user->email = $this->request->input('email');
      $user->dob = $this->request->input('dob');
      $user->gender = $this->request->input('gender');
      if ($choicePhone == 1) {
          $check = $this->user->where('phone',$this->request->input('phone'))->first();
          if (!$check) {
            $user->phone = $this->request->input('phone');
          } 
      }
      if ($choice == 1) {
        $user->password = bcrypt($this->request->input('password'));
      }
      if ($this->request->input('carrier')) {
        $user->carrier = $this->request->input('carrier');
      }
      if ($this->request->input('image') !== 0) {
        $user->profile_image = $this->request->input('image');
      }
      if($user->save()) {
          return ['status'=>true,'message'=>'User update successful'];
      }
      return ['status'=>false,'message'=>'User update failed'];
    }

  public function createUser(CreateUserRequest $req)
  {
    $user_id = Uuid::uuid4();
    $this->user->user_id = $user_id;
    $this->user->firstname = $this->request->input('firstname');
    $this->user->lastname = $this->request->input('lastname');
    $this->user->username = $this->request->input('username');
    $this->user->email = $this->request->input('email');
    $this->user->phone = $this->request->input('phone');
    $this->user->dob = $this->request->input('dob');
    $this->user->gender = $this->request->input('gender');
    $this->user->admin = $this->request->input('role');
    $this->user->brought_by =  Auth::user()->user_id;
    $this->user->password = bcrypt($this->request->input('password'));
    $this->user->carrier = $this->request->input('carrier');
    $this->user->active = 1;
    $country = $this->request->input('country');
    if ($country == 'GH') {
        $this->user->country = 'Ghana';
    }elseif ($country == 'NG') {
         $this->user->country = 'Nigeria';
    }else{
         $this->user->country = 'United States of Ameria';
    }

    if ($this->request->input('image') !== 0) {
       $this->user->profile_image = $this->request->input('image');
    }
    if($user->save()) {
         $user = $this->user->find($user_id);
             $data = array(
                    'name' => $user->firstname
            );

            Mail::send('emails.registration', $data, function ($message) use($user){

                $message->from('admin@globalsuccessminds.com', 'Account Registration');

                $message->to($user->email)->subject('Account Registration');

            });
        return ['status'=>true,'message'=>'Account Creation successful'];
    }
    return ['status'=>false,'message'=>'Account Creation failed'];

 }


   
    //perform login
public function login(UserLoginRequest $req)
{
    //return $this->request->all();
    $username = trim($this->request->input('username'));
    $password = trim($this->request->input('password'));

    $user = $this->user->where('username', $username)->orWhere('phone', $username)->first();
    if ($user) {
      if (is_null($user->referral_link)) {
        $user->generateLink();
      }
        if(Auth::attempt(['username' => $user->username,'password'=>$password])){
            return redirect('dashboard');
        }
    }
    return redirect()->back()->withInput()->with('error', 'Wrong username/password combination');
}

public function logout()
{
  Auth::logout();
  return redirect('/');
}

public function register(StoreUserRequest $req){
   $filepath = '';
    $referredUser = $this->user->whereUsername($this->request->input('brought_by'))->first();
    //dd($referredUser->toArray());
    if(empty($referredUser)) {
            return redirect('register')->with('error', 'Referred User Does Not Exist !');
    }elseif($referredUser->active == 0){
            return redirect('register')->with('error', 'Referred User is pending Activation !');
    }elseif($referredUser->agent == 1){
        return redirect('register')->with('error', 'Referred User Account is not allowed to refer new User !');
    }else{

        $user_id = Uuid::uuid4();
        $this->user->user_id = $user_id;
        $this->user->firstname = $this->request->input('firstname');
        $this->user->lastname = $this->request->input('lastname');
        $this->user->username = $this->request->input('username');
        $this->user->email = $this->request->input('email');
        $this->user->phone = $this->request->input('phone');
        $this->user->dob = $this->request->input('dob');
        $this->user->gender = $this->request->input('gender');
        $this->user->brought_by =  $referredUser->user_id;
        $this->user->password = bcrypt($this->request->input('password'));
        $this->user->carrier = $this->request->input('carrier');
        // $this->user->pin = $this->user->generatePin();
        $this->user->pin = '0000';
        $this->user->verified = true;
        $this->user->active = 1;

        $country = $this->request->input('country');

        if ($country == 'GH') {
            $this->user->country = 'Ghana';
        }elseif ($country == 'NG') {
             $this->user->country = 'Nigeria';
        }else{
             $this->user->country = 'United States of Ameria';
        }

        if ($this->user->save()) {
            $user = $this->user->find($user_id);

            $account_id = Uuid::uuid4();
            $this->account->account_id = $account_id;
            $this->account->user_id = $user->user_id;
            $this->account->current_balance = 0.0;
            $this->account->save();

            $data = array(
                'user' => $user
            );

            // Mail::send('emails.registration', $data, function ($message) use($user){
            //     $message->from('admin@globalsuccessminds.com', 'Account Registration');

            //     $message->to($user->email)->subject('Account Registration');

            // });
            return redirect('/login')->with('flash_notice', 'Registration Succesful');
        }
        return redirect()->back()->with('error', 'Registration Failed. Try again');
    }

}


public function registerUser(StoreLinkUserRequest $req){

   $filepath = '';
   $referral_link = $this->request->input('ref');
    $referredUser = $this->user->where('referral_link',$referral_link)->first();
    //dd($referredUser->toArray());
    if(empty($referredUser)) {
            return redirect('register')->with('error', 'Referred User Does Not Exist !');
    }elseif($referredUser->active == 0){
            return redirect('register')->with('error', 'Referred User is pending Activation !');
    }else{

        $user_id = Uuid::uuid4();
        $this->user->user_id = $user_id;
        $this->user->firstname = $this->request->input('firstname');
        $this->user->lastname = $this->request->input('lastname');
        $this->user->username = $this->request->input('username');
        $this->user->email = $this->request->input('email');
        $this->user->phone = $this->request->input('phone');
        $this->user->dob = $this->request->input('dob');
        $this->user->gender = $this->request->input('gender');
        $this->user->brought_by =  $referredUser->user_id;
        $this->user->password = bcrypt($this->request->input('password'));
        $this->user->carrier = $this->request->input('carrier');
        // $this->user->pin = $this->user->generatePin();
        $this->user->pin = '0000';
        $this->user->verified = true;
        $this->user->active = 1;

        $country = $this->request->input('country');

        if ($country == 'GH') {
            $this->user->country = 'Ghana';
        }elseif ($country == 'NG') {
             $this->user->country = 'Nigeria';
        }else{
             $this->user->country = 'United States of Ameria';
        }

        if ($this->user->save()) {
            $user = $this->user->find($user_id);

            $account_id = Uuid::uuid4();
            $this->account->account_id = $account_id;
            $this->account->user_id = $user->user_id;
            $this->account->current_balance = 0.0;
            $this->account->save();

            $data = array(
                'user' => $user
            );

          
            return redirect('/login')->with('flash_notice', 'Registration Succesful');
        }
        return redirect()->back()->with('error', 'Registration Failed. Try again');
    }

}

public function registerAgent(StoreLinkUserRequest $req){
   $filepath = '';
    $referredUser = $this->user->whereUsername($this->request->input('brought_by'))->first();
    //dd($referredUser->toArray());
    if(empty($referredUser)) {
            return redirect('register')->with('error', 'Referred User Does Not Exist !');
    }elseif($referredUser->active == 0){
            return redirect('register')->with('error', 'Referred User is pending Activation !');
    }else{

        $user_id = Uuid::uuid4();
        $this->user->user_id = $user_id;
        $this->user->firstname = $this->request->input('firstname');
        $this->user->lastname = $this->request->input('lastname');
        $this->user->username = $this->request->input('username');
        $this->user->email = $this->request->input('email');
        $this->user->phone = $this->request->input('phone');
        $this->user->dob = $this->request->input('dob');
        $this->user->gender = $this->request->input('gender');
        $this->user->brought_by =  $referredUser->user_id;
        $this->user->password = bcrypt($this->request->input('password'));
        $this->user->carrier = $this->request->input('carrier');
        $this->user->agent = 1;
        $this->user->pin = '0000';
        $this->user->verified = true;
        $this->user->active = 1;

        $country = $this->request->input('country');

        if ($country == 'GH') {
            $this->user->country = 'Ghana';
        }elseif ($country == 'NG') {
             $this->user->country = 'Nigeria';
        }else{
             $this->user->country = 'United States of Ameria';
        }

        if ($this->user->save()) {
            $user = $this->user->find($user_id);

            $account_id = Uuid::uuid4();
            $this->account->account_id = $account_id;
            $this->account->user_id = $user->user_id;
            $this->account->current_balance = 0.0;
            $this->account->save();

            $data = array(
                'user' => $user
            );

            // Mail::send('emails.registration', $data, function ($message) use($user){
            //     $message->from('admin@globalsuccessminds.com', 'Account Registration');

            //     $message->to($user->email)->subject('Account Registration');

            // });
            return redirect('/login')->with('flash_notice', 'Agent Registration Succesful');
        }
        return redirect()->back()->with('error', 'Agent Registration Failed. Try again');
    }

}

public function confirmEmail($token){
  $user = User::whereToken($token)->first();
  if ($user) {
      $user->confirmEmail();
      $account_id = Uuid::uuid4();
      $this->account->account_id = $account_id;
      $this->account->user_id = $user->user_id;
      $this->account->current_balance = 0.0;
      if ($this->account->save()) {
          return redirect('/login')->with('flash_notice', 'You are now confirmed. Please login');

      } 
  }
}


public function getPin($user_id)
{ 
    if($user = $this->user->find($user_id)){
        if(is_null($user->pin)){
            $pin = $this->user->generatePin();
            $user->pin = $pin;
            if($user->save()){
                return response()->json(['pin' => $pin, 'status'=>true]);
            }
        }
        return response()->json(['pin' => $user->pin, 'status'=>true]);
    }
    return response()->json(['status' => false]);
}

    
public function changePin(Request $request, $user_id){
  $oldpin = $request->input('old_pin');
  $newpin = $request->input('new_pin');
  $password = $request->input('password');

  $user = $this->user->where('user_id', $user_id)->where('pin', base64_encode($oldpin))->first();
  if($user){
      if(Auth::validate(['username'=>$user->username, 'password' => $password])){
          $user->pin = $newpin;
          if($user->save()){
              return response()->json(['status'=>true]);
          }
      }
      return response()->json(['status'=>false]);
  }
    
    return response()->json(['status'=>false, 'message'=>'Wrong details Old pin provided']);
}

public function currentUser(){
       $currentUser = $this->user->with(['account','ranks'])->where('user_id',Auth::user()->user_id)->first();
       $admin = $this->user->whereIn('admin',[2,3,4,5,6,7])->get();

       return ['currentUser' => $currentUser,'admin'=>$admin];
   }

  public function change_username(){
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $new_username = $this->request->input('username');
    $check = $this->user->where('username',$new_username)->first();
    if (!$check) {
      if ($user) {
        $user->username = $new_username;
              if ($user->save()) {
                return ['status'=>true];
              }
                return ['message'=>'Username change failed'];
      }else{
          return ['message'=>'User not found'];
      }
    }else{
      return ['message'=>'Username Already taken'];
    }
    

  }

  public function change_phonenumber(){
    $user_id = $this->request->input('user_id');
    $user = $this->user->where('user_id',$user_id)->first();
    $phone = $this->request->input('phone');
    $check = $this->user->where('phone',$phone)->first();
    if (!$check) {
      if ($user) {
        $user->phone = $phone;
              if ($user->save()) {
                return ['status'=>true];
              }
                return ['message'=>'Phone Number change failed'];
      }else{
          return ['message'=>'User not found'];
      }
    }else{
      return ['message'=>'Phone Number Already Exists'];
    }
    

  }

  //check for user referal shit
protected function checkUserReferral ($userId){
    $res = $this->upgrade->getAvailableUser($userId);
    if ($res) {
      foreach ($res as $key) {
          if (count($key) > 0) {
            foreach ($key as $value) {
              foreach ($value as $v) {
                if($v->total_pre < 5) {
                  return $v;
                }
              }
            }
          }
        }
      }else{
        return false;
    }
}

protected function checkBagamReferral ($userId){
    $res = $this->upgrade->getAvailableBagamUser($userId);
    if ($res) {
      foreach ($res as $key) {
          if (count($key) > 0) {
            foreach ($key as $value) {
              foreach ($value as $v) {
                if($v->bagam_direct_count < 5) {
                  return $v;
                }
              }
            }
          }
        }
      }else{
        return false;
    }
}

public function checkDirectRC($id){
  $total = User::where('premium_referrer',$id)->count();
  return ['total'=>$total];
}



public function userPremiumUpgrade(Request $request){
  $username = $request->input('username');
  $balance = $request->input('wbalance');
  $user = $this->user->where('username',$username)->first();
  $rank = $this->rank->first();
  $owner = $this->user->where('user_id',Auth::user()->user_id)->first();
  $withdraw = 3; //indicatiing transfer
  $amount = 45.00;
  $recipient_name = 'GSM';
  $bonus = 7.00;
  if ($user) {
     if($user->premium_referrer ){
        if ($user->total_pre < 5) {
          if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Premium User Account";
                        $message->status="unread";
                        $message->save();
                        $owner->premium_referrer = $user->user_id;
                        $owner->rank_id = $rank->rank_id;
                        $owner->total_pre = 0;
                        $owner->premium_update_date = date('Y-m-d');

                        if ($owner->save()) {
                          $user->total_pre = $user->total_pre + 1;
                          $user->total_bonus = $user->total_bonus + $bonus;
                            if ($user->save()) {
                            return response()->json(['status' => true,'message' => $user]);
                          }
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
          return response()->json(['status'=>false,'message'=>'Insufficient Funds for upgrade']);
        }else{
          $res = $this->checkUserReferral($user->user_id);
          if ($res) {
             if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Premium User Account";
                        $message->status="unread";
                        $message->save();
                       
                        $owner->premium_referrer = $res->user_id;
                        $owner->rank_id = $rank->rank_id;
                        $owner->total_pre = 0;
                        $owner->premium_update_date = date('Y-m-d');
                        if ($owner->save()) {
                          $user->total_bonus = $user->total_bonus + $bonus;
                          if ($user->save()) {
                            $res->total_pre = $res->total_pre + 1;
                            if ($res->save()) {
                              return response()->json(['status' => true,'message' => $res]);
                            }
                            return response()->json(['status'=>false, 'message' => 'something went wrong']);
                          }                   
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
            
          }else{
             return response()->json(['status'=>false, 'message'=>'Error. Try another user']);
          }
         
        }
      }
        
        return response()->json(['status'=>false, 'message'=>'Selected Sponsor is not a Premium User']);
    }
      return response()->json(['status'=>false, 'message'=>'Selected Sponsor does not exist']);
  }


public function userBagamUpgrade(Request $request){
  $username = $request->input('username');
  $balance = $request->input('wbalance');
  $user = $this->user->where('username',$username)->first();
  $rank = $this->rank->first();
  $owner = $this->user->where('user_id',Auth::user()->user_id)->first();
  $withdraw = 3; //indicatiing transfer
  $amount = 100.00;
  $recipient_name = 'GSM';
 
  if ($user) {
     if($user->bagam ){
        if ($user->bagam_direct_count < 5) {
          if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Bagam Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Bagam User Account";
                        $message->status="unread";
                        $message->save();
                        $owner->bagam_brought = $user->user_id;
                        $owner->bagam = 1;
                        $owner->bagam_date = date('Y-m-d');

                        if ($owner->save()) {
                          $user->bagam_direct_count = $user->bagam_direct_count + 1;
                          
                            if ($user->save()) {
                            return response()->json(['status' => true,'message' => $user]);
                          }
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
          return response()->json(['status'=>false,'message'=>'Insufficient Funds for upgrade']);
        }else{
          $res = $this->checkBagamReferral($user->user_id);
          if ($res) {
             if($this->account->getCurrentBalance(Auth::user()->user_id) >= $amount){
                    if($this->account->debitAccount(Auth::user()->user_id, $amount,$withdraw,$recipient_name)){
                        $message =new Notification;
                        $message->notification_id = Uuid::uuid4();
                        $message->user_id = $owner->user_id;
                        $message->title = 'User Bagam Account Upgraded';
                        $message->detail = "You Have Succesfully Upgraded to Bagam User Account";
                        $message->status="unread";
                        $message->save();
                       
                        $owner->bagam_brought = $res->user_id;
                       
                        $owner->bagam_date = date('Y-m-d');
                        if ($owner->save()) {
                        
                          if ($user->save()) {
                            $res->bagam_direct_count = $res->bagam_direct_count + 1;
                            if ($res->save()) {
                              return response()->json(['status' => true,'message' => $res]);
                            }
                            return response()->json(['status'=>false, 'message' => 'something went wrong']);
                          }                   
                          return response()->json(['status'=>false, 'message' => 'something went wrong']);
                        }
                        return response()->json(['status'=>false, 'message' => 'something went wrong']);
                    }
                    return response()->json(['status'=>false, 'message' => 'unable to debit account']);
            }
            
          }else{
             return response()->json(['status'=>false, 'message'=>'Error. Try another user']);
          }
         
        }
      }
        
        return response()->json(['status'=>false, 'message'=>'Selected Sponsor is not a Premium User']);
    }
      return response()->json(['status'=>false, 'message'=>'Selected Sponsor does not exist']);
  }


//for now
  public function levelUser($id)
{
    $getPayment = $this->user->with('account')->where('user_id',$id)->first();
    $referrer = $this->user->with('account')->where('user_id',$getPayment->brought_by)->first();
    $level1 = User::where('brought_by','=',$getPayment->user_id)->where('active','=',1)->get();
    $total1 = 0.0;
    $total2 = 0.0;
    $total3 = 0.0;
    $total4 = 0.0;
    $total5 = 0.0;
    $total6 = 0.0;

          //read l1 from cache
      $level1 = Cache::get($id.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image', 'active','paymentstatus')->where('brought_by','=',$id)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }
      
      
      //read l2
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }
             
            $balance = 0.0;
            
            $account = $this->account->where('user_id',$value->user_id)->first();
             //dd($account);
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value->amt = $balance;
               $total1 = $total1 + $balance;

               if (count($dummy) > 0) {
                  // $level2->add($dummy); 
                  array_push($level2, $dummy);   
               }
            }
         }

         $level2 = array_collapse($level2);
         // dd($level2);
        
         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
            $balance = 0.0;
            $account = $this->account->where('user_id',$value1->user_id)->first();
            if ($account) {
               $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
               $balance = $transaction;
               $value1->amt = $balance;
               $total2 = $total2 + $balance;
                  if (count($dummy) > 0) {
                     array_push($level3, $dummy);                     
                  }
     
            }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
               $balance = 0.0;
               $account = $this->account->where('user_id',$value1->user_id)->first();
                 //dd($account);
               if ($account) {
                  $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                  $balance = $transaction;
                  $value1->amt = $balance;
                  $total3 = $total3 + $balance;
                  if (count($dummy) > 0) {                
                     array_push($level4, $dummy);
                     
                  }
                   
               }      
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  $balance = 0.0;
                  $account = $this->account->where('user_id',$value1->user_id)->first();
                  //dd($account);
                  if ($account) {
                     $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                   
                     $balance = $transaction;
                     $value1->amt = $balance;
                     $total4 = $total4 + $balance;
                     if (count($dummy)> 0) {
                        array_push($level5, $dummy);
                     } 
                  
                  }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     $balance = 0.0;
                     $account = $this->account->where('user_id',$value1->user_id)->first();
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
                
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total5 = $total5 + $balance;
                        if (count($dummy)> 0) {
                           array_push($level6, $dummy);
                        }
                     }
                  }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::select('user_id', 'username', 'firstname', 'lastname', 'profile_image','active','paymentstatus')->where('brought_by','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if ($account) {
                        $transaction = $this->transaction->where('account_id',$account->account_id)->where('type','debit')->where('transfer',0)->where('created_at', '>=', Carbon::now()->startOfMonth())->where(DB::raw("Year(created_at)"),date('Y'))->sum('amount');
             
                        $balance = $transaction;
                        $value1->amt = $balance;
                        $total6 = $total6 + $balance;    
                     }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
          }
      }else{
         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
      }

   }else{
      return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
   }
    
}
public function levelPremiumUser($id){
   if (Auth::check()){

      $search = $id;
      $getPayment = $this->user->with('account')->where('user_id',$id)->first();
      $referrer = $this->user->with('account')->where('user_id',$getPayment->premium_referrer)->first();
      //dd();
      $total1 = 0.0;
      $total2 = 0.0;
      $total3 = 0.0;
      $total4 = 0.0;
      $total5 = 0.0;
      $total6 = 0.0;

      //read l1 from cache
      $level1 = Cache::get($search.'_tree_l1');

      if(!$level1){
         // error_log('no cache');
         $level1 = User::where('premium_referrer','=',$search)->where('active','=',1)->get();
         // Cache::forever($search.'_tree_l1', $level1);
      }     
      //read l2
      
      if (count($level1) > 0) {
         //$level2 = new \Illuminate\Database\Eloquent\Collection();
         $level2 = [];
         foreach ($level1 as $value) {
            $dummy = Cache::get($value->user_id.'_tree_l1');
            if(!$dummy){
               $dummy = User::where('premium_referrer','=',$value->user_id)->where('active','=',1)->get(); 
               // Cache::forever($value->user_id.'_tree_l1', $dummy);
            }

            if (User::where('user_id',$value->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total1 = $total1 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level2, $dummy);   
                 }
         }
         
         $level2 = array_collapse($level2);

         // read l3
         if (count($level2) > 0) {
         // $level3=new \Illuminate\Database\Eloquent\Collection();
         $level3 = [];
         foreach ($level2 as $key => $value1) {
            $dummy = Cache::get($value1->user_id.'_tree_l1');
            if (!$dummy) {
               $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
               // Cache::forever($value1->user_id.'_tree_l1', $dummy);
            }
            
             if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total2 = $total2 + 1;
                  
            }
            if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level3, $dummy);   
                 }
         }
         $level3 = array_collapse($level3);

         if (count($level3) > 0) {
            $level4=[];//new \Illuminate\Database\Eloquent\Collection();

            foreach ($level3 as $key => $value1) {
               $dummy = Cache::get($value1->user_id.'_tree_l1');
               if (!$dummy) {
                  $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                  // Cache::forever($value1->user_id.'_tree_l1', $dummy);
               }
              if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total3 = $total3 + 1;
                  
              }  
              if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level4, $dummy);   
                 }    
            }
            $level4 = array_collapse($level4, $dummy);

            if (count($level4) > 0) {
               $level5=[];//new \Illuminate\Database\Eloquent\Collection();
               foreach ($level4 as $key => $value1) {
                  $dummy = Cache::get($value1->user_id.'_tree_l1');
                  if (!$dummy) {
                     $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                     // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                  }
                  if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                  $total4 = $total4 + 1;
                  
                  }
                  if (count($dummy) > 0) {
                    // $level2->add($dummy); 
                    array_push($level5, $dummy);   
                 }
               }
               $level5 = array_collapse($level5);

               if (count($level5) > 0) {
                  $level6=[];//new \Illuminate\Database\Eloquent\Collection();
                  foreach ($level5 as $key => $value1) {
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                     if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                        $total5 = $total5 + 1;
                        
                      }
                      if (count($dummy) > 0) {
                              // $level2->add($dummy); 
                              array_push($level6, $dummy);   
                           }
                      }
                  $level6 = array_collapse($level6);


                  if (count($level6) > 0) {
                  foreach ($level6 as $key => $value1) {
                     $balance = 0.0;
                     $dummy = Cache::get($value1->user_id.'_tree_l1');
                     if (!$dummy) {
                        $dummy = User::where('premium_referrer','=',$value1->user_id)->where('active','=',1)->get();
                        // Cache::forever($value1->user_id.'_tree_l1', $dummy);
                     }
                    if (User::where('user_id',$value1->user_id)->whereMonth('premium_update_date', '=', date('m'))->whereYear("premium_update_date","=",date('Y'))->first()) {
                          $total6 = $total6 + 1;
                         
                    }
                     
                  }
                }

                  if (count($level6) > 0) {
                     // dd($level2);
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>$level6,'t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                  else{
                      return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>$level5,'level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                  }
                }
                else{
                    return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>$level4,'level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
                }
              }else{
                  return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>$level3,'level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
              }
          }else{
            return ['status'=>true,'level1'=>$level1,'level2'=>$level2,'level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
          }
      }else{

         return ['status'=>true,'level1'=>$level1,'level2'=>'','level3'=>'','level4'=>'','level5'=>'','level6'=>'','t1'=>$total1,'t2'=>$total2,'t3'=>$total3,'t4'=>$total4,'t5'=>$total5,'t6'=>$total6,'userDetails'=>$getPayment,'referrer'=>$referrer];
      }

   }else{
      return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
   }
   }else{
          return redirect('/login')->with('flash_notice', 'Your Session has expired');
   }
   //return View::make('dashboard.viewrefferals')->with('message',$message);

}

public function levelUserDetail($id)
{
  $getPayment = $this->user->with('account')->where('user_id',$id)->first();
  $referrer = $this->user->with('account')->where('user_id',$getPayment->brought_by)->first();
  return ['status'=>true,'userDetails'=>$getPayment,'referrer'=>$referrer];
}

public function total_active_inactive()
{ 
    $active = $this->user->where('paymentstatus',1)->count();
    $inactive = $this->user->where('paymentstatus',0)->count();
       
    return response()->json(['active' => $active,'inactive'=>$inactive, 'status'=>true]);
    
   
}


}

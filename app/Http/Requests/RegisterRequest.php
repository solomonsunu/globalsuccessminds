<?php
namespace App\Http\Requests;
use App\Http\Requests\Request;
class RegisterRequest extends Request
{
  public function authorize()
  {
    return true;
  }
  public function rules()
  {
    return [
        'firstname' => 'required',
        'lastname' => 'required',
        'username' => 'required|unique:users,username',
        'password' => 'required',
        'password_confirmation' => 'required|same:password',
        'email' => 'required|email|unique:users,email',
        'country' => 'required',
        'dob' => 'required',
        'phone' => 'required|unique:users,phone',
         'carrier' => 'required'
    ];
  }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cycledetail extends Model
{
    protected $table = 'cycle_datails';
    protected $primaryKey = 'cycle_detail_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['department_id','school_id','department_name'];
    public function cycle()
    {
        return $this->belongsTo('App\Cycle','cycle_id');
    }
     
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceLog extends Model
{
    protected $table = 'insurance_logs';
    protected $primaryKey = 'insurance_log_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['company_id','company_name','company_email','company_number','company_address'];

   
     public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}

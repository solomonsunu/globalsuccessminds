angular.module('asemApp.directives', [])
.directive('slimscroll', function () {
  	'use strict';
  	return {
	    restrict: 'A',
	    link: function ($scope, $elem, $attr) {
		      var off = [];
		      var option = {};
		      
		      var refresh = function () {		
		         if ($attr.slimscroll) {		         
		           option = $scope.$eval($attr.slimscroll);		           
		         } else if ($attr.slimscrollOption) {		        
		           option = $scope.$eval($attr.slimscrollOption);		           
		         }
		         
		        $($elem).slimScroll({ destroy: true });		        

		         $($elem).slimScroll(option);		         
		      };
		      
		      var registerWatch = function () {
		        if ($attr.slimscroll && !option.noWatch) {
		          off.push($scope.$watchCollection($attr.slimscroll, refresh));
		        }

		        if ($attr.slimscrollWatch) {
		          off.push($scope.$watchCollection($attr.slimscrollWatch, refresh));
		        }

		        if ($attr.slimscrolllistento) {
		          off.push($scope.$on($attr.slimscrolllistento, refresh));
		        }
		      };

		      var destructor = function () {
		        $($elem).slimScroll({ destroy: true });
		        off.forEach(function (unbind) {
		          unbind();
		        });
		        off = null;
		      };

		      off.push($scope.$on('$destroy', destructor));
		      
		      registerWatch();
    	}
  	};
})
.directive('starRating', function () {
    return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&"
   
        },
        restrict: 'EA',
        template:
            "<div style='display: inline-block; margin: 0px; padding: 0px; cursor:pointer;' ng-repeat='idx in maxRatings track by $index'> \
                    <img ng-src='{{((hoverValue + _rating) <= $index) && \"/assets/images/star-empty-lg.png\" || \"/assets/images/star-fill-lg.png\"}}' \
                    ng-Click='isolatedClick($index + 1)' \
                  ></img> \
            </div>",
        compile: function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        },
        controller: function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;
			
			$scope.isolatedClick = function (param) {
				if ($scope.readOnly == 'true') return;

				$scope.rating = $scope._rating = param;
				$scope.hoverValue = 0;
				$scope.click({
					param: param
				});
			};


		
        }
    };
})
.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "dd/mm/yy",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
})
// .directive('loading', function () {
//   return {
//     restrict: 'E',
//     replace:true,
//     template: '<div class="loading"><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" width="60" height="60" />LOADING...</div>',
//     link: function (scope, element, attr) {
//           scope.$watch('loading', function (val) {
//               if (val)
//                   $(element).show();
//               else
//                   $(element).hide();
//           });
//     }
//   }
// })
// .directive('fileModel', ['$parse', function ($parse) {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             var model = $parse(attrs.fileModel);
//             var modelSetter = model.assign;
            
//             element.bind('change', function(){
//                 scope.$apply(function(){
//                     modelSetter(scope, element[0].files[0]);
//                     scope.uploadFile();
//                 });
//             });
//         }
//     };
// }])
// .directive("ngFileSelect",function(){
//   	return {
// 	    link: function($scope,el){
	      
// 		    el.bind("change", function(e){		      
// 		        var file = (e.srcElement || e.target).files[0];
// 		        // console.log($scope.file)
// 		        if (typeof file !== 'undefined') {
// 		        	$scope.getFile(file);
// 		        }
// 		    })      
// 	    } 
//   	}
// })
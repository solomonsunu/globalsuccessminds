angular.module('asemApp.controllers', ['asemApp.services','angularUtils.directives.dirPagination','highcharts-ng','jkuri.slimscroll','angularMoment','angular.chosen','jkuri.datepicker','truncate'])

.controller('DashboardCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {};
	$scope.currentData = [];
	$scope.user = {}
	$scope.messages ={success:'',error:false,errorMessage:''};
	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			$scope.currentData[0] = {email_notification_id:$scope.user.email_notification.email_notification_id};
			$scope.postData.choice = res.data.user.self_notification;
			console.log($scope.user);
		});
	}
	$scope.emailPreference = function(){
		Request.get('/emailnotification').then(function(res){
			$scope.emailSetting = res.data.eNotification;
			console.log($scope.emailSetting );
		});
	}

	$scope.getUser();
	$scope.emailPreference(); 

	$scope.saveSetting = function(id) {
		$scope.postData.user_id = $scope.user.user_id;
		$scope.postData.setting_id = id;
		$scope.loading = {setting:true};
		// console.log($scope.postData);
		var req = Request.post('/save-preference', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Email Preference updated";
					$scope.loading.setting = false;
					$scope.getUser();
					$scope.emailPreference(); 
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.setting = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
		
	 }
	
})

.controller('RoleCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.activeUser = {};
	$scope.role = {};
	$scope.myArray = [];
	


	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.permission	= function(){};

	$scope.getRole = function(){
		Request.get('/getRoles').then(function(res){
			$scope.role  = res.data.roles;
			$scope.permission  = res.data.permissions;
			console.log($scope.role);
		});
	}

	$scope.getRole();
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
})
.controller('PermissionCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.activeUser = {};
	$scope.permission = {};
	$scope.myArray = [];
	

	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.permission	= function(){};

	$scope.getPermission = function(){
		Request.get('/getPermissions').then(function(res){
			$scope.permission  = res.data.permissions;
		});
	}

	$scope.getPermission();
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
})
.controller('CompanyCtrl', function ($scope,Request,$filter,$window,$timeout,$location) {
	$scope.postData = {  };
	$scope.postUser = {  };
	// $scope.postPrivate = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.activeUser = {};
	$scope.allCompany = {};
	$scope.postCompany = {};
	$scope.updateCompany = {};
	$scope.myArray = [];
	$scope.allCompany  = [];
	$scope.specFree = [];
	var date = new Date();
	$scope.filterFn = function(item) {
        return item.company && item.company.length === 0;
      };

    $scope.filterRole = function(item) {
    	// item.
        // must have array, and array must be empty
        return item.roles && item.roles[0].id != 1 ;
      };

    $scope.getRole = function(){
		Request.get('/getRoles').then(function(res){
			$scope.role  = res.data.roles;
			$scope.permission  = res.data.permissions;
			// console.log($scope.role);
		});
	}

	$scope.getRole();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    
    
	// $scope.getSingle = abc;
	$scope.getSingle = function(id,all){
		$scope.single_id  = all
		$scope.getAvailUser();
		if (id.length > 0) {
			var allUsers = $scope.users;
			$scope.specFree = [];
			var freeUser = id; 
			var x,y;
			for (x in freeUser) {
				for(y in allUsers){
					
					if(allUsers[y].id == freeUser[x].id){
						allUsers.splice(y,1); 
					}
				}
			}
			$scope.specFree = allUsers;
		}
		else{
			$scope.specFree = $scope.users;
		}
		
		//console.log(allUsers);
		
	}
	
	$scope.companyDelete = function(id) {
		$scope.postData.company_id =id;
		$scope.loading.company = true;
		// $scope.messages ={students:{success:'',error:false}}
		var req = Request.post('/remove-company', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Company Removal successful";
					$scope.messages.error = false;
					$scope.loading.company = false;
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.company = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
		
	}
	$scope.getCompany = function(){
		Request.get('/getCompany').then(function(res){
			$scope.allCompany  = res.data.company;
			// console.log($scope.allCompany);
		});
	}
	$scope.getAvailUser = function(){
		Request.get('/getFreeUser').then(function(res){
			$scope.users  = res.data.users;
			$scope.postUserData = res.data.users;
			// allUsers = res.data.users
			 // console.log($scope.postUserData);
		});
	}
	$scope.getAvailUserCompany = function(){
		Request.get('/userCompany').then(function(res){
			$scope.userAvail  = res.data.users;
			// $scope.specFree = $scope.users ;
			console.log($scope.userAvail);
		});
	}
	$scope.getAvailUser();
	$scope.getAvailUserCompany();
	$scope.getCompany();
	$scope.saveCompany = function() {
		$scope.loading = {company:true};
		
		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/save-company', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getCompany();
						$scope.messages.success = "Company creation successful";

						$scope.messages.error = false;
						$scope.loading.company = false;
						$scope.postData = '';
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.company = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/save-company', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getCompany();
						$scope.messages.success = "Company creation successful";
						$scope.messages.error = false;
						$scope.loading.company = false;
						$scope.postData = '';
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.company = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}	
	}

	$scope.updateCompany = function(id,fil) {
		$scope.loading = {company:true};
		$scope.postData = {};
		$scope.postData = id;
		$scope.postData.file = fil;
		
		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/update-company', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getCompany();
						$scope.messages.success = "Company update successful";
						$scope.loading.company = false;
						$scope.postData = '';
						location.reload();
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.company = false;
					
				});
				// $timeout(function() {
			 //       $scope.messages.success = '';
				// 	$scope.messages.errorMessage = '';
			 //    }, 3000);
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/update-company', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getCompany();
						$scope.messages.success = "Company update successful";
						$scope.loading.company = false;
						$scope.postData = '';
						location.reload();
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.company = false;
				});
				// $timeout(function() {
			 //       $scope.messages.success = '';
				// 	$scope.messages.errorMessage = '';
			 //    }, 3000);
			
		}	
	}

	$scope.addUserCompany = function() {
		$scope.postUser.company_id = $scope.single_id;
		$scope.loading = {user:true};
		    var req = Request.post('/userCompany',$scope.postUser);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.Msuccess = "Company management assigned user";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.postUser = {}
					location.reload();
				}
				if(!res.data.status){
					$scope.messages.es = "An Error occured with your input";
					$scope.messages.error = false;
					$scope.loading.user = false;
				}
				
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.user = false;
				location.reload();
			});
			
			
		
	}

	$scope.setPrivacy = function (id,val) {
		$scope.loading = {company_setting:true};
		$scope.messages = {success:'',errorMessage:''};
		$scope.postPrivate ={};
		$scope.postPrivate.company_id = id.id;
		$scope.postPrivate.visibility = val;
		 
		    var req = Request.post('/setPrivate',$scope.postPrivate);
			req.then(function(res){
				if(res.data.status){
					console.log('yes')
					$scope.getCompany();
					$scope.loading.company_setting = false;
					$scope.messages.success = "Company post visibility successfully changed";
					
				}
				
			});
			req.error(function(res){
				 $scope.loading.company_setting = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
	}
})
.controller('UserCtrl', function ($scope,Request,$filter,$window) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.activeUser = {};
	$scope.newUser = {};
	$scope.postRec = {};
	$scope.updateRec = {};
	$scope.myArray = [];
	

	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.permission	= function(){};

	$scope.getNewUser = function(){
		Request.get('/getNewUser').then(function(res){
			$scope.users  = res.data.users;
			console.log($scope.users);
		});
	}
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	
	// $scope.currentUser = function(){
	// 	Request.get('/currentUserOne').then(function(res){
	// 		$scope.currentData  = res.data.currentUser;
	// 		$scope.currentData.choice = 0;
	// 		$scope.currentData.choiceUser = 0;
	// 		//console.log($scope.currentData);
	// 	});
	// }
	

	$scope.userDetail = function(id){
		$scope.userInfo = {};
		$scope.loading.userInfo = true;
		Request.get('/users/'+id).then(function(res){
			$scope.userInfo = res.data;
			$scope.loading.userInfo = false;
		});
	}
	$scope.getNewUser();
	// $scope.currentUser();
	
	// $window.setInterval(function(){
	//  	$scope.getNotification();

	//   }, 6000);


	$scope.activate = function(id) {
		$scope.postData.user_id =id;
		 $scope.showModal = true;
		var req = Request.post('/activate-user', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNewUser();
					$scope.messages.success = "Activation successful";
					$scope.messages.error = false;
					$scope.loading.order = false;
					$scope.showModal = false;
					
				}
				$window.setTimeout(function(){
					$scope.messages.success = '';
				},3000)
			});
			req.error(function(res){
				$scope.messages.error = res;
				$scope.loading.order = false;
			});
			
		
	}

	$scope.removeUser = function(id) {
		$scope.postData.user_id =id;

		//console.log($scope.postData );
		// $scope.loading.students = true;
		// $scope.messages ={students:{success:'',error:false}}
		var req = Request.post('/remove-user', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNewUser();
					$scope.getNotification();
					$scope.messages.success = "User Removal successful";
					$scope.messages.error = false;
					$scope.loading.order = false;
					
					
				}
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$window.setTimeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}



	$scope.updateUser = function() {
		$scope.loading.user = true;
		//$scope.messages ={success:'',error:false};

		if(typeof($scope.currentData.file)  !== 'undefined'){
	    	// var flag = file.type.split('/')[0];
	     	// var filesize = file.size;

	        Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
	        .then(function(res){
	        	$scope.currentData.image = res.data.imageUrl;
	        	
	       		//console.log($scope.currentData);
	        	var req = Request.post('/user-management/updateUser', $scope.currentData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "User Record successfully updated";
						$scope.messages.error = false;
						$scope.loading.user = false;
						
					}
					$window.setTimeout(function(){
						$scope.messages.success = '';
						
					},3000)
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.user = false;
					$window.setTimeout(function(){
					
					$scope.messages.error = '';
					},3000)
				});
				
	        })
	    }
	    else{
		    $scope.currentData.image = 0;
		    if ($scope.currentData.choiceUser == 0) {
		    	$scope.currentData.username = '';
		    }
		    
		    var req = Request.post('/user-management/updateUser',$scope.currentData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "User Record successfully updated";
					$scope.messages.error = false;
					$scope.loading.user = false;
					
					$window.setTimeout(function(){
						$scope.messages.success = '';
						//alert("hello")
					},3000)
				}
				
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				//console.log(res);
				$scope.loading.user = false;
				$window.setTimeout(function(){
					
					$scope.messages.error = '';
				},3000)
			});
			
		}
	}

	$scope.createUser = function() {
		$scope.loading.user = true;
		//$scope.messages ={success:'',error:false};

		if(typeof($scope.postData.file)  !== 'undefined'){
	    	// var flag = file.type.split('/')[0];
	     	// var filesize = file.size;

	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	
	       		//console.log($scope.currentData);
	        	var req = Request.post('/user-management/createUser', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Admin Record successfully created";
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.getNotification();
					}
					$window.setTimeout(function(){
						$scope.messages.success = '';
						
					},3000)
				});
				req.error(function(res){
					$scope.messages.error = res.messages;
					$scope.loading.user = false;
					$window.setTimeout(function(){
					
					$scope.messages.error = '';
					},3000)
				});
				
	        })
	    }
	    else{
		    $scope.postData.image = 0;
		    
		    var req = Request.post('/user-management/createUser',$scope.createData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Admin Record successfully created";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.getNotification();
					$window.setTimeout(function(){
						$scope.messages.success = '';
						//alert("hello")
					},3000)
				}
				
			});
			req.error(function(res){
				$scope.messages.error = res;
				//console.log(res);
				$scope.loading.user = false;
				$window.setTimeout(function(){
					
					$scope.messages.error = '';
				},3000)
			});
			
		}
	}
})
.controller('PostCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};

	$scope.id = location.pathname.split('/')[2];
	console.log($scope.id);
	$scope.getCompanyPost = function(){
		// $scope.loading = true;
		Request.get('/companyPost/'+$scope.id).then(function(res){
			$scope.Post  = res.data.post;
			// $scope.complimentPost  = res.data.postCompliment;
			$scope.chosenCompany = res.data.company;
			$scope.chosenCategory = res.data.category;
			// $scope.loading = false;
			//console.log(res.data.category)
		});
	}
	$scope.getCompanyPost();
	
	$scope.searchPost= function(){
		$scope.postData.company_id =$scope.id;
		// console.log($scope.postData)
		Request.post('/searchPost',$scope.postData).then(function(res){
			$scope.searchResult  = res.data.post;
			// console.log($scope.searchResult.length)
		});
	}

    $scope.starRating1 = 1;
	$scope.rating = 1;
    $scope.click1 = function (param) {
    	$scope.rating = param;
        // console.log($scope.rating);
    };
    
     $scope.starRating2 = 0;
   	$scope.ratingEx = "Very Bad";
   	$scope.label = 'danger';
   	$scope.changeRating = function (param) {
   		//alert(param);
		switch(param){
			case 1:
				$scope.ratingEx = "Very Bad";
				$scope.label = 'danger';
				break
			case 2:
				$scope.ratingEx = "Bad";
				$scope.label = 'warning';
				break
			case 3:
				$scope.ratingEx = "Average";
				$scope.label = 'primary';
				break
			case 4:
				$scope.ratingEx = "Good";
				$scope.label = 'success';
				break
			case 4:
				$scope.ratingEx = "Very Good";
				$scope.label = 'success';
				break
			default:
				$scope.ratingEx = "Very Bad";
				break;
		}
		
	}
   
   
	$scope.complain = {
        name: 0
      };
	
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	

	
	$scope.getCompany = function(){
		Request.get('/publicCompany').then(function(res){
			$scope.publicCompany  = res.data.company;
			//console.log($scope.activeUser.length);
		});
	}
	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			$scope.postUser = res.data.user;

			// console.log($scope.postUser);
		});
	}


	$scope.getCompany();
	$scope.getUser();
	

	$scope.savePost = function() {
		$scope.loading.posts = true;
		$scope.postData.post_type = $scope.complain.name;
		$scope.postData.user_id = $scope.user.id;
		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/save-post', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getPublicPost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/save-post', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getPublicPost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}	
	}
	$scope.updateAccount = function() {
		$scope.loading.users = true;
		if(typeof($scope.postUser.file)  !== 'undefined'){
	    	// var flag = file.type.split('/')[0];
	     	// var filesize = file.size;

	        Request.uploadFile("/api/v1/files/upload", $scope.postUser.file)
	        .then(function(res){
	        	$scope.postUser.image = res.data.imageUrl;
	        	
	        	var req = Request.post('/ui/updateUser', $scope.postUser);
				req.then(function(res){
					if(res.data.status){
						$scope.getUser();
						$scope.messages.success = "User Record successfully updated";
						$scope.loading.users = false;
						$scope.messages.error = false;
						
					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.users = false;
					
				});

				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
	    	//console.log($scope.postUser);
		    $scope.postUser.image = 0;
		    var req = Request.post('/ui/updateUser',$scope.postUser);
			req.then(function(res){
				if(res.data.status){
					$scope.getUser();
					$scope.messages.success = "User Record successfully updated";
					$scope.loading.users = false;
					$scope.messages.error = false;
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.users = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}
	}

	
})
.controller('MainPostCtrl', function ($scope, Request, $location, $window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};

	$scope.starRating1 = 1;
	$scope.rating = 1;
    $scope.click1 = function (param) {
    	$scope.rating = param;
        // console.log($scope.rating);
    };
    
     $scope.starRating2 = 0;
   	$scope.ratingEx = "Very Bad";
   	$scope.label = 'danger';
   	$scope.changeRating = function (param) {
   		//alert(param);
		switch(param){
			case 1:
				$scope.ratingEx = "Very Bad";
				$scope.label = 'danger';
				break
			case 2:
				$scope.ratingEx = "Bad";
				$scope.label = 'warning';
				break
			case 3:
				$scope.ratingEx = "Average";
				$scope.label = 'primary';
				break
			case 4:
				$scope.ratingEx = "Good";
				$scope.label = 'success';
				break
			case 4:
				$scope.ratingEx = "Very Good";
				$scope.label = 'success';
				break
			default:
				$scope.ratingEx = "Very Bad";
				break;
		}
		
	}
   

	$scope.id = location.pathname.split('/')[2];

	$scope.getSinglePost = function() {
		$scope.loading = true;
		Request.get('/post/'+$scope.id).then(function(res){
			$scope.singlePost  = res.data.post;
			$scope.loading = false;
			 console.log($scope.singlePost);

		});
	}
	$scope.getSinglePost();

	
	$scope.saveReply = function(id) {
		$scope.loading ={posts:true};
		$scope.postData.post_id = id;
		$scope.postData.company = true;

		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/post-reply', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "successfully posted comment";
						$scope.getSinglePost();
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
						

					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				// $window.setTimeout(function(){
				// 		$scope.messages.success = '';
				// 		$scope.messages.errorMessage = '';
				// 	},3000)
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/post-reply', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						
						$scope.getSinglePost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
			
		}	
	}

	
})
.controller('CompanyPostCategoryCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};

	
   
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	

	
	$scope.getCompanyCategory = function(){
		Request.get('/userCompany').then(function(res){
			$scope.managedAvail  = res.data.users;
			// $scope.specFree = $scope.users ;
			console.log($scope.managedAvail);
		});
	}
	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			$scope.postUser = res.data.user;

			// console.log($scope.postUser);
		});
	}


	$scope.getCompanyCategory();
	$scope.getUser();

	$scope.saveCategory = function() {
		$scope.loading ={category:true};
		var req = Request.post('/save-category', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Category successfully created";
					$scope.getCompanyVisibility();
					$scope.loading.category = false;
					$scope.postData = {};
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.company = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
		
	}
		
})
.controller('CompanyPostVisibilityCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};

	
   
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	

	
	$scope.getCompanyVisibility = function(){
		Request.get('/userCompany').then(function(res){
			$scope.managedAvail  = res.data.users;
			// $scope.specFree = $scope.users ;
			console.log($scope.managedAvail);
		});
	}
	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			$scope.postUser = res.data.user;

			// console.log($scope.postUser);
		});
	}


	$scope.getCompanyVisibility();
	$scope.getUser();

	$scope.setPrivacy = function (id,val) {
		$scope.loading = {company_setting:true};
		$scope.messages = {success:'',errorMessage:''};
		$scope.postPrivate ={};
		$scope.postPrivate.company_id = id.id;
		$scope.postPrivate.visibility = val;
		 
		    var req = Request.post('/setPrivate',$scope.postPrivate);
			req.then(function(res){
				if(res.data.status){
					console.log('yes')
					$scope.getCompanyVisibility();
					$scope.loading.company_setting = false;
					$scope.messages.success = "Company post visibility successfully changed";
					
				}
				
			});
			req.error(function(res){
				 $scope.loading.company_setting = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
	}

		
})
.controller('UserInviteCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {};
	$scope.sendInvite = function() {
		$scope.loading = {user:true};
		$scope.messages = {success:'',errorMessage:''};
		    var req = Request.post('/sendInvite',$scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.loading.user = false;
					$scope.messages.success = "Invite successfully sent";
				}
				
			});
			req.error(function(res){
				 $scope.loading.user = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
	}

		
})
.controller('ReputationCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {};
	$scope.postCompany = {};
	$scope.ratio = {total:'',bad:'',good:''};
	$scope.getCompanyVisibility = function(){
		Request.get('/userCompany').then(function(res){
			$scope.managedAvail  = res.data.users;
			// $scope.specFree = $scope.users ;
			console.log($scope.managedAvail);
		});
	}
	$scope.getCompanyVisibility();
	$scope.sendInvite = function() {
		$scope.loading = {user:true};
		$scope.messages = {success:'',errorMessage:''};
		    var req = Request.post('/sendInvite',$scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.loading.user = false;
					$scope.messages.success = "Invite successfully sent";
				}
				
			});
			req.error(function(res){
				 $scope.loading.user = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
	}
	


	$scope.getReputation = function() {
		$scope.loading = {company:true};
		$scope.messages = {success:'',errorMessage:''};
		    var req = Request.post('/getRatio',$scope.postCompany);
			req.then(function(res){
				if(res.data.status){
					$scope.loading.company = false;
					$scope.messages.success = "Invite successfully sent";
					$scope.ratio.total = res.data.total;
					$scope.ratio.bad = res.data.bad;
					$scope.ratio.good = res.data.good;
					console.log($scope.ratio);
					var good = ($scope.ratio.good/$scope.ratio.total)*100;
					var bad = ($scope.ratio.bad/$scope.ratio.total)*100;
					var totals = [good,bad];
					console.log(totals);
						$scope.highchartsNG = {
								 options: {
							            chart: {
							                type: 'bar'
							            }

							        },
							        
									xAxis: {
								            categories: ['Good User Rating','Bad User Rating'],
								            title: {
								                text: 'User Review'
								            }
								        },
							         yAxis: {
								            min: 0,
								            max:100,
								            title: {
								                text: 'Review Percent (%)',
								                align: 'high'
								            },
								            labels: {
								                overflow: 'justify'
								            }
								        },
									title: {
							            text: 'Company Performance Measure',
							            x: -20 //center
							        },
							         tooltip: {
							            valueSuffix: ' % ',
							            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
							            
							        },
							        plotOptions: {
							            bar: {
							                dataLabels: {
							                    enabled: true
							                }
							            }
							        },
							        
							        series: [
							        {
								      "name": "Good Rates",
								      "data": [
								        good,0
								       
								      ]
								    },
								    {
								      "name": "Bad Rates",
								      "data": [
								        0,bad
								      ]
								    }
  
							        ]
							        ,
									"credits": {
									    "enabled": false
									  }
									 
							    }
				}
				
			});
			req.error(function(res){
				 $scope.loading.user = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
	}
		
})
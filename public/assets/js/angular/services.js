angular.module('asemApp.services', [])
.service('Request', function($http){
    var baseUrl = location.origin+"/api/v1";

	this.get = function(endpoint){		
		return $http.get(baseUrl+endpoint);
	}

	this.post = function(endpoint, postData){
		return $http.post(baseUrl+endpoint, postData);
	}

    this.put = function(endpoint, postData){
        return $http.put(baseUrl+endpoint, postData);
    }

    this.delete = function (endpoint) {
        return $http.delete(baseUrl+endpoint);
    }

    this.uploadFile = function(endpoint, file){
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(endpoint, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    }

})

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    }
}])
.filter('age', function(){
    return function(dateString){
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
})
.filter('cutoff', function(){
    return function(str){
        var words = str.split(' ');
        var newStr = '';
        var i = 0;
        var count = (words.length < 36) ? words.length : 35;

        while(i < count){           
            newStr += words[i]+" ";
            if(words.length === 35){
                newStr += "...";
            }
            i++;
        }
        return newStr;
    }
})
.filter('ago', function() {
    return function(date) {
        return moment(date).fromNow();
      }
})
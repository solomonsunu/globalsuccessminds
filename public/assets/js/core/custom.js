$(document).ready(function() {
    setInterval(function(){
        $("#fInput").change(function () {
            
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
           
            if(this.value.length > 0){
                if (regex.test($(this).val().toLowerCase())) {
                    
                    if (typeof (FileReader) != "undefined") {                    
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $(".profilePic").attr("src", e.target.result);
                            // console.log( e.target.result)
                        }
                        reader.readAsDataURL($(this)[0].files[0]);
                    } else {
                        console.log("This browser does not support FileReader.");
                    }
                    
                }
            }
        });
    }, 1000);
});
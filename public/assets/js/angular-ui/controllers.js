angular.module('asemUI.controllers', ['asemUI.services','angularUtils.directives.dirPagination','asemUI.directives','angularMoment','angular.chosen','geolocation','truncate','720kb.socialshare'])

.controller('PostCtrl', function ($scope,Request,$filter,$window,$timeout,geolocation,$interval) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.postCategory ={};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};
     
    geolocation.getLocation().then(function(data){
      $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
      console.log($scope.coords)
    });

	$scope.starRating1 = 1;
	$scope.rating = 1;
    $scope.click1 = function (param) {
    	$scope.rating = param;
    	$scope.changeRating(param);
        // console.log($scope.rating);
    };
    
   	$scope.starRating2 = 0;
   	$scope.ratingEx = "Very Bad";
   	$scope.label = 'danger';
   	$scope.changeRating = function (param) {
   		//alert(param);
		switch(param){
			case 1:
				$scope.ratingEx = "Very Bad";
				$scope.label = 'danger';
				break
			case 2:
				$scope.ratingEx = "Bad";
				$scope.label = 'warning';
				break
			case 3:
				$scope.ratingEx = "Average";
				$scope.label = 'primary';
				break
			case 4:
				$scope.ratingEx = "Good";
				$scope.label = 'success';
				break
			case 4:
				$scope.ratingEx = "Very Good";
				$scope.label = 'success';
				break
			default:
				$scope.ratingEx = "Very Bad";
				break;
		}
		
	}
   
	$scope.complain = {
        name: 0
      };
	
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	$scope.getPublicPost = function(){
		 $scope.loading = true;
		Request.get('/getPost').then(function(res){
			$scope.publicPost  = res.data.post;
			$scope.loading = false;
			console.log($scope.publicPost);
		});
	}

	$scope.getUserPost = function(){
		 $scope.loading = true;
		Request.get('/userPost').then(function(res){
			$scope.userPost  = res.data.post;
			$scope.loading = false;
		});
	}
	$scope.getCategory = function(id){
		Request.get('/getPostCategory/'+id).then(function(res){
			$scope.postCategory  = res.data.category;
			console.log($scope.postCategory)
			
		});
	}
	$scope.getCompany = function(){
		 
		Request.get('/publicCompany').then(function(res){
			$scope.publicCompany  = res.data.company;
			 
			console.log($scope.publicCompany);
		});
	}
	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			$scope.postUser = res.data.user;

			// console.log($scope.postUser);
		});
	}


	$scope.getPublicPost();
	$scope.getCompany();
	$scope.getUser();
	$scope.getUserPost();

	$interval( function(){ $scope.getPublicPost(); }, 60000);
	

	$scope.savePost = function() {
		$scope.loading= {posts:true}
		// $scope.messages= {success:''}
		$scope.postData.post_location = $scope.coords;
		$scope.postData.post_rating = $scope.rating;
		$scope.postData.user_id = $scope.user.id;
		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/save-post', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getPublicPost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.postData = {}
						$scope.rating = 1;
						$scope.starRating1 = 1;
					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/save-post', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getPublicPost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
						$scope.rating = 1;
						$scope.starRating1 = 1;
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}	
	}
	$scope.updateAccount = function() {
		$scope.loading.users = true;
		if(typeof($scope.postUser.file)  !== 'undefined'){
	    	// var flag = file.type.split('/')[0];
	     	// var filesize = file.size;

	        Request.uploadFile("/api/v1/files/upload", $scope.postUser.file)
	        .then(function(res){
	        	$scope.postUser.image = res.data.imageUrl;
	        	
	        	var req = Request.post('/ui/updateUser', $scope.postUser);
				req.then(function(res){
					if(res.data.status){
						$scope.getUser();
						$scope.messages.success = "User Record successfully updated";
						$scope.loading.users = false;
						$scope.messages.error = false;
						
					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.users = false;
					
				});

				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
	    	//console.log($scope.postUser);
		    $scope.postUser.image = 0;
		    var req = Request.post('/ui/updateUser',$scope.postUser);
			req.then(function(res){
				if(res.data.status){
					$scope.getUser();
					$scope.messages.success = "User Record successfully updated";
					$scope.loading.users = false;
					$scope.messages.error = false;
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.users = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}
	}

	
})
.controller('MainPostCtrl', function ($scope, Request, $location, $window,$timeout,$interval) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};

	$scope.starRating2 = 0;
	$scope.fullpath = location.pathname;
	console.log($scope.fullpath);
	$scope.id = location.pathname.split('/')[3];

	$scope.getSinglePost = function() {
		 $scope.loading = true;
		Request.get('/ui/post/'+$scope.id).then(function(res){
			$scope.singlePost  = res.data.post;
			 $scope.loading = false;
			 // console.log($scope.singlePost);
		});
	}
	$scope.getSinglePost();
	 $interval( function(){ $scope.getSinglePost(); }, 60000);
	

	$scope.getPublicPost = function(){
		 $scope.loading = true;
		Request.get('/getPost').then(function(res){
			$scope.publicPost  = res.data.post;
			 $scope.loading = false;
			console.log($scope.publicPost.length);
		});
	}

	$scope.getUser = function(){
		Request.get('/loggeduser').then(function(res){
			$scope.user  = res.data.user;
			// console.log($scope.postUser);
		});
	}
	$scope.getPublicPost();
	$scope.getUser();
	$scope.saveReply = function(id) {
		$scope.loading = {posts:true};
		$scope.postData.post_id = id;
		// $scope.postData.company_id = $scope.singlePost.company.id;

		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/post-reply', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "successfully posted comment";
						$scope.getPublicPost();
						$scope.getSinglePost();
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
					}
					
				})
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/post-reply', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getPublicPost();
						$scope.getSinglePost();
						$scope.messages.success = "successfully posted comment";
						$scope.loading.posts = false;
						$scope.messages.error = false;
						$scope.postData = {}
					}
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.posts = false;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
				
			
		}	
	}

	$scope.closeIssue = function (id) {
		console.log(id);
		Request.get('/closePost/'+id).then(function(res){
			if (res.data.status) {
				$scope.getSinglePost();
				$scope.getPublicPost();
			}
			
		});
	}

	
})
.controller('NotificationCtrl', function ($scope, Request, $location, $window,$timeout,$interval) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.publicPost = [];
	$scope.postCompany = {};
	$scope.myArray = [];
	$scope.postUser = { };
	$scope.loading ={users:false, posts:false,student:false};
	$scope.notification = [];
	
	$scope.getNotification = function() {
		 // $scope.loading = true;
		Request.get('/ui/notification/').then(function(res){
			$scope.notification  = res.data.notification;
			 // $scope.loading = false;
			 console.log($scope.notification);
		});
	}
	$scope.getNotification();
	 $interval( function(){ $scope.getNotification(); }, 60000);

	$scope.markRead = function(id){ 
		Request.get('/ui/markRead/'+id).then(function(res){
			if (res.data.status) {
				$scope.getNotification();
			}
			
		});
	}
	
		$scope.postData = {};
	$scope.sendInvite = function() {
		$scope.loading = {user:true};
		$scope.messages = {success:'',errorMessage:''};
		    var req = Request.post('/sendInvite',$scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.loading.user = false;
					$scope.messages.success = "Invite successfully sent";
					$scope.postData = {};
				}
				
			});
			req.error(function(res){
				 $scope.loading.user = false;
			});
			$timeout(function() {
			       $scope.messages.success = '';
			    }, 3000);
			
	}
})

angular.module('asemUI', [
					'ui.router',
                    'asemUI.controllers',
                    'asemUI.services',
					'asemUI.directives'
					
				]
)

	

// .config( function($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider) {
// $stateProvider
// 	.state('app', {
//         abstract: true,
//         templateUrl: 'views/base.html',
//         controller: function () {            
//             var e=$(window).height()-$("body > .navbar").outerHeight()-$("body > .navbar + .navbar").outerHeight()-$("body > .navbar + .navbar-collapse").outerHeight();
//             $(".page-container").attr("style","min-height:"+e+"px");
            
//         }
//     })
//     .state('login', {
//         url: '/auth/login',
//         templateUrl: 'views/pages/login.html',
//         controller: 'LoginCtrl'
//     })
//     .state('app.dashboard', {
//         url: '/',
//         views:{
//             'content': {
//                 templateUrl: 'views/pages/dashboard.html',
//                 controller: 'DashboardCtrl'
//             }
//         }
//     })

//     $urlRouterProvider.otherwise( function($injector) {
//         var $state = $injector.get("$state");
//         $state.go("app.dashboard");
//     });
    
//     $httpProvider.defaults.useXDomain = true;
//     $httpProvider.defaults.headers.common = 'Content-Type: application/json';
//     $locationProvider.html5Mode(true);
// })
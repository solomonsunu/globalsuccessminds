angular.module('gsmApp')
.controller('RegularAdminUserMgtCtrl', function ($scope,Request,$filter,$window,$interval,$timeout,UserService) {
	$scope.postData = {  };
	$scope.orders = [];
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.requisition = {};
	$scope.requisitionStatus = {status:false};
	$scope.checkStatus = false;
	$scope.activeUser = {};
	$scope.newUser = {};
	$scope.postRec = {};
	$scope.updateRec = {};
	$scope.myArray = [];
	$scope.Company = {};
	$scope.showModal = false;

	$scope.carrier = '';
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.getTotalBalance = function(user) {
		var count = 0;
	    angular.forEach(user, function(todo) {
	      count += todo.account.current_balance;
	    });
	    return count;
	}

	$scope.getNewUser = function(){
		Request.get('/getNewUser').then(function(res){
			$scope.totalBalance = res.data.totalBalance;
		});
	}
	$scope.full = true;
	$scope.lastpage=1;
	$scope.get_user_info = function(){
		$scope.lastpage=1;
        UserService.networkUsers($scope.lastpage,$scope.carrier)
            .then(function (res) {
            	if (res) {
            		$scope.activeUser  = res.data.data;
	                $scope.currentpage = res.data.current_page;
	                console.log(res)
            	}
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.get_user_info();
	$scope.objpush = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.activeUser.push(todo)
	    });
	    return $scope.activeUser;
	}
	$scope.loadMore = function() {
        $scope.lastpage +=1;
         UserService.networkUsers($scope.lastpage,$scope.carrier)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.data.length > 0){
        		$scope.objpush(res.data.data);
        		console.log($scope.activeUser);
        	}else{
        		$scope.full = false;
        	}
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	$scope.getNotification = function(){
		Request.get('/getNotification').then(function(res){
			$scope.read  = res.data.readNotification;
			$scope.unRead = res.data.unReadNotification; 
			
			//console.log($scope.read.length);
		});
	}
	
	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choicePhone = 0;
			$scope.currentData.choiceUser = 0;
			$scope.carrier = $scope.currentData.carrier;
			//console.log($scope.currentData);
		});
	}
	$scope.markRead = function(id) {
		$scope.postData.notification_id =id;
		$scope.loading= {order:true};
		var req = Request.post('/markRead', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNotification();
					$scope.messages.success = "Done";
					$scope.messages.error = false;
					$scope.loading.order = false;
					
					
				}
				$timeout(function(){
					$scope.messages.success = '';
				},3000)
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}

	$scope.userDetail = function(id){
		$scope.userInfo = {};
		$scope.loading= {userInfo:true};
		Request.get('/users/'+id).then(function(res){
			$scope.userInfo = res.data;
			$scope.loading.userInfo = false;
		});
	}
	// $scope.getNewUser();
	// $scope.getNewUser();
	$scope.currentUser();
	$scope.getNotification();

	$scope.updateUser = function() {
		$scope.loading= {user:true};
		if(typeof($scope.currentData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
	        .then(function(res){
	        	$scope.currentData.image = res.data.imageUrl;
	        	if ($scope.currentData.choiceUser == 0) {
	        		$scope.currentData.username = '';
	       		}	
	       		if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}	
	       		//console.log($scope.currentData);
	        	var req = Request.post('/user-management/updateUser', $scope.currentData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "User Record successfully updated";
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.getNotification();
						$scope.currentUser();
					}
					
				});
				req.error(function(res){
					$scope.messages.error = res.messages;
					$scope.loading.user = false;
					
				});
				
	        })
	    }
	    else{
		    $scope.currentData.image = 0;
		    if ($scope.currentData.choiceUser == 0) {
		    	$scope.currentData.username = '';
		    }
		    if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}
		    var req = Request.post('/user-management/updateUser',$scope.currentData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "User Record successfully updated";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.getNotification();
					$scope.currentUser();
					
				}
				
			});
			req.error(function(res){
				$scope.messages.error = res;
				//console.log(res);
				$scope.loading.user = false;
				
			});
			
		}
	}

})
.controller('NotificationCtrl', function ($scope,Request,$filter,$window,$interval,$timeout,UserService,$fancyModal) {
	$scope.postData = {  };
	$scope.orders = [];
	$scope.messages ={success:'',error:false}

	$scope.getNotification = function(){
		Request.get('/getNotification').then(function(res){
			$scope.read  = res.data.readNotification;
			$scope.unRead = res.data.unReadNotification; 
			
			//console.log($scope.read.length);
		});
	}
	$scope.getNotification();

	$scope.markRead = function(id) {
		$scope.postData.notification_id =id;
		$scope.loading= {order:true};
		var req = Request.post('/markRead', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNotification();
					$scope.messages.success = "Done";
					$scope.messages.error = false;
					$scope.loading.order = false;
					
					
				}
				$timeout(function(){
					$scope.messages.success = '';
				},3000)
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}

	
})


.controller('adminWalletCtrl', function ($scope,Request,$filter,$window,$timeout,UserService) {
	$scope.postData = {};
	$scope.postDataS = {};
	$scope.postDataR = {};
	$scope.senderData = {};
	$scope.transferDataS = {};
	$scope.transferDataR = {};
	$scope.resData = {};
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.walletHistory = "";
	$scope.walletBalance ="";
	//$scope.userPin = "";
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.adminSender = function(id){
		if (id.length > 9) {
			Request.get('/managedUser/'+id).then(function(res){
				$scope.userS  = res.data.manageUser;
				
			});
		}
		
	}
	$scope.superAdminSender = function(id){
		if (id.length > 9) {
			Request.get('/allUser/'+id).then(function(res){
				$scope.userS  = res.data.allusers;
				
			});
		}
	}

	$scope.selectSender = function(id) {
		$scope.transferDataS = id;
		$scope.userS = {};
	}
	$scope.adminRecipient = function(id){
		if (id.length > 9) {
			Request.get('/managedUser/'+id).then(function(res){
				$scope.userR  = res.data.manageUser;
				
			});
		}
	}
	$scope.superAdminRecipient = function(id){
		if (id.length > 9) {
			Request.get('/allUser/'+id).then(function(res){
				$scope.userR  = res.data.allusers;
				
			});
		}
	}

	$scope.selectRecipient = function(id) {
		$scope.transferDataR = id;
		$scope.userR = {};
	}

	$scope.transferBalance = function() {
			$scope.loading = {transfer:true}
			$scope.postData.sender_id = $scope.transferDataS.user_id;
			$scope.postData.recipient_id = $scope.transferDataR.user_id;
			console.log($scope.postData)
			var req = Request.post('/accounts/transfer', $scope.postData);
				req.then(function(res){
					if(res.data.status){
					$scope.messages.success = "Fund Transfer successful";
					$scope.loading.transfer = false;
					$scope.postData = {};
					$scope.transferDataS = {};
					$scope.transferDataR = {};
					}else{
						$scope.messages.success = res.data.message;
						$scope.loading.transfer = false;
						$scope.postData = {};
						$scope.transferDataS = {};
						$scope.transferDataR = {};
					}

				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.transfer = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.admin = res.data.admin;
		});
	}

	$scope.checkPaid = function(id){
		$scope.loading = {check:true}
		Request.get('/accounts/withdrawal/paid/'+id).then(function(res){
			if (res.data.status) {
				$scope.messages.success = "Operation successful";
				$scope.loading.check = false;
				$scope.getWithdrawal();
			}
			
		});
	}

	$scope.filter2 = function(p){
	    if (p.user !== null){
	        return true;
	    } else{
	        return;
	    }
	};
	$scope.getWithdrawal = function() {
		$scope.loading = {withdrawal:true};
		    var req = Request.get('/accounts/user/withdrawal');
			req.then(function(res){
				$scope.withdrawal = res.data.withdrawal;
				$scope.loading.withdrawal = false;
				console.log($scope.withdrawal);
			});
			
			
		}

	
	$scope.getWithdrawalHistory = function(){
		//console.log($scope.postData.user_id)
		$scope.postData.user_id = $scope.senderData.user_id;
		console.log($scope.postData.user_id)
		var req = Request.post('/accounts/user/withdrawal',$scope.postData);
				req.then(function(res){
					
					$scope.withdrawalHistory  =  res.data.withdrawalHistory;
					// $scope.postData = {};
					console.log($scope.withdrawalHistory)
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
		
	}
	$scope.getWalletHistory = function(){
		// console.log($scope.postData.user_id)
		$scope.postData.user_id = $scope.senderData.user_id;
		Request.get('/accounts/transactions/'+$scope.postData.user_id).then(function(res){
			$scope.walletHistory  =  res.data.transactions;
		
			// console.log($scope.walletHistory);
		});
	}

	$scope.getAdminWalletHistory = function(){
		console.log($scope.postData.user_id)
		Request.get('/admin/transactions/'+$scope.postData.user_id).then(function(res){
			$scope.adminWalletHistory  =  res.data.transactions;
		
			// console.log($scope.walletHistory);
		});
	}

	$scope.getWalletBalance = function(){
		$scope.postData.user_id = $scope.senderData.user_id;
		Request.get('/accounts/'+$scope.postData.user_id+'/balance').then(function(res){
			$scope.walletBalance  = res.data.balance;
			// console.log($scope.walletBalance )
		});
	}
	$scope.adminManageUser = function(id){
		if (id.length > 9) {
			Request.get('/managedUser/'+id).then(function(res){
				$scope.managedUser  = res.data.manageUser;
				console.log($scope.managedUser)
			});
		}
	}
	$scope.adminAllUser = function(id){
		if (id.length > 9) {
			Request.get('/allUser/'+id).then(function(res){;
				$scope.alluser  = res.data.allusers;
				console.log($scope.alluser)
			});
		}
	}
	$scope.getNewUser = function(){
		Request.get('/getNewUser').then(function(res){
			$scope.activeUser  = res.data.activeUser;
		});
	}
	$scope.selectUser = function(id) {
		$scope.senderData = id;
		$scope.managedUser = {};
		$scope.alluser = {};
	}
	// $scope.getNewUser();
	$scope.currentUser();
	// $scope.adminManageUser();
	
	// $scope.getWalletBalance();
	 // $scope.getWalletHistory();
	
	
	
	$scope.creditAccount = function() {
			$scope.loading = {credit:true}
			$scope.postData.user_id = $scope.senderData.user_id;
			var req = Request.post('/accounts/credit', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Transaction successful. Current Balance is: GH&#8373; "+res.data.current_balance;
						$scope.loading.credit = false;
						 $scope.postData = {};
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.credit = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
			
		}
	
	$scope.full = true;
	$scope.lastpage=1;
	$scope.adminWHistory = function(){
		$scope.lastpage=1;
        UserService.networkAdminHistory($scope.lastpage)
            .then(function (res) {
            	if (res) {
            		$scope.adminHistory  =  res.data.data;
	                $scope.currentpage = res.data.current_page;
	                console.log(res)
            	}
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	//solocode
	// $scope.adminWHistory();
	$scope.objpush = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.adminHistory.push(todo)
	    });
	    return $scope.adminHistory;
	}
	$scope.loadMore = function() {
        $scope.lastpage +=1;
         UserService.networkAdminHistory($scope.lastpage)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.data.length > 0){
        		$scope.objpush(res.data.data);
        		console.log($scope.adminHistory);
        	}else{
        		$scope.full = false;
        	}
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };
	// $scope.adminWHistory = function(){
		
	// 	Request.get('/admin/transactions/').then(function(res){
	// 		$scope.adminHistory  =  res.data.transactions;
		
	// 		 console.log($scope.adminHistory);
	// 	});
	// }
	// $scope.adminWHistory();
})
.controller('adminRankCtrl', function ($scope,Request,$filter,$window,$timeout,UserService) {
	$scope.postData = {};
	$scope.postDataS = {};
	$scope.postDataR = {};
	$scope.senderData = {};
	$scope.transferDataS = {};
	$scope.transferDataR = {};
	$scope.resData = {};
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};

	
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');


	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

	
	$scope.filter2 = function(p){
	    if (p.user !== null){
	        return true;
	    } else{
	        return;
	    }
	};
	$scope.getRanks = function() {
		$scope.loading = {ranks:true};
		    var req = Request.get('/admin/ranks');
			req.then(function(res){
				$scope.ranks = res.data.ranks;
				$scope.loading.ranks = false;
				console.log($scope.ranks);
			});
			
			
		}

	// $scope.getRanks();

	$scope.getUserRanks = function() {
		$scope.loading = {ranks:true};
		    var req = Request.get('/user-attained/ranks');
			req.then(function(res){
				$scope.user_ranks = res.data.user_ranks;
				$scope.loading.ranks = false;
				console.log($scope.user_ranks);
			});
			
			
		}
	// $scope.getUserRanks();
	$scope.payRank = function(data) {
		    var req = Request.get('/user-attained/ranks/pay/'+data.upgrade_id);
			req.then(function(res){
				$scope.getUserRanks();
			});
			
		}
	$scope.getCurrentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.getWalletBalance();
			
			// soso
		});
	}
	
	$scope.getCurrentUser();
	$scope.saveRank = function() {
			$scope.loading = {ranks:true}
			var req = Request.post('/admin/ranks', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Rank created successfully";
						$scope.loading.ranks = false;
						$scope.postData = {};
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.ranks = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
			
		}

	$scope.getWalletBalance = function(){
		Request.get('/accounts/'+$scope.currentData.user_id+'/balance').then(function(res){
			$scope.walletBalance  = res.data.balance;
			$scope.postData.wbalance = res.data.balance;
			console.log($scope.walletBalance);
		});
	}
	
	$scope.saveUpgrade = function() {
			$scope.loading = {user:true}
			var req = Request.post('/premium-user-upgrades', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Account upgrade successful. You have been placed under this user : "+res.data.message.fullname;
						$scope.loading.user = false;
						$scope.postData = {};
					}else{
						$scope.messages.success = res.data.message;
						$scope.loading.user = false;
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.user = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
			
		}

	

	
})

.controller('superAdminWalletCtrl', function ($scope,Request,$filter,$window,$timeout) {
	$scope.postData = {};
	$scope.senderData = {};
	$scope.resData = {};
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.walletHistory = "";
	$scope.walletBalance ="";
	//$scope.userPin = "";
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.admin = res.data.admin;
		});
	}

	$scope.checkPaid = function(id){
		$scope.loading = {check:true}
		Request.get('/accounts/withdrawal/paid/'+id).then(function(res){
			if (res.data.status) {
				$scope.messages.success = "Operation successful";
				$scope.loading.check = false;
				$scope.getWithdrawal();
			}
			
		});
	}

	$scope.filter2 = function(p){
	    if (p.user !== null){
	        return true;
	    } else{
	        return;
	    }
	};
	$scope.getWithdrawal = function() {
		$scope.loading = {withdrawal:true};
		    var req = Request.get('/accounts/user/withdrawal');
			req.then(function(res){
				$scope.withdrawal = res.data.withdrawal;
				$scope.loading.withdrawal = false;
				console.log($scope.withdrawal);
			});
			
			
		}

	$scope.getWithdrawal();
	$scope.getWithdrawalHistory = function(){
		console.log($scope.postData.user_id)
		var req = Request.post('/accounts/user/withdrawal',$scope.postData);
				req.then(function(res){
					
					$scope.withdrawalHistory  =  res.data.withdrawalHistory;
					// $scope.postData = {};
					console.log($scope.withdrawalHistory)
					
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
		
	}
	$scope.getWalletHistory = function(){
		// console.log($scope.postData.user_id)
		$scope.postData.user_id = $scope.senderData.user_id;
		Request.get('/accounts/transactions/'+$scope.postData.user_id).then(function(res){
			$scope.walletHistory  =  res.data.transactions;
		
			// console.log($scope.walletHistory);
		});
	}

	$scope.getAdminWalletHistory = function(){
		console.log($scope.postData.user_id)
		Request.get('/admin/transactions/'+$scope.postData.user_id).then(function(res){
			$scope.adminWalletHistory  =  res.data.transactions;
		
			// console.log($scope.walletHistory);
		});
	}

	$scope.getWalletBalance = function(){
		$scope.postData.user_id = $scope.senderData.user_id;
		Request.get('/accounts/'+$scope.postData.user_id+'/balance').then(function(res){
			$scope.walletBalance  = res.data.balance;
			// console.log($scope.walletBalance )
		});
	}
	$scope.adminManageUser = function(id){
		Request.get('/managedUser/'+id).then(function(res){
			$scope.managedUser  = res.data.manageUser;
			console.log($scope.managedUser)
		});
	}
	$scope.adminAllUser = function(id){
		Request.get('/allUser/'+id).then(function(res){;
			$scope.alluser  = res.data.allusers;
			console.log($scope.alluser)
		});
	}
	$scope.getNewUser = function(){
		Request.get('/getNewUser').then(function(res){
			$scope.activeUser  = res.data.activeUser;
		});
	}
	$scope.selectUser = function(id) {
		$scope.senderData = id;
		$scope.managedUser = {};
		$scope.alluser = {};
	}
	// $scope.getNewUser();
	$scope.currentUser();
	// $scope.adminManageUser();
	
	// $scope.getWalletBalance();
	// $scope.getWalletHistory();
	
	$scope.transferBalance = function() {
			$scope.loading = {transfer:true}
			console.log($scope.postData)
			var req = Request.post('/accounts/transfer', $scope.postData);
				req.then(function(res){
					if(res.data.status){
					$scope.messages.success = "Fund Transfer successful";
					$scope.loading.transfer = false;
					$scope.postData = {};
					}else{
						$scope.messages.success = res.data.message;
						
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.transfer = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}
	
	$scope.creditAccount = function() {
			$scope.loading = {credit:true}
			$scope.postData.user_id = $scope.senderData.user_id;
			var req = Request.post('/accounts/credit', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Transaction successful. Current Balance is: GH&#8373; "+res.data.current_balance;
						$scope.loading.credit = false;
						 $scope.postData = {};
					}
				});
				req.error(function(res){
					$scope.messages.errorMessage = res;
					$scope.loading.credit = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
			
		}
	
	$scope.adminWHistory = function(){
		
		Request.get('/admin/transactions').then(function(res){
			$scope.adminHistory  =  res.data.transactions;
		
			 // console.log($scope.adminHistory);
		});
	}
	// $scope.adminWHistory();
})


.controller('UserMgtCtrl', function ($scope,Request,$filter,$window,$interval,$timeout,UserService,$fancyModal) {
	$scope.postData = {  };
	$scope.orders = [];
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.requisition = {};
	$scope.requisitionStatus = {status:false};
	$scope.checkStatus = false;
	$scope.activeUser = {};
	$scope.newUser = {};
	$scope.postRec = {};
	$scope.updateRec = {};
	$scope.myArray = [];
	$scope.Company = {};
	$scope.showModal = false;
	$scope.loadActive = true;

	$scope.open = function () {
        $fancyModal.open({ 
        		templateUrl: '/pages/popupTmpl.html',
        		showCloseButton:false,
        		openingClass: 'animated rollIn',
		        closingClass: 'animated rollOut',
		        openingOverlayClass: 'animated fadeIn',
		        closingOverlayClass: 'animated fadeOut',
		        closeOnOverlayClick: false
         		});
        
    };

    $scope.close = function () {
    	$fancyModal.close();
    }
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.getTotalBalance = function(user) {
		var count = 0;
	    angular.forEach(user, function(todo) {
	      count += todo.account.current_balance;
	    });
	    return count;
	}

	$scope.getNewUser = function(){
		Request.get('/getNewUser').then(function(res){
			$scope.totalBalance = res.data.totalBalance;
		});
	}
	$scope.full = true;
	$scope.lastpage=1;
	$scope.getNew_User = function(){
		$scope.open();
		$scope.loadActive = false;
		$scope.lastpage=1;
        UserService.allUsers($scope.lastpage)
            .then(function (res) {
            	if (res) {
            		$scope.close();
            		$scope.activeUser  = res.data.activeUser.data;
	                $scope.currentpage = res.data.activeUser.current_page;
	                console.log(res)
            	}
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getNew_User();
	$scope.objpush = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.activeUser.push(todo)
	    });
	    return $scope.activeUser;
	}

	$scope.loadMore = function() {
		// $scope.open();
        $scope.lastpage +=1;
        UserService.allUsers($scope.lastpage)
        .then(function (res) {
        	console.log(res);
        	if (res.data.paginate.current_page < res.data.paginate.last_page){
        		$scope.currentpage = res.data.paginate.current_page;
        		$scope.objpush(res.data.activeUser.data);
        		console.log($scope.currentpage);
        		// $scope.loadMore();
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		
        		$scope.full = false;
        	}
        	$scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	$scope.getNotification = function(){
		Request.get('/getNotification').then(function(res){
			$scope.read  = res.data.readNotification;
			$scope.unRead = res.data.unReadNotification; 
			
			//console.log($scope.read.length);
		});
	}
	
	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choicePhone = 0;
			$scope.currentData.choiceUser = 0;
			//console.log($scope.currentData);
		});
	}
	$scope.markRead = function(id) {
		$scope.postData.notification_id =id;
		$scope.loading= {order:true};
		var req = Request.post('/markRead', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNotification();
					$scope.messages.success = "Done";
					$scope.messages.error = false;
					$scope.loading.order = false;
					
					
				}
				$timeout(function(){
					$scope.messages.success = '';
				},3000)
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}

	$scope.userDetail = function(id){
		$scope.userInfo = {};
		$scope.loading= {userInfo:true};
		Request.get('/users/'+id).then(function(res){
			$scope.userInfo = res.data;
			$scope.loading.userInfo = false;
		});
	}
	// $scope.getNewUser();
	// $scope.getNewUser();
	$scope.currentUser();
	$scope.getNotification();

	// $window.setInterval(function(){
	//  	$scope.getNotification();

	//   }, 6000);


	$scope.activate = function(id) {
		$scope.postData.user_id =id;
		 $scope.showModal = true;
		 $scope.loading= {order:true};
		var req = Request.post('/activate-user', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNewUser();
					$scope.getNotification();
					$scope.messages.success = "Activation successful";
					$scope.messages.error = false;
					$scope.loading.order = false;
					$scope.showModal = false;
					
				}
				$timeout(function(){
					$scope.messages.success = '';
				},3000)
			});
			req.error(function(res){
				$scope.messages.error = res;
				$scope.loading.order = false;
			});
			
		
	}

	$scope.removeUser = function(id) {
		$scope.postData.user_id =id;
		$scope.loading= {order:true};
		var req = Request.post('/remove-user', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNewUser();
					$scope.getNotification();
					$scope.messages.success = "User Removal successful";
					$scope.messages.error = false;
					$scope.loading.order = false;		
				}
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}



	$scope.updateUser = function() {
		$scope.loading= {user:true};
		if(typeof($scope.currentData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
	        .then(function(res){
	        	$scope.currentData.image = res.data.imageUrl;
	        	if ($scope.currentData.choiceUser == 0) {
	        		$scope.currentData.username = '';
	       		}	
	       		if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}	
	       		//console.log($scope.currentData);
	        	var req = Request.post('/user-management/updateUser', $scope.currentData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "User Record successfully updated";
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.getNotification();
						$scope.currentUser();
					}
					
				});
				req.error(function(res){
					$scope.messages.error = res.messages;
					$scope.loading.user = false;
					
				});
				
	        })
	    }
	    else{
		    $scope.currentData.image = 0;
		    if ($scope.currentData.choiceUser == 0) {
		    	$scope.currentData.username = '';
		    }
		    if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}
		    var req = Request.post('/user-management/updateUser',$scope.currentData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "User Record successfully updated";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.getNotification();
					$scope.currentUser();
					
				}
				
			});
			req.error(function(res){
				$scope.messages.error = res;
				//console.log(res);
				$scope.loading.user = false;
				
			});
			
		}
	}

	$scope.createUser = function() {
		$scope.loading= {user:true};
		if(typeof($scope.postData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
	        .then(function(res){
	        	$scope.postData.image = res.data.imageUrl;
	        	var req = Request.post('/user-management/createUser', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Admin Record successfully created";
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.getNotification();
					}
				});
				req.error(function(res){
					$scope.messages.error = res.messages;
					$scope.loading.user = false;
				});
				$timeout(function(){
						$scope.messages.success = '';
						
					},3000)
				
	        })
	    }
	    else{
		    $scope.postData.image = 0; 
		    var req = Request.post('/user-management/createUser',$scope.createData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Admin Record successfully created";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.getNotification();
				}
				
			})
			req.error(function(res){
				$scope.messages.error = res;
				$scope.loading.user = false;
			});
			$timeout(function(){
						$scope.messages.success = '';
						
					},3000)

			
		}
	}


	$scope.fullPremium = true;
	$scope.prelastpage=1;
	$scope.getPremiumUserList = function(){
		$scope.loadPremium = false;
		$scope.open();
        UserService.premiumUsers($scope.prelastpage)
            .then(function (res) {
            	if (res) {
            		$scope.close();
            		$scope.premiumUser  = res.data.user.data;
	                $scope.precurrentpage = res.data.paginate.current_page;
	                $scope.prelastpage = res.data.paginate.last_page;
	                console.log($scope.premiumUser)
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getPremiumUserList();
	$scope.objpushPremium = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.premiumUser.push(todo)
	    });
	    return $scope.premiumUser;
	}
	$scope.loadMorePremiumList = function() {
		// $scope.open();
      	$scope.r = $scope.prelastpage+1;
        UserService.premiumUsers($scope.r)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.paginate.current_page <= res.data.paginate.last_page){
        		$scope.precurrentpage = res.data.paginate.current_page;
        		$scope.objpushPremium(res.data.user.data);
        		console.log($scope.precurrentpage);
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		// $scope.currentpage = res.data.paginate.current_page;
        		// $scope.objpush(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.fullPremium = false;
        	}
        	
        	// $scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };
	
})
.controller('UserRanksMgtCtrl', function ($scope,Request,$filter,$window,$interval,$timeout,UserService,$fancyModal) {
	$scope.postData = {};
	$scope.orders = [];
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.requisition = {};
	$scope.requisitionStatus = {status:false};
	$scope.checkStatus = false;
	$scope.activeUser = {};
	$scope.newUser = {};
	$scope.postRec = {};
	$scope.updateRec = {};
	$scope.myArray = [];
	$scope.Company = {};
	$scope.showModal = false;

	$scope.open = function () {
        $fancyModal.open({ 
        		templateUrl: '/pages/popupTmpl.html',
        		showCloseButton:false,
        		openingClass: 'animated rollIn',
		        closingClass: 'animated rollOut',
		        openingOverlayClass: 'animated fadeIn',
		        closingOverlayClass: 'animated fadeOut',
		        closeOnOverlayClick: false
         		});
        
    };

    $scope.close = function () {
    	$fancyModal.close();
    }
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

	
	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choicePhone = 0;
			$scope.currentData.choiceUser = 0;
			//console.log($scope.currentData);
		});
	}


	$scope.userDetail = function(id){
		$scope.userInfo = {};
		$scope.loading= {userInfo:true};
		Request.get('/users/'+id).then(function(res){
			$scope.userInfo = res.data;
			$scope.loading.userInfo = false;
		});
	}

	$scope.currentUser();

	$scope.getPremiumUserListFix = function(){
		$scope.open();
        UserService.premiumUsersFix()
            .then(function (res) {
            	if (res) {
            		$scope.close();
            		$scope.premiumUser  = res.data.user;
	                console.log($scope.premiumUser)
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}

//this was not working well
	$scope.fullPremium = true;
	$scope.prelastpage=1;

	$scope.getPremiumUserList = function(){
		$scope.loadPremium = false;
		$scope.open();
        UserService.premiumUsers($scope.prelastpage)
            .then(function (res) {
            	if (res) {
            		$scope.close();
            		$scope.premiumUser  = res.data.user.data;
	                $scope.precurrentpage = res.data.paginate.current_page;
	                $scope.prelastpage = res.data.paginate.last_page;
	                console.log($scope.premiumUser)
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getPremiumUserList();
	$scope.objpushPremium = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.premiumUser.push(todo)
	    });
	    return $scope.premiumUser;
	}
	$scope.loadMorePremiumList = function() {
		// $scope.open();
      	$scope.r = $scope.prelastpage+1;
        UserService.premiumUsers($scope.r)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.paginate.current_page <= res.data.paginate.last_page){
        		$scope.precurrentpage = res.data.paginate.current_page;
        		$scope.objpushPremium(res.data.user.data);
        		console.log($scope.precurrentpage);
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		// $scope.currentpage = res.data.paginate.current_page;
        		// $scope.objpush(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.fullPremium = false;
        	}
        	
        	// $scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };
	
})
.controller('regularUserCtrl', function ($scope,Request,$filter,$window,$interval,$timeout,$fancyModal,DisplayService) {
	$scope.postData = {};
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.pt1 = 0;
	$scope.pt2 = 0;
	$scope.pt3 = 0;
	$scope.pt4 = 0;
	$scope.pt5 = 0;
	$scope.pt6 = 0;

	$scope.showLevel1 = false;
	$scope.showLevel2 = false;
	$scope.showLevel3 = false;
	$scope.showLevel4 = false;
	$scope.showLevel5 = false;
	$scope.showLevel6 = false;

	$scope.showPLevel1 = false;
	$scope.showPLevel2 = false;
	$scope.showPLevel3 = false;
	$scope.open = function (id) {
        $fancyModal.open({ 
        	templateUrl: '/pages/popupTmpl1.html',
        	showCloseButton:true,
    		openingClass: 'animated rollIn',
	        closingClass: 'animated rollOut',
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	        closeOnOverlayClick: true,
		    controller: ['$scope', function($scope) {
	          $scope.message = id;
	        }]
        	 });
    };
    
    $scope.openWait = function (id) {
        $fancyModal.open({ 
        	templateUrl: '/pages/popupTmpl.html',
        	showCloseButton:true,
    		openingClass: 'animated rollIn',
	        closingClass: 'animated rollOut',
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	        closeOnOverlayClick: true,
		    controller: ['$scope', function($scope) {
	          $scope.message = id;
	        }]
        	 });
    };
    
   $scope.close = function () {
    	$fancyModal.close();
    }
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choiceUser = 0;
			$scope.currentData.choicePhone = 0;
			//console.log($scope.currentData.account);
		});
	}
	$scope.getReferral = function(){
		$scope.openWait();
		Request.get('/getReferral').then(function(res){
			$scope.close();
			$scope.level1  = res.data.level1;
			$scope.level2  = res.data.level2;
			$scope.level3  = res.data.level3;
			$scope.level4  = res.data.level4;
			$scope.level5  = res.data.level5;
			$scope.level6  = res.data.level6;
			$scope.t1  = res.data.t1;
			$scope.t2  = res.data.t2;
			$scope.t3  = res.data.t3;
			$scope.t4  = res.data.t4;
			$scope.t5  = res.data.t5;
			$scope.t6  = res.data.t6;

		});
	}
	// $scope.getReferral();

	$scope.getRefL1 = function(){

		$scope.openWait();
		Request.get('/getReferral/level1').then(function(res){
			$scope.showLevel1 = true;
			$scope.close();
			console.log(res);
			$scope.level1  = res.data.level1;
			$scope.level_2  = res.data.nextLevel;
			$scope.t1  = res.data.total1;
		});
	}
	//$scope.getRefL1();

	$scope.getRefL2 = function(data){
		$scope.openWait();
		$scope.postData.l2 = data;
		Request.post('/getReferral/level2',$scope.postData).then(function(res){
			$scope.close();
			console.log(res);
			$scope.showLevel2 = true;
			$scope.level2  = res.data.level2;
			$scope.level_3  = res.data.nextLevel;
			$scope.t2  = res.data.total2;
			console.log($scope.level2)
		});
	}

	$scope.getRefL3 = function(data){
		$scope.openWait();
		$scope.postData.l3 = data;
		Request.post('/getReferral/level3',$scope.postData).then(function(res){
			$scope.close();
			console.log(res);
			$scope.showLevel3 = true;
			$scope.level3  = res.data.level3;
			$scope.level_4  = res.data.nextLevel;
			$scope.t3  = res.data.total3;
			console.log($scope.level3)
		});
	}

	$scope.getRefL4 = function(data){
		$scope.openWait();
		$scope.postData.l4 = data;
		Request.post('/getReferral/level4',$scope.postData).then(function(res){
			$scope.close();
			console.log(res);
			$scope.showLevel4 = true;
			$scope.level4  = res.data.level4;
			$scope.level_5  = res.data.nextLevel;
			$scope.t4  = res.data.total4;
			console.log($scope.level4)
		});
	}

	$scope.getRefL5 = function(data){
		$scope.openWait();
		$scope.postData.l5 = data;
		Request.post('/getReferral/level5',$scope.postData).then(function(res){
			$scope.close();
			console.log(res);
			$scope.showLevel5 = true;
			$scope.level5  = res.data.level5;
			$scope.level_6  = res.data.nextLevel;
			$scope.t5  = res.data.total5;
			console.log($scope.level5)
		});
	}

	$scope.getRefL6 = function(data){
		$scope.openWait();
		$scope.postData.l6 = data;
		Request.post('/getReferral/level6',$scope.postData).then(function(res){
			$scope.close();
			console.log(res);
			$scope.showLevel6 = true;
			$scope.level6  = res.data.level6;
			$scope.t6  = res.data.total6;
			console.log($scope.level6)
		});
	}
	//level 1
	$scope.getPremiumReferral = function(){
		$scope.openWait();
		Request.get('/get-premium-referral').then(function(res){
			console.log(res.data)
			$scope.close();
			$scope.plevel1  = res.data.level1;
			$scope.pt1  = res.data.t1;

			for(var i in $scope.plevel1){
				$scope.plevel1[i].key = i
				// console.log($scope.plevel1[i]);
			}
		

		});
	}

	$scope.getRefPremiumL2 = function(data){
		$scope.openWait();
		$scope.postData.level = [];
		angular.forEach(data,function(value, key) {
			this.push(value.user_id);
		},$scope.postData.level);
		
		Request.post('/get-premium-referral-l2',$scope.postData).then(function(res){
			console.log(res.data)
			$scope.close();
			$scope.plevel2  = res.data.level;
			$scope.pt2  = res.data.t;
			$scope.l2Ids = [];
			$scope.showPLevel2 = true;
		
			for(var i in $scope.plevel2){
				$scope.plevel1[i].nextLevel = [];
				for(var j in $scope.plevel1){
					if($scope.plevel1[i].key == j){
						$scope.plevel1[i].nextLevel = $scope.plevel2[j];

						for(var x in $scope.plevel1[i].nextLevel){
							var l2 = $scope.plevel1[i].nextLevel[x];
							$scope.l2Ids.push(l2.user_id)
						}
					}
				}
			}
		});
	}

	$scope.getRefPremiumL3 = function () {
		$scope.openWait();
		$scope.postData.level = $scope.l2Ids;
		
		Request.post('/get-premium-referral-l3',$scope.postData).then(function(res){
			console.log(res.data)
			$scope.close();
			$scope.plevel3  = res.data.level;
			$scope.pt3  = res.data.t;
			$scope.l3Ids = [];
			$scope.showPLevel3 = true;
			$scope.fullLevel3 = [];
		
			for(var m in $scope.plevel3){ //l3
				for(var i in $scope.plevel1){ //l1

					for(var x in $scope.plevel1[i].nextLevel){ //l2
						var l2 = $scope.plevel1[i].nextLevel[x];
						if(l2.user_id == m){
							$scope.plevel1[i].nextLevel[x].nextLevel = $scope.plevel3[m];
							$scope.fullLevel3[m] = $scope.plevel3[m];
						}
					}
					
				}
			}
			console.log($scope.fullLevel3);
		});
	}




	$scope.getPayHistory = function(){
		Request.get('/getPayHistory').then(function(res){
			$scope.payHistory  =  res.data.payHistory;
		
			//console.log($scope.payHistory.accountdetail.length);
		});
	}
	
	// $scope.getNotification();
	$scope.currentUser();
	// $scope.getReferral();
	// $scope.getPremiumReferral();
	// $scope.getPayHistory();

	$scope.markRead = function(id) {
		$scope.postData.notification_id =id;
		var req = Request.post('/markRead', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getNotification();
					$scope.messages.success = "Done";
					$scope.messages.error = false;
					$scope.loading.order = false;
				}
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}

	$scope.updateUser = function() {
		$scope.loading={user:true};
		if(typeof($scope.currentData.file)  !== 'undefined'){
	        Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
	        .then(function(res){
	        	$scope.currentData.image = res.data.imageUrl;
	        	if ($scope.currentData.choiceUser == 0) {
	        		$scope.currentData.username = '';
	       		}	
	       		if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}	
	       		//console.log($scope.currentData);
	        	var req = Request.post('/user-management/updateUser', $scope.currentData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "User Record successfully updated";
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.currentUser();
					}
					
				});
				req.error(function(err){
					$scope.messages.error = err.messages;
					$scope.loading.user = false;
				});
				$timeout(function(){
					$scope.messages.success = '';
					$scope.messages.error = '';
				},3000)
				
	        })
	    }
	    else{
		    $scope.currentData.image = 0;
		    if ($scope.currentData.choiceUser == 0) {
		    	$scope.currentData.username = '';
		    }
		    if ($scope.currentData.choice == 0) {
	        		$scope.currentData.password = '';
	        		$scope.currentData.password_confirmation = '';
	       		}
		    var req = Request.post('/user-management/updateUser',$scope.currentData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "User Record successfully updated";
					$scope.messages.error = false;
					$scope.loading.user = false;
					$scope.currentUser();
				}
				
			});
			req.error(function(res){
				$scope.messages.error = res;
				$scope.loading.user = false;
			});
			$timeout(function(){
					$scope.messages.success = '';
					$scope.messages.error = '';
				},3000)
		}
	}

	$scope.saveAccount = function() {
			$scope.loading = {order:true};
			var req = Request.post('/saveAccount', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getNotification();
						$scope.currentUser();
						$scope.messages.success = "Bank Information saved successful";
						$scope.messages.error = false;
						$scope.loading.order = false;
						
						
					}
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.order = false;
				});
				$timeout(function(){
					$scope.messages.success = '';
					$scope.messages.error = '';
				},3000)
			
		}
	$scope.updateAccount = function() {
			$scope.loading = {order:true}
			var req = Request.post('/updateAccount', $scope.currentData.account);
				req.then(function(res){
					if(res.data.status){
						$scope.getNotification();
						$scope.currentUser();
						$scope.messages.success = "Bank Information update successful";
						$scope.messages.error = false;
						$scope.loading.order = false;
						
						
					}
				});
				req.error(function(res){
					$scope.messages.order.error = res;
					$scope.loading.order = false;
				});
				$timeout(function(){
					$scope.messages.success = '';
				},3000)
			
		}	
	
})

.controller('MessageCtrl', function ($scope,Request,$filter,$window,$interval,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choiceUser = 0;
			$scope.currentData.choicePhone = 0;
			//console.log($scope.currentData.account);
		});
	}
	
	
	$scope.currentUser();
	

	$scope.sendMessage = function() {
			$scope.loading = {user:true};
			var req = Request.post('/sendMessage', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Message sent successfully";
						//alert($scope.messages.success)
						$scope.messages.error = false;
						$scope.loading.user = false;
						$scope.getNotification();
						$scope.currentUser();
						
						
					}
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.user = false;
				});
				$timeout(function() {
					$scope.postData = {}
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}
	$scope.sendMail = function() {
			$scope.loading = {order:true}
			var req = Request.post('/sendMail', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getNotification();
						$scope.currentUser();
						$scope.messages.success = "Emails sent successfully";
						$scope.messages.error = false;
						$scope.loading.order = false;
					}
				});
				req.error(function(res){
					$scope.messages.order.error = res;
					$scope.loading.order = false;
				});
				$timeout(function() {
					$scope.postData = {};
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 3000);
			
		}	
	
})

.controller('InsuranceCtrl', function ($scope,Request,$filter,$window,$fancyModal,$timeout,UserService) {
	$scope.load = true;
	
	$scope.open = function () {
        $fancyModal.open({ 
        		templateUrl: '/pages/popupTmpl.html',
        		showCloseButton:false,
        		openingClass: 'animated rollIn',
		        closingClass: 'animated rollOut',
		        openingOverlayClass: 'animated fadeIn',
		        closingOverlayClass: 'animated fadeOut',
		        closeOnOverlayClick: false
         		});
        
    };

    $scope.close = function () {
    	$fancyModal.close();
    }
	$scope.postData = {  };
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.sales = {};

	 $scope.items = [
    { id: 1, name: 'GSMadmin'},
    { id: 2, name: 'GSM 1'},
    { id: 3, name: 'GSM 2'},
    { id: 4, name: 'GSM 3'},
    { id: 5, name: 'GSM 4'},
    { id: 6, name: 'GSM 5'},
    { id: 7, name: 'GSM 6'}];
    

	//$scope.paymentList = {};

	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choiceUser = 0;
			$scope.currentData.choicePhone = 0;
			//console.log($scope.currentData.account);
		});
	}
	$scope.getReferral = function(){
		Request.get('/getReferral').then(function(res){
			$scope.level1  = res.data.level1;
			$scope.level2  = res.data.level2;
			$scope.level3  = res.data.level3;
			$scope.level4  = res.data.level4;
			$scope.level5  = res.data.level5;
			$scope.level6  = res.data.level6;
			$scope.t1  = res.data.t1;
			$scope.t2  = res.data.t2;
			$scope.t3  = res.data.t3;
			$scope.t4  = res.data.t4;
			$scope.t5  = res.data.t5;
			$scope.t6  = res.data.t6;
			
			//console.log(res.data);
		});
	}
	
	$scope.getAdmin = function(){
		Request.get('/getAdmin').then(function(res){
			$scope.adminList  = res.data.adminList;
			//console.log($scope.adminList);
		});
	}
	$scope.resetInsurance = function(){
		if($window.confirm('Are you sure you want to reset insurance activation ?')){
			$scope.open();
		Request.get('/resetInsurance').then(function(res){
			$scope.getInsurance();
			$scope.getNotification();
			$scope.currentUser();
			$scope.getReferral();
			$scope.getPayment();
			//console.log($scope.unRead);
			$scope.close();
		});
	}
	}

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	
	$scope.getAdmin();
	$scope.currentUser();
	

	$scope.assignRole = function(id,user_id) {
		$scope.loading = {order:true};
		$scope.postData.user_id = user_id;
		$scope.postData.role_id = id;

		console.log($scope.postData );
		
		var req = Request.post('/activate-role', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getAdmin();
					$scope.messages.success = "Role Changed successfully";
					$scope.messages.error = false;
					$scope.loading.order = false;
					
				}
			
			});
			req.error(function(res){
				$scope.messages.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
				$scope.messages.error = ''
			},3000)
		
	}

	$scope.activateInsurance = function(id) {
		$scope.postData.user_id =id;

		//console.log($scope.postData );
		 $scope.loading.insurance = true;
		// $scope.messages ={students:{success:'',error:false}}
		var req = Request.post('/activate-insurance', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.getPayment();
					$scope.getNotification();
					$scope.currentUser();
					$scope.getReferral();
					$scope.getInsurance();
					$scope.messages.success = "Activation successful";
					$scope.messages.error = false;
					$scope.loading.insurance = false;
					
					
					
				}
				$timeout(function(){
					$scope.messages.success = '';
				},1000)
			});
			req.error(function(res){
				$scope.messages.order.error = res;
				$scope.loading.order = false;
			});
			$timeout(function(){
				$scope.messages.success = '';
			},3000)
		
	}

	$scope.getFullList = function(a,b){
		var check , z, j
		z = Object.keys(b);
		angular.forEach(a, function(item) {
			check = item.user_id
			for (var i = 0; i < z.length; i++) {
			 	if(check === z[i] ){
			 		j = b[check];
			 	}
			 	
			 };
			item.amt = ''+j+'';
		})
		return a;
	}
	$scope.getPayment = function(){
		$scope.load = false;
		$scope.open();
		Request.get('/getPayment').then(function(res){
			// $scope.getPay  = res.data.payment;
			$scope.paymentList  = res.data.paymentUser;
			// $scope.share = res.data.amtShare;
			$scope.close();
		});
	}
	$scope.getProfit = function(){
		Request.get('/getProfit').then(function(res){
			$scope.sales  = res.data.earning;
			//console.log($scope.paymentList.length);

		});
	}

	$scope.getProfit();
	// $scope.getPayment();
	$scope.sendMessage = function() {
			$scope.loading = {user:true};
			var req = Request.post('/sendMessage', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.getNotification();
						$scope.currentUser();
						$scope.messages.success = "Message sent successfully";
						$scope.messages.error = false;
						$scope.loading.user = false;
						
						
					}
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.user = false;
				});
				$timeout(function(){
					$scope.messages.success = '';
					$scope.messages.error ='';
				},3000)
			
		}
	$scope.sendMail = function() {
			$scope.loading = {order:true}
			var req = Request.post('/sendMail', $scope.currentData.account);
				req.then(function(res){
					if(res.data.status){
						$scope.getNotification();
						$scope.currentUser();
						$scope.messages.success = "Emails sent successfully";
						$scope.messages.error = false;
						$scope.loading.order = false;
						
						
					}
				});
				req.error(function(res){
					$scope.messages.order.error = res;
					$scope.loading.order = false;
				});
				$timeout(function(){
					$scope.messages.success = '';
					$scope.messages.error = '';
				},3000)
			
		}	

	$scope.generate = function() {
			$scope.open();
			Request.get('/generate').then(function(res){
				//if(res.data.status){
					$scope.getNotification();
					$scope.currentUser();
					$scope.messages.success = "List generated successfully";
					var dlink =angular.element('<a/>');
					dlink.attr({
						'href':'/excel/exports/GSM-Payment-Sheet.xls',
						'target':'_self',
						'download': 'GSM-Payment-Sheet'+$scope.d+'.xls'
					})[0].click();
					$scope.close()
					
				//}

			});
			
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 9000);
			
		}	

	
	
})
.controller('UserCtrl', function ($scope, Request, $location) {
	// $scope.userId = location.pathname.split('/')[2];

	// $scope.getUser = function () {
	// 	Request.get('/users/'+$scope.userId).then(function(res){
	// 		console.log(res.data)
	// 		$scope.userInfo = res.data.userDetails;
	// 		$scope.level1  = res.data.level1;
	// 		$scope.level2  = res.data.level2;
	// 		$scope.level3  = res.data.level3;
	// 		$scope.level4  = res.data.level4;
	// 		$scope.level5  = res.data.level5;
	// 		$scope.level6  = res.data.level6;
	// 		$scope.t1  = res.data.t1;
	// 		$scope.t2  = res.data.t2;
	// 		$scope.t3  = res.data.t3;
	// 		$scope.t4  = res.data.t4;
	// 		$scope.t5  = res.data.t5;
	// 		$scope.t6  = res.data.t6;


	// 	});
	// }	
	//  $scope.getUser();

	$scope.profit = function () {
		Request.get('/totalProfit').then(function(res){
			console.log(res.data)
			$scope.total_profit = res.data.profit;

			
		});
	}	
	$scope.profit();

	$scope.userInsurance = function () {
		Request.get('/totalInsurance').then(function(res){
			console.log(res.data)
			$scope.admin1 = res.data.admin1;
			$scope.admin2  = res.data.admin2;
			$scope.admin3  = res.data.admin3;
			$scope.admin4  = res.data.admin4;
			$scope.admin5  = res.data.admin5;
			$scope.admin6  = res.data.admin6;
			$scope.topup_profit = res.data.profit_topup;
			
			
		});
	}	
	$scope.userInsurance();

	$scope.defActivity = function(id) {
			$scope.postData.setDate = id;
			
			var req = Request.post('/defActivity', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.message = "Results for Selected Date";
						$scope.admin1 = res.data.admin1;
						$scope.admin2  = res.data.admin2;
						$scope.admin3  = res.data.admin3;
						$scope.admin4  = res.data.admin4;
						$scope.admin5  = res.data.admin5;
						$scope.admin6  = res.data.admin6;
						$scope.topup_profit = res.data.profit_topup;
			

						console.log(res.data)
					}
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.user = false;
				});
				
			
		}
})
.controller('ViewUserCtrl', function ($scope, Request, $location,$fancyModal,$timeout) {
	$scope.postData = {};
	$scope.directRC = false;
	$scope.messages ={success:'',error:false}
	$scope.userId = location.pathname.split('/')[2];
	$scope.viewUser = false;
	$scope.open = function (id) {
        $fancyModal.open({ 
        	templateUrl: '/pages/popupTmpl1.html',
        	showCloseButton:true,
    		openingClass: 'animated rollIn',
	        closingClass: 'animated rollOut',
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	        closeOnOverlayClick: true,
		    controller: ['$scope', function($scope) {
	          $scope.message = id;
	        }]
        	 });
    };
    $scope.openWait = function () {
        $fancyModal.open({ 
        	templateUrl: '/pages/popupTmpl.html',
        	showCloseButton:true,
    		openingClass: 'animated rollIn',
	        closingClass: 'animated rollOut',
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	        closeOnOverlayClick: true,
		  
        	 });
    };
    $scope.close = function () {
    	$fancyModal.close();
    }
    $scope.checkDirectRC = function () {
		$scope.openWait();
		Request.get('/check-direct-rc/'+$scope.userId).then(function(res){
			console.log(res.data)
			$scope.directRC = res.data.total;

			$scope.close();
		});
	}	
	$scope.getUser = function () {
		$scope.openWait();
		Request.get('/user/level/'+$scope.userId).then(function(res){
			console.log(res.data)
			
			$scope.viewUser = true;
			$scope.userInfo = res.data.userDetails;
			$scope.referrer = res.data.referrer;
			$scope.level1  = res.data.level1;
			$scope.level2  = res.data.level2;
			$scope.level3  = res.data.level3;
			$scope.level4  = res.data.level4;
			$scope.level5  = res.data.level5;
			$scope.level6  = res.data.level6;
			$scope.t1  = res.data.t1;
			$scope.t2  = res.data.t2;
			$scope.t3  = res.data.t3;
			$scope.t4  = res.data.t4;
			$scope.t5  = res.data.t5;
			$scope.t6  = res.data.t6;

			$scope.close();
		});
	}	
	 $scope.getUser();
	$scope.getPremiumUser = function () {
		$scope.openWait();
		Request.get('/premium-user/level/'+$scope.userId).then(function(res){
			console.log(res.data)
			
			$scope.viewUser = true;
			$scope.userPremiumInfo = res.data.userDetails;
			$scope.premium_referrer = res.data.referrer;
			$scope.premium_level1  = res.data.level1;
			$scope.premium_level2  = res.data.level2;
			$scope.premium_level3  = res.data.level3;
			$scope.premium_evel4  = res.data.level4;
			$scope.premium_level5  = res.data.level5;
			$scope.premium_level6  = res.data.level6;
			$scope.premium_t1  = res.data.t1;
			$scope.premium_t2  = res.data.t2;
			$scope.premium_t3  = res.data.t3;
			$scope.premium_t4  = res.data.t4;
			$scope.premium_t5  = res.data.t5;
			$scope.premium_t6  = res.data.t6;

			$scope.close();
		});
	}	
	 $scope.getPremiumUser();
	$scope.resetPassword = function(id) {
				$scope.postData.user_id = id;
				$scope.loading ={password:true};
				var req = Request.post('/resetpassword', $scope.postData);
					req.then(function(res){
						if(res.data.status){
							$scope.messages.success = "Password reset successful";
							$scope.messages.error = false;
							$scope.loading.password = false;
							$scope.postData = {};
						}else{
							$scope.messages.success = res.data.message;
							$scope.loading.password = false;
						}
					
					});
					req.error(function(res){
						$scope.messages.error = res;
						$scope.loading.password = false;
					});
					$timeout(function() {
				       $scope.messages.success = '';
						$scope.messages.errorMessage = '';
				    }, 9000);
					
				
			}
	$scope.changeUsername = function(id) {
				console.log('lol');
				$scope.postData.user_id = id;
				// $scope.postData.username = id;
				$scope.loading ={user:true};
				var req = Request.post('/change-username', $scope.postData);
					req.then(function(res){
						if(res.data.status){
							$scope.messages.success = "Username change successful";
							$scope.messages.error = false;
							$scope.loading.user = false;
						}else{
							$scope.messages.success = res.data.message;
							$scope.loading.user = false;
						}
					
					});
					req.error(function(res){
						$scope.messages.error = res;
						$scope.loading.user = false;
					});
					$timeout(function() {
				       $scope.messages.success = '';
						$scope.messages.errorMessage = '';
				    }, 9000);
					
				
			}

		$scope.changePremiumBonus = function(id) {
			console.log('lol');
			$scope.postData.user_id = id;
			// $scope.postData.username = id;
			$scope.loading ={bonus:true};
			var req = Request.post('/change-bonus', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Bonus Update successful";
						$scope.messages.error = false;
						$scope.loading.bonus = false;
					}else{
						$scope.messages.success = res.data.message;
						$scope.loading.bonus = false;
					}
				
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.user = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
				
			
		}
	
})

.controller('regularWalletCtrl', function ($scope,Request,$filter,$window,$fancyModal,$timeout,$interval,TransactionService) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:false,errorMessage:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.walletHistory = "";
	$scope.walletBalance ="";
	$scope.postData.mobile = false;

	$scope.userPin = false;
	
	$scope.openWait = function () {
        $fancyModal.open({ 
        	templateUrl: '/pages/popupTmpl.html',
        	showCloseButton:true,
    		openingClass: 'animated rollIn',
	        closingClass: 'animated rollOut',
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	        closeOnOverlayClick: true,
		  
        	 });
    };
    $scope.close = function () {
    	$fancyModal.close();
    }
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	
	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.getWalletBalance();
			// $scope.getPin();
			// soso
		});
	}
	
	$scope.getWalletHistory = function(data){
		var check = TransactionService.getTransactionHistory();
		if (check != null && data == 0) {
			alert('hello');
			$scope.walletHistory = check;
		}else{
			$scope.openWait();
			Request.get('/accounts/transactions/'+$scope.currentData.user_id).then(function(res){
			$scope.walletHistory  =  res.data.transactions;
			TransactionService.saveTransactionHistory(res.data.transactions);
				//console.log($scope.walletHistory);
				$scope.close();
			});
		}
		
	}

	$scope.getWalletBalance = function(){
		Request.get('/accounts/'+$scope.currentData.user_id+'/balance').then(function(res){
			$scope.walletBalance  = res.data.balance;
			console.log($scope.walletBalance);
		});
	}
	
	$scope.resetBalance = function() {
		delete $scope.walletBalance;
	}
	
	$scope.currentUser();
	// $scope.getWalletBalance();
	
	
	$scope.changePin = function() {
		var req = Request.post('/users/'+$scope.currentData.user_id+'/pin', $scope.postData);
			req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Pin Change successful";
						$scope.loading.change = false;
						$scope.postData = {};
						$scope.getPin();
					}
				});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.change = false;
			});
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 3000);			
	}
	$scope.check = function() {

		if ($scope.postData.mobile) {
			console.log($scope.postData.mobile);
			$scope.postData.mobile = true;
		}else{
			$scope.postData.mobile = false;
			console.log($scope.postData.mobile);
		}
		 
	}
	$scope.withdrawalRequest = function() {

		$scope.loading = {withdrawal:true} ;
		$scope.postData.user_id = $scope.currentData.user_id;
		console.log($scope.postData)
		var req = Request.post('/accounts/withdrawal', $scope.postData);
			req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Request successfully made";
						$scope.loading.withdrawal = false;
						$scope.postData = {};
					}else{
						$scope.messages.success = res.data.message;
					}
				});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.withdrawal = false;
			});
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 3000);			
	}
	$scope.getWithdrawal = function() {
		$scope.loading = {withdrawal:true};
		    var req = Request.get('/accounts/withdrawal/'+$scope.currentData.user_id);
			req.then(function(res){
				$scope.withdrawal = res.data.withdrawal;
				$scope.loading.withdrawal = false;
				console.log($scope.withdrawal);
			});
			
			
		}

	$scope.resetPin = function() {
		delete $scope.userPin;
	}
	$scope.getPin = function() {
		$scope.loading = {pin:true};
		    var req = Request.get('/users/'+$scope.currentData.user_id+'/pin');
			req.then(function(res){
				$scope.userPin = res.data.pin;
				$scope.loading.pin = false;
				console.log(res.data);
			});
			
			
		}
	
	$scope.transferBalance = function() {

		$scope.postData.sender_id = $scope.currentData.user_id;
		console.log($scope.postData)
		$scope.loading = {transfer:true}
		var req = Request.post('/accounts/usertransfer', $scope.postData);
			req.then(function(res){
				if(res.data.status){
					$scope.messages.success = "Fund Transfer successful";
					$scope.loading.transfer = false;
					$scope.getWalletBalance();
					$scope.postData = {};
				}else{
					$scope.messages.success = res.data.message;
					$scope.loading.transfer = false;
				}
			});
			req.error(function(res){
				$scope.messages.errorMessage = res;
				$scope.loading.transfer = false;
			});
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 3000);
			
		}
	
	
})


.controller('passwordCtrl', function ($scope,Request,$filter,$timeout) {
	$scope.postData = {  };
	$scope.messages ={success:'',error:''}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	
	
	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	

	

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			console.log($scope.currentData);
		});
	}
	
	
	
	$scope.currentUser();
	

	$scope.changePassword = function() {
			$scope.postData.user_id = $scope.currentData.user_id;
			$scope.loading.password = true;
			var req = Request.post('/changepassword', $scope.postData);
				req.then(function(res){
					if(res.data.status){
						$scope.messages.success = "Password changed successfully";
						$scope.messages.error = false;
						$scope.loading.password = false;
						$scope.postData = {};
					}else{
						$scope.messages.success = res.data.message;
						$scope.loading.password = false;
					}
				
				});
				req.error(function(res){
					$scope.messages.error = res;
					$scope.loading.password = false;
				});
				$timeout(function() {
			       $scope.messages.success = '';
					$scope.messages.errorMessage = '';
			    }, 9000);
				
			
		}
	
})
.controller('AdminInsuranceCtrl', function ($scope,Request,$filter,$window,$fancyModal,$timeout,UserService) {
	$scope.load = true;
	$scope.loadRank = true;
	$scope.loadPremium = true;
	$scope.postData = {};
	$scope.open = function () {
        $fancyModal.open({ 
        		templateUrl: '/pages/popupTmpl.html',
        		showCloseButton:false,
        		openingClass: 'animated rollIn',
		        closingClass: 'animated rollOut',
		        openingOverlayClass: 'animated fadeIn',
		        closingOverlayClass: 'animated fadeOut',
		        closeOnOverlayClick: false
         		});
        
    };

    $scope.close = function () {
    	$fancyModal.close();
    }
	$scope.postData = {  };
	$scope.messages ={success:'',error:false}
	$scope.loading ={logs:false};
	$scope.checkStatus = false;
	$scope.read = {};
	$scope.unRead = {};
	$scope.sales = {};

	 $scope.items = [
    { id: 1, name: 'GSMadmin'},
    { id: 2, name: 'GSM 1'},
    { id: 3, name: 'GSM 2'},
    { id: 4, name: 'GSM 3'},
    { id: 5, name: 'GSM 4'},
    { id: 6, name: 'GSM 5'},
    { id: 7, name: 'GSM 6'}];
    

	//$scope.paymentList = {};

	// $scope.User = {}
	var date = new Date();
	$scope.d = $filter('date')(new Date(),'dd/MM/yyyy');
	

	$scope.getNotification = function(){
		Request.get('/getNotification').then(function(res){
			$scope.read  = res.data.readNotification;
			$scope.unRead = res.data.unReadNotification; 
			
			//console.log($scope.unRead);
		});
	}

	$scope.currentUser = function(){
		Request.get('/currentUser').then(function(res){
			$scope.currentData  = res.data.currentUser;
			$scope.currentData.choice = 0;
			$scope.currentData.choiceUser = 0;
			$scope.currentData.choicePhone = 0;
			//console.log($scope.currentData.account);
		});
	}

	
	$scope.resetInsurance = function(){
		if($window.confirm('Are you sure you want to reset insurance activation ?')){
			$scope.postData.list = $scope.paymentList
			$scope.open();
		Request.post('/resetInsurance',$scope.postData).then(function(res){
			$scope.getPayment();
			//console.log($scope.unRead);
			$scope.close();
		});
	}
	}

	$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    //Generating payment list
	$scope.full = true;
	$scope.getPayment = function(){
		$scope.load = false;
		$scope.open();
		$scope.currentpage=1;
        UserService.payUsers($scope.currentpage)
            .then(function (res) {
            	if (res) {
            		$scope.paymentList  = res.data.paymentUser;
	                $scope.currentpage = res.data.paginate.current_page;
	                $scope.lastpage = res.data.paginate.last_page;
	                console.log($scope.currentpage)
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getPayment();
	$scope.objpush = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.paymentList.push(todo)
	    });
	    return $scope.paymentList;
	}
	$scope.loadMore = function() {
		$scope.open();
      	$scope.r = $scope.currentpage+1;
        UserService.payUsers($scope.r)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.paginate.current_page < res.data.paginate.last_page){
        		$scope.currentpage = res.data.paginate.current_page;
        		$scope.objpush(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.loadMore();
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		// $scope.currentpage = res.data.paginate.current_page;
        		// $scope.objpush(res.data.paymentUser);
        		// console.log($scope.currentpage);
        		$scope.full = false;
        	}
        	
        	$scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };

	$scope.generateXl = function() {
			$scope.open();
			$scope.postData.list = $scope.paymentList
			Request.post('/generate',$scope.postData).then(function(res){
				//if(res.data.status){
					$scope.getNotification();
					$scope.currentUser();
					$scope.messages.success = "List generated successfully";
					var dlink =angular.element('<a/>');
					dlink.attr({
						'href':'/excel/exports/GSM-Payment-Sheet.xls',
						'target':'_self',
						'download': 'GSM-Payment-Sheet'+$scope.d+'.xls'
					})[0].click();
					$scope.close()
					
				//}

			});
			
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 9000);
			
		}	


//Premium User  Generating payment list
	$scope.fullPremium = true;
	$scope.getPremiumPayment = function(){
		$scope.loadPremium = false;
		$scope.open();
		$scope.currentpage=1;
        UserService.payPremiumUsers($scope.currentpage)
            .then(function (res) {
            	if (res) {
            		$scope.paymentListPremium  = res.data.paymentUser;
	                $scope.currentpage = res.data.paginate.current_page;
	                $scope.lastpage = res.data.paginate.last_page;
	                console.log($scope.currentpage)
	                localStorage.payPremiumUsers = JSON.stringify($scope.paymentListPremium);
	                localStorage.payPremiumPage = $scope.currentpage;
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getPayment();
	$scope.objpushPremium = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.paymentListPremium.push(todo)
	    });
	    return $scope.paymentListPremium;
	}
	$scope.loadMorePremium = function() {
		// $scope.open();
      	$scope.r = $scope.currentpage+1;
        UserService.payPremiumUsers($scope.r)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.paginate.current_page <= res.data.paginate.last_page){
        		$scope.currentpage = res.data.paginate.current_page;
        		$scope.objpushPremium(res.data.paymentUser);
        		console.log($scope.currentpage);

        		localStorage.payPremiumUsers = JSON.stringify($scope.paymentListPremium);
	            localStorage.payPremiumPage = $scope.currentpage;


        		$scope.loadMorePremium();
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		// $scope.currentpage = res.data.paginate.current_page;
        		// $scope.objpush(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.fullPremium = false;
        	}
        	
        	$scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };


	$scope.generateXlPremium = function() {
			$scope.open();
			$scope.postData.listPremium = $scope.paymentListPremium
			Request.post('/generatePremium',$scope.postData).then(function(res){
				//if(res.data.status){
					$scope.getNotification();
					$scope.currentUser();
					$scope.messages.success = "Premium List generated successfully";
					var dlink =angular.element('<a/>');
					dlink.attr({
						'href':'/excel/exports/GSM-Payment-Premium-Sheet.xls',
						'target':'_self',
						'download': 'GSM-Payment-Premium-Sheet'+$scope.d+'.xls'
					})[0].click();
					$scope.close()
					
				//}

			});
			
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 9000);
			
		}	

	$scope.resetPremiumInsurance = function(){
		if($window.confirm('Are you sure you want to clear premium insurance claims ?')){
			$scope.postData.listPremium = $scope.paymentListPremium;
			console.log($scope.postData.listPremium)
			$scope.open();
			Request.post('/resetPremiumInsurance',$scope.postData).then(function(res){
				$scope.getPremiumPayment();
				$scope.close();
			});
		}
	}

	//Premium User  Rank list
	$scope.fullRank = true;
	$scope.showMore = false;
	$scope.lastpage = 0;
	$scope.currentpage = 0;
	$scope.getRankPayment = function(){
		$scope.loadRank = false;
		$scope.open();
		$scope.currentpage=1;
        UserService.payRankUsers($scope.currentpage)
            .then(function (res) {
            	if (res) {
            		$scope.paymentRankList = res.data.paymentUser;
	                $scope.currentpage = res.data.paginate.current_page;
	                $scope.lastpage = res.data.paginate.last_page;
	                console.log($scope.currentpage)
	                $scope.showMore = true;
            	}
            	$scope.close();
               
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
		
	}
	// $scope.getPayment();
	$scope.objpushRank = function(user) {
	    angular.forEach(user, function(todo) {
	      $scope.paymentRankList.push(todo)
	    });
	    return $scope.paymentRankList;
	}
	$scope.loadRanksMore = function() {
		// $scope.open();
      	$scope.r = $scope.currentpage+1;
        UserService.payRankUsers($scope.r)
        .then(function (res) {
        	// console.log(res.data.data);
        	if (res.data.paginate.current_page <= res.data.paginate.last_page){
        		$scope.currentpage = res.data.paginate.current_page;
        		$scope.objpushRank(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.loadRanksMore();
        	}
        	if (res.data.paginate.current_page == res.data.paginate.last_page){
        		// $scope.currentpage = res.data.paginate.current_page;
        		// $scope.objpush(res.data.paymentUser);
        		console.log($scope.currentpage);
        		$scope.fullRank = false;
        	}
        	
        	$scope.close();
        }, function (error) {
            $scope.status = 'Unable to load data: ' + error.message;
        });
        
    };

	$scope.generateRanksExcel = function() {
			$scope.open();
			$scope.postData.listRank = $scope.paymentRankList;
			Request.post('/generate/ranks',$scope.postData).then(function(res){
				//if(res.data.status){
					$scope.messages.success = "Rank List generated successfully";
					var dlink =angular.element('<a/>');
					dlink.attr({
						'href':'/excel/exports/GSM-Rank-Sheet.xls',
						'target':'_self',
						'download': 'GSM-Rank-Sheet'+$scope.d+'.xls'
					})[0].click();
					$scope.close()
					
				//}

			});
			
			$timeout(function() {
		       $scope.messages.success = '';
				$scope.messages.errorMessage = '';
		    }, 9000);
			
		}	

	
})
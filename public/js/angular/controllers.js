angular.module('gsmApp')

    .controller('AppCtrl', function($scope, Request, NotificationService, $timeout, UserService, $http, $rootScope, $fancyModal) {
        $scope.postData = {};
        $scope.loading = {};
        $scope.getNotification = function() {
            Request.get('/getNotification').then(function(res) {
                $rootScope.read = res.data.readNotification;
                $rootScope.unRead = res.data.unReadNotification;

                //console.log($scope.read.length);
            });
        }
        $scope.getNotification();

        $scope.getCurrentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $rootScope.currentData = res.data.currentUser;
                $rootScope.currentData.choice = 0;
                $rootScope.currentData.choicePhone = 0;
                $rootScope.currentData.choiceUser = 0;
                $rootScope.carrier = $scope.currentData.carrier;

                UserService.saveToken(res.data.currentUser.token);
                UserService.saveCurrentUser(res.data.currentUser);
                $rootScope.currentUser = res.data.currentUser;
                console.log(res.data.currentUser);
            });
        }


        // console.log();

        if (!UserService.getCurrentUser()) {
            $scope.getCurrentUser();
        } else {
            $rootScope.currentUser = UserService.getCurrentUser();
            $rootScope.currentData = UserService.getCurrentUser();
        }

        $scope.getCurrentUser();

        $scope.markRead = function(id) {
            $scope.postData.notification_id = id;
            $scope.loading = { order: true };
            var req = Request.post('/markRead', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    NotificationService.send('success', 'Message Moved Successfully');
                    $scope.loading.order = false;


                }

            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });

        }
        $rootScope.openWait = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: true,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: true,

            });
        };
        $rootScope.close = function() {
            $fancyModal.close();
        }


        $rootScope.open = function(id) {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl1.html',
                showCloseButton: true,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: true,
                controller: ['$scope', function($scope) {
                    $scope.message = id;
                }]
            });
        };

        $scope.updateUser = function() {
            $scope.loading = { user: true };
            if (typeof($scope.currentData.file) !== 'undefined') {
                Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
                    .then(function(res) {
                        $scope.currentData.image = res.data.imageUrl;
                        if ($scope.currentData.choiceUser == 0) {
                            $scope.currentData.username = '';
                        }
                        if ($scope.currentData.choice == 0) {
                            $scope.currentData.password = '';
                            $scope.currentData.password_confirmation = '';
                        }
                        //console.log($scope.currentData);
                        var req = Request.post('/user-management/updateUser', $scope.currentData);
                        req.then(function(res) {
                            if (res.data.status) {
                                NotificationService.send('success', res.data.message);

                                $scope.messages.error = false;
                                $scope.loading.user = false;

                            } else {
                                NotificationService.send('error', res.data.message);
                            }

                        });
                        req.error(function(err) {
                            NotificationService.send('error', res.data.message);
                            $scope.loading.user = false;
                        });

                    })
            } else {
                $scope.currentData.image = 0;
                if ($scope.currentData.choiceUser == 0) {
                    $scope.currentData.username = '';
                }
                if ($scope.currentData.choice == 0) {
                    $scope.currentData.password = '';
                    $scope.currentData.password_confirmation = '';
                }
                var req = Request.post('/user-management/updateUser', $scope.currentData);
                req.then(function(res) {
                    if (res.data.status) {
                        NotificationService.send('success', res.data.message);

                        $scope.messages.error = false;
                        $scope.loading.user = false;

                    } else {
                        NotificationService.send('error', res.data.message);
                    }
                });
                req.error(function(res) {
                    $scope.messages.error = res;
                    $scope.loading.user = false;
                });

            }
        }

    })
    .controller('MobileMoneyCtrl', function($scope, $rootScope,Request, $filter, $window, $timeout, UserService, NotificationService) {
        $scope.postData = {};

        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $rootScope.currentUser = UserService.getCurrentUser();
        //console.log($rootScope.currentUser);
        $scope.postData.mtn_number = $rootScope.currentUser.phone;

        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.requestWalletLoad = function() {
            $scope.loading = { user: true }
            var req = Request.post('/mtn-load-wallet', $scope.postData);
            req.then(function(res) {
                if (res.data.success) {
                    NotificationService.send('success', "Prompt Successfully Sent");
                    $scope.postData.amount = '';
                } else {
                    NotificationService.send('error', "Wallet Load request not sent");

                }
                $scope.loading.user = false;
            });
            req.error(function(res) {
                NotificationService.send('error', "Error Occured");
                $scope.loading.user = false;
            });


        }

        $scope.requestDirectWalletLoad = function() {
            $scope.loading = { user: true }
             NotificationService.send('success', "Check Screen for Prompt");
            var req = Request.post('/gt-direct-load-wallet', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Wallet Load Successful");
                    $scope.postData.amount = '';
                } else {
                    NotificationService.send('error', "Wallet Load request failed");

                }
                $scope.loading.user = false;
            });
            req.error(function(res) {
                NotificationService.send('error', "Error Occured");
                $scope.loading.user = false;
            });


        }


        $scope.requestGtLoad = function() {
            $scope.loading = { user: true }
            var req = Request.post('/gt-load-wallet', $scope.postData);
            req.then(function(res) {
                if (res.data.success) {
                    NotificationService.send('success', "Prompt Successfully Sent");
                    $scope.postData.amount = '';
                } else {
                    NotificationService.send('error', "Wallet Load request not sent");

                }
                $scope.loading.user = false;
            });
            req.error(function(res) {
                NotificationService.send('error', "Error Occured");
                $scope.loading.user = false;
            });


        }



        $scope.topup = function() {
            $scope.loading = { user: true }
            var req = Request.post('/topup-airtime', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    NotificationService.send('success', res.data.msg);


                } else {
                    NotificationService.send('error', res.data.msg);

                }
                $scope.loading.user = false;
            });
            req.error(function(res) {
                NotificationService.send('error', "Error Occured");
                $scope.loading.user = false;
            });


        }



    })
    .controller('WalletCtrl', function($scope, $rootScope, Request, $filter, $window, $fancyModal, $timeout, $interval, UserService, NotificationService, TransactionService) {
        $scope.postData = {};
        $scope.messages = { success: '', error: false, errorMessage: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $scope.walletHistory = "";
        $scope.walletBalance = "";
        $scope.postData.mobile = false;

        $scope.userPin = false;
        $rootScope.currentUser = UserService.getCurrentUser();

        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.getWalletHistory = function(data) {
            var check = TransactionService.getTransactionHistory();
            if (check != null && data == 0) {

                $scope.walletHistory = check;
            } else {
                $rootScope.openWait();
                Request.get('/accounts/transactions/' + $rootScope.currentUser.user_id).then(function(res) {
                    $scope.walletHistory = res.data.transactions;
                    TransactionService.saveTransactionHistory(res.data.transactions);
                    //console.log($scope.walletHistory);
                    $rootScope.close();
                });
            }

        }

        $scope.getWalletBalance = function() {
            Request.get('/accounts/' + $rootScope.currentUser.user_id + '/balance').then(function(res) {
                $scope.walletBalance = res.data.balance;

            });
        }

        $scope.resetBalance = function() {
            delete $scope.walletBalance;
        }



        $scope.changePin = function() {
            var req = Request.post('/users/' + $rootScope.currentUser.user_id + '/pin', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Pin Change Successful");
                    $scope.loading.change = false;
                    $scope.postData = {};
                    $scope.getPin();
                }
            });
            req.error(function(res) {
                NotificationService.send('error', "System Error");
                $scope.loading.change = false;
            });

        }
        $scope.check = function() {

            if ($scope.postData.mobile) {

                $scope.postData.mobile = true;
            } else {
                $scope.postData.mobile = false;

            }

        }
        $scope.withdrawalRequest = function() {

            $scope.loading = { withdrawal: true };
            $scope.postData.user_id = $rootScope.currentUser.user_id;
            var req = Request.post('/accounts/withdrawal', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Request Successfully made");
                    $scope.loading.withdrawal = false;
                    $scope.postData = {};
                } else {
                    NotificationService.send('error', res.data.message);
                }
            });
            req.error(function(res) {
                NotificationService.send('error', "System Error");
                $scope.loading.withdrawal = false;
            });

        }

        $scope.getWithdrawal = function() {
            $scope.loading = { withdrawal: true };
            var req = Request.get('/accounts/withdrawal/' + $rootScope.currentUser.user_id);
            req.then(function(res) {
                $scope.withdrawal = res.data.withdrawal;
                $scope.loading.withdrawal = false;

            });


        }

        $scope.resetPin = function() {
            delete $scope.userPin;
        }
        $scope.getPin = function() {
            $scope.loading = { pin: true };
            var req = Request.get('/users/' + $rootScope.currentUser.user_id + '/pin');
            req.then(function(res) {
                $scope.userPin = res.data.pin;
                $scope.loading.pin = false;

            });


        }

        $scope.transferBalance = function() {

            $scope.postData.sender_id = $rootScope.currentUser.user_id;
            $scope.loading = { transfer: true }
            var req = Request.post('/accounts/usertransfer', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Fund Transfer successful");
                    $scope.loading.transfer = false;
                    $scope.getWalletBalance();
                    $scope.postData = {};
                } else {
                    NotificationService.send('error', res.data.message);
                    $scope.loading.transfer = false;
                }
            });
            req.error(function(res) {
                NotificationService.send('error', "Prompt Successfully Sent");
                $scope.loading.transfer = false;
            });


        }


    })
    .controller('passwordCtrl', function($scope, $rootScope, Request, $filter, $timeout, NotificationService, UserService) {
        $scope.postData = {};
        $scope.messages = { success: '', error: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $rootScope.currentUser = UserService.getCurrentUser();

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.changePassword = function() {
            $scope.postData.user_id = $rootScope.currentUser.user_id;
            $scope.loading.password = true;
            var req = Request.post('/changepassword', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', res.data.message);

                    $scope.messages.error = false;
                    $scope.loading.password = false;
                    $scope.postData = {};
                } else {
                    NotificationService.send('error', res.data.message)
                    $scope.loading.password = false;
                }

            });
            req.error(function(res) {
                NotificationService.send('success', res.data)
                $scope.loading.password = false;
            });


        }

    })
    .controller('StatementCtrl', function($scope, $rootScope, Request, $filter, $window, $fancyModal, $timeout, UserService, NotificationService) {
        $scope.load = true;
        $scope.loadRank = true;
        $scope.loadPremium = true;
        $scope.postData = {};
        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });

        };

        $scope.close = function() {
            $fancyModal.close();
        }

        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.sales = {};


        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        //Generating payment list
        $scope.generateTransactionStatement = function() {

            $scope.open();
            // $scope.postData.list = $scope.paymentList
            Request.post('/generate-transaction-statement', $scope.postData).then(function(res) {
                // console.log(res);
                if (res.data.status) {
                    NotificationService.send('success', res.data.message);
                    var dlink = angular.element('<a/>');
                    dlink.attr({
                        'href': '/excel/exports/transaction_statement_' + $rootScope.currentUser.user_id + '.xlsx',
                        'target': '_self',
                        'download': 'Transaction Statement.xlsx'
                    })[0].click();
                    $scope.close()

                } else {
                    $scope.close()
                    NotificationService.send('error', res.data.message);

                }

            });


        }



        $scope.generateAccountStatement = function() {

            $scope.open();
            // $scope.postData.list = $scope.paymentList
            Request.post('/generate-month-transaction-statement', $scope.postData).then(function(res) {

                if (res.data.status) {
                    // $scope.getNotification();
                    NotificationService.send('success', res.data.message);

                    var dlink = angular.element('<a/>');
                    dlink.attr({
                        'href': '/excel/exports/transaction_statement_' + $rootScope.currentUser.user_id + '.xlsx',
                        'target': '_self',
                        'download': 'Transaction Statement.xlsx'
                    })[0].click();
                    $scope.close()

                } else {
                    $scope.close()
                    NotificationService.send('error', res.data.message);

                }

            });

        }



    })



    //Done with update
    .controller('regularUserCtrl', function($scope, $rootScope, NotificationService, UserService, Request, $filter, $window, $interval, $timeout, $fancyModal, DisplayService) {
        $scope.postData = {};
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};

        $rootScope.currentUser = UserService.getCurrentUser();

        console.log($rootScope.currentUser)
        $scope.pt1 = 0;
        $scope.pt2 = 0;
        $scope.pt3 = 0;
        $scope.pt4 = 0;
        $scope.pt5 = 0;
        $scope.pt6 = 0;

        $scope.t1 = 0;
        $scope.t2 = 0;
        $scope.t3 = 0;
        $scope.t4 = 0;
        $scope.t5 = 0;
        $scope.t6 = 0;

        $scope.tB1 = 0;
        $scope.tB2 = 0;
        $scope.tB3 = 0;
        $scope.tB4 = 0;

        $scope.showLevel1 = false;
        $scope.showLevel2 = false;
        $scope.showLevel3 = false;
        $scope.showLevel4 = false;
        $scope.showLevel5 = false;
        $scope.showLevel6 = false;

        $scope.showBLevel1 = false;
        $scope.showBLevel2 = false;
        $scope.showBLevel3 = false;
        $scope.showBLevel4 = false;


        $scope.showPremiumLevel1 = false;
        $scope.showPremiumLevel2 = false;
        $scope.showPremiumLevel3 = false;
        $scope.showPremiumLevel4 = false;
        $scope.showPremiumLevel5 = false;
        $scope.showPremiumLevel6 = false;


        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }

        console.log($rootScope.currentUser);
        $rootScope.currentUser = UserService.getCurrentUser();

        $scope.getPremiumReferralFigure = function() {

            $rootScope.openWait();
            Request.get('/get-premium-qualification').then(function(res) {

                $scope.close();

                $scope.userRankCount = res.data.paymentUser;

            });
        }

        $scope.getAgentReferral = function() {


            Request.get('/agent-referral').then(function(res) {

                $scope.agentRef = res.data.ref;
                $scope.totalRef = 0.000000;
                angular.forEach($scope.agentRef, function(value, key) {
                    var j = 0.000000;
                    if (value.commisions.length > 0) {
                        angular.forEach(value.commisions, function(value, key) {

                            j = j + value.amount;
                        });

                    }
                    value.totalCom = j;
                    $scope.totalRef = $scope.totalRef + value.totalCom;

                    // console.log($scope.totalRef);
                }, $scope.agentRef);

                // console.log($scope.totalRef);

            });
        }



        $scope.getRefL1 = function() {

            $rootScope.openWait();
            Request.get('/getReferral/level1').then(function(res) {
                $scope.showLevel1 = true;
                $scope.close();
                console.log(res);
                $scope.level1 = res.data.level1;
                // $scope.level_2 = res.data.nextLevel;
                // $scope.t1 = res.data.total1;
                angular.forEach(res.data.level1, function(value, key) {
                    $scope.t1 = $scope.t1 + value.amt;
                });
            });
        }


        //$scope.getRefL1();

        $scope.getRefL2 = function(data) {
            $rootScope.openWait();
            $scope.postData.l2 = data;
            Request.post('/getReferral/level2', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showLevel2 = true;
                $scope.level2 = res.data.level2;
                angular.forEach(res.data.level2, function(value, key) {
                    $scope.t2 = $scope.t2 + value.amt;
                });
                console.log($scope.level2)
            });
        }

        $scope.getRefL3 = function(data) {
            $rootScope.openWait();
            $scope.postData.l3 = data;
            Request.post('/getReferral/level3', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showLevel3 = true;
                $scope.level3 = res.data.level3;
                angular.forEach(res.data.level3, function(value, key) {
                    $scope.t3 = $scope.t3 + value.amt;
                });
                console.log($scope.level3)
            });
        }

        $scope.getRefL4 = function(data) {
            $rootScope.openWait();
            $scope.postData.l4 = data;
            Request.post('/getReferral/level4', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showLevel4 = true;
                $scope.level4 = res.data.level4;
                angular.forEach(res.data.level4, function(value, key) {
                    $scope.t4 = $scope.t4 + value.amt;
                });
                console.log($scope.level4)
            });
        }

        $scope.getRefL5 = function(data) {
            $rootScope.openWait();
            $scope.postData.l5 = data;
            Request.post('/getReferral/level5', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showLevel5 = true;
                $scope.level5 = res.data.level5;
                angular.forEach(res.data.level5, function(value, key) {
                    $scope.t5 = $scope.t5 + value.amt;
                });
                console.log($scope.level5)
            });
        }

        $scope.getRefL6 = function(data) {
            $rootScope.openWait();
            $scope.postData.l6 = data;
            Request.post('/getReferral/level6', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showLevel6 = true;
                $scope.level6 = res.data.level6;
                angular.forEach(res.data.level6, function(value, key) {
                    $scope.t6 = $scope.t6 + value.amt;
                });
                console.log($scope.level6)
            });
        }

        //bagam Tree
         $scope.getRefBL1 = function() {

            $rootScope.openWait();
            Request.get('/getBagamReferral/level1').then(function(res) {
                $scope.showBLevel1 = true;
                $scope.close();
                console.log(res);
                $scope.Blevel1 = res.data.level1;
                // $scope.level_2 = res.data.nextLevel;
                // $scope.t1 = res.data.total1;
                // angular.forEach(res.data.level1, function(value, key) {
                //     $scope.tB1 = $scope.tB1 + value.amt;
                // });
            });
        }


        //$scope.getRefBL1();

        $scope.getRefBL2 = function(data) {
            $rootScope.openWait();
            $scope.postData.l2 = data;
            console.log($scope.postData);
            Request.post('/getBagamReferral/level2', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showBLevel2 = true;
                $scope.Blevel2 = res.data.level2;
                // angular.forEach(res.data.level2, function(value, key) {
                //     $scope.tB2 = $scope.tB2 + value.amt;
                // });
                console.log($scope.Blevel2)
            });
        }

        $scope.getRefBL3 = function(data) {
            $rootScope.openWait();
            $scope.postData.l3 = data;
            Request.post('/getBagamReferral/level3', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showBLevel3 = true;
                $scope.Blevel3 = res.data.level3;
                // angular.forEach(res.data.level3, function(value, key) {
                //     $scope.tB3 = $scope.tB3 + value.amt;
                // });
                console.log($scope.Blevel3)
            });
        }

        $scope.getRefBL4 = function(data) {
            $rootScope.openWait();
            $scope.postData.l4 = data;
            Request.post('/getBagamReferral/level4', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showBLevel4 = true;
                $scope.Blevel4 = res.data.level4;
                // angular.forEach(res.data.level4, function(value, key) {
                //     $scope.tB4 = $scope.tB4 + value.amt;
                // });
                console.log($scope.Blevel4)
            });
        }

        //Premium Tree New Look


        $scope.getPremiumRefL1 = function() {

            $rootScope.openWait();
            Request.get('/getPremiumReferral/level1').then(function(res) {
                $scope.showPremiumLevel1 = true;
                $scope.close();
                console.log(res);
                $scope.plevel1 = res.data.level1;
                angular.forEach(res.data.plevel1, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt1 = $scope.pt1 + 1;
                    }

                });
            });
        }


        //$scope.getRefL1();

        $scope.getPremiumRefL2 = function(data) {
            $rootScope.openWait();
            $scope.postData.pl2 = data;
            Request.post('/getPremiumReferral/level2', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showPremiumLevel2 = true;
                $scope.plevel2 = res.data.level2;
                angular.forEach(res.data.plevel2, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt2 = $scope.pt2 + 1;
                    }
                });
                console.log($scope.plevel2)
            });
        }

        $scope.getPremiumRefL3 = function(data) {
            $rootScope.openWait();
            $scope.postData.pl3 = data;
            Request.post('/getPremiumReferral/level3', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showPremiumLevel3 = true;
                $scope.plevel3 = res.data.level3;
                angular.forEach(res.data.plevel3, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt3 = $scope.pt3 + 1;
                    }
                });
                console.log($scope.plevel3)
            });
        }

        $scope.getPremiumRefL4 = function(data) {
            $rootScope.openWait();
            $scope.postData.pl4 = data;
            Request.post('/getPremiumReferral/level4', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showPremiumLevel4 = true;
                $scope.plevel4 = res.data.level4;
                angular.forEach(res.data.plevel4, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt4 = $scope.pt4 + 1;
                    }
                });
                console.log($scope.plevel4)
            });
        }

        $scope.getPremiumRefL5 = function(data) {
            $rootScope.openWait();
            $scope.postData.pl5 = data;
            Request.post('/getPremiumReferral/level5', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showPremiumLevel5 = true;
                $scope.plevel5 = res.data.level5;
                angular.forEach(res.data.plevel5, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt5 = $scope.pt5 + 1;
                    }
                });
                console.log($scope.plevel5)
            });
        }

        $scope.getPremiumRefL6 = function(data) {
            $rootScope.openWait();
            $scope.postData.pl6 = data;
            Request.post('/getPremiumReferral/level6', $scope.postData).then(function(res) {
                $scope.close();
                console.log(res);
                $scope.showPremiumLevel6 = true;
                $scope.plevel6 = res.data.level6;
                angular.forEach(res.data.plevel6, function(value, key) {
                    if (value.premium_status == 1) {
                        $scope.pt6 = $scope.pt6 + 1;
                    }
                });
                console.log($scope.plevel6)
            });
        }

        $scope.getPayHistory = function() {
            Request.get('/getPayHistory').then(function(res) {
                $scope.payHistory = res.data.payHistory;

                //console.log($scope.payHistory.accountdetail.length);
            });
        }





        $scope.saveAccount = function() {
            $scope.loading = { order: true };
            var req = Request.post('/saveAccount', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.messages.success = "Bank Information saved successful";
                    $scope.messages.error = false;
                    $scope.loading.order = false;


                }
            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.error = '';
            }, 3000)

        }
        $scope.updateAccount = function() {
            $scope.loading = { order: true }
            var req = Request.post('/updateAccount', $scope.currentData.account);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.messages.success = "Bank Information update successful";
                    $scope.messages.error = false;
                    $scope.loading.order = false;


                }
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
            }, 3000)

        }

    })






    //old shit
    .controller('RegularAdminUserMgtCtrl', function($scope, $rootScope, Request, $filter, $window, $interval, $timeout, UserService, NotificationService) {
        $scope.postData = {};
        $scope.orders = [];
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.requisition = {};
        $scope.requisitionStatus = { status: false };
        $scope.checkStatus = false;
        $scope.activeUser = {};
        $scope.newUser = {};
        $scope.postRec = {};
        $scope.updateRec = {};
        $scope.myArray = [];
        $scope.Company = {};
        $scope.showModal = false;

        $scope.carrier = '';
        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $rootScope.currentUser = UserService.getCurrentUser();
        $scope.getTotalBalance = function(user) {
            var count = 0;
            angular.forEach(user, function(todo) {
                count += todo.account.current_balance;
            });
            return count;
        }

        $scope.getNewUser = function() {
            Request.get('/getNewUser').then(function(res) {
                $scope.totalBalance = res.data.totalBalance;
            });
        }
        $scope.full = true;
        $scope.lastpage = 1;
        $scope.get_user_info = function() {
            $scope.lastpage = 1;
            UserService.networkUsers($scope.lastpage, $scope.carrier)
                .then(function(res) {
                    if (res) {
                        $scope.activeUser = res.data.data;
                        $scope.currentpage = res.data.current_page;
                        console.log(res)
                    }

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.get_user_info();
        $scope.objpush = function(user) {
            angular.forEach(user, function(todo) {
                $scope.activeUser.push(todo)
            });
            return $scope.activeUser;
        }
        $scope.loadMore = function() {
            $scope.lastpage += 1;
            UserService.networkUsers($scope.lastpage, $scope.carrier)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.data.length > 0) {
                        $scope.objpush(res.data.data);
                        console.log($scope.activeUser);
                    } else {
                        $scope.full = false;
                    }
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };
        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }




        $scope.userDetail = function(id) {
            $scope.userInfo = {};
            $scope.loading = { userInfo: true };
            Request.get('/users/' + id).then(function(res) {
                $scope.userInfo = res.data;
                $scope.loading.userInfo = false;
            });
        }




    })

    //Done
    .controller('adminWalletCtrl', function($scope, $rootScope, Request, $filter, $window, $timeout, UserService, $fancyModal, NotificationService) {
        $scope.postData = {};
        $scope.postDataS = {};
        $scope.postDataR = {};
        $scope.senderData = {};
        $scope.transferDataS = {};
        $scope.transferDataR = {};
        $scope.resData = {};
        $scope.messages = { success: '', error: false, errorMessage: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $scope.walletHistory = "";
        $scope.walletBalance = "";
        //$scope.userPin = "";
        $scope.admin = [];

        $rootScope.currentUser = UserService.getCurrentUser();
         $scope.getAdmins = function() {
            Request.get('/getAdmin').then(function(res) {
                $scope.admin = res.data.adminList;
                  console.log($scope.admin);               
            });
        }

        $scope.getAdmins();
       // $scope.admin =  $rootScope.currentUser.admin;
      

        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });

        };

        $scope.close = function() {
            $fancyModal.close();
        }
        $scope.postData = {};
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;

        $scope.sales = {};

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.adminSender = function(id) {
            if (id.length > 9) {
                Request.get('/managedUser/' + id).then(function(res) {
                    $scope.userS = res.data.manageUser;

                });
            }

        }
        $scope.superAdminSender = function(id) {
            if (id.length > 9) {
                Request.get('/allUser/' + id).then(function(res) {
                    $scope.userS = res.data.allusers;

                });
            }
        }

        $scope.selectSender = function(id) {
            $scope.transferDataS = id;
            $scope.userS = {};
        }
        $scope.adminRecipient = function(id) {
            if (id.length > 9) {
                Request.get('/managedUser/' + id).then(function(res) {
                    $scope.userR = res.data.manageUser;

                });
            }
        }
        $scope.superAdminRecipient = function(id) {
            if (id.length > 9) {
                Request.get('/allUser/' + id).then(function(res) {
                    $scope.userR = res.data.allusers;

                });
            }
        }

        $scope.selectRecipient = function(id) {
            $scope.transferDataR = id;
            $scope.userR = {};
        }

        $scope.transferBalance = function() {
            $scope.loading = { transfer: true }
            $scope.postData.sender_id = $scope.transferDataS.user_id;
            $scope.postData.recipient_id = $scope.transferDataR.user_id;
            console.log($scope.postData)
            var req = Request.post('/accounts/transfer', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    NotificationService.send('success', "Fund Transfer successful");
                    $scope.loading.transfer = false;
                    $scope.postData = {};
                    $scope.transferDataS = {};
                    $scope.transferDataR = {};
                } else {
                    NotificationService.send('success', res.data.message);

                    $scope.loading.transfer = false;
                    $scope.postData = {};
                    $scope.transferDataS = {};
                    $scope.transferDataR = {};
                }

            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.transfer = false;
            });


        }
        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.checkPaid = function(id) {
            $scope.loading = { check: true }
            Request.get('/accounts/withdrawal/paid/' + id).then(function(res) {
                if (res.data.status) {
                    NotificationService.send("success", "Operation successful");

                    $scope.loading.check = false;
                    $scope.getWithdrawal();
                }

            });
        }

        $scope.filter2 = function(p) {
            if (p.user !== null) {
                return true;
            } else {
                return;
            }
        };
        $scope.getWithdrawal = function() {
            $scope.loading = { withdrawal: true };
            var req = Request.get('/accounts/user/withdrawal');
            req.then(function(res) {
                $scope.withdrawal = res.data.withdrawal;
                $scope.loading.withdrawal = false;
                console.log($scope.withdrawal);
            });


        }


        $scope.getWithdrawalHistory = function() {
            //console.log($scope.postData.user_id)
            $scope.postData.user_id = $scope.senderData.user_id;
            console.log($scope.postData.user_id)
            var req = Request.post('/accounts/user/withdrawal', $scope.postData);
            req.then(function(res) {

                $scope.withdrawalHistory = res.data.withdrawalHistory;
                // $scope.postData = {};


            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;

            });


        }

        $scope.getWalletHistory = function() {
            $scope.open();
            $scope.postData.user_id = $scope.senderData.user_id;
            Request.post('/accounts/transactions/', $scope.postData).then(function(res) {
                $scope.close();
                $scope.walletHistory = res.data.transactions;

            });
        }

        $scope.getWalletHistoryAdminSearch = function() {
            $scope.open();
            $scope.postData.user_id = $scope.senderData.user_id;
            Request.post('/accounts/transactions/admin', $scope.postData).then(function(res) {
                $scope.close();
                $scope.walletHistory = res.data.transactions;

                // console.log($scope.walletHistory);
            });
        }

        $scope.getAdminWalletHistory = function() {
            console.log($scope.postData.user_id)
            Request.get('/admin/transactions/' + $scope.postData.user_id).then(function(res) {
                $scope.adminWalletHistory = res.data.transactions;

                // console.log($scope.walletHistory);
            });
        }

        //new to include date

        $scope.getAdminWalletHistoryWithDate = function() {
            $scope.loading.history = true;
            //console.log($scope.postData.user_id)
            Request.post('/admin/transactions/' + $scope.postData.user_id,$scope.postData).then(function(res) {
                $scope.loading.history = false;
                $scope.adminWalletHistory = res.data.transactions;
                 
                console.log(res.data);
            });
        }

        $scope.getWalletBalance = function() {
            $scope.postData.user_id = $scope.senderData.user_id;
            Request.get('/accounts/' + $scope.postData.user_id + '/balance').then(function(res) {
                $scope.walletBalance = res.data.balance;
                // console.log($scope.walletBalance )
            });
        }
        $scope.adminManageUser = function(id) {
            if (id.length > 9) {
                Request.get('/managedUser/' + id).then(function(res) {
                    $scope.managedUser = res.data.manageUser;
                    console.log($scope.managedUser)
                });
            }
        }
        $scope.adminAllUser = function(id) {
            if (id.length > 9) {
                Request.get('/allUser/' + id).then(function(res) {;
                    $scope.alluser = res.data.allusers;
                    console.log($scope.alluser)
                });
            }
        }
        $scope.getNewUser = function() {
            Request.get('/getNewUser').then(function(res) {
                $scope.activeUser = res.data.activeUser;
            });
        }
        $scope.selectUser = function(id) {
            $scope.senderData = id;
            $scope.managedUser = {};
            $scope.alluser = {};
        }



        $scope.creditAccount = function() {
            $scope.loading = { credit: true }
            $scope.postData.user_id = $scope.senderData.user_id;
            var req = Request.post('/accounts/credit', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Transaction successful. Current Balance is: GH&#8373; " + res.data.current_balance);

                    $scope.loading.credit = false;
                    $scope.postData = {};
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.credit = false;
            });


        }

        //new admin transaction load with date
        $scope.full = true;
        $scope.lastpage = 1;
        $scope.adminWHistoryWithDate = function() {
            $scope.loading = { history: true }
            console.log($scope.postData)
            var req = Request.post('/admin/transactions/date?page='+$scope.lastpage, $scope.postData);
            req.then(function(res) {
                if (res) {
                     //$scope.adminHistory = res.data.history;
                      $scope.adminHistory = res.data.data;
                        $scope.currentpage = res.data.current_page;
                        console.log(res)
                    // $scope.messages.success = "Fund Transfer successful";
                    NotificationService.send('success', "Data retrieval successful");
                    $scope.loading.history = false;
                   
                } else {
                    NotificationService.send('success', res.data.message);

                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.history = false;
            });


        }

        $scope.objpushDate = function(user) {
            angular.forEach(user, function(todo) {
                $scope.adminHistory.push(todo)
            });
            return $scope.adminHistory;
        }
        $scope.loadMoreDate = function() {
            $scope.lastpage += 1;
            Request.post('/admin/transactions/date?page='+$scope.lastpage, $scope.postData)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.data.length > 0) {
                         NotificationService.send('success', "Additional Data retrieval successful");
                        $scope.objpushDate(res.data.data);
                        console.log($scope.adminHistory);
                    } else {
                         NotificationService.send('success', "Additional Data retrieval complete");
                        $scope.full = false;
                    }
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };

//end

        $scope.full = true;
        $scope.lastpage = 1;
        $scope.adminWHistory = function() {
            $scope.lastpage = 1;
            UserService.networkAdminHistory($scope.lastpage)
                .then(function(res) {
                    if (res) {
                        $scope.adminHistory = res.data.data;
                        $scope.currentpage = res.data.current_page;
                        console.log(res)
                    }

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        //solocode
        // $scope.adminWHistory();
        $scope.objpush = function(user) {
            angular.forEach(user, function(todo) {
                $scope.adminHistory.push(todo)
            });
            return $scope.adminHistory;
        }
        $scope.loadMore = function() {
            $scope.lastpage += 1;
            UserService.networkAdminHistory($scope.lastpage)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.data.length > 0) {
                        $scope.objpush(res.data.data);
                        console.log($scope.adminHistory);
                    } else {
                        $scope.full = false;
                    }
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };
        // $scope.adminWHistory = function(){

        //  Request.get('/admin/transactions/').then(function(res){
        //      $scope.adminHistory  =  res.data.transactions;

        //       console.log($scope.adminHistory);
        //  });
        // }
        // $scope.adminWHistory();
    })
    //Done
    .controller('adminRankCtrl', function($scope, $rootScope, Request, $filter, $window, $timeout, UserService, NotificationService) {
        $scope.postData = {};
        $scope.postDataS = {};
        $scope.postDataR = {};
        $scope.senderData = {};
        $scope.transferDataS = {};
        $scope.transferDataR = {};
        $scope.resData = {};
        $scope.messages = { success: '', error: false, errorMessage: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};


        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.filter2 = function(p) {
            if (p.user !== null) {
                return true;
            } else {
                return;
            }
        };
        $scope.getRanks = function() {
            $scope.loading = { ranks: true };
            var req = Request.get('/admin/ranks');
            req.then(function(res) {
                $scope.ranks = res.data.ranks;
                $scope.loading.ranks = false;
                console.log($scope.ranks);
            });


        }

        // $scope.getRanks();

        $scope.getUserRanks = function() {
            $scope.loading = { ranks: true };
            var req = Request.get('/user-attained/ranks');
            req.then(function(res) {
                $scope.user_ranks = res.data.user_ranks;
                $scope.loading.ranks = false;
                console.log($scope.user_ranks);
            });


        }
        // $scope.getUserRanks();
        $scope.payRank = function(data) {
            var req = Request.get('/user-attained/ranks/pay/' + data.upgrade_id);
            req.then(function(res) {
                $scope.getUserRanks();
            });

        }


        $rootScope.currentUser = UserService.getCurrentUser();
        $rootScope.currentData = UserService.getCurrentUser();
        $scope.postData.phone = $rootScope.currentUser.phone;

        $scope.saveRank = function() {
            $scope.loading = { ranks: true }
            var req = Request.post('/admin/ranks', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send("success", "Rank created successfully");
                    $scope.loading.ranks = false;
                    $scope.postData = {};
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.ranks = false;
            });

        }

        $scope.getWalletBalance = function() {
            Request.get('/accounts/' + $rootScope.currentUser.user_id + '/balance').then(function(res) {
                $scope.walletBalance = res.data.balance;
                $scope.postData.wbalance = res.data.balance;
                console.log($scope.walletBalance);
            });
        }
        console.log($scope.currentUser);

        $scope.saveUpgrade = function() {
            $scope.loading = { user: true }
            var req = Request.post('/premium-user-upgrades', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Account upgrade successful. You have been placed under this user : " + res.data.message.fullname);
                    $scope.loading.user = false;
                    $scope.postData = {};
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.saveBagamUpgrade = function() {
            $scope.loading = { user: true }
            var req = Request.post('/bagam-user-upgrades', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', "Bagam Account upgrade successful. You have been placed under this user : " + res.data.message.fullname);
                    $scope.loading.user = false;
                    $scope.postData = {};
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.requestWalletLoad = function() {
            $scope.loading = { user: true }
            var req = Request.post('/mtn-load-wallet', $scope.postData);
            req.then(function(res) {
                if (res.data.success) {
                    NotificationService.send('success', "Prompt Successfully Sent");
                    $scope.loading.user = false;
                    $scope.postData.amount = '';
                } else {
                    NotificationService.send('success', "Wallet Load request not sent");
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.invoiceHistory = [];
        $scope.searchInvoice = function() {
            $scope.loading = { user: true }
            var req = Request.post('/invoice-search', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    $scope.loading.user = false;
                    $scope.invoiceHistory = res.data.invoiceHistory;
                    $scope.invoiceHistorySum = res.data.invoiceHistorySum;
                } else {
                    $scope.invoiceHistory = [];
                    $scope.invoiceHistory = res.data.invoiceHistory;
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }


        $scope.invoiceHistory2 = [];
        $scope.searchInvoice2 = function() {
            $scope.loading = { user: true }
            var req = Request.post('/invoice-search-gt', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    $scope.loading.user = false;
                    $scope.invoiceHistory2 = res.data.invoiceHistory;
                    $scope.invoiceHistorySum2 = res.data.invoiceHistorySum;
                } else {
                    $scope.invoiceHistory2 = [];
                    // $scope.invoiceHistory2 = res.data.invoiceHistory;
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.removeInvoice = function(id) {
            $scope.loading = { user: true }
            var req = Request.get('/invoice-search-gt/remove/'+id);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', 'Action Completed');
                    $scope.loading.user = false;
                    $scope.searchInvoice2();
                    
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }



        $scope.regularTransfer = function() {
            $scope.loading = { user: true }
            var req = Request.post('/regular-transfer', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;

                } else {

                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.premiumTransfer = function() {
            $scope.loading = { user: true }
            var req = Request.post('/premium-transfer', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;

                } else {

                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }

        $scope.topup = function() {
            $scope.loading = { user: true }
            var req = Request.post('/topup-airtime', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;

                } else {
                    NotificationService.send('success', res.data.msg);
                    $scope.loading.user = false;
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.user = false;
            });


        }



    })

    .controller('superAdminWalletCtrl', function($scope, $rootScope, Request, $filter, $window, $timeout, UserService, NotificationService) {
        $scope.postData = {};
        $scope.senderData = {};
        $scope.resData = {};
        $scope.messages = { success: '', error: false, errorMessage: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $scope.walletHistory = "";
        $scope.walletBalance = "";
        //$scope.userPin = "";

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.admin = res.data.admin;
            });
        }

        $scope.checkPaid = function(id) {
            $scope.loading = { check: true }
            Request.get('/accounts/withdrawal/paid/' + id).then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Operation successful";
                    $scope.loading.check = false;
                    $scope.getWithdrawal();
                }

            });
        }

        $scope.filter2 = function(p) {
            if (p.user !== null) {
                return true;
            } else {
                return;
            }
        };
        $scope.getWithdrawal = function() {
            $scope.loading = { withdrawal: true };
            var req = Request.get('/accounts/user/withdrawal');
            req.then(function(res) {
                $scope.withdrawal = res.data.withdrawal;
                $scope.loading.withdrawal = false;
                console.log($scope.withdrawal);
            });


        }

        $scope.getWithdrawal();
        $scope.getWithdrawalHistory = function() {
            console.log($scope.postData.user_id)
            var req = Request.post('/accounts/user/withdrawal', $scope.postData);
            req.then(function(res) {

                $scope.withdrawalHistory = res.data.withdrawalHistory;
                // $scope.postData = {};
                console.log($scope.withdrawalHistory)

            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;

            });


        }
        $scope.getWalletHistory = function() {
            // console.log($scope.postData.user_id)
            $scope.postData.user_id = $scope.senderData.user_id;
            Request.get('/accounts/transactions/' + $scope.postData.user_id).then(function(res) {
                $scope.walletHistory = res.data.transactions;

                // console.log($scope.walletHistory);
            });
        }

        $scope.getAdminWalletHistory = function() {
            console.log($scope.postData.user_id)
            Request.get('/admin/transactions/' + $scope.postData.user_id).then(function(res) {
                $scope.adminWalletHistory = res.data.transactions;

                // console.log($scope.walletHistory);
            });
        }

        $scope.getWalletBalance = function() {
            $scope.postData.user_id = $scope.senderData.user_id;
            Request.get('/accounts/' + $scope.postData.user_id + '/balance').then(function(res) {
                $scope.walletBalance = res.data.balance;
                // console.log($scope.walletBalance )
            });
        }
        $scope.adminManageUser = function(id) {
            Request.get('/managedUser/' + id).then(function(res) {
                $scope.managedUser = res.data.manageUser;
                console.log($scope.managedUser)
            });
        }
        $scope.adminAllUser = function(id) {
            Request.get('/allUser/' + id).then(function(res) {;
                $scope.alluser = res.data.allusers;
                console.log($scope.alluser)
            });
        }
        $scope.getNewUser = function() {
            Request.get('/getNewUser').then(function(res) {
                $scope.activeUser = res.data.activeUser;
            });
        }
        $scope.selectUser = function(id) {
            $scope.senderData = id;
            $scope.managedUser = {};
            $scope.alluser = {};
        }
        // $scope.getNewUser();
        $scope.currentUser();
        // $scope.adminManageUser();

        // $scope.getWalletBalance();
        // $scope.getWalletHistory();

        $scope.transferBalance = function() {
            $scope.loading = { transfer: true }
            console.log($scope.postData)
            var req = Request.post('/accounts/transfer', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    // $scope.messages.success = "Fund Transfer successful";
                    NotificationService.send('success', "Fund Transfer successful");
                    $scope.loading.transfer = false;
                    $scope.postData = {};
                } else {
                    NotificationService.send('success', res.data.message);

                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.transfer = false;
            });


        }

        $scope.creditAccount = function() {
            $scope.loading = { credit: true }
            $scope.postData.user_id = $scope.senderData.user_id;
            var req = Request.post('/accounts/credit', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {

                    NotificationService.send('success', "Transaction successful. Current Balance is: GH&#8373; " + res.data.current_balance);
                    $scope.loading.credit = false;
                    $scope.postData = {};
                }
            });
            req.error(function(res) {
                $scope.messages.errorMessage = res;
                $scope.loading.credit = false;
            });


        }

        $scope.adminWHistory = function() {

            Request.get('/admin/transactions').then(function(res) {
                $scope.adminHistory = res.data.transactions;

            });
        }

    })


    .controller('UserMgtCtrl', function($scope, $rootScope, Request, $filter, $window, $interval, $timeout, UserService, $fancyModal, NotificationService) {
        $scope.postData = {};
        $scope.orders = [];
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.requisition = {};
        $scope.requisitionStatus = { status: false };
        $scope.checkStatus = false;
        $scope.activeUser = {};
        $scope.newUser = {};
        $scope.postRec = {};
        $scope.updateRec = {};
        $scope.myArray = [];
        $scope.Company = {};
        $scope.showModal = false;
        $scope.loadActive = true;

        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });

        };

        $scope.close = function() {
            $fancyModal.close();
        }

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.getTotalBalance = function(user) {
            var count = 0;
            angular.forEach(user, function(todo) {
                count += todo.account.current_balance;
            });
            return count;
        }

        $scope.getNewUser = function() {
            Request.get('/getNewUser').then(function(res) {
                $scope.totalBalance = res.data.totalBalance;
            });
        }
        $scope.adminAllUser = function(id) {
            if (id.length > 3) {
                Request.get('/allUser/' + id).then(function(res) {;
                    $scope.allUser = res.data.allusers;
                    console.log($scope.allUser)
                });
            }
        }

        $scope.selectUser = function(id) {
            $scope.senderData = id;
            $scope.managedUser = {};
            $scope.alluser = {};
        }

        $scope.full = true;
        $scope.lastpage = 1;
        $scope.getNew_User = function() {
            $scope.open();
            $scope.loadActive = false;
            UserService.allUsers($scope.lastpage)
                .then(function(res) {
                    if (res) {
                        $scope.close();
                        $scope.activeUser = res.data.activeUser.data;
                        $scope.currentpage = res.data.activeUser.current_page;
                        console.log(res)
                    }

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.currentData.choice = 0;
                $scope.currentData.choicePhone = 0;
                $scope.currentData.choiceUser = 0;
                //console.log($scope.currentData);
            });
        }
        $scope.markRead = function(id) {
            $scope.postData.notification_id = id;
            $scope.loading = { order: true };
            var req = Request.post('/markRead', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.messages.success = "Done";
                    $scope.messages.error = false;
                    $scope.loading.order = false;


                }
                $timeout(function() {
                    $scope.messages.success = '';
                }, 3000)
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
            }, 3000)

        }

        $scope.userDetail = function(id) {
            $scope.userInfo = {};
            $scope.loading = { userInfo: true };
            Request.get('/users/' + id).then(function(res) {
                $scope.userInfo = res.data;
                $scope.loading.userInfo = false;
            });
        }
        // $scope.getNewUser();
        // $scope.getNewUser();
        $scope.currentUser();


        $scope.activate = function(id) {
            $scope.postData.user_id = id;
            $scope.showModal = true;
            $scope.loading = { order: true };
            var req = Request.post('/activate-user', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNewUser();
                    $scope.getNotification();
                    $scope.messages.success = "Activation successful";
                    $scope.messages.error = false;
                    $scope.loading.order = false;
                    $scope.showModal = false;

                }
                $timeout(function() {
                    $scope.messages.success = '';
                }, 3000)
            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.order = false;
            });


        }

        $scope.removeUser = function(id) {
            $scope.postData.user_id = id;
            $scope.loading = { order: true };
            var req = Request.post('/remove-user', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNewUser();
                    $scope.getNotification();
                    $scope.messages.success = "User Removal successful";
                    $scope.messages.error = false;
                    $scope.loading.order = false;
                }
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
            }, 3000)

        }



        $scope.updateUser = function() {
            $scope.loading = { user: true };
            if (typeof($scope.currentData.file) !== 'undefined') {
                Request.uploadFile("/api/v1/files/upload", $scope.currentData.file)
                    .then(function(res) {
                        $scope.currentData.image = res.data.imageUrl;
                        if ($scope.currentData.choiceUser == 0) {
                            $scope.currentData.username = '';
                        }
                        if ($scope.currentData.choice == 0) {
                            $scope.currentData.password = '';
                            $scope.currentData.password_confirmation = '';
                        }
                        //console.log($scope.currentData);
                        var req = Request.post('/user-management/updateUser', $scope.currentData);
                        req.then(function(res) {
                            if (res.data.status) {
                                $scope.messages.success = "User Record successfully updated";
                                $scope.messages.error = false;
                                $scope.loading.user = false;
                                $scope.getNotification();
                                $scope.currentUser();
                            }

                        });
                        req.error(function(res) {
                            $scope.messages.error = res.messages;
                            $scope.loading.user = false;

                        });

                    })
            } else {
                $scope.currentData.image = 0;
                if ($scope.currentData.choiceUser == 0) {
                    $scope.currentData.username = '';
                }
                if ($scope.currentData.choice == 0) {
                    $scope.currentData.password = '';
                    $scope.currentData.password_confirmation = '';
                }
                var req = Request.post('/user-management/updateUser', $scope.currentData);
                req.then(function(res) {
                    if (res.data.status) {
                        $scope.messages.success = "User Record successfully updated";
                        $scope.messages.error = false;
                        $scope.loading.user = false;
                        $scope.getNotification();
                        $scope.currentUser();

                    }

                });
                req.error(function(res) {
                    $scope.messages.error = res;
                    //console.log(res);
                    $scope.loading.user = false;

                });

            }
        }

        $scope.createUser = function() {
            $scope.loading = { user: true };
            if (typeof($scope.postData.file) !== 'undefined') {
                Request.uploadFile("/api/v1/files/upload", $scope.postData.file)
                    .then(function(res) {
                        $scope.postData.image = res.data.imageUrl;
                        var req = Request.post('/user-management/createUser', $scope.postData);
                        req.then(function(res) {
                            if (res.data.status) {
                                $scope.messages.success = "Admin Record successfully created";
                                $scope.messages.error = false;
                                $scope.loading.user = false;
                                $scope.getNotification();
                            }
                        });
                        req.error(function(res) {
                            $scope.messages.error = res.messages;
                            $scope.loading.user = false;
                        });
                        $timeout(function() {
                            $scope.messages.success = '';

                        }, 3000)

                    })
            } else {
                $scope.postData.image = 0;
                var req = Request.post('/user-management/createUser', $scope.createData);
                req.then(function(res) {
                    if (res.data.status) {
                        $scope.messages.success = "Admin Record successfully created";
                        $scope.messages.error = false;
                        $scope.loading.user = false;
                        $scope.getNotification();
                    }

                })
                req.error(function(res) {
                    $scope.messages.error = res;
                    $scope.loading.user = false;
                });
                $timeout(function() {
                    $scope.messages.success = '';

                }, 3000)


            }
        }


        $scope.fullPremium = true;
        $scope.prelastpage = 1;
        $scope.getPremiumUserList = function() {
            $scope.loadPremium = false;
            $scope.open();
            UserService.premiumUsers($scope.prelastpage)
                .then(function(res) {
                    if (res) {
                        $scope.close();
                        $scope.premiumUser = res.data.user.data;
                        $scope.precurrentpage = res.data.paginate.current_page;
                        $scope.prelastpage = res.data.paginate.last_page;
                        console.log($scope.premiumUser)
                    }
                    $scope.close();

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.getPremiumUserList();
        $scope.objpushPremium = function(user) {
            angular.forEach(user, function(todo) {
                $scope.premiumUser.push(todo)
            });
            return $scope.premiumUser;
        }
        $scope.loadMorePremiumList = function() {
            // $scope.open();
            $scope.r = $scope.prelastpage + 1;
            UserService.premiumUsers($scope.r)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.paginate.current_page < res.data.paginate.last_page) {
                        $scope.precurrentpage = res.data.paginate.current_page;
                        $scope.objpushPremium(res.data.user.data);
                        console.log($scope.precurrentpage);
                    }
                    if (res.data.paginate.current_page == res.data.paginate.last_page) {
                        // $scope.currentpage = res.data.paginate.current_page;
                        // $scope.objpush(res.data.paymentUser);
                        console.log($scope.currentpage);
                        $scope.fullPremium = false;
                    }

                    // $scope.close();
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };

    })
    .controller('UserRanksMgtCtrl', function($scope, $rootScope, Request, $filter, $window, $interval, $timeout, UserService, $fancyModal) {
        $scope.postData = {};
        $scope.orders = [];
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.requisition = {};
        $scope.requisitionStatus = { status: false };
        $scope.checkStatus = false;
        $scope.activeUser = {};
        $scope.newUser = {};
        $scope.postRec = {};
        $scope.updateRec = {};
        $scope.myArray = [];
        $scope.Company = {};
        $scope.showModal = false;

        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });


        };

        // kwkwkw
        $scope.close = function() {
            $fancyModal.close();
        }

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.currentData.choice = 0;
                $scope.currentData.choicePhone = 0;
                $scope.currentData.choiceUser = 0;
                //console.log($scope.currentData);
            });
        }


        $scope.userDetail = function(id) {
            $scope.userInfo = {};
            $scope.loading = { userInfo: true };
            Request.get('/users/' + id).then(function(res) {
                $scope.userInfo = res.data;
                $scope.loading.userInfo = false;
            });
        }

        $scope.currentUser();

        // $scope.getPremiumUserListFix = function(){
        //  $scope.open();
        //        UserService.premiumUsersFix()
        //            .then(function (res) {
        //              if (res) {
        //                  $scope.close();
        //                  $scope.premiumUser  = res.data.user.data;
        //                 console.log($scope.premiumUser)
        //              }
        //              $scope.close();

        //            }, function (error) {
        //                $scope.status = 'Unable to load data: ' + error.message;
        //            });

        // }

        //this was not working well
        $scope.fullPremium = true;
        $scope.prelastpage = 1;

        $scope.getPremiumUserListFix = function() {
            $scope.loadPremium = false;
            $scope.open();
            UserService.premiumUsersFix($scope.prelastpage)
                .then(function(res) {
                    if (res) {
                        $scope.close();
                        $scope.premiumUser = res.data.user.data;
                        $scope.precurrentpage = res.data.paginate.current_page;
                        $scope.prelastpage = res.data.paginate.last_page;
                        console.log($scope.premiumUser)
                    }
                    $scope.close();

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.getPremiumUserList();
        $scope.objpushPremium = function(user) {
            angular.forEach(user, function(todo) {
                $scope.premiumUser.push(todo)
            });
            return $scope.premiumUser;
        }
        $scope.loadMorePremiumList = function() {
            // $scope.open();
            $scope.r = $scope.precurrentpage + 1;
            UserService.premiumUsers($scope.r)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.paginate.current_page < res.data.paginate.last_page) {
                        $scope.precurrentpage = res.data.paginate.current_page;
                        $scope.objpushPremium(res.data.user.data);
                        console.log($scope.precurrentpage);
                        $scope.loadMorePremiumList();
                    }
                    if (res.data.paginate.current_page == res.data.paginate.last_page) {
                        $scope.generateRankStatement();
                        // $scope.currentpage = res.data.paginate.current_page;
                        // $scope.objpush(res.data.paymentUser);
                        console.log($scope.currentpage);
                        $scope.fullPremium = false;
                    }

                    // $scope.close();
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };


        $scope.generateRankStatement = function() {

            $scope.open();
            $scope.postData.listRank = $scope.premiumUser;
            Request.post('/generate-rank-attain-statement', $scope.postData).then(function(res) {
                if (res.data.status) {
                    // $scope.getNotification();
                    $scope.messages.success = res.data.message;
                    var dlink = angular.element('<a/>');
                    dlink.attr({
                        'href': '/excel/exports/rank_statement_' + $rootScope.currentUser.user_id + '.xlsx',
                        'target': '_self',
                        'download': 'Rank Attainment Statement.xlsx'
                    })[0].click();
                    $scope.close()

                } else {
                    $scope.close()
                    $scope.messages.success = res.data.message;
                }

            });

            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }


    })


    .controller('MessageCtrl', function($scope, $rootScope, Request, $filter, $window, $interval, $timeout) {
        $scope.postData = {};
        $scope.messages = { success: '', error: '' }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};


        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.currentData.choice = 0;
                $scope.currentData.choiceUser = 0;
                $scope.currentData.choicePhone = 0;
                //console.log($scope.currentData.account);
            });
        }


        $scope.currentUser();


        $scope.sendMessage = function() {
            $scope.loading = { user: true };
            var req = Request.post('/sendMessage', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Message sent successfully";
                    //alert($scope.messages.success)
                    $scope.messages.error = false;
                    $scope.loading.user = false;
                    $scope.getNotification();
                    $scope.currentUser();


                }
            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });
            $timeout(function() {
                $scope.postData = {}
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 3000);

        }
        $scope.sendMail = function() {
            $scope.loading = { order: true }
            var req = Request.post('/sendMail', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.messages.success = "Emails sent successfully";
                    $scope.messages.error = false;
                    $scope.loading.order = false;
                }
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.postData = {};
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 3000);

        }

    })

    .controller('InsuranceCtrl', function($scope, $rootScope, Request, $filter, $window, $fancyModal, $timeout, UserService) {
        $scope.load = true;

        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });

        };

        $scope.close = function() {
            $fancyModal.close();
        }
        $scope.postData = {};
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $scope.sales = {};

        $scope.items = [
            { id: 1, name: 'GSMadmin' },
            { id: 2, name: 'GSM 1' },
            { id: 3, name: 'GSM 2' },
            { id: 4, name: 'GSM 3' },
            { id: 5, name: 'GSM 4' },
            { id: 6, name: 'GSM 5' },
            { id: 7, name: 'GSM 6' }
        ];


        //$scope.paymentList = {};

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.currentData.choice = 0;
                $scope.currentData.choiceUser = 0;
                $scope.currentData.choicePhone = 0;
                //console.log($scope.currentData.account);
            });
        }
        $scope.getReferral = function() {
            Request.get('/getReferral').then(function(res) {
                $scope.level1 = res.data.level1;
                $scope.level2 = res.data.level2;
                $scope.level3 = res.data.level3;
                $scope.level4 = res.data.level4;
                $scope.level5 = res.data.level5;
                $scope.level6 = res.data.level6;
                $scope.t1 = res.data.t1;
                $scope.t2 = res.data.t2;
                $scope.t3 = res.data.t3;
                $scope.t4 = res.data.t4;
                $scope.t5 = res.data.t5;
                $scope.t6 = res.data.t6;

                //console.log(res.data);
            });
        }

        $scope.getAdmin = function() {
            Request.get('/getAdmin').then(function(res) {
                $scope.adminList = res.data.adminList;
                //console.log($scope.adminList);
            });
        }
        $scope.resetInsurance = function() {
            if ($window.confirm('Are you sure you want to reset insurance activation ?')) {
                $scope.open();
                Request.get('/resetInsurance').then(function(res) {
                    $scope.getInsurance();
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.getReferral();
                    $scope.getPayment();
                    //console.log($scope.unRead);
                    $scope.close();
                });
            }
        }

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }

        $scope.getAdmin();
        $scope.currentUser();


        $scope.assignRole = function(id, user_id) {
            $scope.loading = { order: true };
            $scope.postData.user_id = user_id;
            $scope.postData.role_id = id;

            console.log($scope.postData);

            var req = Request.post('/activate-role', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getAdmin();
                    $scope.messages.success = "Role Changed successfully";
                    $scope.messages.error = false;
                    $scope.loading.order = false;

                }

            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.error = ''
            }, 3000)

        }

        $scope.activateInsurance = function(id) {
            $scope.postData.user_id = id;

            //console.log($scope.postData );
            $scope.loading.insurance = true;
            // $scope.messages ={students:{success:'',error:false}}
            var req = Request.post('/activate-insurance', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getPayment();
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.getReferral();
                    $scope.getInsurance();
                    $scope.messages.success = "Activation successful";
                    $scope.messages.error = false;
                    $scope.loading.insurance = false;



                }
                $timeout(function() {
                    $scope.messages.success = '';
                }, 1000)
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
            }, 3000)

        }

        $scope.getFullList = function(a, b) {
            var check, z, j
            z = Object.keys(b);
            angular.forEach(a, function(item) {
                check = item.user_id
                for (var i = 0; i < z.length; i++) {
                    if (check === z[i]) {
                        j = b[check];
                    }

                };
                item.amt = '' + j + '';
            })
            return a;
        }
        $scope.getPayment = function() {
            $scope.load = false;
            $scope.open();
            Request.get('/getPayment').then(function(res) {
                // $scope.getPay  = res.data.payment;
                $scope.paymentList = res.data.paymentUser;
                // $scope.share = res.data.amtShare;
                $scope.close();
            });
        }
        $scope.getProfit = function() {
            Request.get('/getProfit').then(function(res) {
                $scope.sales = res.data.earning;
                //console.log($scope.paymentList.length);

            });
        }

        $scope.getProfit();
        // $scope.getPayment();
        $scope.sendMessage = function() {
            $scope.loading = { user: true };
            var req = Request.post('/sendMessage', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.messages.success = "Message sent successfully";
                    $scope.messages.error = false;
                    $scope.loading.user = false;


                }
            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.error = '';
            }, 3000)

        }
        $scope.sendMail = function() {
            $scope.loading = { order: true }
            var req = Request.post('/sendMail', $scope.currentData.account);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.getNotification();
                    $scope.currentUser();
                    $scope.messages.success = "Emails sent successfully";
                    $scope.messages.error = false;
                    $scope.loading.order = false;


                }
            });
            req.error(function(res) {
                $scope.messages.order.error = res;
                $scope.loading.order = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.error = '';
            }, 3000)

        }

        $scope.generate = function() {
            $scope.open();
            Request.get('/generate').then(function(res) {
                //if(res.data.status){
                $scope.getNotification();
                $scope.currentUser();
                $scope.messages.success = "List generated successfully";
                var dlink = angular.element('<a/>');
                dlink.attr({
                    'href': '/excel/exports/GSM-Payment-Sheet.xls',
                    'target': '_self',
                    'download': 'GSM-Payment-Sheet' + $scope.d + '.xls'
                })[0].click();
                $scope.close()

                //}

            });

            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }



    })
    .controller('UserCtrl', function($scope, $rootScope, Request, $location) {
        // $scope.userId = location.pathname.split('/')[2];

        // $scope.getUser = function () {
        //  Request.get('/users/'+$scope.userId).then(function(res){
        //      console.log(res.data)
        //      $scope.userInfo = res.data.userDetails;
        //      $scope.level1  = res.data.level1;
        //      $scope.level2  = res.data.level2;
        //      $scope.level3  = res.data.level3;
        //      $scope.level4  = res.data.level4;
        //      $scope.level5  = res.data.level5;
        //      $scope.level6  = res.data.level6;
        //      $scope.t1  = res.data.t1;
        //      $scope.t2  = res.data.t2;
        //      $scope.t3  = res.data.t3;
        //      $scope.t4  = res.data.t4;
        //      $scope.t5  = res.data.t5;
        //      $scope.t6  = res.data.t6;


        //  });
        // }    
        //  $scope.getUser();

        $scope.profit = function() {
            Request.get('/totalProfit').then(function(res) {
                console.log(res.data)
                $scope.total_profit = res.data.profit;


            });
        }
        $scope.profit();

        $scope.userInsurance = function() {
            Request.get('/totalInsurance').then(function(res) {
                console.log(res.data)
                $scope.admin1 = res.data.admin1;
                $scope.admin2 = res.data.admin2;
                $scope.admin3 = res.data.admin3;
                $scope.admin4 = res.data.admin4;
                $scope.admin5 = res.data.admin5;
                $scope.admin6 = res.data.admin6;
                $scope.topup_profit = res.data.profit_topup;


            });
        }
        $scope.userInsurance();

        $scope.defActivity = function(id) {
            $scope.postData.setDate = id;

            var req = Request.post('/defActivity', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.message = "Results for Selected Date";
                    $scope.admin1 = res.data.admin1;
                    $scope.admin2 = res.data.admin2;
                    $scope.admin3 = res.data.admin3;
                    $scope.admin4 = res.data.admin4;
                    $scope.admin5 = res.data.admin5;
                    $scope.admin6 = res.data.admin6;
                    $scope.topup_profit = res.data.profit_topup;


                    console.log(res.data)
                }
            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });


        }
    })
    .controller('ViewUserCtrl', function($scope, $rootScope, Request, $location, $fancyModal, $timeout) {
        $scope.postData = {};
        $scope.directRC = false;
        $scope.messages = { success: '', error: false }
        $scope.userId = location.pathname.split('/')[2];
        $scope.viewUser = false;
        $scope.open = function(id) {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl1.html',
                showCloseButton: true,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: true,
                controller: ['$scope', function($scope) {
                    $scope.message = id;
                }]
            });
        };
        $scope.openWait = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: true,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: true,

            });
        };
        $scope.close = function() {
            $fancyModal.close();
        }
        $scope.checkDirectRC = function() {
            $rootScope.openWait();
            Request.get('/check-direct-rc/' + $scope.userId).then(function(res) {
                console.log(res.data)
                $scope.directRC = res.data.total;

                $scope.close();
            });
        }
        $scope.getUser = function() {
            $rootScope.openWait();
            Request.get('/single-user/detail/' + $scope.userId).then(function(res) {
                console.log(res.data)

                $scope.viewUser = true;
                $scope.userInfo = res.data.userDetails;
                $scope.referrer = res.data.referrer;

                $scope.close();
            });
        }
        $scope.getUser();

        $scope.getUserRefferal = function() {
            $rootScope.openWait();
            Request.get('/user/level/' + $scope.userId).then(function(res) {
                console.log(res.data)
                $scope.level1 = res.data.level1;
                $scope.level2 = res.data.level2;
                $scope.level3 = res.data.level3;
                $scope.level4 = res.data.level4;
                $scope.level5 = res.data.level5;
                $scope.level6 = res.data.level6;
                $scope.t1 = res.data.t1;
                $scope.t2 = res.data.t2;
                $scope.t3 = res.data.t3;
                $scope.t4 = res.data.t4;
                $scope.t5 = res.data.t5;
                $scope.t6 = res.data.t6;

                $scope.close();
            });
        }

        $scope.getPremiumUser = function() {
            $rootScope.openWait();
            Request.get('/premium-user/level/' + $scope.userId).then(function(res) {
                console.log(res.data)

                $scope.viewUser = true;
                $scope.userPremiumInfo = res.data.userDetails;
                $scope.premium_referrer = res.data.referrer;
                $scope.premium_level1 = res.data.level1;
                $scope.premium_level2 = res.data.level2;
                $scope.premium_level3 = res.data.level3;
                $scope.premium_evel4 = res.data.level4;
                $scope.premium_level5 = res.data.level5;
                $scope.premium_level6 = res.data.level6;
                $scope.premium_t1 = res.data.t1;
                $scope.premium_t2 = res.data.t2;
                $scope.premium_t3 = res.data.t3;
                $scope.premium_t4 = res.data.t4;
                $scope.premium_t5 = res.data.t5;
                $scope.premium_t6 = res.data.t6;

                $scope.close();
            });
        }
        // $scope.getPremiumUser();
        $scope.resetPassword = function(id) {
            $scope.postData.user_id = id;
            $scope.loading = { password: true };
            var req = Request.post('/resetpassword', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Password reset successful";
                    $scope.messages.error = false;
                    $scope.loading.password = false;
                    $scope.postData = {};
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.password = false;
                }

            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.password = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);


        }
        $scope.changeUsername = function(id) {
            console.log('lol');
            $scope.postData.user_id = id;
            // $scope.postData.username = id;
            $scope.loading = { user: true };
            var req = Request.post('/change-username', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Username change successful";
                    $scope.messages.error = false;
                    $scope.loading.user = false;
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.user = false;
                }

            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);


        }

        $scope.changePremiumBonus = function(id) {
            console.log('lol');
            $scope.postData.user_id = id;
            // $scope.postData.username = id;
            $scope.loading = { bonus: true };
            var req = Request.post('/change-bonus', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Bonus Update successful";
                    $scope.messages.error = false;
                    $scope.loading.bonus = false;
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.bonus = false;
                }

            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });
            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);


        }

          $scope.updatePhone = function(id) {
            console.log(id);
            $scope.postData.user_id = id;
            // $scope.postData.username = id;
            $scope.loading = { phone: true };
            var req = Request.post('/change-phone-number', $scope.postData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.messages.success = "Phone Number Update successful";
                    $scope.messages.error = false;
                    $scope.loading.phone = false;
                } else {
                    $scope.messages.success = res.data.message;
                    $scope.loading.phone = false;
                }

            });
            req.error(function(res) {
                $scope.messages.error = res;
                $scope.loading.user = false;
            });
            
             $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }

    })





    .controller('AdminInsuranceCtrl', function($scope, $rootScope, Request, $filter, $window, $fancyModal, $timeout, UserService) {
        $scope.load = true;
        $scope.loadRank = true;
        $scope.loadPremium = true;
        $scope.postData = {};
        $scope.open = function() {
            $fancyModal.open({
                templateUrl: '/pages/popupTmpl.html',
                showCloseButton: false,
                openingClass: 'animated rollIn',
                closingClass: 'animated rollOut',
                openingOverlayClass: 'animated fadeIn',
                closingOverlayClass: 'animated fadeOut',
                closeOnOverlayClick: false
            });

        };

        $scope.close = function() {
            $fancyModal.close();
        }
        $scope.postData = {};
        $scope.messages = { success: '', error: false }
        $scope.loading = { logs: false };
        $scope.checkStatus = false;
        $scope.read = {};
        $scope.unRead = {};
        $scope.sales = {};

        $scope.items = [
            { id: 1, name: 'GSMadmin' },
            { id: 2, name: 'GSM 1' },
            { id: 3, name: 'GSM 2' },
            { id: 4, name: 'GSM 3' },
            { id: 5, name: 'GSM 4' },
            { id: 6, name: 'GSM 5' },
            { id: 7, name: 'GSM 6' }
        ];


        //$scope.paymentList = {};

        // $scope.User = {}
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.getNotification = function() {
            Request.get('/getNotification').then(function(res) {
                $scope.read = res.data.readNotification;
                $scope.unRead = res.data.unReadNotification;

                //console.log($scope.unRead);
            });
        }

        $scope.currentUser = function() {
            Request.get('/currentUser').then(function(res) {
                $scope.currentData = res.data.currentUser;
                $scope.currentData.choice = 0;
                $scope.currentData.choiceUser = 0;
                $scope.currentData.choicePhone = 0;
                //console.log($scope.currentData.account);
            });
        }


        $scope.resetInsurance = function() {
            if ($window.confirm('Are you sure you want to reset insurance activation ?')) {
                $scope.postData.list = $scope.paymentList
                $scope.open();
                Request.post('/resetInsurance', $scope.postData).then(function(res) {
                    $scope.getPayment();
                    //console.log($scope.unRead);
                    $scope.close();
                });
            }
        }

        $scope.sort = function(keyname) {
            $scope.sortKey = keyname; //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }

        //Generating payment list
        $scope.full = true;
        $scope.getPayment = function() {
            $scope.load = false;
            $scope.open();
            $scope.currentpage = 1;
            UserService.payUsers($scope.currentpage)
                .then(function(res) {
                    if (res) {
                        $scope.paymentList = res.data.paymentUser;
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.lastpage = res.data.paginate.last_page;
                        console.log($scope.currentpage)
                    }
                    $scope.close();

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.getPayment();
        $scope.objpush = function(user) {
            angular.forEach(user, function(todo) {
                $scope.paymentList.push(todo)
            });
            return $scope.paymentList;
        }
        $scope.loadMore = function() {
            $scope.open();
            $scope.r = $scope.currentpage + 1;
            UserService.payUsers($scope.r)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.paginate.current_page < res.data.paginate.last_page) {
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.objpush(res.data.paymentUser);
                        console.log($scope.currentpage);
                        $scope.loadMore();
                    }
                    if (res.data.paginate.current_page == res.data.paginate.last_page) {
                        // $scope.currentpage = res.data.paginate.current_page;
                        // $scope.objpush(res.data.paymentUser);
                        // console.log($scope.currentpage);
                        $scope.generateXl();
                        $scope.full = false;
                    }

                    $scope.close();
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };

        $scope.generateXl = function() {
            $scope.open();
            $scope.postData.list = $scope.paymentList
            Request.post('/generate', $scope.postData).then(function(res) {
                //if(res.data.status){
                $scope.getNotification();
                $scope.currentUser();
                $scope.messages.success = "List generated successfully";
                var dlink = angular.element('<a/>');
                dlink.attr({
                    'href': '/excel/exports/GSM-Payment-Sheet.xls',
                    'target': '_self',
                    'download': 'GSM-Payment-Sheet' + $scope.d + '.xls'
                })[0].click();
                $scope.close()

                //}

            });

            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }


        //Premium User  Generating payment list
        $scope.fullPremium = true;
        $scope.getPremiumPayment = function() {
            $scope.loadPremium = false;
            $scope.open();
            $scope.currentpage = 1;
            UserService.payPremiumUsers($scope.currentpage)
                .then(function(res) {
                    if (res) {
                        $scope.paymentListPremium = res.data.paymentUser;
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.lastpage = res.data.paginate.last_page;
                        console.log($scope.currentpage)
                        localStorage.payPremiumUsers = JSON.stringify($scope.paymentListPremium);
                        localStorage.payPremiumPage = $scope.currentpage;
                    }
                    $scope.close();

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.getPayment();
        $scope.objpushPremium = function(user) {
            angular.forEach(user, function(todo) {
                $scope.paymentListPremium.push(todo)
            });
            return $scope.paymentListPremium;
        }
        $scope.loadMorePremium = function() {
            // $scope.open();
            $scope.r = $scope.currentpage + 1;
            UserService.payPremiumUsers($scope.r)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.paginate.current_page < res.data.paginate.last_page) {
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.objpushPremium(res.data.paymentUser);
                        console.log($scope.currentpage);

                        // localStorage.payPremiumUsers = JSON.stringify($scope.paymentListPremium);
                        //    localStorage.payPremiumPage = $scope.currentpage;


                        $scope.loadMorePremium();
                    }
                    if (res.data.paginate.current_page == res.data.paginate.last_page) {
                        // $scope.currentpage = res.data.paginate.current_page;
                        // $scope.objpush(res.data.paymentUser);
                        $scope.generateXlPremium();
                        console.log($scope.currentpage);
                        $scope.fullPremium = false;
                    }

                    $scope.close();
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };


        $scope.generateXlPremium = function() {
            $scope.open();
            $scope.postData.listPremium = $scope.paymentListPremium
            Request.post('/generatePremium', $scope.postData).then(function(res) {
                //if(res.data.status){
                $scope.getNotification();
                $scope.currentUser();
                $scope.messages.success = "Premium List generated successfully";
                var dlink = angular.element('<a/>');
                dlink.attr({
                    'href': '/excel/exports/GSM-Payment-Premium-Sheet.xls',
                    'target': '_self',
                    'download': 'GSM-Payment-Premium-Sheet' + $scope.d + '.xls'
                })[0].click();
                $scope.close()

                //}

            });

            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }

        $scope.resetPremiumInsurance = function() {
            if ($window.confirm('Are you sure you want to clear premium insurance claims ?')) {
                $scope.postData.listPremium = $scope.paymentListPremium;
                console.log($scope.postData.listPremium)
                $scope.open();
                Request.post('/resetPremiumInsurance', $scope.postData).then(function(res) {
                    $scope.getPremiumPayment();
                    $scope.close();
                });
            }
        }

        //Premium User  Rank list
        $scope.fullRank = true;
        $scope.showMore = false;
        $scope.lastpage = 0;
        $scope.currentpage = 0;
        $scope.getRankPayment = function() {
            $scope.loadRank = false;
            $scope.open();
            $scope.currentpage = 1;
            UserService.payRankUsers($scope.currentpage)
                .then(function(res) {
                    if (res) {
                        $scope.paymentRankList = res.data.paymentUser;
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.lastpage = res.data.paginate.last_page;
                        console.log($scope.currentpage)
                        $scope.showMore = true;
                    }
                    $scope.close();

                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        }
        // $scope.getPayment();
        $scope.objpushRank = function(user) {
            angular.forEach(user, function(todo) {
                $scope.paymentRankList.push(todo)
            });
            return $scope.paymentRankList;
        }
        $scope.loadRanksMore = function() {
            // $scope.open();
            $scope.r = $scope.currentpage + 1;
            UserService.payRankUsers($scope.r)
                .then(function(res) {
                    // console.log(res.data.data);
                    if (res.data.paginate.current_page < res.data.paginate.last_page) {
                        $scope.currentpage = res.data.paginate.current_page;
                        $scope.objpushRank(res.data.paymentUser);
                        console.log($scope.currentpage);
                        $scope.loadRanksMore();
                    }
                    if (res.data.paginate.current_page == res.data.paginate.last_page) {
                        // $scope.currentpage = res.data.paginate.current_page;
                        // $scope.objpush(res.data.paymentUser);
                        $scope.generateRanksExcel();
                        console.log($scope.currentpage);
                        $scope.fullRank = false;
                    }

                    $scope.close();
                }, function(error) {
                    $scope.status = 'Unable to load data: ' + error.message;
                });

        };

        $scope.generateRanksExcel = function() {
            $scope.open();
            $scope.postData.listRank = $scope.paymentRankList;
            Request.post('/generate/ranks', $scope.postData).then(function(res) {
                //if(res.data.status){
                $scope.messages.success = "Rank List generated successfully";
                var dlink = angular.element('<a/>');
                dlink.attr({
                    'href': '/excel/exports/GSM-Rank-Sheet.xls',
                    'target': '_self',
                    'download': 'GSM-Rank-Sheet' + $scope.d + '.xls'
                })[0].click();
                $scope.close()

                //}

            });

            $timeout(function() {
                $scope.messages.success = '';
                $scope.messages.errorMessage = '';
            }, 9000);

        }


    })
    .controller('GTPaymentCtrl', function($location, $http, $scope, Request, $filter, $window, $timeout, NotificationService) {
        $scope.postData = {};

        $scope.customer = {};

        $scope.loading = { logs: false };
        var urlParams = new URLSearchParams(window.location.search);
        $scope.can_cel = false;

        var statusCode = urlParams.get('statusCode');
        var refCode = urlParams.get('refCode');
        var Message = urlParams.get('Message');
        var paymentMode = urlParams.get('paymentMode');
        var clientref = urlParams.get('clientref');
        console.log(urlParams.get('clientref'));
        var date = new Date();
        $scope.d = $filter('date')(new Date(), 'dd/MM/yyyy');


        $scope.checkPaymentStatus = function() {
            $scope.loading = { check: true, search: true }
            $scope.resultData = { statusCode: statusCode, refCode: refCode, paymentMode: paymentMode,clientref:clientref,Message:Message };
                            
            var req = Request.post('/update-payment-status', $scope.resultData);
            req.then(function(res) {
                if (res.data.status) {
                    $scope.can_cel = false;
                    NotificationService.send('success', "Self Load Successful");

                } else {
                   
                    NotificationService.send('error', "Self Load Not succesful");

                }
                $scope.loading.check = false;
               
            });
            req.catch(function(res) {
                NotificationService.send('error', "Error Occured");
                $scope.loading.user = false;
            });

        }


        $scope.checkPaymentStatus();

        




    })
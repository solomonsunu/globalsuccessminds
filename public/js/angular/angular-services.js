angular.module('gsmApp')

.service('Request', function($http){
    var baseUrl = location.origin+"/api/v1";

	this.get = function(endpoint){		
		return $http.get(baseUrl+endpoint);
	}

	this.post = function(endpoint, postData){
		return $http.post(baseUrl+endpoint, postData);
	}

    this.put = function(endpoint, postData){
        return $http.put(baseUrl+endpoint, postData);
    }

    this.delete = function (endpoint) {
        return $http.delete(baseUrl+endpoint);
    }

    this.uploadFile = function(endpoint, file){
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(endpoint, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    }

})
.factory('NotificationService', function($rootScope,ngToast) {
    return {
        send: function(type, text) {
            var bgColor;
                if (type == 'success') {
                    bgColor = 'success';
                }else if('error'){
                    bgColor = 'warning';
                }
                ngToast.create({
                  className: bgColor,
                  content: text
                });
               
        }
    }
})

.factory('UserService', function(Request,$timeout,$localStorage) {
   var factory = {};
    // factory.saveReferral = function(data){
    //     localStorage.setItem('referral_data', JSON.stringify(data));
    //     localStorage.setItem('referral_data_time',+new Date);
    // }
    // factory.getLocalReferral = function(){
    //     var curr_date = new Date;
    //     var date = new Date(parseInt(localStorage.getItem('referral_data_time')));
    //     if (curr_date.now() - date  ) {}
    //     localStorage.setItem('referral_data_time',+new Date);
    // }
    // factory.savePremiumReferral = function(data){
    //     localStorage.setItem('premium_referral_data', JSON.stringify(data));
    //     localStorage.setItem('premium_referral_data_time',+new Date);
    // }
    factory.saveToken = function (token) {
            $localStorage.jwt = JSON.stringify(token);
        }
        factory.saveCurrentUser = function (data) {
            $localStorage.currentUser = JSON.stringify(data);
        }
        factory.getCurrentUser = function () {
            if ($localStorage.currentUser) {
                return JSON.parse($localStorage.currentUser);
            }
            return null;
        }
        
        factory.logoutCurrentUser = function () {
            if ($localStorage.$reset()) {
                return true;
            }
            return false;

        }
 
    factory.allUsers = function(id) {
       return Request.get('/getAllUser?page='+id);
       }
    factory.networkUsers = function(id,carrier) {
       return Request.get('/carrier/'+carrier+'/getNetworkUser?page='+id);
       }
    factory.networkAdminHistory = function(id) {
       return Request.get('/admin/transactions?page='+id);
       }
    factory.payUsers = function(id) {
       return Request.get('/getPayment?page='+id);
       }
    factory.payPremiumUsers = function(id) {
       return Request.get('/getPaymentPremium?page='+id);
       }
    factory.premiumUsers = function(id) {
       return Request.get('/getUserPremium?page='+id);
       }
    factory.premiumUsersFix = function(id) {
       return Request.get('/getUserPremiumFix?page='+id);
       }
    factory.payRankUsers = function(id) {
       return Request.get('/getUserRankUpgrades?page='+id);
       }

    factory.getTransaction = function() {
       return Request.get('/get-transaction-statement');
       }

    return factory;
   
})

.factory('DisplayService', function(Request,$timeout,$localStorage) {
   var factory = {};
  
    factory.saveRegularDisplay = function (data,level) {
        $localStorage.level = JSON.stringify(data);
    }
    factory.getRegularDisplay = function (level) {
        if ($localStorage.level) {
            return JSON.parse($localStorage.level);
        }
        return null;
    }

    factory.savePremiumDisplay = function (data) {
        $localStorage.premiumDisplay = JSON.stringify(data);
    }
    factory.getPremiumDisplay = function () {
        if ($localStorage.premiumDisplay) {
            return JSON.parse($localStorage.premiumDisplay);
        }
        return null;
    }
 
    return factory;
   
})
.factory('TransactionService', function(Request,$timeout,$localStorage) {
   var factory = {};
    
    factory.saveTransactionHistory = function (data) {
        $localStorage.transactionHistory = JSON.stringify(data);
    }
    factory.getTransactionHistory = function () {
        if ($localStorage.transactionHistory) {
            return JSON.parse($localStorage.transactionHistory);
        }
        return null;
    }
 
    return factory;
   
})
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    }
}])
.filter('age', function(){
    return function(dateString){
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
})
.filter('cutoff', function(){
    return function(str){
        var words = str.split(' ');
        var newStr = '';
        var i = 0;
        var count = (words.length < 36) ? words.length : 35;

        while(i < count){           
            newStr += words[i]+" ";
            if(words.length === 35){
                newStr += "...";
            }
            i++;
        }
        return newStr;
    }
})
.filter('ago', function() {
    return function(date) {
        return moment(date).fromNow();
      }
})
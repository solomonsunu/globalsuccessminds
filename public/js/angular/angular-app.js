angular.module('gsmApp',[
	'ngAnimate',
    'ngAria',
	'angularUtils.directives.dirPagination',
	'angular.chosen',
	'ngSanitize',
	'vesparny.fancyModal',
	'ngStorage',
	'ngMaterial',
	'ngToast',
	'720kb.socialshare'
	])
.config(['ngToastProvider', function(ngToastProvider) {
    ngToastProvider.configure({
      animation: 'slide' // or 'fade'
    });
  }])


